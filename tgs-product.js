function logError(sender, args) {
    console.log(args.get_message());
}

var result = [];
var ctx = SP.ClientContext.get_current();
var lists = ctx.get_web().get_lists();
ctx.load(lists, "Include(Id,Title,BaseType)");

ctx.executeQueryAsync(
    function () {

        lists.get_data().forEach(function (list) {
            if (list.get_baseType() == "1") {

                var query;
                var startDate = "2019-05-01T00:00:01Z";
                query = "<View Scope='RecursiveAll'><Query><Where><Geq><FieldRef Name='Modified'/><Value Type='DateTime' IncludeTimeValue='FALSE'>" + startDate + "</Value></Geq></Where></Query></View>";

                var camlQuery = new SP.CamlQuery();
                camlQuery.set_viewXml(query);

                var items = list.getItems(camlQuery);
                ctx.load(items);
                var listEntry = {
                    id: list.get_id().toString(),
                    title: list.get_title()
                }
                result.push({
                    list: listEntry,
                    items: items
                });
            }
        });
        ctx.executeQueryAsync(
            function () {
                //transform listitem properties
                result.forEach(function (item) {

                    item.items = item.items.get_data().map(function (listItem) {
                        //return listItem.get_fieldValues(); 
                        return {
                            Folder: listItem.get_item('FileRef'),
                            File: listItem.get_item('FileLeafRef'),
                            Size: listItem.get_item('File_x0020_Size'),
                            CreatedBy: listItem.get_item('Author'),
                            CreatedDate: listItem.get_item('Created'),
                            ModifiedBy: listItem.get_item('Editor'),
                            ModifiedDate: listItem.get_item('Modified')
                        };
                    });
                });

                //console.log(JSON.stringify(result));
                var csv = new Array();
                for (var i = 0; i < result.length; i++) {
                    var items = result[i].items;
                    for (var j = 0; j < items.length; j++) {
                        var item = items[j];
                        csv.push(
                            {
                                Library: result[i].list.title,
                                Folder: item.Folder,
                                File: item.File,
                                Size: item.Size,
                                CreatedBy: item.CreatedBy.$7_2,
                                CreatedDate: item.CreatedDate,
                                ModifiedBy: item.ModifiedBy.$7_2,
                                ModifiedDate: item.ModifiedDate
                            }
                        )
                    }
                }

                console.log(JSON.stringify(csv));
            }, logError);


    }, logError);