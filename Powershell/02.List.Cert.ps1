﻿$url = '<URL>'

Connect-PnPOnline -Url $url -Credentials (Get-Credential)

Write-Host 'Creando lista Favoritos'
New-PnPList -Title 'Favoritos' -Template GenericList
Add-PnPField -List "Favoritos" -DisplayName "URL" -InternalName "DocUrl" -Type URL -Group "B2b" -AddToDefaultView

Write-Host 'Creando lista Bookmarks'
New-PnPList -Title 'Bookmarks' -Template GenericList
Add-PnPField -List "Bookmarks" -DisplayName "URL" -InternalName "DocUrl" -Type URL -Group "B2b" -AddToDefaultView

Write-Host 'Creando lista Familia de productos'
try{
	New-PnPList -Title 'Familia de productos' -Template GenericList

	Write-Host 'Agregando elementos...'
	$bundleProduct = Add-PnPListItem -List "Familia de productos" -Values @{"Title" = "Bundle Product"}
	$networking = Add-PnPListItem -List "Familia de productos" -Values @{"Title" = "Networking"}
	$personalComms = Add-PnPListItem -List "Familia de productos" -Values @{"Title" = "Personal Comms"}
	$snd = Add-PnPListItem -List "Familia de productos" -Values @{"Title" = "SDN/NFV"}
}
catch{
	Write-Host $_.Exception.Message
}

Write-Host 'Creando lista Productos'
try{
	New-PnPList -Title 'Productos' -Template GenericList
	$lookupList = Get-PnPList -Identity "Familia de productos"

	Write-Host 'Agregando columna ProductFamily...'
	$lookupXml = '<Field DisplayName="Familia" Name="ProductFamily" StaticName="ProductFamily" Type="Lookup" Required="FALSE" EnforceUniqueValues="FALSE" List="'+$lookupList.Id+'" ShowField="Title" />';
	Add-PnPFieldFromXml -List "Productos" -FieldXml $lookupXml

	Write-Host 'Agregando elementos de la familia Bundle Product...'
	Add-PnPListItem -List "Productos" -Values @{"Title" = "Bundle Product"; "ProductFamily" = $bundleProduct.Id} 

	Write-Host 'Agregando elementos de la familia Networking...'
	Add-PnPListItem -List "Productos" -Values @{"Title" = "BoD"; "ProductFamily" = $networking.Id} 
	Add-PnPListItem -List "Productos" -Values @{"Title" = "Enterprise Mobile Apps (EMA)"; "ProductFamily" = $networking.Id} 
	Add-PnPListItem -List "Productos" -Values @{"Title" = "Ethernet"; "ProductFamily" = $networking.Id} 
	Add-PnPListItem -List "Productos" -Values @{"Title" = "Ethernet VPN"; "ProductFamily" = $networking.Id} 
	Add-PnPListItem -List "Productos" -Values @{"Title" = "Fast Mobile"; "ProductFamily" = $networking.Id} 
	Add-PnPListItem -List "Productos" -Values @{"Title" = "Internet"; "ProductFamily" = $networking.Id} 
	Add-PnPListItem -List "Productos" -Values @{"Title" = "IP VPN"; "ProductFamily" = $networking.Id} 
	Add-PnPListItem -List "Productos" -Values @{"Title" = "LAN/wLAN"; "ProductFamily" = $networking.Id} 
	Add-PnPListItem -List "Productos" -Values @{"Title" = "Local Billing"; "ProductFamily" = $networking.Id} 
	Add-PnPListItem -List "Productos" -Values @{"Title" = "Marketing WiFi"; "ProductFamily" = $networking.Id} 
	Add-PnPListItem -List "Productos" -Values @{"Title" = "Operating Catalogue"; "ProductFamily" = $networking.Id} 
	Add-PnPListItem -List "Productos" -Values @{"Title" = "UBB"; "ProductFamily" = $networking.Id} 
	Add-PnPListItem -List "Productos" -Values @{"Title" = "WAN2Cloud"; "ProductFamily" = $networking.Id} 
	Add-PnPListItem -List "Productos" -Values @{"Title" = "Web Performance (Akamai)"; "ProductFamily" = $networking.Id} 
	Add-PnPListItem -List "Productos" -Values @{"Title" = "WOS"; "ProductFamily" = $networking.Id} 

	Write-Host 'Agregando elementos de la familia Personal Comms...'
	Add-PnPListItem -List "Productos" -Values @{"Title" = "Web Conferencing"; "ProductFamily" = $personalComms.Id} 
	Add-PnPListItem -List "Productos" -Values @{"Title" = "Webex Teams"; "ProductFamily" = $personalComms.Id} 
	Add-PnPListItem -List "Productos" -Values @{"Title" = "Cloud UC (Broadsoft)"; "ProductFamily" = $personalComms.Id} 
	Add-PnPListItem -List "Productos" -Values @{"Title" = "mConferencing (Telepresence)"; "ProductFamily" = $personalComms.Id} 
	Add-PnPListItem -List "Productos" -Values @{"Title" = "mSIP (SIP Trunking)"; "ProductFamily" = $personalComms.Id} 
	Add-PnPListItem -List "Productos" -Values @{"Title" = "Demos / Trials UCC"; "ProductFamily" = $personalComms.Id} 
	Add-PnPListItem -List "Productos" -Values @{"Title" = "Hosted UC (HCS-Cisco)"; "ProductFamily" = $personalComms.Id} 
	Add-PnPListItem -List "Productos" -Values @{"Title" = "EMA (Enterprise Mobile Application)"; "ProductFamily" = $personalComms.Id} 
	Add-PnPListItem -List "Productos" -Values @{"Title" = "TEM (Telecom Expense Management)"; "ProductFamily" = $personalComms.Id} 
	Add-PnPListItem -List "Productos" -Values @{"Title" = "GME (Global Expense Management)"; "ProductFamily" = $personalComms.Id} 
	Add-PnPListItem -List "Productos" -Values @{"Title" = "Core Mobility"; "ProductFamily" = $personalComms.Id} 
	Add-PnPListItem -List "Productos" -Values @{"Title" = "Secure Mobility"; "ProductFamily" = $personalComms.Id} 
	Add-PnPListItem -List "Productos" -Values @{"Title" = "MDM (Mobile Device Management)"; "ProductFamily" = $personalComms.Id} 

	Write-Host 'Agregando elementos de la familia SDN/NFV...'
	Add-PnPListItem -List "Productos" -Values @{"Title" = "Network Virtualization"; "ProductFamily" = $snd.Id} 
	Add-PnPListItem -List "Productos" -Values @{"Title" = "SD-WAN Nokia"; "ProductFamily" = $snd.Id} 
	Add-PnPListItem -List "Productos" -Values @{"Title" = "SD-WAN Nokia - unmanaged"; "ProductFamily" = $snd.Id} 
	Add-PnPListItem -List "Productos" -Values @{"Title" = "SD-WAN CISCO"; "ProductFamily" = $snd.Id} 
	Add-PnPListItem -List "Productos" -Values @{"Title" = "VNFs"; "ProductFamily" = $snd.Id} 
	Add-PnPListItem -List "Productos" -Values @{"Title" = "vFirewall"; "ProductFamily" = $snd.Id} 
	Add-PnPListItem -List "Productos" -Values @{"Title" = "vRAS"; "ProductFamily" = $snd.Id} 
	Add-PnPListItem -List "Productos" -Values @{"Title" = "CloudVPN"; "ProductFamily" = $snd.Id} 
	Add-PnPListItem -List "Productos" -Values @{"Title" = "SEDA Ecosystem"; "ProductFamily" = $snd.Id} 
}
catch{
	Write-Host $_.Exception.Message
}

Write-Host 'Creando lista Imágenes tipos de documento'
New-PnPList -Title 'Imagenes tipos de documento' -Template GenericList
Add-PnPFieldFromXml -List "Imagenes tipos de documento" -FieldXml '<Field Type="URL" DisplayName="Imagen" Format="Image" StaticName="Image" Name="Image"/>'

Write-Host 'Agregando archivos...'

Add-PnPListItem -List "Imagenes tipos de documento" -Values @{"Title" = "Documento B2B"; "Image" = $relativeUrl + "/Style Library/B2B/images/documentob2b.jpg, Documento B2B"}
Add-PnPListItem -List "Imagenes tipos de documento" -Values @{"Title" = "Documento de concepto"; "Image" = $relativeUrl + "/Style Library/B2B/images/documento_de_concepto.jpg, Documento de concepto"}
Add-PnPListItem -List "Imagenes tipos de documento" -Values @{"Title" = "Formularios"; "Image" = $relativeUrl + "/Style Library/B2B/images/formularios.jpg, Formularios"}
Add-PnPListItem -List "Imagenes tipos de documento" -Values @{"Title" = "Marketing Manual"; "Image" = $relativeUrl + "/Style Library/B2B/images/marketing_manual.jpg, Marketing Manual"}
Add-PnPListItem -List "Imagenes tipos de documento" -Values @{"Title" = "Bid Text"; "Image" = $relativeUrl + "/Style Library/B2B/images/bid_text.jpg, Bid Text"}
Add-PnPListItem -List "Imagenes tipos de documento" -Values @{"Title" = "Anexos contractuales servicio"; "Image" = $relativeUrl + "/Style Library/B2B/images/anexos_contractuales_servicio.jpg, Anexos contractuales servicio"}
Add-PnPListItem -List "Imagenes tipos de documento" -Values @{"Title" = "Cotizador"; "Image" = $relativeUrl + "/Style Library/B2B/images/cotizador.jpg, Cotizador"}
Add-PnPListItem -List "Imagenes tipos de documento" -Values @{"Title" = "SOF"; "Image" = $relativeUrl + "/Style Library/B2B/images/sof.jpg, SOF"}
Add-PnPListItem -List "Imagenes tipos de documento" -Values @{"Title" = "Mapa de cobertura"; "Image" = $relativeUrl + "/Style Library/B2B/images/mapa_de_cobertura.jpg, Mapa de cobertura"}
Add-PnPListItem -List "Imagenes tipos de documento" -Values @{"Title" = "Guías de uso"; "Image" = $relativeUrl + "/Style Library/B2B/images/guías_de_uso.jpg, Guías de uso"}
Add-PnPListItem -List "Imagenes tipos de documento" -Values @{"Title" = "Plantilla de oferta"; "Image" = $relativeUrl + "/Style Library/B2B/images/plantilla_de_oferta.jpg, Plantilla de oferta"}
Add-PnPListItem -List "Imagenes tipos de documento" -Values @{"Title" = "Elevator Pitch"; "Image" = $relativeUrl + "/Style Library/B2B/images/elevator_pitch.jpg, Elevator Pitch"}
Add-PnPListItem -List "Imagenes tipos de documento" -Values @{"Title" = "Presentación comercial a clientes"; "Image" = $relativeUrl + "/Style Library/B2B/images/presentación_comercial_a_clientes.jpg, Presentación comercial a clientes"}
Add-PnPListItem -List "Imagenes tipos de documento" -Values @{"Title" = "Presentación preventas - ventas"; "Image" = $relativeUrl + "/Style Library/B2B/images/presentacion_preventas.jpg, Presentación preventas - ventas"}
Add-PnPListItem -List "Imagenes tipos de documento" -Values @{"Title" = "Formaciones"; "Image" = $relativeUrl + "/Style Library/B2B/images/formaciones.jpg, Formaciones"}
Add-PnPListItem -List "Imagenes tipos de documento" -Values @{"Title" = "Casos de éxito"; "Image" = $relativeUrl + "/Style Library/B2B/images/casos_de_exito.jpg, Casos de éxito"}
Add-PnPListItem -List "Imagenes tipos de documento" -Values @{"Title" = "Factsheet"; "Image" = $relativeUrl + "/Style Library/B2B/images/factsheet.jpg, Factsheet"}
Add-PnPListItem -List "Imagenes tipos de documento" -Values @{"Title" = "Infografías"; "Image" = $relativeUrl + "/Style Library/B2B/images/infografías.jpg, Infografías"}
Add-PnPListItem -List "Imagenes tipos de documento" -Values @{"Title" = "Brochure"; "Image" = $relativeUrl + "/Style Library/B2B/images/Brochure.jpg, Brochure"}
Add-PnPListItem -List "Imagenes tipos de documento" -Values @{"Title" = "Battlecard"; "Image" = $relativeUrl + "/Style Library/B2B/images/Battlecard.jpg, Battlecard"}
Add-PnPListItem -List "Imagenes tipos de documento" -Values @{"Title" = "Whitepaper"; "Image" = $relativeUrl + "/Style Library/B2B/images/Whitepaper.jpg, Whitepaper"}
Add-PnPListItem -List "Imagenes tipos de documento" -Values @{"Title" = "Vídeos"; "Image" = $relativeUrl + "/Style Library/B2B/images/Vídeos.jpg, Vídeos"}
Add-PnPListItem -List "Imagenes tipos de documento" -Values @{"Title" = "Informes de analistas"; "Image" = $relativeUrl + "/Style Library/B2B/images/Informes_de_analistas.jpg, Informes de analistas"}
Add-PnPListItem -List "Imagenes tipos de documento" -Values @{"Title" = "Roadmap"; "Image" = $relativeUrl + "/Style Library/B2B/images/Roadmap.jpg, Roadmap"}
Add-PnPListItem -List "Imagenes tipos de documento" -Values @{"Title" = "Propuesta de valor B2B"; "Image" = $relativeUrl + "/Style Library/B2B/images/Propuesta_de_valor_B2B.jpg, Propuesta de valor B2B"}
Add-PnPListItem -List "Imagenes tipos de documento" -Values @{"Title" = "Planes de activación"; "Image" = $relativeUrl + "/Style Library/B2B/images/Planes_de_activacion.jpg, Planes de activación"}
Add-PnPListItem -List "Imagenes tipos de documento" -Values @{"Title" = "Demos"; "Image" = $relativeUrl + "/Style Library/B2B/images/Demos.jpg, Demos"}
Add-PnPListItem -List "Imagenes tipos de documento" -Values @{"Title" = "Business Case"; "Image" = $relativeUrl + "/Style Library/B2B/images/Business_Case.jpg, Business Case"}
Add-PnPListItem -List "Imagenes tipos de documento" -Values @{"Title" = "Service Description"; "Image" = $relativeUrl + "/Style Library/B2B/images/Service_Description.jpg, Service Description"}
Add-PnPListItem -List "Imagenes tipos de documento" -Values @{"Title" = "Manual técnico"; "Image" = $relativeUrl + "/Style Library/B2B/images/Manual_tecnico.jpg, Manual técnico"}
Add-PnPListItem -List "Imagenes tipos de documento" -Values @{"Title" = "Manual operativo"; "Image" = $relativeUrl + "/Style Library/B2B/images/Manual_operativo.jpg, Manual operativo"}
Add-PnPListItem -List "Imagenes tipos de documento" -Values @{"Title" = "Otros documentos DSS - Operaciones"; "Image" = $relativeUrl + "/Style Library/B2B/images/Otros_documentos_DSS_Operaciones.jpg, Otros documentos DSS - Operaciones"}
Add-PnPListItem -List "Imagenes tipos de documento" -Values @{"Title" = "Local capabilities"; "Image" = $relativeUrl + "/Style Library/B2B/images/Local_capabilities.jpg, Local capabilities"}
Add-PnPListItem -List "Imagenes tipos de documento" -Values @{"Title" = "FAQs"; "Image" = $relativeUrl + "/Style Library/B2B/images/FAQs.jpg, FAQs"}
Add-PnPListItem -List "Imagenes tipos de documento" -Values @{"Title" = "Otros materiales"; "Image" = $relativeUrl + "/Style Library/B2B/images/Otros_materiales.jpg, Otros materiales"}
Add-PnPListItem -List "Imagenes tipos de documento" -Values @{"Title" = "Reporting"; "Image" = $relativeUrl + "/Style Library/B2B/images/Reporting.jpg, Reporting"}
Add-PnPListItem -List "Imagenes tipos de documento" -Values @{"Title" = "Global RPFs"; "Image" = $relativeUrl + "/Style Library/B2B/images/Global_RPFs.jpg, Global RPFs"}

Write-Host 'Creando lista Descripción tipos de documento'
New-PnPList -Title 'Descripcion tipos de documento' -Template GenericList
Add-PnPFieldFromXml -List "Descripcion tipos de documento" -FieldXml '<Field Type="Note" DisplayName="Descripción" Required="FALSE" NumLines="6" RichText="FALSE" Sortable="FALSE" StaticName="DocumentTypeDescription" Name="DocumentTypeDescription"/>'

Write-Host 'Agregando archivos...'

Add-PnPListItem -List "Descripcion tipos de documento" -Values @{"Title" = "Documento B2B"; "DocumentTypeDescription" = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc congue, massa ut maximus laoreet, magna nibh scelerisque magna, ac venenatis neque libero sed nisl."}
Add-PnPListItem -List "Descripcion tipos de documento" -Values @{"Title" = "Documento de concepto"; "DocumentTypeDescription" = "Vivamus convallis dapibus sem, id mattis lorem facilisis in. Curabitur sit amet scelerisque dolor, non hendrerit neque."}
Add-PnPListItem -List "Descripcion tipos de documento" -Values @{"Title" = "Formularios"; "DocumentTypeDescription" = "Pellentesque nisi libero, sodales at urna et, porttitor luctus dui. Aenean a erat a lectus iaculis porttitor ut id ante. Etiam laoreet erat sit amet mollis pretium. In auctor non dolor ut venenatis."}
Add-PnPListItem -List "Descripcion tipos de documento" -Values @{"Title" = "Marketing Manual"; "DocumentTypeDescription" = "Vestibulum ac sem ut elit bibendum luctus quis eget risus. Integer efficitur elit sapien, non pellentesque est elementum nec."}
Add-PnPListItem -List "Descripcion tipos de documento" -Values @{"Title" = "Bid Text"; "DocumentTypeDescription" = "Sed vel arcu eros. Etiam vitae elit laoreet, rutrum odio sit amet, finibus magna. Proin eget neque ultrices, aliquam odio id, lacinia dui."}
Add-PnPListItem -List "Descripcion tipos de documento" -Values @{"Title" = "Anexos contractuales servicio"; "DocumentTypeDescription" = "Vivamus feugiat finibus velit, ac aliquam massa gravida eget. Nam feugiat finibus mauris et tempus. Donec mollis, diam eu elementum convallis, velit neque aliquet lectus, eu faucibus libero metus finibus quam."}
Add-PnPListItem -List "Descripcion tipos de documento" -Values @{"Title" = "Cotizador"; "DocumentTypeDescription" = "Pellentesque consectetur, sapien non finibus commodo, lacus lacus placerat massa, a volutpat neque nibh eu diam. Proin vel tellus et massa feugiat faucibus ut eget nisi."}
Add-PnPListItem -List "Descripcion tipos de documento" -Values @{"Title" = "SOF"; "DocumentTypeDescription" = "Cras blandit varius turpis, at tempor lacus condimentum id. Ut dolor ex, consectetur et ante id, mattis sodales leo. Pellentesque id aliquet lorem, eget convallis augue. Ut id bibendum elit. Ut a quam vel urna faucibus mattis vitae vitae mauris."}
Add-PnPListItem -List "Descripcion tipos de documento" -Values @{"Title" = "Mapa de cobertura"; "DocumentTypeDescription" = "Pellentesque viverra felis sit amet ex semper gravida. Donec eu vestibulum ante. Nam quis mollis metus, at volutpat sem. Cras dictum euismod est, ut consequat enim pharetra sit amet."}
Add-PnPListItem -List "Descripcion tipos de documento" -Values @{"Title" = "Guías de uso"; "DocumentTypeDescription" = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent volutpat non ex vel egestas. Nulla iaculis neque pellentesque fringilla volutpat."}
Add-PnPListItem -List "Descripcion tipos de documento" -Values @{"Title" = "Plantilla de oferta"; "DocumentTypeDescription" = "Cras at pellentesque mi, eget mollis orci. Sed at orci mauris. Suspendisse pellentesque iaculis diam sed malesuada. Cras feugiat turpis in urna lacinia, vel maximus mi dictum."}
Add-PnPListItem -List "Descripcion tipos de documento" -Values @{"Title" = "Elevator Pitch"; "DocumentTypeDescription" = "Aliquam hendrerit magna arcu, in eleifend lacus tempor vitae. Maecenas a massa et justo vestibulum luctus ac porta risus."}
Add-PnPListItem -List "Descripcion tipos de documento" -Values @{"Title" = "Presentación comercial a clientes"; "DocumentTypeDescription" = "Phasellus interdum metus in quam luctus pharetra."}
Add-PnPListItem -List "Descripcion tipos de documento" -Values @{"Title" = "Presentación preventas - ventas"; "DocumentTypeDescription" = "Ut nisi dolor, aliquam at aliquet non, facilisis ut orci. Mauris enim justo, mattis eu diam sed, dictum feugiat augue."}
Add-PnPListItem -List "Descripcion tipos de documento" -Values @{"Title" = "Formaciones"; "DocumentTypeDescription" = "Nam egestas consectetur sem. Vivamus mattis maximus lacinia. Praesent felis purus, imperdiet a velit a, sollicitudin commodo purus."}
Add-PnPListItem -List "Descripcion tipos de documento" -Values @{"Title" = "Casos de éxito"; "DocumentTypeDescription" = "Phasellus varius pretium dolor vitae rutrum. Praesent pulvinar ornare nisi,"}
Add-PnPListItem -List "Descripcion tipos de documento" -Values @{"Title" = "Factsheet"; "DocumentTypeDescription" = "in hendrerit ipsum. Proin quis aliquet leo. Nam sit amet eros nunc. Vestibulum sit amet rhoncus velit. In eu lectus sit amet nunc interdum accumsan. Aliquam ex magna, placerat ut sapien sed, tincidunt rutrum arcu."}
Add-PnPListItem -List "Descripcion tipos de documento" -Values @{"Title" = "Infografías"; "DocumentTypeDescription" = "Duis volutpat lorem nibh, eu feugiat eros pretium eu. Nam faucibus ligula arcu, quis pellentesque velit condimentum quis. Donec commodo tristique eros, ut cursus neque fringilla et."}
Add-PnPListItem -List "Descripcion tipos de documento" -Values @{"Title" = "Brochure"; "DocumentTypeDescription" = "Maecenas non ultrices tortor. Etiam aliquet, tellus quis varius egestas, quam arcu condimentum odio, in venenatis lorem ex at nisi."}
Add-PnPListItem -List "Descripcion tipos de documento" -Values @{"Title" = "Battlecard"; "DocumentTypeDescription" = "Mauris vitae justo varius, molestie velit et, ullamcorper quam. Nunc augue odio, elementum et elementum ac, pretium nec est. Maecenas aliquam efficitur erat."}
Add-PnPListItem -List "Descripcion tipos de documento" -Values @{"Title" = "Whitepaper"; "DocumentTypeDescription" = "Etiam in enim quis nisl porta vehicula a at erat. Morbi fermentum, velit sit amet rhoncus lobortis, libero tortor congue ipsum, volutpat consectetur quam nisi ac arcu. Nullam hendrerit dignissim sodales. Nam ac diam eget mi rhoncus molestie in vestibulum lacus."}
Add-PnPListItem -List "Descripcion tipos de documento" -Values @{"Title" = "Vídeos"; "DocumentTypeDescription" = "Mauris porta scelerisque vestibulum. Quisque elementum vulputate aliquam. Integer sit amet hendrerit nibh, eu faucibus ante. Donec sagittis euismod neque quis auctor."}
Add-PnPListItem -List "Descripcion tipos de documento" -Values @{"Title" = "Informes de analistas"; "DocumentTypeDescription" = "Maecenas molestie nisl non leo aliquet, at rutrum metus dictum. Pellentesque malesuada, nulla in pharetra ornare, eros ligula cursus leo, ac vulputate erat urna eu libero. "}
Add-PnPListItem -List "Descripcion tipos de documento" -Values @{"Title" = "Roadmap"; "DocumentTypeDescription" = "Cras dignissim augue vitae justo sollicitudin, vitae vulputate purus pretium. Etiam mauris mi, tincidunt non ante eu, placerat imperdiet massa."}
Add-PnPListItem -List "Descripcion tipos de documento" -Values @{"Title" = "Propuesta de valor B2B"; "DocumentTypeDescription" = "Donec posuere posuere turpis vel consequat. Nullam dictum mi non facilisis ultrices. Pellentesque condimentum at lorem vel volutpat."}
Add-PnPListItem -List "Descripcion tipos de documento" -Values @{"Title" = "Planes de activación"; "DocumentTypeDescription" = "Pellentesque in condimentum quam."}
Add-PnPListItem -List "Descripcion tipos de documento" -Values @{"Title" = "Demos"; "DocumentTypeDescription" = "Quisque molestie nunc vel facilisis egestas. Aliquam fringilla varius felis. Cras egestas quis metus bibendum cursus."}
Add-PnPListItem -List "Descripcion tipos de documento" -Values @{"Title" = "Business Case"; "DocumentTypeDescription" = "Pellentesque porta consectetur mauris, vel dictum mauris accumsan in. Nullam tellus augue, egestas et ipsum ac, auctor rhoncus arcu."}
Add-PnPListItem -List "Descripcion tipos de documento" -Values @{"Title" = "Service Description"; "DocumentTypeDescription" = "Proin iaculis vitae tellus et gravida. Suspendisse at quam sed metus aliquet cursus."}
Add-PnPListItem -List "Descripcion tipos de documento" -Values @{"Title" = "Manual técnico"; "DocumentTypeDescription" = "Nullam dapibus, mauris ac eleifend scelerisque, sapien dolor mollis augue, eget ullamcorper augue mauris vel ligula."}
Add-PnPListItem -List "Descripcion tipos de documento" -Values @{"Title" = "Manual operativo"; "DocumentTypeDescription" = "Etiam orci nunc, maximus suscipit turpis sed, condimentum elementum lorem. Phasellus in urna eget risus commodo feugiat id ut diam."}
Add-PnPListItem -List "Descripcion tipos de documento" -Values @{"Title" = "Otros documentos DSS - Operaciones"; "DocumentTypeDescription" = "Donec lectus purus, iaculis ultricies vulputate nec, fringilla eget ante. Vestibulum sit amet neque varius, tincidunt diam id, semper quam."}
Add-PnPListItem -List "Descripcion tipos de documento" -Values @{"Title" = "Local capabilities"; "DocumentTypeDescription" = "Interdum et malesuada fames ac ante ipsum primis in faucibus."}
Add-PnPListItem -List "Descripcion tipos de documento" -Values @{"Title" = "FAQs"; "DocumentTypeDescription" = "Nulla eu orci in magna egestas molestie ac ut lectus. In imperdiet dictum justo sit amet luctus."}
Add-PnPListItem -List "Descripcion tipos de documento" -Values @{"Title" = "Otros materiales"; "DocumentTypeDescription" = "Etiam posuere velit id nunc maximus, vel vestibulum turpis imperdiet."}
Add-PnPListItem -List "Descripcion tipos de documento" -Values @{"Title" = "Reporting"; "DocumentTypeDescription" = "In facilisis tristique magna, a convallis urna mollis at. In sodales ex lectus, nec condimentum mauris tincidunt sit amet."}
Add-PnPListItem -List "Descripcion tipos de documento" -Values @{"Title" = "Global RPFs"; "DocumentTypeDescription" = "Etiam mauris mauris, molestie non malesuada eu, tempus id nisi. Sed vestibulum velit in efficitur fringilla. Pellentesque vulputate facilisis varius."}