﻿$b2bUrl = "https://telefonicacorp.sharepoint.com/sites/B2BFinder.OCEWU/es-es"
$b2bTopSiteUrl = "https://telefonicacorp.sharepoint.com/sites/B2BFinder.OCEWU"
$tenantUrl = "https://telefonicacorp.sharepoint.com"
$logosUrl = "https://logos.telefonicabusinesssolutions.com/pm"
$sdnUrl = "https://telefonicacorp.sharepoint.com/sites/tgs-product"

#$b2bUrl = "https://avantgardeits.sharepoint.com/sites/telefonica/es-es"
#$b2bTopSiteUrl = "https://avantgardeits.sharepoint.com/sites/telefonica"
#$tenantUrl = "https://avantgardeits.sharepoint.com"
#$logosUrl = "https://logos.telefonicabusinesssolutions.com/pm"
#$sdnUrl = "https://telefonicacorp.sharepoint.com/sites/tgs-product"

$outItems = New-Object System.Collections.Generic.List[System.Object]
$baseFolderUrl = "Documentos B2B"
$tempFolder = $PSScriptRoot
$user = 'tefgad\wuedji01'
$pwd = convertto-securestring -String 'Telefonica@19' -AsPlainText -Force
$logosCredentials = new-object -typename System.Management.Automation.PSCredential -argumentlist $user, $pwd


Write-Host "Introduzca el usuario y contraseña para" $b2bUrl -ForeGround Red
$b2bCredentials = Get-Credential

$date = Get-Date
$Logfile = $PSScriptRoot + "\" + $date.toString('ddMMyyyyHHmmss') + ".log"

Function WriteLog
{
   Param ([string]$message)

   $date = Get-Date
   $m = $date.ToString('HH:mm:ss') + $message

   Write-Host $m
   Add-content $Logfile -value $m
}

Function Get-DocumentTypeImage {
	Param(
        [parameter(Mandatory=$true)][string]$documentType
    )	
	
	
	$message = "	Obteniendo imagen para " + $documentType
	WriteLog $message

	Connect-PnPOnline -Url $b2bTopSiteUrl -Credentials $b2bCredentials

	$query = "<View><Query><Where><Eq><FieldRef Name='Title'/><Value Type='Text'>" + $documentType + "</Value></Eq></Where></Query></View>"

	$listItem = Get-PnPListItem -List "Imagenes tipos de documento" -Query $query

	if($listItem -ne $null){
		
		$message = "	Imagen para tipo de documento " + $documentType + ": " + $listItem["Image"].Url
		
		WriteLog $message
	}

	return $listItem["Image"].Url
}

Function Get-Enabler {
	Param(
        [parameter(Mandatory=$true)][string]$documentType
    )	
	
	
	$message = "	Obteniendo Enabler para " + $documentType
	
	WriteLog $message

	Connect-PnPOnline -Url $b2bTopSiteUrl -Credentials $b2bCredentials

	$query = "<View><Query><Where><Eq><FieldRef Name='B2BContentType'/><Value Type='Text'>" + $documentType + "</Value></Eq></Where></Query></View>"

	$listItem = Get-PnPListItem -List "Enablers" -Query $query

	if($listItem -ne $null){
		
		$message = "	Enabler para tipo de documento " + $documentType + ": " + $listItem["Enabler"]
		
		WriteLog $message
	}
	else{
		
		$message = "	El tipo de documento " + $documentType + " no tiene Enabler asignado"
		
		WriteLog $message
	}

	return $listItem["Enabler"]
}

Function Get-Permission {
	Param(
        [parameter(Mandatory=$true)][string]$documentType
    )	
	
	$message = "	Obteniendo Permisos para " + $documentType
	
	WriteLog $message

	Connect-PnPOnline -Url $b2bTopSiteUrl -Credentials $b2bCredentials

	$query = "<View><Query><Where><Eq><FieldRef Name='B2BContentType'/><Value Type='Text'>" + $documentType + "</Value></Eq></Where></Query></View>"

	$listItem = Get-PnPListItem -List "Permisos por tipo de documento" -Query $query


	if($listItem -ne $null){
		$message = "	Permisos para tipo de documento " + $documentType + ": "
		WriteLog $message
		
		for($i = 0; $i -lt $listItem.Count; $i++){
			$message = "		" + $listItem[0]['SecurityGroup'] + ": " + $listItem[0]['PermissionLevel']
			WriteLog $message
		}
	}
	else{
		
		$message = "	El tipo de documento " + $documentType + " no tiene Enabler asignado"
		
		WriteLog $message
	}

	return $listItem
}

Function SetPermissions{
	Param(
        [parameter(Mandatory=$true)][int]$itemId,
		[parameter(Mandatory=$true)][string]$contenType
    )


	$permissions = Get-Permission -documentType $contenType

	if($permissions.Count -gt 0){
		$message = "	Conectando con " + $b2bUrl
		WriteLog $message

		Connect-PnPOnline -Url $b2bUrl -Credentials $b2bCredentials

		$message = "	Rompiendo herencia de permisos en elemento " + $itemId
		WriteLog $message
		
		$listItem = Get-PnPListItem -List $baseFolderUrl -id $itemId
		$listItem.BreakRoleInheritance($false, $false)

		for($i = 0; $i -lt $permissions.Count; $i++){
			$message = "	Estableciendo permisos para el grupo " + $permissions[$i]['SecurityGroup'] + " a " + $permissions[$i]['PermissionLevel']
			WriteLog $message

			$role = ''

			if($permissions[$i]['PermissionLevel'] -eq 'Contributor'){
				$role = 'Colaborar'
			}

			if($permissions[$i]['PermissionLevel'] -eq 'Administrator'){
				$role = 'Control total'
			}

			if($permissions[$i]['PermissionLevel'] -eq 'Reader'){
				$role = 'Read'
			}

			Set-PnPListItemPermission -List $baseFolderUrl -Identity $itemId -Group $permissions[$i]['SecurityGroup'] -AddRole $role
		}
	}
}

Function Proccess{

	$csv = Import-Csv -Path ".\csv\Documentos.csv" -Delimiter ";" | 
    ForEach-Object {
		Write-Host ""

		$fileFolder = $_.Folder
		$fileValue = $_.File
		$authorName = $_.CreatedByName
		$author = $_.CreatedBy
		$createdDate = $_.CreatedDate
		$contenType = $_.TipoDocumento
		$productFamily = $_.FamiliaProducto
		$product = $_.Producto
		$language = $_.Idioma
		$responsable = $_.Responsable

		$use = $_.Uso
		if($use -eq $null -or $use -eq ''){
			$use = "Externo"
		}

		if($contenType -eq $null -or $contenType -eq ''){
			$message = "	El archivo no tiene asignado un tipo de documento."
			WriteLog $message

			$message = "	Asignando tipo de documento Documento B2B."
			WriteLog $message

			$contenType = 'Documento B2B'
		}

		if($language -eq $null -or $language -eq ''){
			$language = 'es'
		}

		$folder = $baseFolderUrl

		if($productFamily -ne $null -and $productFamily -ne ''){
			$folder += "/" + $productFamily
		}

		if($product -ne $null -and $product -ne ''){
			$folder += "/" + $product
		}

		if($contenType -ne $null -and $contenType -ne ''){
			$folder += "/" + $contenType
		}

		$folder = $folder.Replace('//', '/')

		WriteLog ""
		WriteLog ""
		WriteLog ""

		$message = "	Procesando archivo " + $fileValue
		WriteLog $message

		WriteLog ""

		$message = "	Propiedades:"
		WriteLog $message

		$message = "		Folder: " + $fileFolder
		WriteLog $message

		$message = "		File: " + $fileValue
		WriteLog $message

		$message = "		AuthorName: " + $authorName
		WriteLog $message

		$message = "		Author: " + $author
		WriteLog $message

		$message = "		Created: " + $createdDate
		WriteLog $message

		$message = "		TipoDocumento: " + $contenType
		WriteLog $message

		$message = "		FamiliaProducto: " + $productFamily
		WriteLog $message

		$message = "		Producto: " + $product
		WriteLog $message

		$message = "		Uso: " + $use
		WriteLog $message

		$message = "		Idioma: " + $language
		WriteLog $message

		$message = "		Responsable: " + $responsable
		WriteLog $message

		WriteLog ""

		
		$message = "	Conectando con " + $b2bUrl
		WriteLog $message

		Connect-PnPOnline -Url $b2bUrl -Credentials $b2bCredentials

		#Buscamos el Fichero
		$fileNameSplit = $fileValue.split('/')
		$fileName = $fileNameSplit[$fileNameSplit.length - 1]

		$message = "	Buscando archivo " + $fileName + " en " + $folder
		WriteLog $message

		$files = Find-PnPFile -List $baseFolderUrl -Match $fileName

		if($files.Count -eq 1){
			$message = '	Archivo encontrado'
			WriteLog $message

			$f = $files[0]

			$uploadedFile = Get-PnPFile -Url $f.ServerRelativeUrl -AsListItem

			$message = "	Estableciendo propiedades en " + $uploadedFile.Id
			WriteLog $message
				
			Set-PnPListItem -List $baseFolderUrl -Identity $uploadedFile.Id -Values @{"ContentType" = $contenType; "Title" = $title; "ProductFamily" = $productFamily; "Product" = $product; "DocumentUse" = $use; "DocumentTypeImage" = $documentTypeImage; "Enabler" = $enabler; "Idioma" = $language} -SystemUpdate -ea SilentlyContinue

			$currentResponsable = $responsable

			if ($currentResponsable -eq $null -or $currentResponsable -eq ''){
				$currentResponsable = $authorName
			}

			$message = "	Buscando usuario " + $currentResponsable
			WriteLog $message

			$responsableUser = Get-PnPUser | ? Email -eq $currentResponsable

			if($responsableUser -eq $null){
				$message = "	No se ha encontrado al usuario " + $currentResponsable
				WriteLog $message

				$message = "	Estableciendo ResponsableText a " + $authorName
				WriteLog $message

				$message = "	Estableciendo ResponsableMail a " + $author
				WriteLog $message

				Set-PnPListItem -List $baseFolderUrl -Identity $uploadedFile.Id -Values @{"ResponsibleText"= $authorName;"ResponsibleEmail"= $author} -SystemUpdate  
			}
			else {
				$message = "	Estableciendo responsable del fichero a " + $currentResponsable
				WriteLog $message

				Set-PnPListItem -List $baseFolderUrl -Identity $uploadedFile.Id -Values @{"Responsible" = $currentResponsable} -SystemUpdate  
			}

			SetPermissions -itemId $uploadedFile.Id -contenType $contenType
		}
		else{

			$message = "	No se ha encontrado el archivo " + $fileName
			WriteLog $message

			$fileUrl = $fileFolder + '/' + $fileValue

			if($productFamily -ne 'SDN/NFV'){
					
				$message = "	Conectando con " + $logosUrl
				WriteLog $message

				Connect-PnPOnline -Url $logosUrl -Credentials $logosCredentials
			}
			else{
					
				$message = "	Conectando con " + $sdnUrl
				WriteLog $message

				Connect-PnPOnline -Url $sdnUrl -Credentials $b2bCredentials
			}

			$message = "	Descargando " + $fileUrl
			WriteLog $message

			Get-PnPFile -Url $fileUrl -Path $tempFolder -FileName $fileName -AsFile
			$filePath = $tempFolder + "\" + $fileName
			
			$exists = test-path $filePath

			if($exists) {

				$message = "	Fichero " + $fileName + " descargado en " + $filePath
				WriteLog $message

				$documentTypeImage = Get-DocumentTypeImage -documentType $contenType
				$enabler = Get-Enabler -documentType $contenType

				$message = "	Conectando con " + $b2bUrl
				WriteLog $message

				Connect-PnPOnline -Url $b2bUrl -Credentials $b2bCredentials

				$message = "	Generando estructura de carpetas en " + $baseFolderUrl
				WriteLog $message

				$message = "	Creando carpeta " + $folder + " en " + $baseFolderUrl
				WriteLog $message

				Resolve-PnPFolder -SiteRelativePath $folder

				$title = $contenType + " - ";

				if ($product -ne $null -and $product -ne ''){
					$title += $product + " - "
				}

				$title += (Get-Item $filePath).Basename
				
				$message = "	Título del fichero: " + $title
				WriteLog $message

				$message = "	Cargando " + $fileName + " en " + $folder
				WriteLog $message
			
				$f = Add-PnPFile -Path $filePath -Folder $folder

				$message = "	Fichero cargado"
				WriteLog $message

				$message = "	Obteniendo elemento asociado"
				WriteLog $message

				$uploadedFile = Get-PnPFile -Url $f.ServerRelativeUrl -AsListItem

				$message = "	Estableciendo propiedades en " + $uploadedFile.Id
				WriteLog $message
				
				Set-PnPListItem -List $baseFolderUrl -Identity $uploadedFile.Id -Values @{"ContentType" = $contenType; "Title" = $title; "ProductFamily" = $productFamily; "Product" = $product; "DocumentUse" = $use; "DocumentTypeImage" = $documentTypeImage; "Enabler" = $enabler; "Idioma" = $language} -SystemUpdate -ea SilentlyContinue

				$currentResponsable = $responsable

				if ($currentResponsable -eq $null -or $currentResponsable -eq ''){
					$currentResponsable = $authorName
				}

				$message = "	Buscando usuario " + $currentResponsable
				WriteLog $message

				$responsableUser = Get-PnPUser | ? Email -eq $currentResponsable

				if($responsableUser -eq $null){
					$message = "	No se ha encontrado al usuario " + $currentResponsable
					WriteLog $message

					$message = "	Estableciendo ResponsableText a " + $authorName
					WriteLog $message

					$message = "	Estableciendo ResponsableMail a " + $author
					WriteLog $message

					Set-PnPListItem -List $baseFolderUrl -Identity $uploadedFile.Id -Values @{"ResponsibleText"= $authorName;"ResponsibleEmail"= $author} -SystemUpdate  
				}
				else {
					$message = "	Estableciendo responsable del fichero a " + $currentResponsable
					WriteLog $message

					Set-PnPListItem -List $baseFolderUrl -Identity $uploadedFile.Id -Values @{"Responsible"= $responsableUser} -SystemUpdate  
				}

				Set-PnPFileCheckedIn -Url $f.ServerRelativeUrl -Approve

				SetPermissions -itemId $uploadedFile.Id -contenType $contenType
			}
			else{
				$message = "	No se ha encontrado el archivo " + $fileUrl
				WriteLog $message

				$outItems.Add($fileUrl)
			}

			$message = "	Eliminando " + $fileName
			
			WriteLog $message
			
			Remove-Item $filePath
				
			$message = "	Fichero " + $fileName + " eliminado"
			WriteLog $message
		}
    }

	if($outItems.Count -gt 0){
		$date = Get-Date
		$m = $date.ToString('HH:mm:ss') + "Ficheros no encontrados"
		Add-content $Logfile -value $m

		for($i = 0; $i -lt $outItems.Count; $i++){
			$date = Get-Date
			$m = $date.ToString('HH:mm:ss') + $outItems[0]
			Add-content $Logfile -value $m
		}
	}
}

Proccess