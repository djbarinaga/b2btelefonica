﻿Write-Host 'Conectando...'

$url = "https://telefonicacorp.sharepoint.com/sites/B2BFinder.OCEWU"

Connect-PnPOnline -Url $url -Credentials (Get-Credential)

Write-Host 'Creando grupo PMs'
New-PnPGroup -Title "PMs"

Write-Host 'Creando grupo MNCs Sales'
New-PnPGroup -Title "MNCs Sales"

Write-Host 'Creando grupo Presales-MNCs - Corporate - OBB'
New-PnPGroup -Title "Presales-MNCs - Corporate - OBB"

Write-Host 'Creando grupo Operaciones Global'
New-PnPGroup -Title "Operaciones Global"

Write-Host 'Creando grupo OBs'
New-PnPGroup -Title "OBs"

Write-Host 'Creando campo Grupo de seguridad'
Add-PnPField -DisplayName 'Grupo de seguridad' -InternalName 'SecurityGroup' -Type 'Choice' -Choices "PMs", "MNCs Sales", "Presales-MNCs - Corporate - OBB", "Operaciones Global", "OBs" -Group 'B2B'

Write-Host 'Creando campo Nivel de permisos'
Add-PnPField -DisplayName 'Nivel de permisos' -InternalName 'PermissionLevel' -Type 'Choice' -Choices "Administrator", "Contributor", "Reader" -Group 'B2B'


Write-Host 'Creando lista Permisos por tipo de documento'
New-PnPList -Title 'Permisos por tipo de documento' -Template GenericList
Add-PnPField -List "Permisos por tipo de documento" -Field 'SecurityGroup'
Add-PnPField -List "Permisos por tipo de documento" -Field 'PermissionLevel'

Write-Host 'Agregando elementos para el tipo de documento Documento B2B'
Add-PnPListItem -List 'Permisos por tipo de documento' -Values @{"Title" = "Documento B2B"; 'SecurityGroup' = 'PMs'; 'PermissionLevel' = 'Contributor'}
Add-PnPListItem -List 'Permisos por tipo de documento' -Values @{"Title" = "Documento B2B"; 'SecurityGroup' = 'MNCs Sales'; 'PermissionLevel' = 'Reader'}
Add-PnPListItem -List 'Permisos por tipo de documento' -Values @{"Title" = "Documento B2B"; 'SecurityGroup' = 'Presales-MNCs - Corporate - OBB'; 'PermissionLevel' = 'Reader'}
Add-PnPListItem -List 'Permisos por tipo de documento' -Values @{"Title" = "Documento B2B"; 'SecurityGroup' = 'Operaciones Global'; 'PermissionLevel' = 'Reader'}
Add-PnPListItem -List 'Permisos por tipo de documento' -Values @{"Title" = "Documento B2B"; 'SecurityGroup' = 'OBs'; 'PermissionLevel' = 'Reader'}

Write-Host 'Agregando elementos para el tipo de documento Documento de concepto'
Add-PnPListItem -List 'Permisos por tipo de documento' -Values @{"Title" = "Documento de concepto"; 'SecurityGroup' = 'PMs'; 'PermissionLevel' = 'Contributor'}

Write-Host 'Agregando elementos para el tipo de documento Formularios'
Add-PnPListItem -List 'Permisos por tipo de documento' -Values @{"Title" = "Formularios"; 'SecurityGroup' = 'PMs'; 'PermissionLevel' = 'Contributor'}
Add-PnPListItem -List 'Permisos por tipo de documento' -Values @{"Title" = "Formularios"; 'SecurityGroup' = 'Operaciones Global'; 'PermissionLevel' = 'Reader'}

Write-Host 'Agregando elementos para el tipo de documento Marketing Manual'
Add-PnPListItem -List 'Permisos por tipo de documento' -Values @{"Title" = "Marketing Manual"; 'SecurityGroup' = 'PMs'; 'PermissionLevel' = 'Contributor'}
Add-PnPListItem -List 'Permisos por tipo de documento' -Values @{"Title" = "Marketing Manual"; 'SecurityGroup' = 'MNCs Sales'; 'PermissionLevel' = 'Reader'}
Add-PnPListItem -List 'Permisos por tipo de documento' -Values @{"Title" = "Marketing Manual"; 'SecurityGroup' = 'Presales-MNCs - Corporate - OBB'; 'PermissionLevel' = 'Reader'}
Add-PnPListItem -List 'Permisos por tipo de documento' -Values @{"Title" = "Marketing Manual"; 'SecurityGroup' = 'Operaciones Global'; 'PermissionLevel' = 'Reader'}
Add-PnPListItem -List 'Permisos por tipo de documento' -Values @{"Title" = "Marketing Manual"; 'SecurityGroup' = 'OBs'; 'PermissionLevel' = 'Reader'}

Write-Host 'Agregando elementos para el tipo de documento Bid Text'
Add-PnPListItem -List 'Permisos por tipo de documento' -Values @{"Title" = "Bid Text"; 'SecurityGroup' = 'PMs'; 'PermissionLevel' = 'Contributor'}
Add-PnPListItem -List 'Permisos por tipo de documento' -Values @{"Title" = "Bid Text"; 'SecurityGroup' = 'MNCs Sales'; 'PermissionLevel' = 'Reader'}
Add-PnPListItem -List 'Permisos por tipo de documento' -Values @{"Title" = "Bid Text"; 'SecurityGroup' = 'Presales-MNCs - Corporate - OBB'; 'PermissionLevel' = 'Reader'}
Add-PnPListItem -List 'Permisos por tipo de documento' -Values @{"Title" = "Bid Text"; 'SecurityGroup' = 'Operaciones Global'; 'PermissionLevel' = 'Reader'}
Add-PnPListItem -List 'Permisos por tipo de documento' -Values @{"Title" = "Bid Text"; 'SecurityGroup' = 'OBs'; 'PermissionLevel' = 'Reader'}

Write-Host 'Agregando elementos para el tipo de documento Anexos contractuales servicio'
Add-PnPListItem -List 'Permisos por tipo de documento' -Values @{"Title" = "Anexos contractuales servicio"; 'SecurityGroup' = 'PMs'; 'PermissionLevel' = 'Contributor'}
Add-PnPListItem -List 'Permisos por tipo de documento' -Values @{"Title" = "Anexos contractuales servicio"; 'SecurityGroup' = 'MNCs Sales'; 'PermissionLevel' = 'Reader'}
Add-PnPListItem -List 'Permisos por tipo de documento' -Values @{"Title" = "Anexos contractuales servicio"; 'SecurityGroup' = 'Presales-MNCs - Corporate - OBB'; 'PermissionLevel' = 'Reader'}
Add-PnPListItem -List 'Permisos por tipo de documento' -Values @{"Title" = "Anexos contractuales servicio"; 'SecurityGroup' = 'Operaciones Global'; 'PermissionLevel' = 'Reader'}

Write-Host 'Agregando elementos para el tipo de documento Cotizador'
Add-PnPListItem -List 'Permisos por tipo de documento' -Values @{"Title" = "Cotizador"; 'SecurityGroup' = 'PMs'; 'PermissionLevel' = 'Contributor'}
Add-PnPListItem -List 'Permisos por tipo de documento' -Values @{"Title" = "Cotizador"; 'SecurityGroup' = 'MNCs Sales'; 'PermissionLevel' = 'Reader'}

Write-Host 'Agregando elementos para el tipo de documento SOF'
Add-PnPListItem -List 'Permisos por tipo de documento' -Values @{"Title" = "SOF"; 'SecurityGroup' = 'PMs'; 'PermissionLevel' = 'Contributor'}
Add-PnPListItem -List 'Permisos por tipo de documento' -Values @{"Title" = "SOF"; 'SecurityGroup' = 'MNCs Sales'; 'PermissionLevel' = 'Reader'}
Add-PnPListItem -List 'Permisos por tipo de documento' -Values @{"Title" = "SOF"; 'SecurityGroup' = 'Presales-MNCs - Corporate - OBB'; 'PermissionLevel' = 'Reader'}
Add-PnPListItem -List 'Permisos por tipo de documento' -Values @{"Title" = "SOF"; 'SecurityGroup' = 'Operaciones Global'; 'PermissionLevel' = 'Reader'}

Write-Host 'Agregando elementos para el tipo de documento Mapa de cobertura'
Add-PnPListItem -List 'Permisos por tipo de documento' -Values @{"Title" = "Mapa de cobertura"; 'SecurityGroup' = 'PMs'; 'PermissionLevel' = 'Contributor'}
Add-PnPListItem -List 'Permisos por tipo de documento' -Values @{"Title" = "Mapa de cobertura"; 'SecurityGroup' = 'MNCs Sales'; 'PermissionLevel' = 'Reader'}
Add-PnPListItem -List 'Permisos por tipo de documento' -Values @{"Title" = "Mapa de cobertura"; 'SecurityGroup' = 'Presales-MNCs - Corporate - OBB'; 'PermissionLevel' = 'Reader'}
Add-PnPListItem -List 'Permisos por tipo de documento' -Values @{"Title" = "Mapa de cobertura"; 'SecurityGroup' = 'Operaciones Global'; 'PermissionLevel' = 'Reader'}

Write-Host 'Agregando elementos para el tipo de documento Guías de uso'
Add-PnPListItem -List 'Permisos por tipo de documento' -Values @{"Title" = "Guías de uso"; 'SecurityGroup' = 'PMs'; 'PermissionLevel' = 'Contributor'}
Add-PnPListItem -List 'Permisos por tipo de documento' -Values @{"Title" = "Guías de uso"; 'SecurityGroup' = 'MNCs Sales'; 'PermissionLevel' = 'Reader'}
Add-PnPListItem -List 'Permisos por tipo de documento' -Values @{"Title" = "Guías de uso"; 'SecurityGroup' = 'Presales-MNCs - Corporate - OBB'; 'PermissionLevel' = 'Reader'}
Add-PnPListItem -List 'Permisos por tipo de documento' -Values @{"Title" = "Guías de uso"; 'SecurityGroup' = 'Operaciones Global'; 'PermissionLevel' = 'Reader'}

Write-Host 'Agregando elementos para el tipo de documento Plantilla de oferta'
Add-PnPListItem -List 'Permisos por tipo de documento' -Values @{"Title" = "Plantilla de oferta"; 'SecurityGroup' = 'PMs'; 'PermissionLevel' = 'Contributor'}

Write-Host 'Agregando elementos para el tipo de documento Elevator Pitch'
Add-PnPListItem -List 'Permisos por tipo de documento' -Values @{"Title" = "Elevator Pitch"; 'SecurityGroup' = 'PMs'; 'PermissionLevel' = 'Contributor'}

Write-Host 'Agregando elementos para el tipo de documento Presentación comercial a clientes'
Add-PnPListItem -List 'Permisos por tipo de documento' -Values @{"Title" = "Presentación comercial a clientes"; 'SecurityGroup' = 'PMs'; 'PermissionLevel' = 'Contributor'}
Add-PnPListItem -List 'Permisos por tipo de documento' -Values @{"Title" = "Presentación comercial a clientes"; 'SecurityGroup' = 'MNCs Sales'; 'PermissionLevel' = 'Reader'}
Add-PnPListItem -List 'Permisos por tipo de documento' -Values @{"Title" = "Presentación comercial a clientes"; 'SecurityGroup' = 'Presales-MNCs - Corporate - OBB'; 'PermissionLevel' = 'Reader'}
Add-PnPListItem -List 'Permisos por tipo de documento' -Values @{"Title" = "Presentación comercial a clientes"; 'SecurityGroup' = 'Operaciones Global'; 'PermissionLevel' = 'Reader'}
Add-PnPListItem -List 'Permisos por tipo de documento' -Values @{"Title" = "Presentación comercial a clientes"; 'SecurityGroup' = 'OBs'; 'PermissionLevel' = 'Reader'}

Write-Host 'Agregando elementos para el tipo de documento Presentación preventas - ventas'
Add-PnPListItem -List 'Permisos por tipo de documento' -Values @{"Title" = "Presentación preventas - ventas"; 'SecurityGroup' = 'PMs'; 'PermissionLevel' = 'Contributor'}
Add-PnPListItem -List 'Permisos por tipo de documento' -Values @{"Title" = "Presentación preventas - ventas"; 'SecurityGroup' = 'MNCs Sales'; 'PermissionLevel' = 'Reader'}
Add-PnPListItem -List 'Permisos por tipo de documento' -Values @{"Title" = "Presentación preventas - ventas"; 'SecurityGroup' = 'Presales-MNCs - Corporate - OBB'; 'PermissionLevel' = 'Reader'}
Add-PnPListItem -List 'Permisos por tipo de documento' -Values @{"Title" = "Presentación preventas - ventas"; 'SecurityGroup' = 'Operaciones Global'; 'PermissionLevel' = 'Reader'}
Add-PnPListItem -List 'Permisos por tipo de documento' -Values @{"Title" = "Presentación preventas - ventas"; 'SecurityGroup' = 'OBs'; 'PermissionLevel' = 'Reader'}

Write-Host 'Agregando elementos para el tipo de documento Formaciones'
Add-PnPListItem -List 'Permisos por tipo de documento' -Values @{"Title" = "Formaciones"; 'SecurityGroup' = 'PMs'; 'PermissionLevel' = 'Contributor'}
Add-PnPListItem -List 'Permisos por tipo de documento' -Values @{"Title" = "Formaciones"; 'SecurityGroup' = 'MNCs Sales'; 'PermissionLevel' = 'Reader'}
Add-PnPListItem -List 'Permisos por tipo de documento' -Values @{"Title" = "Formaciones"; 'SecurityGroup' = 'Presales-MNCs - Corporate - OBB'; 'PermissionLevel' = 'Reader'}
Add-PnPListItem -List 'Permisos por tipo de documento' -Values @{"Title" = "Formaciones"; 'SecurityGroup' = 'Operaciones Global'; 'PermissionLevel' = 'Reader'}
Add-PnPListItem -List 'Permisos por tipo de documento' -Values @{"Title" = "Formaciones"; 'SecurityGroup' = 'OBs'; 'PermissionLevel' = 'Reader'}

Write-Host 'Agregando elementos para el tipo de documento Casos de éxito'
Add-PnPListItem -List 'Permisos por tipo de documento' -Values @{"Title" = "Casos de éxito"; 'SecurityGroup' = 'PMs'; 'PermissionLevel' = 'Contributor'}
Add-PnPListItem -List 'Permisos por tipo de documento' -Values @{"Title" = "Casos de éxito"; 'SecurityGroup' = 'MNCs Sales'; 'PermissionLevel' = 'Reader'}
Add-PnPListItem -List 'Permisos por tipo de documento' -Values @{"Title" = "Casos de éxito"; 'SecurityGroup' = 'Presales-MNCs - Corporate - OBB'; 'PermissionLevel' = 'Reader'}
Add-PnPListItem -List 'Permisos por tipo de documento' -Values @{"Title" = "Casos de éxito"; 'SecurityGroup' = 'Operaciones Global'; 'PermissionLevel' = 'Reader'}
Add-PnPListItem -List 'Permisos por tipo de documento' -Values @{"Title" = "Casos de éxito"; 'SecurityGroup' = 'OBs'; 'PermissionLevel' = 'Reader'}

Write-Host 'Agregando elementos para el tipo de documento Factsheet'
Add-PnPListItem -List 'Permisos por tipo de documento' -Values @{"Title" = "Factsheet"; 'SecurityGroup' = 'PMs'; 'PermissionLevel' = 'Contributor'}
Add-PnPListItem -List 'Permisos por tipo de documento' -Values @{"Title" = "Factsheet"; 'SecurityGroup' = 'MNCs Sales'; 'PermissionLevel' = 'Reader'}
Add-PnPListItem -List 'Permisos por tipo de documento' -Values @{"Title" = "Factsheet"; 'SecurityGroup' = 'Presales-MNCs - Corporate - OBB'; 'PermissionLevel' = 'Reader'}
Add-PnPListItem -List 'Permisos por tipo de documento' -Values @{"Title" = "Factsheet"; 'SecurityGroup' = 'Operaciones Global'; 'PermissionLevel' = 'Reader'}
Add-PnPListItem -List 'Permisos por tipo de documento' -Values @{"Title" = "Factsheet"; 'SecurityGroup' = 'OBs'; 'PermissionLevel' = 'Reader'}

Write-Host 'Agregando elementos para el tipo de documento Infografías'
Add-PnPListItem -List 'Permisos por tipo de documento' -Values @{"Title" = "Infografías"; 'SecurityGroup' = 'PMs'; 'PermissionLevel' = 'Contributor'}
Add-PnPListItem -List 'Permisos por tipo de documento' -Values @{"Title" = "Infografías"; 'SecurityGroup' = 'MNCs Sales'; 'PermissionLevel' = 'Reader'}
Add-PnPListItem -List 'Permisos por tipo de documento' -Values @{"Title" = "Infografías"; 'SecurityGroup' = 'Presales-MNCs - Corporate - OBB'; 'PermissionLevel' = 'Reader'}
Add-PnPListItem -List 'Permisos por tipo de documento' -Values @{"Title" = "Infografías"; 'SecurityGroup' = 'Operaciones Global'; 'PermissionLevel' = 'Reader'}
Add-PnPListItem -List 'Permisos por tipo de documento' -Values @{"Title" = "Infografías"; 'SecurityGroup' = 'OBs'; 'PermissionLevel' = 'Reader'}

Write-Host 'Agregando elementos para el tipo de documento Brochure'
Add-PnPListItem -List 'Permisos por tipo de documento' -Values @{"Title" = "Brochure"; 'SecurityGroup' = 'PMs'; 'PermissionLevel' = 'Contributor'}
Add-PnPListItem -List 'Permisos por tipo de documento' -Values @{"Title" = "Brochure"; 'SecurityGroup' = 'MNCs Sales'; 'PermissionLevel' = 'Reader'}
Add-PnPListItem -List 'Permisos por tipo de documento' -Values @{"Title" = "Brochure"; 'SecurityGroup' = 'Presales-MNCs - Corporate - OBB'; 'PermissionLevel' = 'Reader'}
Add-PnPListItem -List 'Permisos por tipo de documento' -Values @{"Title" = "Brochure"; 'SecurityGroup' = 'Operaciones Global'; 'PermissionLevel' = 'Reader'}
Add-PnPListItem -List 'Permisos por tipo de documento' -Values @{"Title" = "Brochure"; 'SecurityGroup' = 'OBs'; 'PermissionLevel' = 'Reader'}

Write-Host 'Agregando elementos para el tipo de documento Battlecard'
Add-PnPListItem -List 'Permisos por tipo de documento' -Values @{"Title" = "Battlecard"; 'SecurityGroup' = 'PMs'; 'PermissionLevel' = 'Contributor'}
Add-PnPListItem -List 'Permisos por tipo de documento' -Values @{"Title" = "Battlecard"; 'SecurityGroup' = 'OBs'; 'PermissionLevel' = 'Reader'}

Write-Host 'Agregando elementos para el tipo de documento Whitepaper'
Add-PnPListItem -List 'Permisos por tipo de documento' -Values @{"Title" = "Whitepaper"; 'SecurityGroup' = 'PMs'; 'PermissionLevel' = 'Contributor'}
Add-PnPListItem -List 'Permisos por tipo de documento' -Values @{"Title" = "Whitepaper"; 'SecurityGroup' = 'MNCs Sales'; 'PermissionLevel' = 'Reader'}
Add-PnPListItem -List 'Permisos por tipo de documento' -Values @{"Title" = "Whitepaper"; 'SecurityGroup' = 'Presales-MNCs - Corporate - OBB'; 'PermissionLevel' = 'Reader'}
Add-PnPListItem -List 'Permisos por tipo de documento' -Values @{"Title" = "Whitepaper"; 'SecurityGroup' = 'Operaciones Global'; 'PermissionLevel' = 'Reader'}
Add-PnPListItem -List 'Permisos por tipo de documento' -Values @{"Title" = "Whitepaper"; 'SecurityGroup' = 'OBs'; 'PermissionLevel' = 'Reader'}

Write-Host 'Agregando elementos para el tipo de documento Vídeos'
Add-PnPListItem -List 'Permisos por tipo de documento' -Values @{"Title" = "Vídeos"; 'SecurityGroup' = 'PMs'; 'PermissionLevel' = 'Contributor'}
Add-PnPListItem -List 'Permisos por tipo de documento' -Values @{"Title" = "Vídeos"; 'SecurityGroup' = 'MNCs Sales'; 'PermissionLevel' = 'Reader'}
Add-PnPListItem -List 'Permisos por tipo de documento' -Values @{"Title" = "Vídeos"; 'SecurityGroup' = 'Presales-MNCs - Corporate - OBB'; 'PermissionLevel' = 'Reader'}
Add-PnPListItem -List 'Permisos por tipo de documento' -Values @{"Title" = "Vídeos"; 'SecurityGroup' = 'Operaciones Global'; 'PermissionLevel' = 'Reader'}
Add-PnPListItem -List 'Permisos por tipo de documento' -Values @{"Title" = "Vídeos"; 'SecurityGroup' = 'OBs'; 'PermissionLevel' = 'Reader'}

Write-Host 'Agregando elementos para el tipo de documento Informes de analistas'
Add-PnPListItem -List 'Permisos por tipo de documento' -Values @{"Title" = "Informes de analistas"; 'SecurityGroup' = 'PMs'; 'PermissionLevel' = 'Contributor'}
Add-PnPListItem -List 'Permisos por tipo de documento' -Values @{"Title" = "Informes de analistas"; 'SecurityGroup' = 'MNCs Sales'; 'PermissionLevel' = 'Reader'}
Add-PnPListItem -List 'Permisos por tipo de documento' -Values @{"Title" = "Informes de analistas"; 'SecurityGroup' = 'Presales-MNCs - Corporate - OBB'; 'PermissionLevel' = 'Reader'}
Add-PnPListItem -List 'Permisos por tipo de documento' -Values @{"Title" = "Informes de analistas"; 'SecurityGroup' = 'Operaciones Global'; 'PermissionLevel' = 'Reader'}
Add-PnPListItem -List 'Permisos por tipo de documento' -Values @{"Title" = "Informes de analistas"; 'SecurityGroup' = 'OBs'; 'PermissionLevel' = 'Reader'}

Write-Host 'Agregando elementos para el tipo de documento Roadmap'
Add-PnPListItem -List 'Permisos por tipo de documento' -Values @{"Title" = "Roadmap"; 'SecurityGroup' = 'PMs'; 'PermissionLevel' = 'Contributor'}
Add-PnPListItem -List 'Permisos por tipo de documento' -Values @{"Title" = "Roadmap"; 'SecurityGroup' = 'MNCs Sales'; 'PermissionLevel' = 'Reader'}
Add-PnPListItem -List 'Permisos por tipo de documento' -Values @{"Title" = "Roadmap"; 'SecurityGroup' = 'Presales-MNCs - Corporate - OBB'; 'PermissionLevel' = 'Reader'}
Add-PnPListItem -List 'Permisos por tipo de documento' -Values @{"Title" = "Roadmap"; 'SecurityGroup' = 'Operaciones Global'; 'PermissionLevel' = 'Reader'}
Add-PnPListItem -List 'Permisos por tipo de documento' -Values @{"Title" = "Roadmap"; 'SecurityGroup' = 'OBs'; 'PermissionLevel' = 'Reader'}

Write-Host 'Agregando elementos para el tipo de documento Propuesta de valor B2B'
Add-PnPListItem -List 'Permisos por tipo de documento' -Values @{"Title" = "Propuesta de valor B2B"; 'SecurityGroup' = 'PMs'; 'PermissionLevel' = 'Contributor'}
Add-PnPListItem -List 'Permisos por tipo de documento' -Values @{"Title" = "Propuesta de valor B2B"; 'SecurityGroup' = 'MNCs Sales'; 'PermissionLevel' = 'Reader'}
Add-PnPListItem -List 'Permisos por tipo de documento' -Values @{"Title" = "Propuesta de valor B2B"; 'SecurityGroup' = 'Presales-MNCs - Corporate - OBB'; 'PermissionLevel' = 'Reader'}
Add-PnPListItem -List 'Permisos por tipo de documento' -Values @{"Title" = "Propuesta de valor B2B"; 'SecurityGroup' = 'Operaciones Global'; 'PermissionLevel' = 'Reader'}
Add-PnPListItem -List 'Permisos por tipo de documento' -Values @{"Title" = "Propuesta de valor B2B"; 'SecurityGroup' = 'OBs'; 'PermissionLevel' = 'Reader'}

Write-Host 'Agregando elementos para el tipo de documento Planes de activación'
Add-PnPListItem -List 'Permisos por tipo de documento' -Values @{"Title" = "Planes de activación"; 'SecurityGroup' = 'PMs'; 'PermissionLevel' = 'Contributor'}
Add-PnPListItem -List 'Permisos por tipo de documento' -Values @{"Title" = "Planes de activación"; 'SecurityGroup' = 'Operaciones Global'; 'PermissionLevel' = 'Reader'}
Add-PnPListItem -List 'Permisos por tipo de documento' -Values @{"Title" = "Planes de activación"; 'SecurityGroup' = 'OBs'; 'PermissionLevel' = 'Reader'}

Write-Host 'Agregando elementos para el tipo de documento Demos'
Add-PnPListItem -List 'Permisos por tipo de documento' -Values @{"Title" = "Demos"; 'SecurityGroup' = 'PMs'; 'PermissionLevel' = 'Contributor'}
Add-PnPListItem -List 'Permisos por tipo de documento' -Values @{"Title" = "Demos"; 'SecurityGroup' = 'MNCs Sales'; 'PermissionLevel' = 'Reader'}
Add-PnPListItem -List 'Permisos por tipo de documento' -Values @{"Title" = "Demos"; 'SecurityGroup' = 'Presales-MNCs - Corporate - OBB'; 'PermissionLevel' = 'Reader'}
Add-PnPListItem -List 'Permisos por tipo de documento' -Values @{"Title" = "Demos"; 'SecurityGroup' = 'Operaciones Global'; 'PermissionLevel' = 'Reader'}
Add-PnPListItem -List 'Permisos por tipo de documento' -Values @{"Title" = "Demos"; 'SecurityGroup' = 'OBs'; 'PermissionLevel' = 'Reader'}

Write-Host 'Agregando elementos para el tipo de documento Business Case'
Add-PnPListItem -List 'Permisos por tipo de documento' -Values @{"Title" = "Business Case"; 'SecurityGroup' = 'PMs'; 'PermissionLevel' = 'Contributor'}
Add-PnPListItem -List 'Permisos por tipo de documento' -Values @{"Title" = "Business Case"; 'SecurityGroup' = 'MNCs Sales'; 'PermissionLevel' = 'Reader'}
Add-PnPListItem -List 'Permisos por tipo de documento' -Values @{"Title" = "Business Case"; 'SecurityGroup' = 'Presales-MNCs - Corporate - OBB'; 'PermissionLevel' = 'Reader'}
Add-PnPListItem -List 'Permisos por tipo de documento' -Values @{"Title" = "Business Case"; 'SecurityGroup' = 'Operaciones Global'; 'PermissionLevel' = 'Reader'}
Add-PnPListItem -List 'Permisos por tipo de documento' -Values @{"Title" = "Business Case"; 'SecurityGroup' = 'OBs'; 'PermissionLevel' = 'Reader'}

Write-Host 'Agregando elementos para el tipo de documento Service Description'
Add-PnPListItem -List 'Permisos por tipo de documento' -Values @{"Title" = "Service Description"; 'SecurityGroup' = 'PMs'; 'PermissionLevel' = 'Contributor'}
Add-PnPListItem -List 'Permisos por tipo de documento' -Values @{"Title" = "Service Description"; 'SecurityGroup' = 'MNCs Sales'; 'PermissionLevel' = 'Reader'}
Add-PnPListItem -List 'Permisos por tipo de documento' -Values @{"Title" = "Service Description"; 'SecurityGroup' = 'Presales-MNCs - Corporate - OBB'; 'PermissionLevel' = 'Reader'}
Add-PnPListItem -List 'Permisos por tipo de documento' -Values @{"Title" = "Service Description"; 'SecurityGroup' = 'Operaciones Global'; 'PermissionLevel' = 'Reader'}
Add-PnPListItem -List 'Permisos por tipo de documento' -Values @{"Title" = "Service Description"; 'SecurityGroup' = 'OBs'; 'PermissionLevel' = 'Reader'}

Write-Host 'Agregando elementos para el tipo de documento Manual técnico'
Add-PnPListItem -List 'Permisos por tipo de documento' -Values @{"Title" = "Manual técnico"; 'SecurityGroup' = 'PMs'; 'PermissionLevel' = 'Contributor'}
Add-PnPListItem -List 'Permisos por tipo de documento' -Values @{"Title" = "Manual técnico"; 'SecurityGroup' = 'MNCs Sales'; 'PermissionLevel' = 'Reader'}
Add-PnPListItem -List 'Permisos por tipo de documento' -Values @{"Title" = "Manual técnico"; 'SecurityGroup' = 'Presales-MNCs - Corporate - OBB'; 'PermissionLevel' = 'Reader'}
Add-PnPListItem -List 'Permisos por tipo de documento' -Values @{"Title" = "Manual técnico"; 'SecurityGroup' = 'Operaciones Global'; 'PermissionLevel' = 'Reader'}
Add-PnPListItem -List 'Permisos por tipo de documento' -Values @{"Title" = "Manual técnico"; 'SecurityGroup' = 'OBs'; 'PermissionLevel' = 'Reader'}

Write-Host 'Agregando elementos para el tipo de documento Manual operativo'
Add-PnPListItem -List 'Permisos por tipo de documento' -Values @{"Title" = "Manual operativo"; 'SecurityGroup' = 'PMs'; 'PermissionLevel' = 'Contributor'}
Add-PnPListItem -List 'Permisos por tipo de documento' -Values @{"Title" = "Manual operativo"; 'SecurityGroup' = 'MNCs Sales'; 'PermissionLevel' = 'Reader'}
Add-PnPListItem -List 'Permisos por tipo de documento' -Values @{"Title" = "Manual operativo"; 'SecurityGroup' = 'Presales-MNCs - Corporate - OBB'; 'PermissionLevel' = 'Reader'}
Add-PnPListItem -List 'Permisos por tipo de documento' -Values @{"Title" = "Manual operativo"; 'SecurityGroup' = 'Operaciones Global'; 'PermissionLevel' = 'Reader'}
Add-PnPListItem -List 'Permisos por tipo de documento' -Values @{"Title" = "Manual operativo"; 'SecurityGroup' = 'OBs'; 'PermissionLevel' = 'Reader'}

Write-Host 'Agregando elementos para el tipo de documento Otros documentos DSS - Operaciones'
Add-PnPListItem -List 'Permisos por tipo de documento' -Values @{"Title" = "Documento B2B"; 'SecurityGroup' = 'PMs'; 'PermissionLevel' = 'Contributor'}
Add-PnPListItem -List 'Permisos por tipo de documento' -Values @{"Title" = "Documento B2B"; 'SecurityGroup' = 'Operaciones Global'; 'PermissionLevel' = 'Reader'}

Write-Host 'Agregando elementos para el tipo de documento Local capabilities'
Add-PnPListItem -List 'Permisos por tipo de documento' -Values @{"Title" = "Local capabilities"; 'SecurityGroup' = 'PMs'; 'PermissionLevel' = 'Contributor'}
Add-PnPListItem -List 'Permisos por tipo de documento' -Values @{"Title" = "Local capabilities"; 'SecurityGroup' = 'MNCs Sales'; 'PermissionLevel' = 'Reader'}
Add-PnPListItem -List 'Permisos por tipo de documento' -Values @{"Title" = "Local capabilities"; 'SecurityGroup' = 'Presales-MNCs - Corporate - OBB'; 'PermissionLevel' = 'Reader'}
Add-PnPListItem -List 'Permisos por tipo de documento' -Values @{"Title" = "Local capabilities"; 'SecurityGroup' = 'Operaciones Global'; 'PermissionLevel' = 'Reader'}
Add-PnPListItem -List 'Permisos por tipo de documento' -Values @{"Title" = "Local capabilities"; 'SecurityGroup' = 'OBs'; 'PermissionLevel' = 'Reader'}

Write-Host 'Agregando elementos para el tipo de documento FAQs'
Add-PnPListItem -List 'Permisos por tipo de documento' -Values @{"Title" = "FAQs"; 'SecurityGroup' = 'PMs'; 'PermissionLevel' = 'Contributor'}
Add-PnPListItem -List 'Permisos por tipo de documento' -Values @{"Title" = "FAQs"; 'SecurityGroup' = 'MNCs Sales'; 'PermissionLevel' = 'Reader'}
Add-PnPListItem -List 'Permisos por tipo de documento' -Values @{"Title" = "FAQs"; 'SecurityGroup' = 'Presales-MNCs - Corporate - OBB'; 'PermissionLevel' = 'Reader'}
Add-PnPListItem -List 'Permisos por tipo de documento' -Values @{"Title" = "FAQs"; 'SecurityGroup' = 'Operaciones Global'; 'PermissionLevel' = 'Reader'}
Add-PnPListItem -List 'Permisos por tipo de documento' -Values @{"Title" = "FAQs"; 'SecurityGroup' = 'OBs'; 'PermissionLevel' = 'Reader'}

Write-Host 'Agregando elementos para el tipo de documento Otros materiales'
Add-PnPListItem -List 'Permisos por tipo de documento' -Values @{"Title" = "Otros materiales"; 'SecurityGroup' = 'PMs'; 'PermissionLevel' = 'Contributor'}
Add-PnPListItem -List 'Permisos por tipo de documento' -Values @{"Title" = "Otros materiales"; 'SecurityGroup' = 'MNCs Sales'; 'PermissionLevel' = 'Reader'}
Add-PnPListItem -List 'Permisos por tipo de documento' -Values @{"Title" = "Otros materiales"; 'SecurityGroup' = 'Presales-MNCs - Corporate - OBB'; 'PermissionLevel' = 'Reader'}
Add-PnPListItem -List 'Permisos por tipo de documento' -Values @{"Title" = "Otros materiales"; 'SecurityGroup' = 'Operaciones Global'; 'PermissionLevel' = 'Reader'}
Add-PnPListItem -List 'Permisos por tipo de documento' -Values @{"Title" = "Otros materiales"; 'SecurityGroup' = 'OBs'; 'PermissionLevel' = 'Reader'}

Write-Host 'Agregando elementos para el tipo de documento Reporting'
Add-PnPListItem -List 'Permisos por tipo de documento' -Values @{"Title" = "Reporting"; 'SecurityGroup' = 'PMs'; 'PermissionLevel' = 'Contributor'}
Add-PnPListItem -List 'Permisos por tipo de documento' -Values @{"Title" = "Reporting"; 'SecurityGroup' = 'MNCs Sales'; 'PermissionLevel' = 'Reader'}
Add-PnPListItem -List 'Permisos por tipo de documento' -Values @{"Title" = "Reporting"; 'SecurityGroup' = 'Presales-MNCs - Corporate - OBB'; 'PermissionLevel' = 'Reader'}
Add-PnPListItem -List 'Permisos por tipo de documento' -Values @{"Title" = "Reporting"; 'SecurityGroup' = 'Operaciones Global'; 'PermissionLevel' = 'Reader'}
Add-PnPListItem -List 'Permisos por tipo de documento' -Values @{"Title" = "Reporting"; 'SecurityGroup' = 'OBs'; 'PermissionLevel' = 'Reader'}

Write-Host 'Agregando elementos para el tipo de documento Global RPFs'
Add-PnPListItem -List 'Permisos por tipo de documento' -Values @{"Title" = "Global RPFs"; 'SecurityGroup' = 'PMs'; 'PermissionLevel' = 'Contributor'}
Add-PnPListItem -List 'Permisos por tipo de documento' -Values @{"Title" = "Global RPFs"; 'SecurityGroup' = 'MNCs Sales'; 'PermissionLevel' = 'Reader'}
Add-PnPListItem -List 'Permisos por tipo de documento' -Values @{"Title" = "Global RPFs"; 'SecurityGroup' = 'Presales-MNCs - Corporate - OBB'; 'PermissionLevel' = 'Reader'}
Add-PnPListItem -List 'Permisos por tipo de documento' -Values @{"Title" = "Global RPFs"; 'SecurityGroup' = 'Operaciones Global'; 'PermissionLevel' = 'Reader'}