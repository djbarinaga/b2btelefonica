﻿Write-Host 'Conectando...'

$url = "https://avantgardeits.sharepoint.com"

Connect-PnPOnline -Url $url -Credentials (Get-Credential)

Add-PnPFieldFromXml '<Field Type="Note" DisplayName="Descripcion" Required="FALSE" ID="{4a3377d1-26fd-4037-bb8f-1b1777ad7626}" NumLines="6" RichText="FALSE" Sortable="FALSE" StaticName="WizardDescription" Name="WizardDescription" UnlimitedLengthInDocumentLibrary="TRUE"/>'

Write-Host 'Obteniendo tipo de contenido Document'
$ParentCT = Get-PnPContentType -Identity Documento

Write-Host 'Creando tipo de contenido Documento B2B'
Add-PnPContentType -Name "Documento B2B" -Description "Tipo de contenido base para los documentos B2B" -Group "B2B" -ParentContentType $ParentCT

#TODO: VALORES CHOICE
Write-Host 'Creando columna de sitio Familia Producto'
Add-PnPField -DisplayName 'Familia Producto' -InternalName 'ProductFamily' -Type 'Choice' -Choices "Bundle Product", "Networking", "Personal Comms", "SDN/NFV" -Group 'B2B'
Add-PnPFieldToContentType -Field 'ProductFamily' -ContentType 'Documento B2B'

#TODO: VALORES CHOICE
Write-Host 'Creando columna de sitio Producto'
Add-PnPField -DisplayName 'Producto' -InternalName 'Product' -Type 'MultiChoice' -Choices "BoD", "Bundle Product", "Cloud UC  (Broadsoft)", "CloudVPN", "Core Mobility", "Demos / Trials UCC", "EMA (Enterprise Mobile Application)", "Enterprise Mobile Apps (EMA)", "Ethernet", "Ethernet VPN", "Fast Mobile", "GME (Global Expense Management)", "Hosted UC  (HCS-Cisco)", "Internet", "IP VPN", "LAN/wLAN", "Local Billing", "Marketing WiFi", "mConferencing (Telepresence)", "MDM (Mobile Device Management)", "mSIP (SIP Trunking)", "Network Virtualization", "Operating Catalogue", "SD-WAN CISCO", "SD-WAN Nokia", "SD-WAN Nokia - unmanaged", "Secure Mobility", "SEDA Ecosystem", "TEM (Telecom Expense Management)", "UBB", "vFirewall", "VNFs", "vRAS", "WAN2Cloud", "Web Conferencing", "Web Performance (Akamai)", "Webex Teams", "WOS" -Group 'B2B'
Add-PnPFieldToContentType -Field 'Product' -ContentType 'Documento B2B'

#TODO: VALORES CHOICE
Write-Host 'Creando columna de sitio País'
Add-PnPField -DisplayName 'País' -InternalName 'Country' -Type 'MultiChoice' -Choices "AL", "ARG", "BR", "CH", "COL", "EC", "ESP", "FR", "PE", "UK", "VEN" -Group 'B2B'
Add-PnPFieldToContentType -Field 'Country' -ContentType 'Documento B2B'

Write-Host 'Creando columna de sitio Fecha de caducidad'
Add-PnPFieldFromXml '<Field Type="DateTime" DisplayName="Fecha de caducidad" Required="FALSE" EnforceUniqueValues="FALSE" Indexed="FALSE" Format="DateOnly" Group="B2B" FriendlyDisplayFormat="Disabled" ID="{10ce4fed-921a-4d51-a870-534605bf89be}" StaticName="ExpirationDate" Name="ExpirationDate"></Field>'
Add-PnPFieldToContentType -Field 'ExpirationDate' -ContentType 'Documento B2B'

#TODO: TEXTO O ELECCIÓN?
Write-Host 'Creando columna de sitio Área autor documento'
Add-PnPField -DisplayName 'Área autor documento' -InternalName 'AuthorArea' -Type 'Text' -Group 'B2B'
Add-PnPFieldToContentType -Field 'AuthorArea' -ContentType 'Documento B2B'

Write-Host 'Creando columna de sitio Uso del Documento'
Add-PnPField -DisplayName 'Uso del Documento' -InternalName 'DocumentUse' -Type 'Choice' -Choices 'Interno', 'Externo' -Group 'B2B'
Add-PnPFieldToContentType -Field 'DocumentUse' -ContentType 'Documento B2B'

#TODO: VALORES CHOICE
Write-Host 'Creando columna de sitio Uso del Enabler'
Add-PnPField -DisplayName 'Uso del Enabler' -InternalName 'Enabler' -Type 'MultiChoice' -Choices "Commercial Channel Enablement", "Defined Pricing", "Billing Procedure", "Service Management", "Handover to operations", "Service Delivery", "Handover to operations procedure" -Group 'B2B'
Add-PnPFieldToContentType -Field 'Enabler' -ContentType 'Documento B2B'

Write-Host 'Creando columna de sitio Imagen tipo documento'
Add-PnPField -DisplayName 'Imagen tipo documento' -InternalName 'DocumentTypeImage' -Type 'Text' -Group 'B2B'
Add-PnPFieldToContentType -Field 'DocumentTypeImage' -ContentType 'Documento B2B'

#Creamos el resto de tipos de contenido
$baseCT = Get-PnPContentType -Identity 'Documento B2B'

Write-Host 'Creando tipo de contenido Documento de concepto'
Add-PnPContentType -Name "Documento de concepto" -Description "" -Group "B2B" -ParentContentType $baseCT

Write-Host 'Creando tipo de contenido Formularios'
Add-PnPContentType -Name "Formularios" -Description "" -Group "B2B" -ParentContentType $baseCT

Write-Host 'Creando tipo de contenido Marketing Manual'
Add-PnPContentType -Name "Marketing Manual" -Description "" -Group "B2B" -ParentContentType $baseCT

Write-Host 'Creando tipo de contenido Bid Text'
Add-PnPContentType -Name "Bid Text" -Description "" -Group "B2B" -ParentContentType $baseCT

Write-Host 'Creando tipo de contenido Anexos contractuales servicio'
Add-PnPContentType -Name "Anexos contractuales servicio" -Description "" -Group "B2B" -ParentContentType $baseCT

Write-Host 'Creando tipo de contenido Cotizador'
Add-PnPContentType -Name "Cotizador" -Description "" -Group "B2B" -ParentContentType $baseCT

Write-Host 'Creando tipo de contenido SOF'
Add-PnPContentType -Name "SOF" -Description "" -Group "B2B" -ParentContentType $baseCT

Write-Host 'Creando tipo de contenido Mapa de cobertura'
Add-PnPContentType -Name "Mapa de cobertura" -Description "" -Group "B2B" -ParentContentType $baseCT

Write-Host 'Creando tipo de contenido Guías de uso'
Add-PnPContentType -Name "Guías de uso" -Description "" -Group "B2B" -ParentContentType $baseCT

Write-Host 'Creando tipo de contenido Plantilla de oferta'
Add-PnPContentType -Name "Plantilla de oferta" -Description "" -Group "B2B" -ParentContentType $baseCT

Write-Host 'Creando tipo de contenido Elevator Pitch'
Add-PnPContentType -Name "Elevator Pitch" -Description "" -Group "B2B" -ParentContentType $baseCT

Write-Host 'Creando tipo de contenido Presentación comercial a clientes'
Add-PnPContentType -Name "Presentación comercial a clientes" -Description "" -Group "B2B" -ParentContentType $baseCT

Write-Host 'Creando tipo de contenido Presentación preventas - ventas'
Add-PnPContentType -Name "Presentación preventas - ventas" -Description "" -Group "B2B" -ParentContentType $baseCT

Write-Host 'Creando tipo de contenido Formaciones'
Add-PnPContentType -Name "Formaciones" -Description "" -Group "B2B" -ParentContentType $baseCT

Write-Host 'Creando tipo de contenido Casos de éxito'
Add-PnPContentType -Name "Casos de éxito" -Description "" -Group "B2B" -ParentContentType $baseCT

Write-Host 'Creando tipo de contenido Factsheet'
Add-PnPContentType -Name "Factsheet" -Description "" -Group "B2B" -ParentContentType $baseCT

Write-Host 'Creando tipo de contenido Infografías'
Add-PnPContentType -Name "Infografías" -Description "" -Group "B2B" -ParentContentType $baseCT

Write-Host 'Creando tipo de contenido Brochure'
Add-PnPContentType -Name "Brochure" -Description "" -Group "B2B" -ParentContentType $baseCT

Write-Host 'Creando tipo de contenido Battlecard'
Add-PnPContentType -Name "Battlecard" -Description "" -Group "B2B" -ParentContentType $baseCT

Write-Host 'Creando tipo de contenido Whitepaper'
Add-PnPContentType -Name "Whitepaper" -Description "" -Group "B2B" -ParentContentType $baseCT

Write-Host 'Creando tipo de contenido Vídeos'
Add-PnPContentType -Name "Vídeos" -Description "" -Group "B2B" -ParentContentType $baseCT

Write-Host 'Creando tipo de contenido Informes de analistas'
Add-PnPContentType -Name "Informes de analistas" -Description "" -Group "B2B" -ParentContentType $baseCT

Write-Host 'Creando tipo de contenido Roadmap'
Add-PnPContentType -Name "Roadmap" -Description "" -Group "B2B" -ParentContentType $baseCT

Write-Host 'Creando tipo de contenido Propuesta de valor B2B'
Add-PnPContentType -Name "Propuesta de valor B2B" -Description "" -Group "B2B" -ParentContentType $baseCT

Write-Host 'Creando tipo de contenido Planes de activación'
Add-PnPContentType -Name "Planes de activación" -Description "" -Group "B2B" -ParentContentType $baseCT

Write-Host 'Creando tipo de contenido Demos'
Add-PnPContentType -Name "Demos" -Description "" -Group "B2B" -ParentContentType $baseCT

Write-Host 'Creando tipo de contenido Business Case'
Add-PnPContentType -Name "Business Case" -Description "" -Group "B2B" -ParentContentType $baseCT

Write-Host 'Creando tipo de contenido Service Description'
Add-PnPContentType -Name "Service Description" -Description "" -Group "B2B" -ParentContentType $baseCT

Write-Host 'Creando tipo de contenido Manual técnico'
Add-PnPContentType -Name "Manual técnico" -Description "" -Group "B2B" -ParentContentType $baseCT

Write-Host 'Creando tipo de contenido Manual operativo'
Add-PnPContentType -Name "Manual operativo" -Description "" -Group "B2B" -ParentContentType $baseCT

Write-Host 'Creando tipo de contenido Otros documentos DSS - Operaciones'
Add-PnPContentType -Name "Otros documentos DSS - Operaciones" -Description "" -Group "B2B" -ParentContentType $baseCT

Write-Host 'Creando tipo de contenido Local capabilities'
Add-PnPContentType -Name "Local capabilities" -Description "" -Group "B2B" -ParentContentType $baseCT

Write-Host 'Creando tipo de contenido FAQs'
Add-PnPContentType -Name "FAQs" -Description "" -Group "B2B" -ParentContentType $baseCT

Write-Host 'Creando tipo de contenido Otros materiales'
Add-PnPContentType -Name "Otros materiales" -Description "" -Group "B2B" -ParentContentType $baseCT

Write-Host 'Creando tipo de contenido Reporting'
Add-PnPContentType -Name "Reporting" -Description "" -Group "B2B" -ParentContentType $baseCT

Write-Host 'Creando tipo de contenido Global RPFs'
Add-PnPContentType -Name "Global RPFs" -Description "" -Group "B2B" -ParentContentType $baseCT


Add-PnPField -DisplayName 'B2B Content Type' -InternalName 'B2BContentType' -Type 'Choice' -Choices "Documento B2B", "Documento de concepto", "Formularios", "Marketing Manual", "Bid Text", "Anexos contractuales servicio", "Cotizador", "SOF", "Mapa de cobertura", "Guías de uso", "Plantilla de oferta", "Elevator Pitch", "Presentación comercial a clientes", "Presentación preventas - ventas", "Formaciones", "Casos de éxito", "Factsheet", "Infografías", "Brochure", "Battlecard", "Whitepaper", "Vídeos", "Informes de analistas", "Roadmap", "Propuesta de valor B2B", "Planes de activación", "Demos", "Business Case", "Service Description", "Manual técnico", "Manual operativo", "Otros documentos DSS - Operaciones", "Local capabilities", "FAQs", "Otros materiales", "Reporting", "Global RPFs" -Group 'B2B'