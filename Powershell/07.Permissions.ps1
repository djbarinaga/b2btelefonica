﻿Write-Host 'Conectando...'

$url = "https://telefonicacorp.sharepoint.com/sites/B2BFinder.OCEWU/es-es"

Connect-PnPOnline -Url $url -Credentials (Get-Credential)

Write-Host 'Rompiendo herencia de permisos en Documentos B2B'
$spoList= Get-PnPList "Documentos B2B"
$spoList.BreakRoleInheritance($false, $false)
$spoList.Update()

Write-Host 'Estableciendo permisos para el grupo PMs. Role: Colaborar'
Set-PnPListPermission -Identity 'Documentos B2B' -Group 'PMs' -AddRole 'Colaborar'

Write-Host 'Estableciendo permisos para el grupo MNCs Sales. Role: Leer'
Set-PnPListPermission -Identity 'Documentos B2B' -Group 'MNCs Sales' -AddRole 'Leer'

Write-Host 'Estableciendo permisos para el grupo Presales-MNCs - Corporate - OBB. Role: Leer'
Set-PnPListPermission -Identity 'Documentos B2B' -Group 'Presales-MNCs - Corporate - OBB' -AddRole 'Leer'

Write-Host 'Estableciendo permisos para el grupo Operaciones Global. Role: Leer'
Set-PnPListPermission -Identity 'Documentos B2B' -Group 'Operaciones Global' -AddRole 'Leer'

Write-Host 'Estableciendo permisos para el grupo OBs. Role: Leer'
Set-PnPListPermission -Identity 'Documentos B2B' -Group 'OBs' -AddRole 'Leer'