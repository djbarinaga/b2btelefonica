﻿Write-Host 'Conectando...'

$url = "https://avantgardeits.sharepoint.com/sites/telefonica/es-es"

#Connect-PnPOnline -Url $url -Credentials (Get-Credential)

Write-Host 'Creando biblioteca Documentos B2B'
New-PnPList -Title 'Documentos B2B' -Template DocumentLibrary -EnableContentTypes

Write-Host 'Obteniendo tipo de contenido Documento B2B'
$ParentCT = Get-PnPContentType -Identity 'Documento B2B'

Write-Host 'Agregando tipos de contenido'
Write-Host '	Documento B2B'
Add-PnPContentTypeToList -List 'Documentos B2B' -ContentType 'Documento B2B'

Write-Host '	Documento de concepto'
Add-PnPContentTypeToList -List 'Documentos B2B' -ContentType 'Documento de concepto'

Write-Host '	Formularios'
Add-PnPContentTypeToList -List 'Documentos B2B' -ContentType 'Formularios'

Write-Host '	Marketing Manual'
Add-PnPContentTypeToList -List 'Documentos B2B' -ContentType 'Marketing Manual'

Write-Host '	Bid Text'
Add-PnPContentTypeToList -List 'Documentos B2B' -ContentType 'Bid Text'

Write-Host '	Anexos contractuales servicio'
Add-PnPContentTypeToList -List 'Documentos B2B' -ContentType 'Anexos contractuales servicio'

Write-Host '	Cotizador'
Add-PnPContentTypeToList -List 'Documentos B2B' -ContentType 'Cotizador'

Write-Host '	SOF'
Add-PnPContentTypeToList -List 'Documentos B2B' -ContentType 'SOF'

Write-Host '	Mapa de cobertura'
Add-PnPContentTypeToList -List 'Documentos B2B' -ContentType 'Mapa de cobertura'

Write-Host '	Guías de uso'
Add-PnPContentTypeToList -List 'Documentos B2B' -ContentType 'Guías de uso'

Write-Host '	Plantilla de oferta'
Add-PnPContentTypeToList -List 'Documentos B2B' -ContentType 'Plantilla de oferta'

Write-Host '	Elevator Pitch'
Add-PnPContentTypeToList -List 'Documentos B2B' -ContentType 'Elevator Pitch'

Write-Host '	Presentación comercial a clientes'
Add-PnPContentTypeToList -List 'Documentos B2B' -ContentType 'Presentación comercial a clientes'

Write-Host '	Presentación preventas - ventas'
Add-PnPContentTypeToList -List 'Documentos B2B' -ContentType 'Presentación preventas - ventas'

Write-Host '	Formaciones'
Add-PnPContentTypeToList -List 'Documentos B2B' -ContentType 'Formaciones'

Write-Host '	Casos de éxito'
Add-PnPContentTypeToList -List 'Documentos B2B' -ContentType 'Casos de éxito'

Write-Host '	Factsheet'
Add-PnPContentTypeToList -List 'Documentos B2B' -ContentType 'Factsheet'

Write-Host '	Infografías'
Add-PnPContentTypeToList -List 'Documentos B2B' -ContentType 'Infografías'

Write-Host '	Brochure'
Add-PnPContentTypeToList -List 'Documentos B2B' -ContentType 'Brochure'

Write-Host '	Battlecard'
Add-PnPContentTypeToList -List 'Documentos B2B' -ContentType 'Battlecard'

Write-Host '	Whitepaper'
Add-PnPContentTypeToList -List 'Documentos B2B' -ContentType 'Whitepaper'

Write-Host '	Vídeos'
Add-PnPContentTypeToList -List 'Documentos B2B' -ContentType 'Vídeos'

Write-Host '	Informes de analistas'
Add-PnPContentTypeToList -List 'Documentos B2B' -ContentType 'Informes de analistas'

Write-Host '	Roadmap'
Add-PnPContentTypeToList -List 'Documentos B2B' -ContentType 'Roadmap'

Write-Host '	Propuesta de valor B2B'
Add-PnPContentTypeToList -List 'Documentos B2B' -ContentType 'Propuesta de valor B2B'

Write-Host '	Planes de activación'
Add-PnPContentTypeToList -List 'Documentos B2B' -ContentType 'Planes de activación'

Write-Host '	Demos'
Add-PnPContentTypeToList -List 'Documentos B2B' -ContentType 'Demos'

Write-Host '	Business Case'
Add-PnPContentTypeToList -List 'Documentos B2B' -ContentType 'Business Case'

Write-Host '	Service Description'
Add-PnPContentTypeToList -List 'Documentos B2B' -ContentType 'Service Description'

Write-Host '	Manual técnico'
Add-PnPContentTypeToList -List 'Documentos B2B' -ContentType 'Manual técnico'

Write-Host '	Manual operativo'
Add-PnPContentTypeToList -List 'Documentos B2B' -ContentType 'Manual operativo'

Write-Host '	Otros documentos DSS - Operaciones'
Add-PnPContentTypeToList -List 'Documentos B2B' -ContentType 'Otros documentos DSS - Operaciones'

Write-Host '	Plantilla de oferta'
Add-PnPContentTypeToList -List 'Documentos B2B' -ContentType 'Plantilla de oferta'

Write-Host '	Local capabilities'
Add-PnPContentTypeToList -List 'Documentos B2B' -ContentType 'Local capabilities'

Write-Host '	FAQs'
Add-PnPContentTypeToList -List 'Documentos B2B' -ContentType 'FAQs'

Write-Host '	Otros materiales'
Add-PnPContentTypeToList -List 'Documentos B2B' -ContentType 'Otros materiales'

Write-Host '	Reporting'
Add-PnPContentTypeToList -List 'Documentos B2B' -ContentType 'Reporting'

Write-Host '	Global RPFs'
Add-PnPContentTypeToList -List 'Documentos B2B' -ContentType 'Global RPFs'

Write-Host 'Asignando tipo de contenido por defecto'
Set-PnPDefaultContentTypeToList -List 'Documentos B2B' -ContentType 'Documento B2B'

Write-Host 'Habilitando versionado'
Set-PnPList -Identity "Documentos B2B" -EnableVersioning $true -EnableMinorVersions $true


Write-Host 'Creando carpetas'
Write-Host ''

$location = Get-Location
$location = $location.Path + '\'

Write-Host $location

$rootFolder = 'Documentos B2B'
Get-ChildItem "Documentos B2B" -Recurse -Directory | 
Foreach-Object {
	$folder = $_.FullName.Replace($location, '')
	$folder = $folder.Replace('\', '/')

	$parentFolder = $folder.SubString(0, $folder.LastIndexOf('/'))
	
	Write-Host 'Creando carpeta' $folder 'en' $parentFolder

	Add-PnPFolder -Name $_.Name -Folder $parentFolder
}