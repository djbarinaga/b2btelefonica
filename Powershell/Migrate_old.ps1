﻿$url = "https://avantgardeits.sharepoint.com"
$baseFileUrl = "/sites/B2BFinder.OCEWU/es-es/Documentos B2B/"
$baseFolderUrl = "Documentos B2B"
$logosUrl = "https://logos.telefonicabusinesssolutions.com"

Connect-PnPOnline -Url $logosUrl -Credentials (Get-Credential)
Connect-PnPOnline -Url $url -Credentials (Get-Credential)

$csv = Import-Csv -Path ".\csv\Documentos.csv" -Delimiter ";" | 
    ForEach-Object {
		Write-Host ""
		$date = Get-Date
        $fileValue = $_.File
		$fileFolder = $_.Folder
		$fileNameSplit = $fileValue.split('/')
		$fileName = $fileNameSplit[$fileNameSplit.length - 1]

		$fileUrl = $logosUrl + $fileFolder + '/' + $fileValue

		Write-Host $date.toString('HH:mm:ss') "Descargando" $fileUrl

        Write-Host $date.toString('HH:mm:ss') "Procesando" $fileName
		
		$author = $_.CreatedBy
		$editor = $_.ModifiedBy
		$contenType = $_.TipoDocumento
		$productFamily = $_.FamiliaProducto
		$product = $_.Producto
		$use = $_.Uso

		Write-Host $date.toString('HH:mm:ss') "	Author:" $author
		Write-Host $date.toString('HH:mm:ss') "	Tipo de documento:" $contenType
		Write-Host $date.toString('HH:mm:ss') "	Familia de producto:" $productFamily
		Write-Host $date.toString('HH:mm:ss') "	Producto:" $product
		Write-Host $date.toString('HH:mm:ss') "	Uso:" $use

		#Buscamos el fichero en la biblioteca de documentos
		$fileUrl = $baseFileUrl + $fileName
		
		Write-Host $date.toString('HH:mm:ss') "	Obteniendo fichero" $fileUrl
		
		$file = Get-PnPFile -Url $fileUrl -AsListItem

		if($file -ne $null){
			Write-Host $date.toString('HH:mm:ss') "	Fichero encontrado" -ForeGround Green

			$itemId = $file.Id
			Write-Host $date.toString('HH:mm:ss') "	Asignando propiedades al elemento. ID:" $itemId
		
			Set-PnPListItem -List "Documentos B2B" -Identity $itemId -ContentType $contenType -Values @{"ProductFamily" = $productFamily; "Product" = $product; "DocumentUse" = $use}

			if($author -ne $null){
				Write-Host $date.toString('HH:mm:ss') "	Estableciendo autor a" $author
				Set-PnPListItem -List "Documentos B2B" -Identity $itemId  -Values @{"Author" = $author}
			}
			
			if($editor -ne $null){
				Write-Host $date.toString('HH:mm:ss') "	Estableciendo editor a" $editor
				Set-PnPListItem -List "Documentos B2B" -Identity $itemId -Values @{"Editor" = $editor}
			}
			

			Write-Host $date.toString('HH:mm:ss') "	Propiedades modificadas" -ForeGround Green

			$folderUrl = $baseFolderUrl

			if($productFamily -ne $null){
				 $folderUrl += "/" + $productFamily
			}

			if($product -ne $null){
				$folderUrl += "/" + $product
			}

			if($contenType -ne $null){
				$folderUrl += "/" + $contenType
			}

			Write-Host $date.toString('HH:mm:ss') "	Moviendo documento a" $folderUrl

			$folder = $baseFolderUrl

			if($productFamily -ne $null){
				try{
					Write-Host $date.toString('HH:mm:ss') "	Creando carpeta" $productFamily "en" $folder
					Add-PnPFolder -Name $productFamily -Folder $folder	
				}
				finally{
					$folder += "/" + $productFamily
				}
			}

			if($product -ne $null){
				try{
					Write-Host $date.toString('HH:mm:ss') "	Creando carpeta" $product "en" $folder
					Add-PnPFolder -Name $product -Folder $folder
				}
				finally{
					$folder += "/" + $product
				}
			}

			if($contenType -ne $null){
				Write-Host $date.toString('HH:mm:ss') "	Creando carpeta" $contenType "en" $folder
				Add-PnPFolder -Name $contenType -Folder $folder
			}

			$targetUrl = $folder + "/" + $fileName

			Write-Host $date.toString('HH:mm:ss') "	Moviendo documento de"
			Write-Host $date.toString('HH:mm:ss') "		"$fileUrl
			Write-Host $date.toString('HH:mm:ss') "	a"
			Write-Host $date.toString('HH:mm:ss') "		"$targetUrl

			Move-PnPFile -ServerRelativeUrl $fileUrl -TargetUrl $targetUrl -OverwriteIfAlreadyExists -Force
			Write-Host $date.toString('HH:mm:ss') "	Documento movido" -ForeGround Green
		}
		else{
			Write-Host $date.toString('HH:mm:ss') "	No se ha encontrado el fichero" -ForeGround Yellow
		}
    }