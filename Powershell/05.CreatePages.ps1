﻿$url = '/sites/B2BFinder.OCEWU/es-es'
$relativeUrl = ([System.Uri]$url).AbsolutePath

Write-Host $relativeUrl

#DESARROLLO
Write-Host 'Conectando...'
$user = 'iago.cid@avantgardeit.es'
$pwd = convertto-securestring -String 'Hochtief2019.' -AsPlainText -Force

$cred = new-object -typename System.Management.Automation.PSCredential -argumentlist $user, $pwd

Connect-PnPOnline -Url $url -Credentials $cred
#FIN DESARROLLO


#Connect-PnPOnline -Url $url -Credentials (Get-Credential)

Write-Host 'Creando Página Home'
Add-PnPPublishingPage -PageName 'Home' -Title 'Telefónica Business Solutions' -PageTemplateName 'B2B/Home'

Write-Host 'Creando Página Favoritos'
Add-PnPPublishingPage -PageName 'Favoritos' -Title 'Mis favoritos' -PageTemplateName 'B2B/GenericLayout'

Write-Host 'Estableciendo página de inicio'
Set-PnPHomePage -RootFolderRelativeUrl 'Paginas/Home.aspx'