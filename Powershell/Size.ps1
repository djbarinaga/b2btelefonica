﻿$b2bUrl = "https://avantgardeits.sharepoint.com/sites/telefonica/es-es"
$b2bTopSiteUrl = "https://avantgardeits.sharepoint.com/sites/telefonica"
$tenantUrl = "https://avantgardeits.sharepoint.com"
$baseFolderUrl = "Documentos B2B"
$logosUrl = "https://logos.telefonicabusinesssolutions.com/pm"
$tempFolder = 'C:\Proyectos\Avantgarde\Telefónica\Desarrollo\Migracion\files'

Write-Host "Introduzca el usuario y contraseña para" $logosUrl -ForeGround Red
$logosCredentials = Get-Credential


Function Proccess{
	Connect-PnPOnline -Url $logosUrl -Credentials $logosCredentials
	$total = 0
	$csv = Import-Csv -Path ".\csv\Documentos.csv" -Delimiter ";" | 
    ForEach-Object {
		Write-Host ""

		$author = $_.CreatedBy
		$editor = $_.ModifiedBy
		$contenType = $_.TipoDocumento
		$productFamily = $_.FamiliaProducto
		$product = $_.Producto
		$fileValue = $_.File
		$fileFolder = $_.Folder

		$use = $_.Uso
		if($use -eq $null -or $use -eq ''){
			$use = "Externo"
		}

		$date = Get-Date
		Write-Host $date.toString('HH:mm:ss') "Procesando archivo" $fileValue

		if($contenType -ne $null -and $contenType -ne ''){
			$fileNameSplit = $fileValue.split('/')
			$fileName = $fileNameSplit[$fileNameSplit.length - 1]

			$fileUrl = $fileFolder + '/' + $fileValue

			$listItemBytes = Get-PnPFile -Url $fileUrl -AsString
			$total += $listItemBytes.length
		}
    }

	$date = Get-Date
	Write-Host $date.toString('HH:mm:ss') $total
}

Proccess