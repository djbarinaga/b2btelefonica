﻿$(document).ready(function () {
    SP.SOD.executeFunc('sp.js', 'SP.ClientContext', function () {
        var clientContext = new SP.ClientContext.get_current();
        var oWeb = clientContext.get_web();
        
        clientContext.load(oWeb, 'EffectiveBasePermissions');

        //Execute the batch  
        clientContext.executeQueryAsync(
            function () {
                if (!oWeb.get_effectiveBasePermissions().has(SP.PermissionKind.manageWeb)) {
                    var modal = document.getElementById("modalDiv");
                    modal.style.display = "block";
                }
            },
            function (sender, args) {
                console.log('Request failed' + args.get_message());
            }
        );
    });
});