﻿var targetDocuments = new Array();
var documentTypeImages = new Array();
var documentTypeDescriptions = new Array();
var documentTypes = new Array();
var documentUploaded = new Array();
var products = new Array();
var productFamily = new Array();
var enablers = new Array();
var permissions = new Array();
var maxLength = 16;
var use = ["Interno", "Externo"];
var currentIndex = 0;
var usersNotFound = new Array();


$(document).ready(function () {
    SP.SOD.executeFunc('sp.js', 'SP.ClientContext', function () {
        checkUsers(0); 
    });
});

function checkUsers(index) {
    if (index == docJSON.length) {
        $('#documentList').empty();

        for (var i = 0; i < usersNotFound.length; i++) {
            $('#documentList').prepend('<p>' + usersNotFound[i] + '</p>');
        }

        alert('Fin');
        return;
    }

    var currentIndex = index;
    var currentDocument = docJSON[index];

    if (currentDocument.CreatedBy != '') {
        if (usersNotFound.indexOf(currentDocument.CreatedBy) == -1) {
            var userLogin = "i:0#.f|membership|" + currentDocument.CreatedBy;
            $('#documentList').prepend('<p>Comprobando ' + currentDocument.CreatedBy + '</p>');

            ensureUser(userLogin, function (result) {
                if (result == null) {
                    if (usersNotFound.indexOf(currentDocument.CreatedBy) == -1)
                        usersNotFound.push(currentDocument.CreatedBy);
                }

                currentIndex++;
                checkUsers(currentIndex);
            });
        }
    }
    else {
        currentIndex++;
        checkUsers(currentIndex);
    }
}

function ensureUser(userLogin, callback) {
    var context = new SP.ClientContext("https://telefonicacorp.sharepoint.com/sites/B2BFinder.OCEWU");
    var newUser = context.get_web().ensureUser(userLogin);
    context.load(newUser);

    context.executeQueryAsync(
        function () {
            callback(newUser.get_id());
        },
        function (sender, args) {
            callback(null);
        }
    );
}