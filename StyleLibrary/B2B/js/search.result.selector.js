﻿var documentsSelected = [];

(function ($) {
    $.fn.imageSelector = function () {
        $(this).mouseover(function () {
            $(this).find('.image-selector.uncheck').animate({ top: '0', opacity: '1' });
        });

        $(this).mouseleave(function () {
            $(this).find('.image-selector.uncheck').animate({ top: '100px', opacity: '0' });
        });

        $(this).find('.image-selector').click(function () {
            if ($(this).hasClass('uncheck')) {
                $(this).removeClass('uncheck').addClass('check');
                $(this).text('');
            }
            else {
                $(this).removeClass('check').addClass('uncheck');
                $(this).animate({ top: '100px', opacity: '0' });
                $(this).text(translate('SelectDocument'));
            }

            var documentUrl = $(this).closest('.doc-results').data('url');
            documentUrl = documentUrl.split('?')[0];
            documentUrl = 'https://telefonicacorp.sharepoint.com' + documentUrl;
            selectDocument(documentUrl);
        });

        function selectDocument(documentUrl) {
            var index = documentsSelected.indexOf(documentUrl);
            if (index === -1)
                documentsSelected.push(documentUrl);
            else {
                documentsSelected = jQuery.grep(documentsSelected, function (a) {
                    return a !== documentUrl;
                });
            }

            showShareAllButton();
        }

        var imageSelectorCookie = '';

        if (imageSelectorCookie === '') {
            var popover = $('.doc-type-image:first').popover({
                html: true,
                content: translate('ImageSelectorText'),
                title: translate('ImageSelectorTitle'),
                trigger: 'focus'
            });

            popover.on('hidden.bs.popover', function () {
                setCookie('imageselector', '1', 365);
                popover.popover('dispose');
            });

            var interval = setInterval(function () {
                clearInterval(interval);
                popover.popover('show');
            }, 2000);

            var interval2 = setInterval(function () {
                clearInterval(interval2);
                popover.popover('hide');
            }, 7000);
        }
    };
}(jQuery));

function addButtonClick() {
    var documents = documentsSelected.join('\r\r');
    $('#txtDocuments').val('');

    $('button[data-role="shareAllLinks"]').click(function () {
        $('#modalDocumentsSelected').modal('show');
        $('#txtDocuments').val(documents);
    });
}

function showShareAllButton() {
    if (documentsSelected.length > 0) {
        $('button[data-role="shareAllLinks"]').fadeIn();
        addButtonClick();
    }
    else {
        $('button[data-role="shareAllLinks"]').fadeOut();
    }
}

function checkDocumentsSelected() {
    $('.doc-results').each(function () {
        var documentUrl = $(this).data('url');
        documentUrl = documentUrl.split('?')[0];
        documentUrl = 'https://telefonicacorp.sharepoint.com' + documentUrl;
        if (documentsSelected.indexOf(documentUrl) > -1) {
            $(this).find('.image-selector').removeClass('uncheck').addClass('check');
            $(this).find('.image-selector').text('');
        }
    });

    showShareAllButton();
}