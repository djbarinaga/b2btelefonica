﻿var searchTotalRows = 0;
var searchUrl;
var languages = ["es", "en"];
var cancelDownload = false;

var culture = getCookie('culture');
if (culture == '')
    setCookie('culture', 'es', 365);

$(document).ready(function () {
    $('#txtSearch').val(translate('txtSearchPlaceholder'));
    $('#btnCancel').on('click', function () {
        window.history.go(-1);
    });

    SP.SOD.executeFunc('sp.js', 'SP.ClientContext', initMaster);

    adjustLanguageBar();

    $(document).on('click', '#cancelDownload', function () {
        cancelDownload = true;
        window.location.reload();
    });
});

function initMaster() {
    searchUrl = _spPageContextInfo.serverRequestPath.substring(0, _spPageContextInfo.serverRequestPath.lastIndexOf("/") + 1);
    searchUrl += 'busqueda.aspx?k=';

    setCopyright();
    setNavigation();
    searchButton();
    setVaritationLabels();

    $('#txtSearch').on('focus', function () {
        var text = $(this).val();
        if (text.toLowerCase() == translate('txtSearchPlaceholder').toLowerCase())
            $(this).val('');
    });

    $('#txtSearch').on('blur', function () {
        var text = $(this).val();
        if (text == '')
            $(this).val(translate('txtSearchPlaceholder'));
    });

    $('#txtSearch').keydown(function (event) {

        var keycode = (event.keyCode ? event.keyCode : event.which);
        if (keycode == '13') {
            if ($(this).val() != 'B2B Finder') {
                $('#btnSearch').click();
                return false;
            }
        }

    });

    $('#txtSearch').keyup(function () {
        getQuerySuggestions();
    });

}

function checkPermissions() {
    var clientContext = new SP.ClientContext.get_current();
    var oWeb = clientContext.get_web();

    clientContext.load(oWeb, 'EffectiveBasePermissions');

    //Execute the batch  
    clientContext.executeQueryAsync(
        function () {
            if (oWeb.get_effectiveBasePermissions().has(SP.PermissionKind.manageWeb)) {
                $('#suiteBarDelta').show();
                $('#s4-ribbonrow').show();
            }
        },
        function (sender, args) {
            console.log('Request failed' + args.get_message());
        }
    );
}

function setCopyright() {
    $('#copyrigh-date').text(new Date().getFullYear());
}

function searchButton() {
    $('#btnSearch').on('click', function () {
        var text = $('#txtSearch').val();

        if (text != '' && text != 'B2B Finder') {

            var url = _spPageContextInfo.serverRequestPath.substring(0, _spPageContextInfo.serverRequestPath.lastIndexOf("/") + 1);
            url += 'busqueda.aspx?k=' + text;
            window.location.href = url;
        }
    });
}

function getQuerySuggestions() {
    var text = $('#txtSearch').val();
    var url = _spPageContextInfo.webAbsoluteUrl + "/_api/search/suggest?querytext='" + text + " '";

    var $ajax = $.ajax({
        url: url,
        type: "GET",
        dataType: "json",
        headers: {
            Accept: "application/json;odata=verbose"
        }
    });

    $ajax.done(function (data, textStatus, jqXHR) {
        var results = data.d.suggest.Queries.results;
        if (results.length > 0) {
            $('#searchDropDown').empty();
            
            for (var i = 0; i < results.length; i++) {
                $('#searchDropDown').append('<li class="list-group-item"><a class="dropdown-item" href="' + searchUrl + results[i].Query.replace(/<\/?[^>]+(>|$)/g, "") + '">' + results[i].Query + '</a></li>')
            }

            $('#searchDropDown').fadeIn();
        }
        else {
            $('#searchDropDown').fadeOut();
        }
    });
}

function setNavigation() {
    var url = _spPageContextInfo.webAbsoluteUrl + "/_api/navigation/menustate?mapprovidername='GlobalNavigationSwitchableProvider'";

    var $ajax = $.ajax({
        url: url,
        type: "GET",
        dataType: "json",
        headers: {
            Accept: "application/json;odata=verbose"
        }
    });

    $ajax.done(function (data, textStatus, jqXHR) {
        var menu = data.d.MenuState.Nodes.results;
        var html = '';
        for (var i = 0; i < menu.length; i++) {
            if (!menu[i].IsHidden) {
                var description = getMenuDescription(menu[i]);
                if (window.location.href.toLowerCase().indexOf(menu[i].SimpleUrl.toLowerCase()) > -1) {
                    html += '<li id="menu-' + i + '" class="nav-item active"><a class="nav-link" href="' + menu[i].SimpleUrl + '">' + translate(menu[i].Title) + '</a></li>';
                }
                else {
                    html += '<li id="menu-' + i + '"class="nav-item"><a class="nav-link" href="' + menu[i].SimpleUrl + '" data-intro="' + description + '">' + translate(menu[i].Title) + '</a></li>';
                }
            }
        }

        if (html != '')
            $('#menuLinks').prepend(html);
    });
}

function getMenuIcon(menu) {
    var icon = '';
    var customProperties = menu.CustomProperties.results;
    for (var i = 0; i < customProperties.length; i++) {
        var customProperty = customProperties[i];
        if (customProperty.Key == 'Description') {
            icon = customProperty.Value;
            break;
        }
    }

    return icon;
}

function getMenuDescription(menu) {
    var description = '';
    var customProperties = menu.CustomProperties.results;
    for (var i = 0; i < customProperties.length; i++) {
        var customProperty = customProperties[i];
        if (customProperty.Key == 'Description') {
            description = customProperty.Value;
            break;
        }
    }

    return description;
}

function getVariationLabels(callback) {
    var ctx = new SP.ClientContext(_spPageContextInfo.siteAbsoluteUrl);
    var rootWeb = ctx.get_site().get_rootWeb();
    var webProperties = rootWeb.get_allProperties();
    ctx.load(rootWeb);
    ctx.load(webProperties);
    ctx.executeQueryAsync(
        function () {
            var varLabelsListId = webProperties.get_item('_VarLabelsListId');

            var labelsList = rootWeb.get_lists().getById(varLabelsListId);
            var labelItems = labelsList.getItems(SP.CamlQuery.createAllItemsQuery());

            ctx.load(labelItems);
            ctx.executeQueryAsync(
                function () {
                    var variationLabels = [];
                    var e = labelItems.getEnumerator();
                    while (e.moveNext()) {
                        var labelItem = e.get_current();
                        variationLabels.push({
                            'IsSource': labelItem.get_item('Is_x0020_Source'),
                            'Language': labelItem.get_item('Language'),
                            'Locale': labelItem.get_item('Locale'),
                            'Title': labelItem.get_item('Title'),
                            'TopWebUrl': labelItem.get_item('Top_x0020_Web_x0020_URL')
                        });
                    }

                    if (callback != null)
                        callback(variationLabels);
                },
                function (sender, args) {
                    console.log('Request failed. ' + args.get_message() + '\n' + args.get_stackTrace());
                }
            );

        },
        function (sender, args) {
            console.log('Request failed. ' + args.get_message() + '\n' + args.get_stackTrace());
        }
    );
}

function setVaritationLabels() {
    var language = getCookie("culture");
    if (language == '')
        language = _spPageContextInfo.currentCultureName.split('-')[0];

    $('#currentLanguage').text(language);

    for (var i = 0; i < languages.length; i++) {
        if (language != languages[i]) {
            $('#alternativeLanguage').text(languages[i]);
            break;
        }
    }

    $('#alternativeLanguage').on('click', function () {
        setCookie('culture', $(this).text(), 365);
        window.location.reload();
    });
}

function exportSearchResults(keyWord, totalRows) {
    var url = _spPageContextInfo.webAbsoluteUrl + "/_api/search/query?querytext='" + keyWord + "'&rowlimit=" + totalRows;

    var $ajax = $.ajax({
        url: url,
        type: "GET",
        dataType: "json",
        headers: {
            Accept: "application/json;odata=verbose"
        }
    });

    $ajax.done(function (data, textStatus, jqXHR) {
        var resultsToExport = new Array();

        var results = data.d.query.PrimaryQueryResult.RelevantResults.Table.Rows.results;
        for (var i = 0; i < results.length; i++) {
            var cells = results[i].Cells.results;
            var title = getResult(cells, 'Title');
            var author = getResult(cells, 'Author');
            var path = getResult(cells, 'Path');
            var size = getResult(cells, 'Size');
            var description = getResult(cells, 'Description');

            resultsToExport.push({
                title: title,
                author: author,
                url: path,
                size: size,
                description: description
            });
        }

        var csvString = "";

        //Nombres de las columnas
        var firstResult = resultsToExport[0];
        var header = new Array();
        for (var key in firstResult) {
            header.push(key);
        }

        csvString += header.join(',');
        csvString += "\r\n";


        //Datos
        for (var i = 0; i < resultsToExport.length; i++) {
            var values = new Array();
            var currentResult = resultsToExport[i];
            for (var key in currentResult) {
                values.push(currentResult[key]);
            }

            csvString += values.join(',');
            csvString += "\r\n";
        }

        csvString = "data:application/csv," + encodeURIComponent(csvString);
        var x = document.createElement("a");
        x.setAttribute("href", csvString);
        x.setAttribute("download", "search-results.csv");
        document.body.appendChild(x);
        x.click();
    });
}

function getResult(cells, key) {
    var value;
    for (var i = 0; i < cells.length; i++) {
        var cell = cells[i];
        if (cell.Key == key) {
            value = cell.Value;
            break;
        }
    }

    return value;
}

function downloadZip(keyWord, totalRows) {
    var url = _spPageContextInfo.webAbsoluteUrl + "/_api/search/query?querytext='" + keyWord + "'&selectproperties='DocumentLink'&rowlimit=" + totalRows;

    var $ajax = $.ajax({
        url: url,
        type: "GET",
        dataType: "json",
        headers: {
            Accept: "application/json;odata=verbose"
        }
    });

    $ajax.done(function (data, textStatus, jqXHR) {
        var resultsToExport = new Array();

        var results = data.d.query.PrimaryQueryResult.RelevantResults.Table.Rows.results;
        for (var i = 0; i < results.length; i++) {
            var cells = results[i].Cells.results;
            var path = getResult(cells, 'DocumentLink');

            resultsToExport.push(path);
        }

        var dialog = bootbox.dialog({
            title: translate('CreatingCompressedFile'),
            message: '<div id="zipFiles"></div><a href="#" class="btn btn-blue09" id="cancelDownload">' + translate('Cancel') + '</a>',
            closeButton: false
        });

        var files = new Array();

        var getBinaryContent = function (index) {
            if (index == resultsToExport.length) {
                var zip = new JSZip();
                for (var i = 0; i < files.length; i++) {
                    if (!cancelDownload) {
                        var currentFile = files[i];

                        zip.file(currentFile.name, currentFile.data, { binary: true });
                    }
                }

                zip.generateAsync({ type: 'blob' }).then(function (content) {
                    dialog.modal('hide');
                    saveAs(content, 'resultados-busqueda.zip');
                });

                return;
            }

            var file = resultsToExport[index];
            var fileSplit = file.split('/');
            var filename = fileSplit[fileSplit.length - 1];

            $('#zipFiles').html('<p>' + filename + '</p>');

            JSZipUtils.getBinaryContent(url, function (err, data) {
                if (err) {
                    reject(err);
                } else {
                    files.push({
                        name: filename,
                        data: data
                    });

                    var currentIndex = index + 1;
                    getBinaryContent(currentIndex);
                }
            });
        };

        getBinaryContent(0);
    });
}

function openDialog(url, title) {
    var options = {
        url: url,
        title: title,
        allowMaximize: false,
        showClose: true
    };

    SP.SOD.execute('sp.ui.dialog.js', 'SP.UI.ModalDialog.showModalDialog', options);
    return false;
}

function adjustLanguageBar() {
    var languageBar = $('#languageBar');
    var dropDownMenu = $('#languageBar .dropdown-menu');
    var lbWidth = $(languageBar).outerWidth() * 2;
    var ddWidth = $(dropDownMenu).outerWidth();
    var width = ddWidth - lbWidth;
    dropDownMenu.attr('style', 'left:-' + width + 'px;');
}

