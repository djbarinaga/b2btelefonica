﻿/*
  <textarea rows="10" id="txtLog" class="form-control" style="font-size:12px"></textarea>
  <button type="button" id="btnSetContentType" style="float:left;margin:10px">Establecer tipos de contenido</button>
  <button type="button" id="btnSetMetadata" style="float:left;margin:10px">Establecer metadatos</button>
  <button type="button" id="btnSetResponsible" style="float:left;margin:10px">Establecer responsable</button>
  <button type="button" id="btnSetPermissions" style="float:left;margin:10px">Establecer permisos</button>
  <button type="button" id="btnPublish" style="float:left;margin:10px">Publicar</button>
  <button type="button" id="btnCheck" style="float:left;margin:10px">Check</button>
  <button type="button" id="btnAllDocs" style="float:left;margin:10px">Obtener documentos</button>
  <button type="button" id="btnDeleteVersions" style="float:left;margin:10px">Eliminar versiones</button>
  <button type="button" id="btnClearLog" style="float:left;margin:10px">Limpiar log</button>
*/


var targetDocuments = new Array();
var documentTypeImages = new Array();
var documentTypeDescriptions = new Array();
var documentTypes = new Array();
var documentUploaded = new Array();
var products = new Array();
var productFamily = new Array();
var enablers = new Array();
var permissions = new Array();
var maxLength = 16;
var use = ["Interno", "Externo"];
var currentIndex = 0;
var usersNotFound = new Array();

$(document).ready(function () {
    SP.SOD.executeFunc('sp.js', 'SP.ClientContext', function () {
        loadDocumentTypes(function () {
            loadEnablers();
            loadDocumentTypeImages();
            loadProduct();
            loadPermissions();
            loadDocuments();
        });
    });

    $('#btnClearLog').click(function () {
        $('#txtLog').val('');
    });

    $('#btnSetContentType').click(function () {
        setContentType(0);
    });

    $('#btnSetMetadata').click(function () {
        setMetadata(0);
    });

    $('#btnSetResponsible').click(function () {
        setResponsible(0);
    });

    $('#btnSetPermissions').click(function () {
        setPermissions(0);
    });

    $('#btnPublish').click(function () {
        publish(0);
    });

    $('#btnCheck').click(function () {
        checkMigratedDocuments();
    });

    $('#btnAllDocs').click(function () {
        getAllDocuments();
    });

    $('#btnDeleteVersions').click(function () {
        deleteVersions(0);
    });
});

/*
 *
 *  FUNCIONES DE CARGA DE DATOS
 * 
*/
function loadDocuments() {
    var clientContext = new SP.ClientContext(_spPageContextInfo.webAbsoluteUrl);
    var list = clientContext.get_web().get_lists().getByTitle('Documentos B2B');
    var query = SP.CamlQuery.createAllItemsQuery();
    var items = list.getItems(query);

    clientContext.load(items);

    clientContext.executeQueryAsync(
        function () {
            var listItemEnumerator = items.getEnumerator();

            while (listItemEnumerator.moveNext()) {
                var item = listItemEnumerator.get_current();
                var ctypeId = item.get_item('ContentTypeId');
                var cType = '';

                for (var i = 0; i < documentTypes.length; i++) {
                    if (ctypeId.toString() == documentTypes[i].id) {
                        cType = documentTypes[i].name;
                        break;
                    }
                }
                
                if (ctypeId.toString().indexOf('0x0120') == -1) {
                    targetDocuments.push({
                        id: item.get_id(),
                        fileName: item.get_item('FileLeafRef'),
                        contentType: item.get_item('ContentTypeId'),
                        cType: cType,
                        url: item.get_item('FileRef'),
                        productFamily: item.get_item('ProductFamily'),
                        product: item.get_item('Product'),
                        responsible: (item.get_item('Responsible') != null ? item.get_item('Responsible').$7_2 : null),
                        responsibleText: item.get_item('ResponsibleEmail'),
                        author: (item.get_item('Author').$7_2)
                    });
                }
            }

            log(targetDocuments.length + ' documentos cargados');
        },
        function (sender, args) {
            console.log('Request failed. ' + args.get_message() + '\n' + args.get_stackTrace());
        }
    );
}

function loadDocumentTypes(callback) {
    var clientContext = new SP.ClientContext(_spPageContextInfo.webAbsoluteUrl);
    var list = clientContext.get_web().get_lists().getByTitle('Documentos B2B');
    var contentTypes = list.get_contentTypes();

    clientContext.load(contentTypes);

    clientContext.executeQueryAsync(
        function () {
            var results = [];
            var contentTypesEnumerator = contentTypes.getEnumerator();

            while (contentTypesEnumerator.moveNext()) {
                var cType = contentTypesEnumerator.get_current();
                documentTypes.push({
                    id: cType.get_id(),
                    name: cType.get_name()
                });
            }

            log('Document types cargados');

            if (callback != null) {
                callback();
            }
        },
        function (sender, args) {
            console.log('Request failed. ' + args.get_message() + '\n' + args.get_stackTrace());
        }
    );
}

function loadEnablers() {
    var clientContext = new SP.ClientContext(_spPageContextInfo.siteAbsoluteUrl);
    var list = clientContext.get_web().get_lists().getByTitle('Enablers');
    var query = SP.CamlQuery.createAllItemsQuery();
    var items = list.getItems(query);

    clientContext.load(items);

    clientContext.executeQueryAsync(
        function () {
            var listItemEnumerator = items.getEnumerator();

            while (listItemEnumerator.moveNext()) {
                var item = listItemEnumerator.get_current();
                var docType = item.get_item('B2BContentType');
                var enabler = item.get_item('Enabler');

                enablers.push({
                    documentType: docType,
                    enablers: enabler
                });
            }

            log('Enablers cargados');
        },
        function (sender, args) {
            console.log('Request failed. ' + args.get_message() + '\n' + args.get_stackTrace());
        }
    );
}

function loadPermissions() {
    var clientContext = new SP.ClientContext(_spPageContextInfo.siteAbsoluteUrl);
    var list = clientContext.get_web().get_lists().getByTitle('Permisos por tipo de documento');
    var query = SP.CamlQuery.createAllItemsQuery();
    var items = list.getItems(query);

    clientContext.load(items);

    clientContext.executeQueryAsync(
        function () {
            var listItemEnumerator = items.getEnumerator();

            while (listItemEnumerator.moveNext()) {
                var item = listItemEnumerator.get_current();
                var docType = item.get_item('B2BContentType');
                var securityGroup = item.get_item('SecurityGroup');
                var permission = item.get_item('PermissionLevel');
                var permissionLevel;

                switch (permission) {
                    case 'Contributor':
                        permissionLevel = SP.RoleType.contributor;
                        break;
                    case 'Administrator':
                        permissionLevel = SP.RoleType.administrator;
                        break;
                    default:
                        permissionLevel = SP.RoleType.reader;
                        break;
                }

                permissions.push({
                    documentType: docType,
                    securityGroup: securityGroup,
                    permission: permissionLevel
                });
            }

            log('Permisos cargados');
        },
        function (sender, args) {
            console.log('Request failed. ' + args.get_message() + '\n' + args.get_stackTrace());
        }
    );
}

function loadDocumentTypeImages() {
    var clientContext = new SP.ClientContext(_spPageContextInfo.siteAbsoluteUrl);
    var list = clientContext.get_web().get_lists().getByTitle('Imagenes tipos de documento');
    var query = SP.CamlQuery.createAllItemsQuery();
    var items = list.getItems(query);

    clientContext.load(items);

    clientContext.executeQueryAsync(
        function () {
            var listItemEnumerator = items.getEnumerator();

            while (listItemEnumerator.moveNext()) {
                var item = listItemEnumerator.get_current();
                documentTypeImages.push(
                    {
                        documentType: item.get_item('B2BContentType'),
                        image: item.get_item('Image').get_url()
                    }
                );
            }

            log('Imágenes cargadas');
        },
        function (sender, args) {
            console.log('Request failed. ' + args.get_message() + '\n' + args.get_stackTrace());
        }
    );
}

function loadProduct() {
    var clientContext = new SP.ClientContext(_spPageContextInfo.siteAbsoluteUrl);
    var list = clientContext.get_web().get_lists().getByTitle('Productos');
    var query = SP.CamlQuery.createAllItemsQuery();
    var items = list.getItems(query);

    clientContext.load(items);

    clientContext.executeQueryAsync(
        function () {
            var listItemEnumerator = items.getEnumerator();

            while (listItemEnumerator.moveNext()) {
                var item = listItemEnumerator.get_current();
                products.push({
                    id: item.get_id(),
                    family: item.get_item('ProductFamily'),
                    name: item.get_item('Title'),
                    code: item.get_item('Code')
                });
            }

            log('Productos cargados');
        },
        function (sender, args) {
            console.log('Request failed. ' + args.get_message() + '\n' + args.get_stackTrace());
        }
    );
}


/*
 *
 *  ACCIONES
 * 
*/

function setContentType(index) {
    if (index == targetDocuments.length) {
        log('Content Types asignados');
        return;
    }

    var document = targetDocuments[index];
    var jsonDoc = findDocument(document.fileName);
    if (jsonDoc != null) {
        var contentType = jsonDoc.TipoDocumento;
        if (contentType == '') {
            log('El documento ' + document.fileName + ' no tiene tipo de contenido asignado. Estableciendo tipo de contenido a Documento B2B');

            contentType = 'Documento B2B';
        }

        log('Obteniendo id del tipo de contenido ' + contentType);

        var ctypeId = getContentTypeId(contentType);

        log('ID del tipo de contenido ' + contentType + ': ' + ctypeId);

        log('Actualizando documento ' + document.fileName + '. ID: ' + document.id);

        var properties = {
            'ContentTypeId': ctypeId
        };

        updateListItem(document.id, properties, function (result, message) {
            if (result) {
                log('Documento ' + document.fileName + ' (ID: ' + document.id + ') actualizado');
            }
            else {
                log('No se ha podido actualizar el documento ' + document.fileName + ' (ID: ' + document.id + '): ' + message);
            }

            setContentType(index + 1);
        });
    }
    else {
        setContentType(index + 1);
    }
}

function setMetadata(index) {
    if (index == targetDocuments.length) {
        log('Content Types asignados');
        return;
    }

    var document = targetDocuments[index];
    var jsonDoc = findDocument(document.fileName);

    if (jsonDoc != null) {
        var contentType = jsonDoc.TipoDocumento;
        var product = jsonDoc.Producto;
        var productFamily = jsonDoc.FamiliaProducto;
        var language = jsonDoc.Idioma;
        var use = jsonDoc.Uso;
        var image = '';
        var enabler = '';

        if (use == '') {
            use = 'Externo';
        }

        if (contentType == '') {
            log('El documento ' + document.fileName + ' no tiene tipo de contenido asignado. Estableciendo tipo de contenido a Documento B2B');

            contentType = 'Documento B2B';
        }

        log('Obteniendo imagen asociada al tipo de contenido ' + contentType);

        image = getContentTypeImage(contentType);

        log('Imagen asociada al tipo de contenido ' + contentType + ': ' + image);

        log('Obteniendo enabler asociado al tipo de contenido ' + contentType);

        enabler = getEnabler(contentType);

        if (enabler != null) {
            log('Enabler asociado al tipo de contenido ' + contentType + ': ' + enabler);
        }
        else {
            log('El tipo de contenido ' + contentType + ' no tiene Enabler asociado');
        }

        log('Obteniendo título para el documento ' + document.fileName);
        var title = getTitle(jsonDoc);
        log('Título del documento ' + document.fileName + ': ' + title);

        var properties = {
            'Title': title,
            'ProductFamily': productFamily,
            'Product': product,
            'DocLanguage': language,
            'DocumentTypeImage': image,
            'DocumentUse': use
        };

        if (enabler != null) {
            properties['Enabler'] = enabler;
        }

        updateListItem(document.id, properties, function (result, message) {
            if (result) {
                log('Documento ' + document.fileName + ' (ID: ' + document.id + ') actualizado');
            }
            else {
                log('No se ha podido actualizar el documento ' + document.fileName + ' (ID: ' + document.id + '): ' + message);
            }

            setMetadata(index + 1);
        });
    }
    else {
        setMetadata(index + 1);
    }
}

function setResponsible(index) {
    if (index == targetDocuments.length) {
        log('Responsable asignado');
        return;
    }

    var document = targetDocuments[index];
    var jsonDoc = findDocument(document.fileName);

    if (jsonDoc != null) {
        var responsible = jsonDoc.Responsable;
        var author = jsonDoc.CreatedBy;
        var responsibleFinal = '';

        if (responsible != '') {
            responsibleFinal = responsible;
        }
        else {
            responsibleFinal = author;
        }

        var userLogin = "i:0#.f|membership|" + responsibleFinal;

        log('Buscando usuario ' + userLogin);

        ensureUser(userLogin, function (result) {
            var properties = {};

            if (result != null) {
                log('Usuario ' + userLogin + ' encontrado');

                var user = new SP.FieldUserValue();
                user.set_lookupId(result);

                properties["Responsible"] = user;

                updateListItem(document.id, properties, function (result, message) {
                    if (result) {
                        log('Documento ' + document.fileName + ' (ID: ' + document.id + ') actualizado');
                    }
                    else {
                        log('No se ha podido actualizar el documento ' + document.fileName + ' (ID: ' + document.id + '): ' + message);
                    }

                    setResponsible(index + 1);
                });
            }
            else {
                log('No se ha encontrado al usuario ' + userLogin);

                properties["ResponsibleText"] = jsonDoc.CreatedByName;
                properties["ResponsibleEmail"] = jsonDoc.CreatedBy;

                updateListItem(document.id, properties, function (result, message) {
                    if (result) {
                        log('Documento ' + document.fileName + ' (ID: ' + document.id + ') actualizado');
                    }
                    else {
                        log('No se ha podido actualizar el documento ' + document.fileName + ' (ID: ' + document.id + '): ' + message);
                    }

                    setResponsible(index + 1);
                });
            }
        });
    }
    else {
        setResponsible(index + 1);
    }
}

function setPermissions(index) {
    if (index == targetDocuments.length) {
        log('Permisos asignados');
        return;
    }

    var targetDocument = targetDocuments[index];
    var clientContext = SP.ClientContext.get_current();
    var list = clientContext.get_web().get_lists().getByTitle('Documentos B2B');
    var listItem = list.getItemById(targetDocument.id);

    clientContext.load(listItem);

    clientContext.executeQueryAsync(
        function () {
            var title = listItem.get_item('Title');
            if (title == null || title == '')
                title = listItem.get_item('FileLeafRef');

            var contentType;

            for (var i = 0; i < documentTypes.length; i++) {
                if (documentTypes[i].id == targetDocument.contentType.toString()) {
                    contentType = documentTypes[i].name;
                    break;
                }
            }

            if (contentType.toLowerCase() != "carpeta") {
                var currentPermissions = permissions.filter(obj => {
                    return obj.documentType === contentType;
                });

                listItem.breakRoleInheritance(false, false);

                for (var i = 0; i < currentPermissions.length; i++) {
                    log('Estableciendo nivel de permisos ' + currentPermissions[i].permission + ' al grupo ' + currentPermissions[i].securityGroup);

                    var groups = clientContext.get_web().get_siteGroups();
                    var group = groups.getByName(currentPermissions[i].securityGroup);

                    var collRoleDefinitionBinding = SP.RoleDefinitionBindingCollection.newObject(clientContext);
                    collRoleDefinitionBinding.add(clientContext.get_web().get_roleDefinitions().getByType(currentPermissions[i].permission));

                    listItem.get_roleAssignments().add(group, collRoleDefinitionBinding);

                    clientContext.load(group);
                    clientContext.load(listItem);
                }

                clientContext.executeQueryAsync(
                    function () {
                        log('Documento ' + targetDocument.fileName + ' (ID: ' + targetDocument.id + ') actualizado');

                        setPermissions(index + 1);
                    },
                    function (sender, args) {
                        log('No se han podido establecer los permisos. ' + args.get_message() + '. ' + args.get_stackTrace());
                        setPermissions(index + 1);
                    }
                );
            }
            else {
                setPermissions(index + 1);
            }
        },
        function (sender, args) {
            log('Error al cargar el documento ' + targetDocument.id + '. ' + args.get_message() + '. ' + args.get_stackTrace());
            setPermissions(index + 1);
        }
    );
}

function publish(index) {
    if (index == targetDocuments.length) {
        log('Documentos publicados');
        return;
    }

    var targetDocument = targetDocuments[index];

    if (targetDocument.url != '' && targetDocument.url != null) {
        var clientContext = SP.ClientContext.get_current();
        var file = clientContext.get_web().getFileByServerRelativeUrl(targetDocument.url);

        file.publish();

        clientContext.load(file);

        log('Publicando documento ' + targetDocument.fileName);

        clientContext.executeQueryAsync(
            function () {
                log('Documento ' + targetDocument.fileName + ' (ID: ' + targetDocument.id + ') publicado');
                publish(index + 1);
            },
            function (sender, args) {
                log('No se ha podido publicar el documento ' + targetDocument.fileName + ' (ID: ' + targetDocument.id + '). ' + args.get_message() + '. ' + args.get_stackTrace());
                publish(index + 1);
            }
        );
    }
    else {
        publish(index + 1);
    }
}

function checkMigratedDocuments() {
    var noMigratedDocuments = [];
    for (var i = 0; i < docJSON.length; i++) {
        var jsonDoc = docJSON[i];
        var targetDocument = findTargetDocument(jsonDoc.File);

        if (targetDocument == null) {
            noMigratedDocuments.push(jsonDoc);
        }
    }

    log(JSON.stringify(noMigratedDocuments));
}

function getAllDocuments() {
    var alldocs = [];
    for (var i = 0; i < docJSON.length; i++) {
        var jsonDoc = docJSON[i];
        var targetDocument = findTargetDocument(jsonDoc.File);

        if (targetDocument != null) {
            var doc = {};
            doc['Url origen'] = jsonDoc.Folder + '/' + jsonDoc.File;
            doc['Tipo de documento'] = targetDocument.cType;
            doc['Familia de producto'] = targetDocument.productFamily;
            doc['Producto'] = (targetDocument.product == null ? '' : targetDocument.product[0]);
            doc['Responsable'] = (targetDocument.responsible != null ? targetDocument.responsible : (targetDocument.responsibleText == '' || targetDocument.responsibleText == null ? targetDocument.author : ''));
            doc['Nueva Url'] = targetDocument.url;
            alldocs.push(doc);
        }
    }

    log(JSON.stringify(alldocs));
}

function deleteVersions(index) {
    if (index == targetDocuments.length) {
        log('Versiones eliminadas');
    }

    var fileName = targetDocuments[index].url;

    log('Eliminando versiones de ' + fileName);

    var ctx = SP.ClientContext.get_current();
    var versions = ctx.get_web().getFileByServerRelativeUrl(fileName).get_versions();
    versions.deleteAll();
    ctx.executeQueryAsync(
        function () {
            log('Versiones eliminadas');
            deleteVersions(index + 1);
        },
        function (sender, args) {
            log('No se han podido eliminar las versiones. ' + args.get_message());
            deleteVersions(index + 1);
        }
    );   
}

/*
 * 
 * UTILIDADES
 * 
*/

function findDocument(fileName) {
    var docFound = null;

    for (var i = 0; i < docJSON.length; i++) {
        var doc = docJSON[i];
        if (doc.File.toLowerCase() == fileName.toLowerCase()) {
            docFound = doc;
            break;
        }
    }

    return docFound;
}

function findTargetDocument(fileName) {
    var docFound = null;

    for (var i = 0; i < targetDocuments.length; i++) {
        var doc = targetDocuments[i];
        if (doc.fileName.toLowerCase().trim() == fileName.toLowerCase().trim()) {
            docFound = doc;
            break;
        }
    }

    return docFound;
}

function getContentTypeId(contentType) {
    var ctypeId = null;

    for (var i = 0; i < documentTypes.length; i++) {
        var currentContentType = documentTypes[i];
        if (currentContentType.name.toLowerCase() == contentType.toLowerCase()) {
            ctypeId = currentContentType.id;
            break;
        }
    }

    return ctypeId;
}

function getContentTypeImage(contentType) {
    var image = null;
    
    for (var i = 0; i < documentTypeImages.length; i++) {
        var currentImage = documentTypeImages[i];

        if (currentImage.documentType.toLowerCase() == contentType.toLowerCase()) {
            image = currentImage.image;
            break;
        }
    }

    return image;
}

function getEnabler(contentType) {
    var enabler = null;
    
    for (var i = 0; i < enablers.length; i++) {
        var currentEnabler = enablers[i];

        if (currentEnabler.documentType.toLowerCase() == contentType.toLowerCase()) {
            enabler = currentEnabler.enabler;
            break;
        }
    }

    return enabler;
}

function getTitle(jsonDoc) {
    var titleArray = new Array();
    var cType = jsonDoc.TipoDocumento;
    var product = jsonDoc.Producto;

    if (cType == '') {
        cType = 'Documento B2B';
    }

    titleArray.push(cType);

    if (product != '') {
        titleArray.push(product);
    }

    var title = jsonDoc.File;
    title = title.split('-');
    title = title[title.length - 1].trim();

    //quitamos la extension
    if (title.lastIndexOf('.') > -1)
        title = title.substring(0, title.lastIndexOf('.'));

    titleArray.push(title);

    return titleArray.join(' - ');
}

function ensureUser(userLogin, callback) {
    var context = SP.ClientContext.get_current();
    var newUser = context.get_web().ensureUser(userLogin);
    context.load(newUser);

    context.executeQueryAsync(
        function () {
            callback(newUser.get_id());
        },
        function (sender, args) {
            callback(null);
        }
    );
}

function updateListItem(id, properties, callback) {
    var context = SP.ClientContext.get_current();
    var list = context.get_web().get_lists().getByTitle('Documentos B2B');

    var listItem = list.getItemById(id);

    for (var propName in properties) {
        listItem.set_item(propName, properties[propName]);
    }

    listItem.update();

    context.executeQueryAsync(
        function () {
            callback(true, '');
        },
        function (sender, args) {
            var message = args.get_message() + '. ' + args.get_stackTrace();
            callback(false, message);
        }
    );
}

function log(message) {
    var text = $('#txtLog').val();
    text = message + '\n' + text;
    $('#txtLog').val(text);
}