﻿var documentTypes = new Array();
var targetDocuments = new Array();
var permissions = new Array();

$(document).ready(function () {
    $('#btnDale').on('click', function () {
        loadPermissions(function () {
            loadDocumentTypes(function () {
                loadDocuments(function () {

                    setPermissions(0);
                });
            });
        });
    });
});

function setPermissions(index) {
    if (index == targetDocuments.length) {
        alert('Fin');
        return;
    }

    var targetDocument = targetDocuments[index];
    var clientContext = SP.ClientContext.get_current();
    var list = clientContext.get_web().get_lists().getByTitle('Documentos B2B');
    var listItem = list.getItemById(targetDocument.id);

    clientContext.load(listItem);

    clientContext.executeQueryAsync(
        function () {
            var title = listItem.get_item('Title');
            if (title == null || title == '')
                title = listItem.get_item('FileLeafRef');

            var contentType;

            for (var i = 0; i < documentTypes.length; i++) {
                if (documentTypes[i].id == targetDocument.contentType.toString()) {
                    contentType = documentTypes[i].name;
                    break;
                }
            }

            if (contentType.toLowerCase() != "carpeta") {
                var currentPermissions = permissions.filter(obj => {
                    return obj.documentType === contentType;
                });

                listItem.breakRoleInheritance(false, false);

                for (var i = 0; i < currentPermissions.length; i++) {
                    var groups = clientContext.get_web().get_siteGroups();
                    var group = groups.getByName(currentPermissions[i].securityGroup);

                    var collRoleDefinitionBinding = SP.RoleDefinitionBindingCollection.newObject(clientContext);
                    collRoleDefinitionBinding.add(clientContext.get_web().get_roleDefinitions().getByType(currentPermissions[i].permission));

                    listItem.get_roleAssignments().add(group, collRoleDefinitionBinding);

                    clientContext.load(group);
                    clientContext.load(listItem);
                }

                clientContext.executeQueryAsync(
                    function () {
                        $('#documentList').prepend('<p>Documento ' + targetDocument.id + ': modificado.</p>');

                        var next = index + 1;
                        setPermissions(next);
                    },
                    function (sender, args) {
                        console.log('Request failed. ' + args.get_message() + '\n' + args.get_stackTrace());
                        ULSOnError(args.get_stackTrace(), window.location.href, 'Error occurred while updating File');
                    }
                );
            }
            else {
                var next = index + 1;
                setPermissions(next);
            }
        },
        function (sender, args) {
            console.log('Request failed. ' + args.get_message() + '\n' + args.get_stackTrace());
            ULSOnError(args.get_stackTrace(), window.location.href, 'Error occurred while updating File');
        }
    );
}

function loadDocuments(callback) {
    var clientContext = new SP.ClientContext(_spPageContextInfo.webAbsoluteUrl);
    var list = clientContext.get_web().get_lists().getByTitle('Documentos B2B');
    var query = SP.CamlQuery.createAllItemsQuery();
    var items = list.getItems(query);

    clientContext.load(items);

    clientContext.executeQueryAsync(
        function () {
            var listItemEnumerator = items.getEnumerator();

            while (listItemEnumerator.moveNext()) {
                var item = listItemEnumerator.get_current();
                targetDocuments.push({
                    id: item.get_id(),
                    contentType: item.get_item('ContentTypeId')
                });
            }


            $('#documentList').prepend('<p>Documentos cargados.</p>');
            callback();
        },
        function (sender, args) {
            console.log('Request failed. ' + args.get_message() + '\n' + args.get_stackTrace());
        }
    );
}

function loadDocumentTypes(callback) {
    var clientContext = new SP.ClientContext(_spPageContextInfo.webAbsoluteUrl);
    var list = clientContext.get_web().get_lists().getByTitle('Documentos B2B');
    var contentTypes = list.get_contentTypes();

    clientContext.load(contentTypes);

    clientContext.executeQueryAsync(
        function () {
            var results = [];
            var contentTypesEnumerator = contentTypes.getEnumerator();

            while (contentTypesEnumerator.moveNext()) {
                var cType = contentTypesEnumerator.get_current();
                documentTypes.push({
                    id: cType.get_id(),
                    name: cType.get_name()
                });
            }

            $('#documentList').prepend('<p>Document types cargados.</p>');
            callback();
        },
        function (sender, args) {
            console.log('Request failed. ' + args.get_message() + '\n' + args.get_stackTrace());
        }
    );
}

function loadPermissions(callback) {
    var clientContext = new SP.ClientContext(_spPageContextInfo.siteAbsoluteUrl);
    var list = clientContext.get_web().get_lists().getByTitle('Permisos por tipo de documento');
    var query = SP.CamlQuery.createAllItemsQuery();
    var items = list.getItems(query);

    clientContext.load(items);

    clientContext.executeQueryAsync(
        function () {
            var listItemEnumerator = items.getEnumerator();

            while (listItemEnumerator.moveNext()) {
                var item = listItemEnumerator.get_current();
                var docType = item.get_item('B2BContentType');
                var securityGroup = item.get_item('SecurityGroup');
                var permission = item.get_item('PermissionLevel');
                var permissionLevel;

                switch (permission) {
                    case 'Contributor':
                        permissionLevel = SP.RoleType.contributor;
                        break;
                    case 'Administrator':
                        permissionLevel = SP.RoleType.administrator;
                        break;
                    default:
                        permissionLevel = SP.RoleType.reader;
                        break;
                }

                permissions.push({
                    documentType: docType,
                    securityGroup: securityGroup,
                    permission: permissionLevel
                });
            }

            $('#documentList').prepend('<p>Permisos cargados.</p>');

            callback();
        },
        function (sender, args) {
            console.log('Request failed. ' + args.get_message() + '\n' + args.get_stackTrace());
        }
    );
}