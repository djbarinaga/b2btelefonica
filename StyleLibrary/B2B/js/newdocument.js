﻿var documentTypeImages = new Array();
var documentTypeDescriptions = new Array();
var documentTypeCodes = new Array();
var documentUploaded = new Array();
var products = new Array();
var productFamily = new Array();
var enablers = new Array();
var permissions = new Array();
var documentTypes = new Array();
var maxLength = 16;

$(document).ready(function () {
    SP.SOD.executeFunc('sp.js', 'SP.ClientContext', function () {
        loadEnablers();
        loadPermissions();
        loadDocTypeCodes();
        loadDocumentTypeImages();
        //loadDocumentTypeDescription();
        loadProductFamily();
        loadDocumentTypes();

        if (getUrlParam('doc') != '')
            loadDocumentInfo();
    });

    $('#btnAddLink').tooltip({
        html: true,
        placement: 'right',
        title: translate('btnAddLinkTooltip')
    });

    $('#selectProduct').on('change', function () {
        setTitle($(this));
    });

    $('#btnSelectFile').on('click', function () {
        $('#txtFile').click();
    });

    $(document).on('change', '[data-id="selectDocumentType"]', function () {
        var image = $(this).closest('.doc-form').find('[data-id="documentTypeImage"]');

        var txtDescription = $(this).closest('.doc-form').find('[data-id="txtDescription"]');
        var documentType = $(this).find('option:selected').text();

        //Asignamos la imagen
        for (var i = 0; i < documentTypeImages.length; i++) {
            var documentTypeImage = documentTypeImages[i];

            if (documentTypeImage.documentType == documentType) {
                $(image).on('load', function () {
                    $(image).fadeOut('fast', function () {
                        $(this).fadeIn();
                    });
                }).unbind('load');

                $(image).attr('src', documentTypeImage.image);

                break;
            }
        }

        //Asignamos la descripción
        for (var i = 0; i < documentTypeDescriptions.length; i++) {
            var documentTypeDescription = documentTypeDescriptions[i];
            if (documentTypeDescription.documentType == documentType) {
                $(txtDescription).val(documentTypeDescription.description);
                break;
            }
        }

        setTitle($(this));
    });

    $('#selectProductFamily').on('change', function () {
        var value = $(this).val();
        loadProduct($(this), value);
    });

    $('#btnSave').on('click', function () {
        if (!checkTitle()) {
            bootbox.alert({
                title: translate('Error'),
                message: translate('TitleRequired')
            });

            return;
        }

        if (!checkContentType()) {
            bootbox.alert({
                title: translate('Error'),
                message: translate('ContentTypeRequired')
            });

            return;
        }

        if (!checkProductFamily()) {
            bootbox.alert({
                title: translate('Error'),
                message: translate('ProductFamilyRequired')
            });

            return;
        }

        if (!checkProduct()) {
            bootbox.alert({
                title: translate('Error'),
                message: translate('ProductRequired')
            });

            return;
        }

        var titleHtml = '<ul>';

        $('[data-id="txtTitle"]').each(function () {
            titleHtml += '<li>' + $(this).val() + '</li>';
        });

        titleHtml += '</ul>';

        var confirmMessage = '<p>' + translate('SaveDocumentTitle1') + '</p>' + titleHtml + '<p>' + translate('SaveDocumentTitle2') + '</p>';
        bootbox.confirm({
            title: translate('SaveDocumentTitleTitle'),
            message: confirmMessage,
            closeButton: false,
            callback: function (result) {
                if (result) {
                    var dialog = bootbox.dialog({
                        title: translate('SavingDocInfo'),
                        message: '<div id="dialogMessage"></div>',
                        closeButton: false
                    });
                    saveMetadata(0, false, function () {
                        setPermissions(0, function () {
                            moveDocument(0, function () {
                                dialog.modal('hide');

                                bootbox.alert({
                                    message: translate('NewDocumentOk'),
                                    title: translate('Information'),
                                    closeButton: false,
                                    callback: function () {
                                        window.location.href = _spPageContextInfo.siteAbsoluteUrl;
                                    }
                                });
                            });
                        });
                    });
                }
            }
        });
    });

    $('#btnAddLinkToDocument').on('click', function () {
        if (!checkTitle()) {
            bootbox.alert({
                title: translate('Error'),
                message: translate('TitleRequired')
            });

            return;
        }

        if (!checkUrl()) {
            bootbox.alert({
                title: translate('Error'),
                message: translate('UrlRequired')
            });

            return;
        }

        if (!checkProductFamily()) {
            bootbox.alert({
                title: translate('Error'),
                message: translate('ProductFamilyRequired')
            });

            return;
        }

        if (!checkProduct()) {
            bootbox.alert({
                title: translate('Error'),
                message: translate('ProductRequired')
            });

            return;
        }

        var titleHtml = '<ul>';

        titleHtml += '<li>' + $('#txtTitle').val() + '</li>';

        titleHtml += '</ul>';

        var confirmMessage = '<p>' + translate('SaveDocumentTitle1') + '</p>' + titleHtml + '<p>' + translate('SaveDocumentTitle2') + '</p>';

        bootbox.confirm({
            title: translate('SaveDocumentTitleTitle'),
            message: confirmMessage,
            closeButton: false,
            callback: function (result) {
                if (result)
                    addLinkToDocument();
            }
        });
    });

    $('#txtFile').on('change', function () {
        var fileInput = document.getElementById('txtFile');
        var files = fileInput.files;

        $(dropZone).removeClass('hover');
        $(dropZone).addClass('uploading');
        $('#drop-file').hide();
        $('#file-info').append('<p class="file-info-title">' + translate('UploadingFiles') + '</p>');
        $('#file-info').fadeIn();

        var uploadFile = function (index) {

            if (index == files.length) {
                $('#drag-div-container').fadeOut('fast', function () {
                    $('#document-metadata').fadeIn();
                });
                loadForms();
                return;
            }

            var file = files[index];
            var fileName = file.name;
            var fileSize = file.size;
            var reader = new FileReader();

            reader.onload = function (e2) {
                var fileId = 'file_' + index;
                upload(fileName, e2.target.result, function (result) {
                    if (result != null) {
                        documentUrl = result.d.ServerRelativeUrl;
                        documentUploaded.push(documentUrl);

                        $('#' + fileId).find('img').remove();
                        $('#' + fileId).append('<span class="icon-checkmark float-right"></span>');

                        var next = index + 1;
                        uploadFile(next);
                    }
                });
            };

            reader.readAsArrayBuffer(file);
        };

        for (var i = 0; i < files.length; i++) {
            var file = files[i];
            var fileName = file.name;
            fileName = fileName.substring(0, maxLength);
            if (file.name.length > maxLength)
                fileName += '...';

            var fileNameSplit = file.name.split('.');
            var fileExtension = fileNameSplit[fileNameSplit.length - 1];
            var icon = getDocumentIcon(fileExtension);
            var fileSize = bytesToSize(file.size);

            $('#file-info').append('<div class="col-3 file-info-container">' + icon + '<div class="file-info-caption"><p id="file_' + i + '">' + fileName + '<img src="/_layouts/images/gears_anv4.gif" style="width:16px;float:right;"/></p><p>' + fileSize + '</p></div></div>');
        }

        uploadFile(0);
    });

    function upload(filename, fileData, callback) {
        $.ajax({
            url: _spPageContextInfo.webAbsoluteUrl + "/_api/web/getfolderbyserverrelativeurl('Documentos B2B')/files/add(url='" + filename + "', overwrite=true)",
            method: "POST",
            binaryStringRequestBody: true,
            data: fileData,
            contentLength: fileData.length,
            headers: {
                "accept": "application/json; odata=verbose",
                "X-RequestDigest": $("#__REQUESTDIGEST").val(),
                "content-type": "application/json; odata=verbose"
            },
            async: true,
            processData: false,
            success: function (data) {
                if (callback != null)
                    callback(data);
            },
            error: function onQueryErrorAQ(xhr, ajaxOptions, thrownError) {
                console.log('Error:\n' + xhr.status + '\n' + thrownError + '\n' + xhr.responseText);
                if (callback != null)
                    callback(null);
            }
        });
    }

    var dropZone = document.getElementById('drag-div');

    if (dropZone != null) {
        dropZone.addEventListener('dragover', function (e) {
            $(dropZone).addClass('hover');
            e.stopPropagation();
            e.preventDefault();
            e.dataTransfer.dropEffect = 'copy';
        });

        dropZone.addEventListener('dragleave', function (e) {
            $(dropZone).removeClass('hover');
            e.stopPropagation();
            e.preventDefault();
        });

        dropZone.addEventListener('drop', function (e) {
            $(dropZone).removeClass('hover');
            $(dropZone).addClass('uploading');
            e.stopPropagation();
            e.preventDefault();
            $('#drop-file').hide();
            //$('#file-info').css('text-align', 'left');
            //$('#file-info').css('padding-left', '150px');
            $('#file-info').append('<p class="file-info-title">' + translate('UploadingFiles') + '</p>');
            $('#file-info').fadeIn();
            var files = e.dataTransfer.files;

            var uploadFile = function (index) {

                if (index == files.length) {
                    $('#drag-div-container').fadeOut('fast', function () {
                        $('#document-metadata').fadeIn();
                    });
                    loadForms();
                    return;
                }

                var file = files[index];
                var fileName = file.name;
                var fileSize = file.size;
                var reader = new FileReader();

                reader.onload = function (e2) {
                    var fileId = 'file_' + index;
                    //$('#file-info').append('<p id="' + fileId + '">' + fileName + ': ' + fileSize + ' bytes&nbsp;<img src="/_layouts/images/gears_anv4.gif" style="width:16px"/></p>');
                    upload(fileName, e2.target.result, function (result) {
                        if (result != null) {
                            documentUrl = result.d.ServerRelativeUrl;
                            documentUploaded.push(documentUrl);

                            $('#' + fileId).find('img').remove();
                            $('#' + fileId).append('<span class="icon-checkmark float-right"></span>');

                            var next = index + 1;
                            uploadFile(next);
                        }
                    });
                };

                reader.readAsArrayBuffer(file);
            };

            for (var i = 0; i < files.length; i++) {
                var file = files[i];
                var fileName = file.name;
                fileName = fileName.substring(0, maxLength);
                if (file.name.length > maxLength)
                    fileName += '...';

                var fileNameSplit = file.name.split('.');
                var fileExtension = fileNameSplit[fileNameSplit.length - 1];
                var icon = getDocumentIcon(fileExtension);
                var fileSize = bytesToSize(file.size);

                $('#file-info').append('<div class="col-3 file-info-container">' + icon + '<div class="file-info-caption"><p id="file_' + i + '">' + fileName + '<img src="/_layouts/images/gears_anv4.gif" style="width:16px;float:right;"/></p><p>' + fileSize + '</p></div></div>');
            }

            uploadFile(0);

        });
    }

    function addLinkToDocument() {
        
        var redirectPageString = getRedirectPage($('#txtUrl').val());
        var title = $('#txtTitle').val() + '.aspx';

        var dialog = bootbox.dialog({
            title: translate('SavingDocInfo'),
            message: '<div id="dialogMessage">' + translate('CreatingLinkToDocument') + '</div>',
            closeButton: false
        });

        upload(title, redirectPageString, function (result) {
            documentUploaded.push(result.d.ServerRelativeUrl);
            saveMetadata(0, true, function () {
                setPermissions(0, function () {
                    moveDocument(0, function () {
                        dialog.modal('hide');

                        bootbox.alert({
                            message: translate('NewDocumentOk'),
                            title: translate('Information'),
                            closeButton: false,
                            callback: function () {
                                window.location.href = _spPageContextInfo.siteAbsoluteUrl;
                            }
                        });
                    });
                });
            });
        });
    }

    function getRedirectPage(url) {
        var html = '';
        html += "<%@ Assembly Name='Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c' %>";
        html += "<%@ Register TagPrefix='SharePoint' Namespace='Microsoft.SharePoint.WebControls' Assembly='Microsoft.SharePoint' %>";
        html += "<%@ Import Namespace='System.IO' %>";
        html += "<%@ Import Namespace='Microsoft.SharePoint' %>";
        html += "<%@ Import Namespace='Microsoft.SharePoint.Utilities' %>";
        html += "<%@ Import Namespace='Microsoft.SharePoint.WebControls' %>";
        html += "<html xmlns:mso=\"urn:schemas-microsoft-com:office:office\" xmlns:msdt=\"uuid:C2F41010-65B3-11d1-A29F-00AA00C14882\">";
        html += "<head>";
        html += "<meta name=\"WebPartPageExpansion\" content=\"full\" /> <meta name='progid' content='SharePoint.Link' /> ";
        html += "<!--[if gte mso 9]><SharePoint:CTFieldRefs runat=server Prefix=\"mso:\" FieldList=\"FileLeafRef,URL\"><xml>";
        html += "<mso:CustomDocumentProperties>";
        html += "<mso:ContentTypeId msdt:dt=\"string\">0x01010A00DC3917D9FAD55147B56FF78B40FF3ABB</mso:ContentTypeId>";
        html += "<mso:IconOverlay msdt:dt=\"string\">|docx|linkoverlay.gif</mso:IconOverlay>";
        html += "<mso:URL msdt:dt=\"string\">" + url + ", " + url + "</mso:URL>";
        html += "</mso:CustomDocumentProperties>";
        html += "</xml></SharePoint:CTFieldRefs><![endif]-->";
        html += "</head>";
        html += "<body>";
        html += "<form id='Form1' runat='server'>";
        html += "<SharePoint:UrlRedirector id='Redirector1' runat='server' />";
        html += "</form>";
        html += "</body>";
        html += "</html>";
        return html;
    }

    function stringToBytes(str) {
        var ch, st, re = [];
        for (var i = 0; i < str.length; i++) {
            ch = str.charCodeAt(i);  // get char 
            st = [];                 // set up "stack"
            do {
                st.push(ch & 0xFF);  // push byte to stack
                ch = ch >> 8;          // shift value down by 1 byte
            }
            while (ch);
            // add stack contents to result
            // done because chars have "wrong" endianness
            re = re.concat(st.reverse());
        }
        // return an array of bytes
        return re;
    }
});

function loadDocumentInfo() {
    var dialog = bootbox.dialog({
        title: translate('Information'),
        message: '<div class="spinner-border text-info" role="status"><span class="sr-only">Loading...</span></div> ' + translate('GettingInfo') + '...',
        closeButton: false
    });

    var interval = setInterval(function () {
        if ($('#selectProductFamily option').length > 0) {
            clearInterval(interval);
            var docId = getUrlParam('docid');
            var docUrl = getUrlParam('doc');
            var clientContext = SP.ClientContext.get_current();
            var list = clientContext.get_web().get_lists().getByTitle('Documentos B2B');
            var item = list.getItemById(docId);

            clientContext.load(item);


            clientContext.executeQueryAsync(
                function () {
                    var listItem = item;

                    var productFamily = listItem.get_item('ProductFamily');
                    var product = listItem.get_item('Product');
                    var language = listItem.get_item('DocLanguage');
                    var description = listItem.get_item('DocumentDescription');
                    var title = listItem.get_item('URL').get_description();
                    var url = listItem.get_item('URL').get_url();

                    checkOutUser = listItem.get_item('CheckoutUser');

                    if (checkOutUser != null)
                        checkOutUser = checkOutUser.get_lookupId();

                    if (checkOutUser != null && checkOutUser != _spPageContextInfo.userId) {
                        dialog.modal('hide');
                        bootbox.alert({
                            title: 'Información',
                            message: 'El documento está desprotegido por ' + listItem.get_item('CheckoutUser').get_lookupValue(),
                            closeButton: false,
                            callback: function () {
                                window.history.go(-1);
                            }
                        });
                    }
                    else {
                        $('#selectProductFamily').val(productFamily);
                        $('#txtTitle').val(title);
                        $('#txtUrl').val(url);
                        $('#txtDescription').val(description);
                        $('#selectLanguage').val(language);
                        loadProduct(null, productFamily, product);

                        dialog.modal('hide');
                    }
                },
                function (sender, args) {
                    console.log('Request failed. ' + args.get_message() + '\n' + args.get_stackTrace());
                    ULSOnError(args.get_stackTrace(), window.location.href, 'Error occurred while updating File');
                }
            );
        }
    }, 1000);
}

function loadForms() {
    for (var i = 0; i < documentUploaded.length; i++) {
        var docId = "doc-" + i;
        var docUrlSplit = documentUploaded[i].split('/');
        var docName = docUrlSplit[docUrlSplit.length - 1];

        var form = $('#accordion div.border:first').clone();
        $(form).attr('data-url', documentUploaded[i]);
        $(form).find('[data-id="doc-url"]').attr('data-target', '#' + docId);
        $(form).find('[data-id="doc-url"]').append('&nbsp;' + docName);
        $(form).find('[data-id="doc-collapse"]').attr('id', docId);
        if (i == 0) {
            $(form).find('[data-id="doc-collapse"]').addClass('show');
        }
        $(form).find('[data-id="txtTitle"]').val(docName);

        $('#accordion').append(form);

        $(form).find('[data-id="selectProduct"]').each(function () {
            $(this).on('change', function () {
                setTitle($(this));
            });
        });

        $(form).find('[data-id="selectProductFamily"]').each(function () {
            $(this).on('change', function () {
                var value = $(this).val();
                loadProduct($(this), value);
            });
        });
    }

    $('#accordion div.border:first').remove();
}

function setTitle(element) {
    var title = '';
    var productCode = '';
    var documentTypeCode = '';
    var titleArray = new Array();

    if ($('#txtTitle').length == 0) {
        var selectedDocumentType = $(element).closest('.doc-form').find('[data-id="selectDocumentType"] option:selected').text();
        var selectedProduct = $(element).closest('.doc-form').find('[data-id="selectProduct"]').val().split('-')[1];
        
        for (var i = 0; i < documentTypeCodes.length; i++) {
            var currentDocumentType = documentTypeCodes[i];
            if (currentDocumentType.name == selectedDocumentType) {
                documentTypeCode = currentDocumentType.code;
                titleArray.push(documentTypeCode);
                break;
            }
        }


        for (var i = 0; i < products.length; i++) {
            var currentProduct = products[i];
            if (currentProduct.id == selectedProduct) {
                productCode = currentProduct.code;
                titleArray.push(productCode);
                break;
            }
        }

        title = $(element).closest('.doc-form').find('[data-id="txtTitle"]').val();

        title = title.split('-');
        title = title[title.length - 1].trim();

        //quitamos la extension
        if (title.lastIndexOf('.') > -1)
            title = title.substring(0, title.lastIndexOf('.'));

        titleArray.push(title);

        title = titleArray.join(' - ');

        $(element).closest('.doc-form').find('[data-id="txtTitle"]').val(title);
    }
    else {
        for (var i = 0; i < products.length; i++) {
            var currentProduct = products[i];
            if (currentProduct.id == $('#selectProduct').val().split('-')[1]) {
                productCode = currentProduct.code;
                titleArray.push(productCode);
                break;
            }
        }

        title = $('#txtTitle').val();
        title = title.split('-');
        title = title[title.length - 1].trim();

        titleArray.push(title);

        title = titleArray.join(' - ');

        $('#txtTitle').val(title);
    }
}

function loadDocumentTypes() {
    var clientContext = new SP.ClientContext(_spPageContextInfo.webAbsoluteUrl);
    var list = clientContext.get_web().get_lists().getByTitle('Documentos B2B');
    var contentTypes = list.get_contentTypes();

    clientContext.load(contentTypes);

    clientContext.executeQueryAsync(
        function () {
            var results = [];
            var contentTypesEnumerator = contentTypes.getEnumerator();

            while (contentTypesEnumerator.moveNext()) {
                var cType = contentTypesEnumerator.get_current();
                results.push({
                    id: cType.get_id(),
                    name: cType.get_name()
                });
            }

            results.sort(function (a, b) {
                var textA = a.name.toUpperCase();
                var textB = b.name.toUpperCase();
                return (textA < textB) ? -1 : (textA > textB) ? 1 : 0;
            });

            documentTypes = results;

            $('[data-id="selectDocumentType"]').each(function () {
                $(this).append('<option value="-1">' + translate('SelectDocumentType') + '</option>');
                for (var i = 0; i < results.length; i++) {
                    var result = results[i];
                    if (result.id.toString().indexOf('0x0120') == -1)
                        $(this).append('<option value="' + result.id.toString() + '">' + result.name + '</option>');
                }

                $(this).change();
            });
        },
        function (sender, args) {
            console.log('Request failed. ' + args.get_message() + '\n' + args.get_stackTrace());
        }
    );
}

function loadDocTypeCodes() {
    var clientContext = new SP.ClientContext(_spPageContextInfo.siteAbsoluteUrl);
    var list = clientContext.get_web().get_lists().getByTitle('Codigos tipos de documento');
    var query = SP.CamlQuery.createAllItemsQuery();
    var items = list.getItems(query);

    clientContext.load(items);

    clientContext.executeQueryAsync(
        function () {
            var listItemEnumerator = items.getEnumerator();

            while (listItemEnumerator.moveNext()) {
                var item = listItemEnumerator.get_current();
                documentTypeCodes.push({
                    name: item.get_item('B2BContentType'),
                    code: item.get_item('Code')
                });
            }
        },
        function (sender, args) {
            console.log('Request failed. ' + args.get_message() + '\n' + args.get_stackTrace());
        }
    );
}

function loadProductFamily() {
    var clientContext = new SP.ClientContext(_spPageContextInfo.siteAbsoluteUrl);
    var field = clientContext.castTo(clientContext.get_web().get_availableFields().getByInternalNameOrTitle('ProductFamily'), SP.FieldChoice);

    clientContext.load(field);

    clientContext.executeQueryAsync(
        function () {
            var oldName = decodeURIComponent(getUrlParam('ctname'));
            var newName = $('#txtTitle').val();

            var choices = field.get_choices();

            for (var i = 0; i < choices.length; i++) {
                productFamily.push({
                    name: choices[i]
                });
            }

            var selector = '[data-id="selectProductFamily"]';

            if ($('#selectProductFamily').length)
                selector = '#selectProductFamily';

            $(selector).each(function () {
                $(this).append('<option value="-1">' + translate('SelectProductFamily') + '</option>');
                for (var i = 0; i < productFamily.length; i++) {
                    var result = productFamily[i];
                    $(this).append('<option value="' + result.name + '">' + result.name + '</option>');
                }
            });

            $(selector).change();
        },
        function (sender, args) {
            callback(false);
            console.log('Request failed. ' + args.get_message() + '\n' + args.get_stackTrace());
        }
    );
}

function loadProduct(element, family, product) {
    var clientContext = new SP.ClientContext(_spPageContextInfo.siteAbsoluteUrl);
    var list = clientContext.get_web().get_lists().getByTitle('Productos');
    var query = SP.CamlQuery.createAllItemsQuery();
    var items = list.getItems(query);

    clientContext.load(items);

    clientContext.executeQueryAsync(
        function () {
            var listItemEnumerator = items.getEnumerator();
            products = new Array();

            while (listItemEnumerator.moveNext()) {
                var item = listItemEnumerator.get_current();
                if (family != null) {
                    if (item.get_item('ProductFamily') == family) {
                        products.push({
                            id: item.get_id(),
                            family: item.get_item('ProductFamily'),
                            name: item.get_item('Product')[0],
                            code: item.get_item('Code')
                        });
                    }
                }
                else {
                    products.push({
                        id: item.get_id(),
                        family: item.get_item('ProductFamily'),
                        name: item.get_item('Product')[0],
                        code: item.get_item('Code')
                    });
                }
            }

            products.sort(function (a, b) {
                var textA = a.name.toUpperCase();
                var textB = b.name.toUpperCase();
                return (textA < textB) ? -1 : (textA > textB) ? 1 : 0;
            });

            var selectProduct = $(element).closest('.doc-form').find('[data-id="selectProduct"]');
            if (selectProduct.length == 0)
                selectProduct = $('#selectProduct');

            selectProduct.empty();

            $(selectProduct).append('<option value="-1">' + translate('SelectProduct') + '</option>');
            for (var i = 0; i < products.length; i++) {
                var result = products[i];
                var html = '<option value="' + result.family + '-' + result.id + '"';

                if (product != null && product == result.name)
                    html += ' selected';

                html += '>' + result.name + '</option>';

                $(selectProduct).append(html);
            }

            if (family == null)
                loadDocumentInfo();
        },
        function (sender, args) {
            console.log('Request failed. ' + args.get_message() + '\n' + args.get_stackTrace());
        }
    );
}


function saveMetadata(index, isLink, callback) {
    if (index == documentUploaded.length) {
        callback();
        return;
    }

    var documentUrl = documentUploaded[index];
    var docForm = $('[data-url="' + documentUrl + '"]');
    var title = $(docForm).find('[data-id="txtTitle"]').val();

    //$('#dialogMessage').append('<p>' + title + ' <div class="spinner-border text-info" role="status"><span class="sr-only">Loading...</span></div></p>');

    var clientContext = SP.ClientContext.get_current();
    var file = clientContext.get_web().getFileByServerRelativeUrl(documentUrl);
    clientContext.load(file, 'ListItemAllFields');

    clientContext.executeQueryAsync(
        function () {
            var enabler = getSelectedEnabler($(docForm).find('[data-id="selectDocumentType"] option:selected').text());

            var fileName;

            var listItem = file.get_listItemAllFields();
            fileName = listItem.get_item('FileLeafRef');
            if (!isLink) {
                var docUse = $(docForm).find('[data-id="chkInternalUse"]').is(':checked');
                listItem.set_item('ContentTypeId', $(docForm).find('[data-id="selectDocumentType"]').val());
                listItem.set_item("DocumentUse", (docUse ? 'Interno' : 'Externo'));
                listItem.set_item('DocumentTypeImage', $(docForm).find('[data-id="documentTypeImage"]').attr('src'));
                listItem.set_item("ProductFamily", $(docForm).find('[data-id="selectProductFamily"] option:selected').text());
                listItem.set_item("Product", $(docForm).find('[data-id="selectProduct"] option:selected').text());
                listItem.set_item("DocumentDescription", $(docForm).find('[data-id="txtDescription"]').val());
                listItem.set_item("Title", $(docForm).find('[data-id="txtTitle"]').val());
                listItem.set_item("DocLanguage", $(docForm).find('[data-id="selectLanguage"]').val());

                if (enabler != null)
                    listItem.set_item("Enabler", enabler);
            }
            else {
                var urlValue = new SP.FieldUrlValue();

                urlValue.set_url($('#txtUrl').val());
                urlValue.set_description($('#txtTitle').val());

                listItem.set_item("URL", urlValue);
                listItem.set_item("ProductFamily", $('#selectProductFamily option:selected').text());
                listItem.set_item("Product", $('#selectProduct option:selected').text());
                listItem.set_item("DocumentDescription", $('#txtDescription').val());
                listItem.set_item("Title", $('#txtTitle').val());
                listItem.set_item("DocLanguage", $('#selectLanguage').val());
                listItem.set_item('DocumentTypeImage', 'https://telefonicacorp.sharepoint.com/sites/B2BFinder.OCEWU/Style%20Library/B2B/images/linktodocument.jpg');
            }

            listItem.update();
            
            clientContext.executeQueryAsync(
                function () {
                    $('#dialogMessage img').remove();
                    $('#dialogMessage p').last().append('<span class="icon-checkmark"></span>');
                    var next = index + 1;
                    saveMetadata(next, false, callback);
                },
                function (sender, args) {
                    console.log('Request failed. ' + args.get_message() + '\n' + args.get_stackTrace());
                    ULSOnError(args.get_stackTrace(), window.location.href, 'Error occurred while updating File');
                }
            );
        },
        function (sender, args) {
            console.log('Request failed. ' + args.get_message() + '\n' + args.get_stackTrace());
            ULSOnError(args.get_stackTrace(), window.location.href, 'Error occurred while updating File');
        }
    );
}

function setPermissions(index, callback) {
    if (index == documentUploaded.length) {
        callback();
        return;
    }

    $('#dialogMessage').empty();

    var documentUrl = documentUploaded[index];
    var clientContext = SP.ClientContext.get_current();
    var file = clientContext.get_web().getFileByServerRelativeUrl(documentUrl);
    clientContext.load(file, 'ListItemAllFields');

    clientContext.executeQueryAsync(
        function () {
            var listItem = file.get_listItemAllFields();

            var title = listItem.get_item('Title');
            if (title == null || title == '')
                title = listItem.get_item('FileLeafRef');

            $('#dialogMessage').append('<div class="spinner-border text-info" role="status"><span class="sr-only">Loading...</span></div>' + title);

            var contentTypeId = listItem.get_item('ContentTypeId').toString();
            var contentType;

            for (var i = 0; i < documentTypes.length; i++) {
                if (documentTypes[i].id == contentTypeId) {
                    contentType = documentTypes[i].name;
                    break;
                }
            }

            var currentPermissions = permissions.filter(function(obj) {
                return obj.documentType === contentType;
            });

            listItem.breakRoleInheritance(false);

            for (var i = 0; i < currentPermissions.length; i++) {
                var groups = clientContext.get_web().get_siteGroups();
                var group = groups.getByName(currentPermissions[i].securityGroup);

                var collRoleDefinitionBinding = SP.RoleDefinitionBindingCollection.newObject(clientContext);
                collRoleDefinitionBinding.add(clientContext.get_web().get_roleDefinitions().getByType(currentPermissions[i].permission));

                listItem.get_roleAssignments().add(group, collRoleDefinitionBinding);

                clientContext.load(group);
                clientContext.load(listItem);
            }

            clientContext.executeQueryAsync(
                function () {
                    var next = index + 1;
                    setPermissions(next, callback);
                },
                function (sender, args) {
                    console.log('Request failed. ' + args.get_message() + '\n' + args.get_stackTrace());
                    ULSOnError(args.get_stackTrace(), window.location.href, 'Error occurred while updating File');
                }
            );
        },
        function (sender, args) {
            console.log('Request failed. ' + args.get_message() + '\n' + args.get_stackTrace());
            ULSOnError(args.get_stackTrace(), window.location.href, 'Error occurred while updating File');
        }
    );
}

function moveDocument(index, callback) {
    if (index == documentUploaded.length) {
        callback();
        return;
    }

    $('#dialogMessage').empty();

    var documentUrl = documentUploaded[index];
    var clientContext = SP.ClientContext.get_current();
    var file = clientContext.get_web().getFileByServerRelativeUrl(documentUrl);
    clientContext.load(file, 'ListItemAllFields');

    clientContext.executeQueryAsync(
        function () {
            var listItem = file.get_listItemAllFields();
            var title = listItem.get_item('Title');
            if (title == null || title == '')
                title = listItem.get_item('FileLeafRef');

            var list = clientContext.get_web().get_lists().getByTitle("Documentos B2B");

            $('#dialogMessage').append('<div class="spinner-border text-info" role="status"><span class="sr-only">Loading...</span></div>' + translate('EndProcess'));

            var fileName = listItem.get_item('FileLeafRef');
            var productFamily = listItem.get_item('ProductFamily');
            var product = listItem.get_item('Product');
            var contentTypeId = listItem.get_item('ContentTypeId').toString();
            var contentType;

            for (var i = 0; i < documentTypes.length; i++) {
                if (documentTypes[i].id == contentTypeId) {
                    contentType = documentTypes[i].name;
                    break;
                }
            }

            var folders = [
                productFamily.replace('/', '_'),
                product,
                contentType
            ];

            var destUrl = folders.join('/');
            createFolder(destUrl, function (url) {
                var newUrl = 'Documentos B2B' + '/' + destUrl + '/' + fileName;

                file.moveTo(newUrl, SP.MoveOperations.overwrite);
                file.publish();

                clientContext.executeQueryAsync(
                    function () {
                        var next = index + 1;
                        moveDocument(next, callback);
                    },
                    function (sender, args) {
                        console.log('Request failed. ' + args.get_message() + '\n' + args.get_stackTrace());
                        ULSOnError(args.get_stackTrace(), window.location.href, 'Error occurred while updating File');
                    }
                );
            });
        },
        function (sender, args) {
            console.log('Request failed. ' + args.get_message() + '\n' + args.get_stackTrace());
            ULSOnError(args.get_stackTrace(), window.location.href, 'Error occurred while updating File');
        }
    );
}

function createFolder(folderUrl, success, error) {
    var ctx = SP.ClientContext.get_current();
    var list = ctx.get_web().get_lists().getByTitle("Documentos B2B");

    var createFolderInternal = function (parentFolder, folderUrl, success, error) {
        var ctx = parentFolder.get_context();
        var folderNames = folderUrl.split('/');
        var folderName = folderNames[0];
        var curFolder = parentFolder.get_folders().add(folderName);
        ctx.load(curFolder);
        ctx.executeQueryAsync(
            function () {
                if (folderNames.length > 1) {
                    var subFolderUrl = folderNames.slice(1, folderNames.length).join('/');
                    createFolderInternal(curFolder, subFolderUrl, success, error);
                }
                success(curFolder);
            },
            error);
    };

    createFolderInternal(list.get_rootFolder(), folderUrl, success, error);
};

function getDocumentMetadata(documentUrl) {
    var clientContext = SP.ClientContext.get_current();
    var file = clientContext.get_web().getFileByServerRelativeUrl(documentUrl);
    clientContext.load(file, 'ListItemAllFields');

    clientContext.executeQueryAsync(
        function () {
            var listItem = file.get_listItemAllFields();

            var title = listItem.get_item('Title');
            if (title == null || title == '')
                title = listItem.get_item('FileLeafRef');

            var contentTypeId = listItem.get_item('ContentTypeId');
            var productFamily = listItem.get_item('ProductFamily');
            var product = listItem.get_item('Product');
            var documentUse = listItem.get_item('DocumentUse');
            var description = listItem.get_item('DocumentDescription');

            $('#txtTitle').val(title);
            $('#txtDescription').val(description);
            $('#selectDocumentType option:contains(' + contentTypeId + ')').attr('selected', 'selected');
            $('#selectProductFamily option:contains(' + productFamily + ')').attr('selected', 'selected');
            $('#selectProduct option:contains(' + product + ')').attr('selected', 'selected');
        },
        function (sender, args) {
            console.log('Request failed. ' + args.get_message() + '\n' + args.get_stackTrace());
            ULSOnError(args.get_stackTrace(), window.location.href, 'Error occurred while updating File');
        }
    );
}

function loadDocumentTypeImages() {
    var clientContext = new SP.ClientContext(_spPageContextInfo.siteAbsoluteUrl);
    var list = clientContext.get_web().get_lists().getByTitle('Imagenes tipos de documento');
    var query = SP.CamlQuery.createAllItemsQuery();
    var items = list.getItems(query);

    clientContext.load(items);

    clientContext.executeQueryAsync(
        function () {
            var listItemEnumerator = items.getEnumerator();

            while (listItemEnumerator.moveNext()) {
                var item = listItemEnumerator.get_current();
                documentTypeImages.push(
                    {
                        documentType: item.get_item('B2BContentType'),
                        image: item.get_item('Image').get_url()
                    }
                );
            }
        },
        function (sender, args) {
            console.log('Request failed. ' + args.get_message() + '\n' + args.get_stackTrace());
        }
    );
}

function loadDocumentTypeDescription() {
    var clientContext = new SP.ClientContext(_spPageContextInfo.siteAbsoluteUrl);
    var list = clientContext.get_web().get_lists().getByTitle('Descripcion tipos de documento');
    var query = SP.CamlQuery.createAllItemsQuery();
    var items = list.getItems(query);

    clientContext.load(items);

    clientContext.executeQueryAsync(
        function () {
            var listItemEnumerator = items.getEnumerator();

            while (listItemEnumerator.moveNext()) {
                var item = listItemEnumerator.get_current();
                documentTypeDescriptions.push(
                    {
                        documentType: item.get_item('Title'),
                        description: item.get_item('DocumentTypeDescription')
                    }
                );
            }
        },
        function (sender, args) {
            console.log('Request failed. ' + args.get_message() + '\n' + args.get_stackTrace());
        }
    );
}

function loadEnablers() {
    var clientContext = new SP.ClientContext(_spPageContextInfo.siteAbsoluteUrl);
    var list = clientContext.get_web().get_lists().getByTitle('Enablers');
    var query = SP.CamlQuery.createAllItemsQuery();
    var items = list.getItems(query);

    clientContext.load(items);

    clientContext.executeQueryAsync(
        function () {
            var listItemEnumerator = items.getEnumerator();

            while (listItemEnumerator.moveNext()) {
                var item = listItemEnumerator.get_current();
                var docType = item.get_item('B2BContentType');
                var enabler = item.get_item('Enabler');

                enablers.push({
                    documentType: docType,
                    enablers: enabler
                });
            }
        },
        function (sender, args) {
            console.log('Request failed. ' + args.get_message() + '\n' + args.get_stackTrace());
        }
    );
}

function loadPermissions() {
    var clientContext = new SP.ClientContext(_spPageContextInfo.siteAbsoluteUrl);
    var list = clientContext.get_web().get_lists().getByTitle('Permisos por tipo de documento');
    var query = SP.CamlQuery.createAllItemsQuery();
    var items = list.getItems(query);

    clientContext.load(items);

    clientContext.executeQueryAsync(
        function () {
            var listItemEnumerator = items.getEnumerator();

            while (listItemEnumerator.moveNext()) {
                var item = listItemEnumerator.get_current();
                var docType = item.get_item('B2BContentType');
                var securityGroup = item.get_item('SecurityGroup');
                var permission = item.get_item('PermissionLevel');
                var permissionLevel;

                switch (permission.toLowerCase()) {
                    case 'contributor':
                        permissionLevel = SP.RoleType.contributor;
                        break;
                    case 'administrator':
                        permissionLevel = SP.RoleType.administrator;
                        break;
                    default:
                        permissionLevel = SP.RoleType.reader;
                        break;
                }

                permissions.push({
                    documentType: docType,
                    securityGroup: securityGroup,
                    permission: permissionLevel
                });
            }
        },
        function (sender, args) {
            console.log('Request failed. ' + args.get_message() + '\n' + args.get_stackTrace());
        }
    );
}

function getSelectedEnabler(docType) {
    var value;
    for (var i = 0; i < enablers.length; i++) {
        var enabler = enablers[i];

        if (enabler.documentType == docType) {
            value = enabler.enablers.join(';#');
            break;
        }
    }

    return value;
}

function checkTitle() {
    var selected = true;
    $('[data-id="txtTitle"]').each(function () {
        if ($(this).val() == '') {
            selected = false;
            return false;
        }
    });

    if ($('#txtTitle').val() == '')
        selected = false;

    return selected;
}

function checkUrl() {
    if ($('#txtUrl').val() == '')
        return false;

    return true;
}

function checkContentType() {
    var selected = true;
    $('[data-id="selectDocumentType"]').each(function () {
        if ($(this).val() == '-1') {
            selected = false;
            return false;
        }
    });

    return selected;
}

function checkProductFamily() {
    var selected = true;

    $('[data-id="selectProductFamily"]').each(function () {
        if ($(this).val() == '-1') {
            selected = false;
            return false;
        }
    });

    if ($('#selectProductFamily').val() == '-1')
        selected = false;

    return selected;
}

function checkProduct() {
    var selected = true;

    $('[data-id="selectProduct"]').each(function () {
        if ($(this).val() == '-1') {
            selected = false;
            return false;
        }
    });

    if ($('#selectProduct').val() == '-1')
        selected = false;

    return selected;
}

function resolveUserEmail(element) {
    var responsibleEmail = $(element).val();
    var url = "https://telefonicacorp.sharepoint.com/sites/B2BFinder.OCEWU/_api/web/siteusers?$filter=Email eq '" + responsibleEmail + "'";

    var $ajax = $.ajax({
        url: url,
        type: "GET",
        dataType: "json",
        headers: {
            Accept: "application/json;odata=verbose"
        }
    });

    $ajax.done(function (data, textStatus, jqXHR) {
        console.log(data);
        var results = data.d.results;
        if (results.length > 0) {
            var userId = results[0].Id;
            $(element).next().val(userId);
        }
        else {
            bootbox.alert({
                title: translate('Error'),
                message: translate('UserNotFound'),
                closeButton: false
            });
        }
    });
}