﻿var documentTypeImages = new Array();
var documentTypeDescriptions = new Array();
var documentTypeCodes = new Array();
var documentUploaded = new Array();
var products = new Array();
var productFamily = new Array();
var enablers = new Array();
var permissions = new Array();
var documentTypes = new Array();
var fileName = '';
var fileUrl;
var checkOutUser;

$(document).ready(function () {
    SP.SOD.executeFunc('sp.js', 'SP.ClientContext', function () {
        var dialog = bootbox.dialog({
            title: translate('Information'),
            message: '<div class="spinner-border text-info" role="status"><span class="sr-only">Loading...</span></div> ' + translate('GettingInfo') + '...',
            closeButton: false
        });

        loadProductFamily(function() {
            loadProduct(null, null, function() {
                loadDocumentTypes(function() {
                    loadDocumentTypeImages(function() {
                        loadPermissions(function() {
                            loadEnablers(function () {
                                loadDocTypeCodes(function () {
                                    getDocumentMetadata();
                                    dialog.modal('hide');
                                });
                            });
                        });
                    });
                });
            });
        });
    });

    $(document).on('change', '#selectDocumentType', function () {
        var image = $('#documentTypeImage');

        var txtDescription = $(this).closest('.doc-form').find('#txtDescription');
        var documentType = $(this).find('option:selected').text();

        //Asignamos la imagen
        for (var i = 0; i < documentTypeImages.length; i++) {
            var documentTypeImage = documentTypeImages[i];

            if (documentTypeImage.documentType == documentType) {
                $(image).on('load', function () {
                    $(image).fadeOut('fast', function () {
                        $(this).fadeIn();
                    });
                }).unbind('load');

                $(image).attr('src', documentTypeImage.image);

                break;
            }
        }

        //Asignamos la descripción
        for (var i = 0; i < documentTypeDescriptions.length; i++) {
            var documentTypeDescription = documentTypeDescriptions[i];
            if (documentTypeDescription.documentType == documentType) {
                $(txtDescription).val(documentTypeDescription.description);
                break;
            }
        }

        setTitle($(this));
    });

    $('#selectProduct').each(function () {
        $(this).on('change', function () {
            setTitle($(this));
        });
    });

    $('#selectProductFamily').each(function () {
        $(this).on('change', function () {
            loadProduct($(this).val());
        });
    });

    $('#btnSave').on('click', function () {
        if (!checkTitle()) {
            bootbox.alert({
                title: translate('Error'),
                message: translate('TitleRequired')
            });

            return;
        }

        if (!checkContentType()) {
            bootbox.alert({
                title: translate('Error'),
                message: translate('ContentTypeRequired')
            });

            return;
        }

        if (!checkProductFamily()) {
            bootbox.alert({
                title: translate('Error'),
                message: translate('ProductFamilyRequired')
            });

            return;
        }

        if (!checkProduct()) {
            bootbox.alert({
                title: translate('Error'),
                message: translate('ProductRequired')
            });

            return;
        }

        var confirmMessage = '<p>' + translate('SaveDocumentTitle1') + '</p><p>' + $('#txtTitle').val() + '</p><p>' + translate('SaveDocumentTitle2') + '</p>';

        bootbox.confirm({
            title: translate('SaveDocumentTitleTitle'),
            message: confirmMessage,
            closeButton: false,
            callback: function (result) {
                if (result) {
                    saveMetadata(function (url) {
                        checkOut(url, function () {
                            setPermissions(url, function () {
                                publish(url, function () {
                                    bootbox.hideAll();
                                    bootbox.alert({
                                        message: translate('NewDocumentOk'),
                                        title: translate('Information'),
                                        closeButton: false,
                                        callback: function () {
                                            window.location.href = _spPageContextInfo.siteAbsoluteUrl;
                                        }
                                    });
                                });
                            });
                        });
                    });
                }
            }
        });
    });

    $('#btnDelete').on('click', function () {
        bootbox.confirm({
            message: translate('DeleteDocumentConfirm'),
            title: translate('DeleteDocumentConfirmTitle'),
            closeButton: false,
            callback: function (result) {
                if (result) {
                    var itemId = getUrlParam('docid');
                    if (itemId != null && itemId != '') {
                        var clientContext = SP.ClientContext.get_current();
                        var oList = clientContext.get_web().get_lists().getByTitle('Documentos B2B');

                        var oListItem = oList.getItemById(itemId);

                        oListItem.recycle();

                        clientContext.executeQueryAsync(
                            function () {
                                bootbox.alert({
                                    message: translate('DeleteDocumentOk'),
                                    title: translate('DeleteDocumentOkTitle'),
                                    closeButton: false,
                                    callback: function () {
                                        window.location.href = _spPageContextInfo.siteAbsoluteUrl;
                                    }
                                });
                            },
                            function (sender, args) {
                                console.log('Request failed. ' + args.get_message() + '\n' + args.get_stackTrace());
                            }
                        );
                    }
                }
            }
        });
    });
});

function setTitle(element) {
    var selectedDocumentType = $('#selectDocumentType option:selected').text();
    //var selectedProductFamily = $('#selectProductFamily').val();
    var selectedProduct = $('#selectProduct').val().split('-')[1];
    var title = $('#txtTitle').val();
    title = title.split('-');
    title = title[title.length - 1].trim();

    if (title.lastIndexOf('.') > -1)
        title = title.substring(0, title.lastIndexOf('.'));

    //var productFamilyCode = '';
    //for (var i = 0; i < productFamily.length; i++) {
    //    var currentProductFamily = productFamily[i];
    //    if (currentProductFamily.id == selectedProductFamily) {
    //        productFamilyCode = currentProductFamily.code;
    //        break;
    //    }
    //}

    var productCode = '';
    for (var i = 0; i < products.length; i++) {
        var currentProduct = products[i];
        if (currentProduct.id == selectedProduct) {
            productCode = currentProduct.code;
            break;
        }
    }

    var documentTypeCode = '';
    for (var i = 0; i < documentTypeCodes.length; i++) {
        var currentDocumentType = documentTypeCodes[i];
        if (currentDocumentType.name == selectedDocumentType) {
            documentTypeCode = currentDocumentType.code;
            break;
        }
    }

    //title = productFamilyCode + '-' + productCode + '-' + documentTypeCode + '-' + title;
    title = documentTypeCode + ' - ' + productCode + ' - ' + title;

    $('#txtTitle').val(title);
}

function loadDocumentTypes(callback) {
    var clientContext = new SP.ClientContext(_spPageContextInfo.webAbsoluteUrl);
    var list = clientContext.get_web().get_lists().getByTitle('Documentos B2B');
    var contentTypes = list.get_contentTypes();

    clientContext.load(contentTypes);

    clientContext.executeQueryAsync(
        function () {
            var contentTypesEnumerator = contentTypes.getEnumerator();

            while (contentTypesEnumerator.moveNext()) {
                var cType = contentTypesEnumerator.get_current();
                documentTypes.push({
                    id: cType.get_id(),
                    name: cType.get_name()
                });
            }

            documentTypes.sort(function (a, b) {
                var textA = a.name.toUpperCase();
                var textB = b.name.toUpperCase();
                return (textA < textB) ? -1 : (textA > textB) ? 1 : 0;
            });

            $('#selectDocumentType').append('<option value="-1">' + translate('SelectDocumentType') + '</option>');
            for (var i = 0; i < documentTypes.length; i++) {
                var result = documentTypes[i];
                if (result.id.toString().indexOf('0x0120') == -1)
                    $('#selectDocumentType').append('<option value="' + result.id.toString() + '">' + result.name + '</option>');
            }

            if (callback != null)
                callback();
        },
        function (sender, args) {
            console.log('Request failed. ' + args.get_message() + '\n' + args.get_stackTrace());
            if (callback != null)
                callback();
        }
    );
}

function loadDocTypeCodes(callback) {
    var clientContext = new SP.ClientContext(_spPageContextInfo.siteAbsoluteUrl);
    var list = clientContext.get_web().get_lists().getByTitle('Codigos tipos de documento');
    var query = SP.CamlQuery.createAllItemsQuery();
    var items = list.getItems(query);

    clientContext.load(items);

    clientContext.executeQueryAsync(
        function () {
            var listItemEnumerator = items.getEnumerator();

            while (listItemEnumerator.moveNext()) {
                var item = listItemEnumerator.get_current();
                documentTypeCodes.push({
                    name: item.get_item('B2BContentType'),
                    code: item.get_item('Code')
                });
            }

            if (callback != null)
                callback();
        },
        function (sender, args) {
            console.log('Request failed. ' + args.get_message() + '\n' + args.get_stackTrace());
            if (callback != null)
                callback();
        }
    );
}

function loadProductFamily(callback) {
    var clientContext = new SP.ClientContext(_spPageContextInfo.siteAbsoluteUrl);
    var field = clientContext.castTo(clientContext.get_web().get_availableFields().getByInternalNameOrTitle('ProductFamily'), SP.FieldChoice);

    clientContext.load(field);

    clientContext.executeQueryAsync(
        function () {
            var oldName = decodeURIComponent(getUrlParam('ctname'));
            var newName = $('#txtTitle').val();

            var choices = field.get_choices();

            for (var i = 0; i < choices.length; i++) {
                productFamily.push({
                    name: choices[i]
                });
            }

            $('#selectProductFamily').each(function () {
                for (var i = 0; i < productFamily.length; i++) {
                    var result = productFamily[i];
                    $(this).append('<option value="' + result.name + '">' + result.name + '</option>');
                }
            });

            if (callback != null)
                callback();
        },
        function (sender, args) {
            console.log('Request failed. ' + args.get_message() + '\n' + args.get_stackTrace());
            if (callback != null)
                callback();
        }
    );
}

function loadProduct(family, product, callback) {
    if (products.length == 0) {
        var clientContext = new SP.ClientContext(_spPageContextInfo.siteAbsoluteUrl);
        var list = clientContext.get_web().get_lists().getByTitle('Productos');
        var query = SP.CamlQuery.createAllItemsQuery();
        var items = list.getItems(query);

        clientContext.load(items);

        clientContext.executeQueryAsync(
            function () {
                var listItemEnumerator = items.getEnumerator();
                products = new Array();

                while (listItemEnumerator.moveNext()) {
                    var item = listItemEnumerator.get_current();
                    if (family == null || item.get_item('ProductFamily') == family) {
                        products.push({
                            id: item.get_id(),
                            family: item.get_item('ProductFamily'),
                            name: item.get_item('Product')[0],
                            code: item.get_item('Code')
                        });
                    }
                }

                products.sort(function (a, b) {
                    var textA = a.name.toUpperCase();
                    var textB = b.name.toUpperCase();
                    return (textA < textB) ? -1 : (textA > textB) ? 1 : 0;
                });

                $('#selectProduct').empty();

                $('#selectProduct').each(function () {
                    $(this).append('<option value="-1">' + translate('SelectProduct') + '</option>');
                    for (var i = 0; i < products.length; i++) {
                        var result = products[i];
                        if (result.name == product)
                            $(this).append('<option selected="selected" value="' + result.family + '-' + result.id + '">' + result.name + '</option>');
                        else
                            $(this).append('<option value="' + result.family + '-' + result.id + '">' + result.name + '</option>'); 
                    }
                });

                if (callback != null)
                    callback();
            },
            function (sender, args) {
                console.log('Request failed. ' + args.get_message() + '\n' + args.get_stackTrace());
            }
        );
    }
    else {
        var productsCopy = new Array();

        for (var i = 0; i < products.length; i++) {
            if (family != null) {
                if (products[i].family == family) {
                    productsCopy.push({
                        id: products[i].id,
                        family: products[i].family,
                        name: products[i].name,
                        code: products[i].code
                    });
                }
            }
        }

        $('#selectProduct').empty();

        $('#selectProduct').each(function () {
            $(this).append('<option value="-1">' + translate('SelectProduct') + '</option>');
            for (var i = 0; i < productsCopy.length; i++) {
                var result = productsCopy[i];
                $(this).append('<option value="' + result.family + '-' + result.id + '">' + result.name + '</option>'); 
            }
        });

        if (product != null) {
            $('#selectProduct option').each(function () {
                if ($(this).text() == product)
                    $(this).prop('selected', true);
            });
        }
    }
}


function saveMetadata(callback) {
    bootbox.dialog({
        title: translate('Saving'),
        message: '<div class="spinner-border text-info" role="status"><span class="sr-only">Loading...</span></div> ' + translate('SavingDocInfo') + '...',
        closeButton: false
    });

    var docId = getUrlParam('doc');
    var clientContext = SP.ClientContext.get_current();
    var file = clientContext.get_web().getFileByServerRelativeUrl(docId);
    
    clientContext.load(file, 'ListItemAllFields');

    clientContext.executeQueryAsync(
        function () {
            var docUse = $('#chkInternalUse').is(':checked');

            listItem = file.get_listItemAllFields();
            listItem.set_item('ContentTypeId', $('#selectDocumentType').val());
            listItem.set_item("ProductFamily", $('#selectProductFamily option:selected').text());
            listItem.set_item("Product", $('#selectProduct option:selected').text());
            listItem.set_item("DocumentUse", (docUse ? 'Interno' : 'Externo'));
            listItem.set_item("DocumentDescription", $('#txtDescription').val());
            listItem.set_item('DocumentTypeImage', $('#documentTypeImage').attr('src'));
            listItem.set_item("Title", $('#txtTitle').val());
            listItem.set_item("DocLanguage", $('#selectLanguage').val());

            var enabler = getSelectedEnabler($('#selectDocumentType option:selected').text());
            if (enabler != null)
                listItem.set_item("Enabler", enabler);

            listItem.update();

            var folders = [
                $('#selectProductFamily option:selected').text().replace('/', '_'),
                $('#selectProduct option:selected').text().replace('/', '_'),
                $('#selectDocumentType option:selected').text()
            ];

            var destUrl = folders.join('/');
            createFolder(destUrl, function (url) {
                var newUrl = 'Documentos B2B' + '/' + destUrl + '/' + fileName;

                file.moveTo(newUrl, SP.MoveOperations.overwrite);

                clientContext.executeQueryAsync(
                    function () {
                        callback(newUrl);
                    },
                    function (sender, args) {
                        console.log('Request failed. ' + args.get_message() + '\n' + args.get_stackTrace());
                        ULSOnError(args.get_stackTrace(), window.location.href, 'Error occurred while updating File');
                        callback();
                    }
                );
            });
        },
        function (sender, args) {
            console.log('Request failed. ' + args.get_message() + '\n' + args.get_stackTrace());
            callback();
        }
    );
}

function createFolder(folderUrl, success, error) {
    var ctx = SP.ClientContext.get_current();
    var list = ctx.get_web().get_lists().getByTitle("Documentos B2B");

    var createFolderInternal = function (parentFolder, folderUrl, success, error) {
        var ctx = parentFolder.get_context();
        var folderNames = folderUrl.split('/');
        var folderName = folderNames[0];
        var curFolder = parentFolder.get_folders().add(folderName);
        ctx.load(curFolder);
        ctx.executeQueryAsync(
            function () {
                if (folderNames.length > 1) {
                    var subFolderUrl = folderNames.slice(1, folderNames.length).join('/');
                    createFolderInternal(curFolder, subFolderUrl, success, error);
                }
                else
                    success(curFolder);
            },
            error);
    };

    createFolderInternal(list.get_rootFolder(), folderUrl, success, error);
}

function getDocumentMetadata() {
    var docId = getUrlParam('doc');
    var clientContext = SP.ClientContext.get_current();
    var list = clientContext.get_web().get_lists().getByTitle('Documentos B2B');
    var file = clientContext.get_web().getFileByServerRelativeUrl(docId);
    clientContext.load(file, 'ListItemAllFields');


    clientContext.executeQueryAsync(
        function () {
            var listItem = file.get_listItemAllFields();

            var title = listItem.get_item('Title');
            fileName = listItem.get_item('FileLeafRef');
            fileUrl = listItem.get_item('FileRef');

            if (title == null || title == '')
                title = fileName;

            var contentTypeId = listItem.get_item('ContentTypeId');
            var productFamily = listItem.get_item('ProductFamily');
            var product = listItem.get_item('Product');
            var language = listItem.get_item('DocLanguage');
            var documentUse = listItem.get_item('DocumentUse');
            if (documentUse == null)
                documentUse = "Externo";

            var description = listItem.get_item('DocumentDescription');
            checkOutUser = listItem.get_item('CheckoutUser');

            if (checkOutUser != null)
                checkOutUser = checkOutUser.get_lookupId();

            if (checkOutUser != null && checkOutUser != _spPageContextInfo.userId) {
                bootbox.alert({
                    title: 'Información',
                    message: 'El documento está desprotegido por ' + listItem.get_item('CheckoutUser').get_lookupValue(),
                    closeButton: false,
                    callback: function () {
                        window.history.go(-1);
                    }
                });
            }
            else {
                $('#txtTitle').val(title);
                $('#txtDescription').val(description);
                $('#selectDocumentType').val(contentTypeId.toString());
                $('#selectProductFamily option:contains(' + productFamily + ')').attr('selected', 'selected');
                loadProduct(productFamily, product[0]);

                $('#selectLanguage').val(language);

                $('#chkInternalUse').prop('checked', documentUse.toLowerCase() == "interno");

                //$('#page-title h2').append('<p style="word-break: break-all;">' + listItem.get_item('FileLeafRef') + '</p>');
            }
        },
        function (sender, args) {
            console.log('Request failed. ' + args.get_message() + '\n' + args.get_stackTrace());
            ULSOnError(args.get_stackTrace(), window.location.href, 'Error occurred while updating File');
        }
    );
}

function loadDocumentTypeImages(callback) {
    var clientContext = new SP.ClientContext(_spPageContextInfo.siteAbsoluteUrl);
    var list = clientContext.get_web().get_lists().getByTitle('Imagenes tipos de documento');
    var query = SP.CamlQuery.createAllItemsQuery();
    var items = list.getItems(query);

    clientContext.load(items);

    clientContext.executeQueryAsync(
        function () {
            var listItemEnumerator = items.getEnumerator();

            while (listItemEnumerator.moveNext()) {
                var item = listItemEnumerator.get_current();
                documentTypeImages.push(
                    {
                        documentType: item.get_item('B2BContentType'),
                        image: item.get_item('Image').get_url()
                    }
                );
            }


            if (callback != null)
                callback();
        },
        function (sender, args) {
            console.log('Request failed. ' + args.get_message() + '\n' + args.get_stackTrace());
            if (callback != null)
                callback();
        }
    );
}

function loadDocumentTypeDescription() {
    var clientContext = new SP.ClientContext(_spPageContextInfo.siteAbsoluteUrl);
    var list = clientContext.get_web().get_lists().getByTitle('Descripcion tipos de documento');
    var query = SP.CamlQuery.createAllItemsQuery();
    var items = list.getItems(query);

    clientContext.load(items);

    clientContext.executeQueryAsync(
        function () {
            var listItemEnumerator = items.getEnumerator();

            while (listItemEnumerator.moveNext()) {
                var item = listItemEnumerator.get_current();
                documentTypeDescriptions.push(
                    {
                        documentType: item.get_item('Title'),
                        description: item.get_item('DocumentTypeDescription')
                    }
                );
            }
        },
        function (sender, args) {
            console.log('Request failed. ' + args.get_message() + '\n' + args.get_stackTrace());
        }
    );
}

function loadEnablers(callback) {
    var clientContext = new SP.ClientContext(_spPageContextInfo.siteAbsoluteUrl);
    var list = clientContext.get_web().get_lists().getByTitle('Enablers');
    var query = SP.CamlQuery.createAllItemsQuery();
    var items = list.getItems(query);

    clientContext.load(items);

    clientContext.executeQueryAsync(
        function () {
            var listItemEnumerator = items.getEnumerator();

            while (listItemEnumerator.moveNext()) {
                var item = listItemEnumerator.get_current();
                var docType = item.get_item('B2BContentType');
                var enabler = item.get_item('Enabler');

                enablers.push({
                    documentType: docType,
                    enablers: enabler
                });
            }

            if (callback != null)
                callback();
        },
        function (sender, args) {
            console.log('Request failed. ' + args.get_message() + '\n' + args.get_stackTrace());
            if (callback != null)
                callback();
        }
    );
}

function getSelectedEnabler(docType) {
    var value;
    for (var i = 0; i < enablers.length; i++) {
        var enabler = enablers[i];

        if (enabler.documentType == docType) {
            value = enabler.enablers.join(';#');
            break;
        }
    }

    return value;
}

function setPermissions(documentUrl, callback) {
    $('.bootbox-body').empty();

    var url = _spPageContextInfo.webServerRelativeUrl + '/' + documentUrl;

    var clientContext = SP.ClientContext.get_current();
    var file = clientContext.get_web().getFileByServerRelativeUrl(url);
    clientContext.load(file, 'ListItemAllFields');

    clientContext.executeQueryAsync(
        function () {
            var listItem = file.get_listItemAllFields();

            var title = listItem.get_item('Title');
            if (title == null || title == '')
                title = listItem.get_item('FileLeafRef');

            $('.bootbox-body').append('<div class="spinner-border text-info" role="status"><span class="sr-only">Loading...</span></div> ' + translate('SetPermissionsFor') + ' ' + title);

            var contentTypeId = listItem.get_item('ContentTypeId').toString();
            var contentType;

            for (var i = 0; i < documentTypes.length; i++) {
                if (documentTypes[i].id == contentTypeId) {
                    contentType = documentTypes[i].name;
                    break;
                }
            }

            var currentPermissions = permissions.filter(function(obj) {
                return obj.documentType === contentType;
            });

            listItem.breakRoleInheritance(false);

            for (var i = 0; i < currentPermissions.length; i++) {
                var groups = clientContext.get_web().get_siteGroups();
                var group = groups.getByName(currentPermissions[i].securityGroup);

                var collRoleDefinitionBinding = SP.RoleDefinitionBindingCollection.newObject(clientContext);
                collRoleDefinitionBinding.add(clientContext.get_web().get_roleDefinitions().getByType(currentPermissions[i].permission));

                listItem.get_roleAssignments().add(group, collRoleDefinitionBinding);

                clientContext.load(group);
                clientContext.load(listItem);
            }

            clientContext.executeQueryAsync(
                function () {
                    callback();
                },
                function (sender, args) {
                    console.log('Request failed. ' + args.get_message() + '\n' + args.get_stackTrace());
                    ULSOnError(args.get_stackTrace(), window.location.href, 'Error occurred while updating File');
                }
            );
        },
        function (sender, args) {
            console.log('Request failed. ' + args.get_message() + '\n' + args.get_stackTrace());
            ULSOnError(args.get_stackTrace(), window.location.href, 'Error occurred while updating File');
        }
    );
}

function loadPermissions(callback) {
    var clientContext = new SP.ClientContext(_spPageContextInfo.siteAbsoluteUrl);
    var list = clientContext.get_web().get_lists().getByTitle('Permisos por tipo de documento');
    var query = SP.CamlQuery.createAllItemsQuery();
    var items = list.getItems(query);

    clientContext.load(items);

    clientContext.executeQueryAsync(
        function () {
            var listItemEnumerator = items.getEnumerator();

            while (listItemEnumerator.moveNext()) {
                var item = listItemEnumerator.get_current();
                var docType = item.get_item('B2BContentType');
                var securityGroup = item.get_item('SecurityGroup');
                var permission = item.get_item('PermissionLevel');
                var permissionLevel;

                switch (permission.toLowerCase()) {
                    case 'contributor':
                        permissionLevel = SP.RoleType.contributor;
                        break;
                    case 'administrator':
                        permissionLevel = SP.RoleType.administrator;
                        break;
                    default:
                        permissionLevel = SP.RoleType.reader;
                        break;
                }

                permissions.push({
                    documentType: docType,
                    securityGroup: securityGroup,
                    permission: permissionLevel
                });
            }

            if (callback != null)
                callback();
        },
        function (sender, args) {
            console.log('Request failed. ' + args.get_message() + '\n' + args.get_stackTrace());
            if (callback != null)
                callback();
        }
    );
}

function checkOut(documentUrl, callback) {
    if (checkOutUser == _spPageContextInfo.userId) {
        if (callback != null)
            callback();

        return;
    }

    $('.bootbox-body').empty();
    $('.bootbox-body').append('<div class="spinner-border text-info" role="status"><span class="sr-only">Loading...</span></div> ' + translate('CheckOutDoc'));
    var url = _spPageContextInfo.webServerRelativeUrl + '/' + documentUrl;
    var clientContext = SP.ClientContext.get_current();
    var file = clientContext.get_web().getFileByServerRelativeUrl(url);

    file.checkOut();

    clientContext.load(file);

    clientContext.executeQueryAsync(
        function () {
            if (callback != null)
                callback();
        },
        function (sender, args) {
            console.log('Request failed. ' + args.get_message() + '\n' + args.get_stackTrace());
        }
    );
}

function publish(documentUrl, callback) {
    $('.bootbox-body').empty();
    $('.bootbox-body').append('<div class="spinner-border text-info" role="status"><span class="sr-only">Loading...</span></div> ' + translate('PublishDoc'));

    var url = _spPageContextInfo.webServerRelativeUrl + '/' + documentUrl;
    var clientContext = SP.ClientContext.get_current();
    var file = clientContext.get_web().getFileByServerRelativeUrl(url);

    file.checkIn();
    file.publish();

    clientContext.load(file);

    clientContext.executeQueryAsync(
        function () {
            if (callback != null)
                callback();
        },
        function (sender, args) {
            console.log('Request failed. ' + args.get_message() + '\n' + args.get_stackTrace());
        }
    );
}

function checkTitle() {
    if ($('#txtTitle').val() == '') {
        return false;
    }

    return true;
}

function checkContentType() {
    if ($('#selectDocumentType').val() == '-1') {
        return false;
    }

    return true;
}

function checkProductFamily() {
    if ($('#selectProductFamily').val() == '-1') {
        return false;
    }

    return true;
}

function checkProduct() {
    if ($('#selectProduct').val() == '-1') {
        return false;
    }

    return true;
}

function resolveUserEmail() {
    var responsibleEmail = $('#txtResponsible').val();
    var url = "https://telefonicacorp.sharepoint.com/sites/B2BFinder.OCEWU/_api/web/siteusers?$filter=Email eq '" + responsibleEmail + "'";

    var $ajax = $.ajax({
        url: url,
        type: "GET",
        dataType: "json",
        headers: {
            Accept: "application/json;odata=verbose"
        }
    });

    $ajax.done(function (data, textStatus, jqXHR) {
        console.log(data);
        var results = data.d.results;
        if (results.length > 0) {
            var userId = results[0].Id;
            $('#hdnResponsibleId').val(userId);
        }
        else {
            bootbox.alert({
                title: translate('Error'),
                message: translate('UserNotFound'),
                closeButton: false
            });
        }
    });
}