﻿var refinersSelected = [];

(function ($) {
    $.fn.refiner = function () {
        var $this = this;
        var refinerCount = 5;
        var refiners;
        var queryText = getUrlParam('k');
        var searchUrl = "https://telefonicacorp.sharepoint.com/sites/B2BFinder.OCEWU/_api/search/query?querytext='" + decodeURIComponent(queryText) + "+ListID:39190973-ddd1-426a-9d07-3e5814c9a81e+IsContainer%3dfalse'&refiners='RefinableString01,RefinableString00,RefinableString04,SPContentType,FileType,RefinableString05,RefinableString06'";
        var sort = ['SPContentType', 'RefinableString00', 'RefinableString01', 'RefinableString04', 'RefinableString06', 'FileType', 'RefinableString05'];

        function executeSearch() {
            var $ajax = $.ajax({
                url: searchUrl,
                type: "GET",
                dataType: "json",
                headers: {
                    Accept: "application/json;odata=verbose"
                }
            });

            $ajax.done(function (data, textStatus, jqXHR) {
                refiners = data.d.query.PrimaryQueryResult.RefinementResults.Refiners.results;

                refiners = sortArray(refiners);

                var html = '';
                html += '<div class="text-right mt-5 pt-2" style="clear:both;display:none;"><a href="#" data-command="clear-all">' + translate('UncheckAll-All') + '</a></div>';

                //Pintamos el nombre del refinador
                for (var i = 0; i < refiners.length; i++) {
                    var refiner = refiners[i];
                    if (refiner != null) {
                        var entries = refiner.Entries.results;

                        entries.sort(function (a, b) {
                            var textA = a.RefinementName.toUpperCase();
                            var textB = b.RefinementName.toUpperCase();
                            return (textA < textB) ? -1 : (textA > textB) ? 1 : 0;
                        });

                        html += '<div class="refiner"><div class="ref-header">';
                        html += translate(refiner.Name);
                        html += '</div>';

                        for (var j = 0; j < entries.length; j++) {
                            var entry = entries[j];
                            html += '<label class="check-container"';

                            //Atributos
                            html += ' data-n="' + refiner.Name + '"';
                            html += ' data-t=' + entry.RefinementToken.toString();
                            html += ' data-o="OR"';
                            html += ' data-k="false"';
                            html += ' data-m="' + entry.RefinementName + '"';

                            if (j > refinerCount - 1)
                                html += ' style="display:none"';

                            var refinementName = translate(entry.RefinementName);
                            if (refinementName.length > 30) {
                                refinementName = refinementName;
                            }

                            html += '>' + refinementName + '<input type="checkbox"/><span class="checkmark"></span></label>';
                        }

                        if (entries.length > refinerCount) {
                            html += '<a href="#" data-command="more" class="float-left">' + translate('ShowMore') + '</a>';
                        }

                        html += '<a href="#" data-command="clear" class="float-right" style="display:none;">' + translate('UncheckAll') + '</a>';

                        html += '</div>';
                    }
                }

                html += '<div class="text-right mt-5 pt-2" style="clear:both;border-top: solid 1px #00b6c7;display:none;"><a href="#" data-command="clear-all">' + translate('UncheckAll-All') + '</a></div>';

                $($this).append(html);

                addClickEvent();
                addClearEvent();
                addClearAllEvent();
                addShowMoreEvent();
            });
        }

        function addClickEvent() {
            $('.check-container').each(function () {
                $(this).on('click', function () {
                    searchTotalRows = 0;
                    showClearAll($(this));
                    showClearAllRefiners();
                    getRefinerJSON();
                });
            });
        }

        function addShowMoreEvent() {
            $('a[data-command="more"],a[data-command="less"]').on('click', function () {
                var refinerParent = $(this).closest('div.refiner');

                if ($(this).attr('data-command') == 'more') {
                    $(refinerParent).find('label').each(function (index) {
                        if (index > refinerCount - 1) {
                            $(this).fadeIn();
                        }
                    });
                    $(this).text(translate('ShowLess'));
                    $(this).attr('data-command', 'less');
                }
                else {
                    $(refinerParent).find('label').each(function (index) {
                        if (index > refinerCount - 1) {
                            $(this).fadeOut();
                        }
                    });

                    $(this).text(translate('ShowMore'));
                    $(this).attr('data-command', 'more');
                }
            });
        }

        function addClearEvent() {
            $('a[data-command="clear"]').on('click', function () {
                var refinerParent = $(this).closest('div.refiner');
                $(refinerParent).find('.check-container').each(function () {
                    var checkbox = $(this).find('input[type="checkbox"]');
                    if ($(checkbox).is(':checked')) {
                        $(this).click();
                    }
                });
            });
        }

        function addClearAllEvent() {
            $('a[data-command="clear-all"]').on('click', function () {
                $('div.refiner').each(function () {
                    $(this).find('.check-container').each(function () {
                        var checkbox = $(this).find('input[type="checkbox"]');
                        if ($(checkbox).is(':checked')) {
                            $(this).click();
                        }
                    });
                });
            });
        }

        function showClearAll(element) {
            var counter = 0;
            var refinerParent = $(element).closest('div.refiner');
            $(refinerParent).find('.check-container').each(function () {
                var checkbox = $(this).find('input[type="checkbox"]');
                if ($(checkbox).is(':checked')) {
                    counter++;
                }
            });

            if (counter > 1)
                $(refinerParent).find('a[data-command="clear"]').show();
            else
                $(refinerParent).find('a[data-command="clear"]').hide();
        }

        function showClearAllRefiners() {
            var counter = 0;
            $('div.refiner').each(function () {
                $(this).find('.check-container').each(function () {
                    var checkbox = $(this).find('input[type="checkbox"]');
                    if ($(checkbox).is(':checked')) {
                        counter++;
                    }
                });
            });

            if (counter > 1)
                $('a[data-command="clear-all"]').parent().show();
            else
                $('a[data-command="clear-all"]').parent().hide();
        }


        function getRefinerJSON() {
            var refinerObjectArray = new Array();

            $('.check-container').each(function () {
                var checkbox = $(this).find('input[type="checkbox"]');
                if ($(checkbox).is(':checked')) {
                    //Comprobamos si el refinador está en el array
                    var refinerName = $(this).data('n');
                    var refinerT = [$(this).data('t')];
                    var refinerO = $(this).data('o');
                    var refinerK = $(this).data('k');
                    var refinerM = new Object();
                    refinerM[$(this).data('t')] = $(this).data('m');

                    var refiner = getRefinerFromArray(refinerObjectArray, refinerName);

                    if (refiner == null) {
                        refiner = new Object();
                        refiner["n"] = refinerName;
                        refiner["t"] = refinerT;
                        refiner["o"] = refinerO;
                        refiner["k"] = refinerK;
                        refiner["m"] = refinerM;

                        refinerObjectArray.push(refiner);
                    }
                    else {
                        refiner["t"].push($(this).data('t'));
                        refiner["m"][$(this).data('t')] = $(this).data('m');
                    }
                }
            });

            createRefinerJSON(refinerObjectArray);
        }

        function sortArray(refinerArray) {
            var newRefinerArray = new Array();

            for (var i = 0; i < sort.length; i++) {
                //Buscamos el refinador
                var refinerName = sort[i];
                var refiner = getRefinerFromResults(refinerArray, refinerName);

                newRefinerArray.push(refiner);
            }

            return newRefinerArray;
        }

        function createRefinerJSON(refinerArray) {
            var refinerJson = new Object();
            refinerJson["k"] = decodeURIComponent(queryText);
            refinerJson["r"] = refinerArray;

            console.log(refinerJson);

            var refinerJsonString = JSON.stringify(refinerJson);

            var url = window.location.href;
            url = url.split('#')[0];
            url += '#Default=' + encodeURIComponent(refinerJsonString);

            window.location.href = url;
        }

        function getRefinerFromResults(resultsArray, refinerName) {
            for (var i = 0; i < resultsArray.length; i++) {
                var refiner = resultsArray[i];
                if (refiner.Name == refinerName)
                    return refiner;
            }

            return null;
        }

        function getRefinerFromArray(refinerArray, refinerName) {
            for (var i = 0; i < refinerArray.length; i++) {
                var refiner = refinerArray[i];
                if (refiner["n"] == refinerName)
                    return refiner;
            }

            return null;
        }


        executeSearch();

    };
}(jQuery));

$(document).ready(function () {
    $('#refiners-panel').refiner();
});