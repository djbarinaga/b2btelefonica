﻿$(document).ready(function () {
    SP.SOD.executeFunc('sp.js', 'SP.ClientContext', initFavourites);
});

function initFavourites() {
    //setFavourites();
    var inDesignMode = document.forms[MSOWebPartPageFormName].MSOLayout_InDesignMode.value;
    if (inDesignMode != "1") {
        loadMyFavourites();
    }
}


//Obtiene los elementos favoritos del usuario
function setFavourites(showPopover) {
    var clientContext = new SP.ClientContext(_spPageContextInfo.siteAbsoluteUrl);
    var list = clientContext.get_web().get_lists().getByTitle('Favoritos');
    var camlQuery = new SP.CamlQuery();
    camlQuery.set_viewXml("<View><Query><Where><Eq><FieldRef Name='Author' LookupId='True'/><Value Type='Lookup'>" + _spPageContextInfo.userId + "</Value></Eq></Where><OrderBy><FieldRef Name='ID' Ascending='FALSE'/></OrderBy></Query><RowLimit>5</RowLimit></View>");

    var items = list.getItems(camlQuery);
    clientContext.load(items);

    clientContext.executeQueryAsync(
        function () {
            $('#btnFavourites').popover('dispose');
            var hasResults = false;
            var listItemEnumerator = items.getEnumerator();
            var html = '<ul class="list-group list-group-flush" id="favourites">';
            while (listItemEnumerator.moveNext()) {
                hasResults = true;
                var result = listItemEnumerator.get_current();

                var title = result.get_item('DocUrl').get_description();
                var url = result.get_item('DocUrl').get_url();
                var urlParts = url.split('?');
                var urlParts2 = urlParts[0].split('.');
                var extension = urlParts2[urlParts2.length - 1];

                html += '<li class="list-group-item">';
                html += getDocumentIcon(extension);
                html += '<a href="' + url + '" target="_blank" class="anchor-icon">' + title + '</a>';
                html += '</li>';
            }

            if (!hasResults)
                html = '<p>' + translate('NoFavouritesMessage') + '</p>';

            var popover = customPopover({
                element: $('#btnFavourites'),
                title: translate('Favourites') + '<span class="icon-heart-blue"><span class="path1"></span><span class="path2"></span></span>',
                placement: 'left',
                content: html,
                buttons: [
                    {
                        Id: 'btnViewAll',
                        Url: _spPageContextInfo.listUrl + '/mis-favoritos.aspx',
                        Text: translate('viewall')
                    }
                ]
            });

            if (showPopover) {
                popover.popover('show');
                setTimeout(function () { popover.popover('hide'); }, 2000);
            }
        },
        function (sender, args) {
            console.log('Request failed. ' + args.get_message() + '\n' + args.get_stackTrace());
        }
    );
}

function loadMyFavourites() {
    if ($('#favouritesTable').length > 0) {
        $('#favouritesTable tbody').fadeOut();
        $('#favouritesTable tbody').empty();
        var clientContext = new SP.ClientContext(_spPageContextInfo.siteAbsoluteUrl);
        var list = clientContext.get_web().get_lists().getByTitle('Favoritos');
        var camlQuery = new SP.CamlQuery();
        camlQuery.set_viewXml("<View><Query><Where><Eq><FieldRef Name='Author' LookupId='True'/><Value Type='Lookup'>" + _spPageContextInfo.userId + "</Value></Eq></Where><OrderBy><FieldRef Name='ID' Ascending='FALSE'/></OrderBy></Query></View>");

        var items = list.getItems(camlQuery);
        clientContext.load(items);

        clientContext.executeQueryAsync(
            function () {
                var hasResults = false;
                var listItemEnumerator = items.getEnumerator();

                while (listItemEnumerator.moveNext()) {
                    hasResults = true;
                    var result = listItemEnumerator.get_current();

                    var title = result.get_item('DocUrl').get_description();
                    var url = result.get_item('DocUrl').get_url();
                    var urlParts = url.split('?');
                    var urlParts2 = urlParts[0].split('.');
                    var extension = urlParts2[urlParts2.length - 1];
                    var docIcon = getDocumentIcon(extension);
                    var fileName = getFileNameFromUrl(urlParts[0]);
                    var id = result.get_id();

                    $('#favouritesTable tbody').append('<tr><td>' + docIcon + '</td><td><a href="' + url + '" target="_blank">' + title + '</a></td><td class="last"><a href="#" data-id="' + id + '" title="Eliminar" data-command="delete"><span class="icon-cross"></span></a><a href="' + urlParts[0] + '" title="Descargar" download="' + fileName + '"><span class="icon-download"></span></a></td></tr>');
                }

                setFavouritesTableEvents();

                $('#favouritesTable tbody').fadeIn();
            },
            function (sender, args) {
                console.log('Request failed. ' + args.get_message() + '\n' + args.get_stackTrace());
            }
        );
    }
}

function setFavouritesTableEvents() {
    $('#favouritesTable').find('a[data-command="delete"]').each(function () {
        var id = $(this).data('id');

        var html = '<p>' + translate('DeleteFavouritesConfirm') + '</p>';
        var popover = confirmPopover($(this), translate('Information'), translate('DeleteFavouriteConfirm'), 'right', 'click', function (result) {
            if (result) {
                deleteFavourite(id, function () {
                    popover.popover('dispose');
                    $('a[data-id="' + id + '"]').closest('tr').fadeOut('fast', function () {
                        $(this).remove();
                    });
                });
            }
        });
    });
}

function favouriteCommand() {
    $('[data-command="favourite"]').each(function () {
        var link = $(this);
        $(link).unbind('click');

        $(link).on('click', function () {
            var commandArgs = $(this).attr('data-command-args');

            if (commandArgs == 'add') {
                var parentCard = $(this).closest('.card');
                if (parentCard.length == 0)
                    parentCard = $(this).closest('.doc-results');

                createFavourite(parentCard, link);

                $(this).attr('data-command-args', 'delete');
            }
            else {
                var id = $(link).attr('data-id');

                var popover = confirmPopover($(this), translate('Information'), translate('DeleteFavouriteConfirm'), 'right', 'manual', function (result) {
                    if (result)
                        deleteFavourite(id, function () {
                            $(link).find('.icon-heart-red').removeClass('icon-heart-red').addClass('icon-heart-purple');
                            $(popover).popover('dispose');

                            $(link).attr('data-command-args', 'add');
                            favouriteCommand();
                        });
                });

                popover.popover('show');
            }
        });
    });
}

function setFavouritePopoverToolbar(popover) {
    var id = $(popover).attr('data-id');
    $('.popover-favourite[data-id="' + id + '"]').find('[data-command="delete"]').on('click', function () {
        deleteFavourite(id);
    });

    $('.popover-favourite[data-id="' + id + '"]').find('[data-command="cancel"]').on('click', function () {
        $(popover).popover('hide');
    });
}

function deleteFavourite(id, callback) {
    var clientContext = new SP.ClientContext(_spPageContextInfo.siteAbsoluteUrl);
    var list = clientContext.get_web().get_lists().getByTitle('Favoritos');

    var listItem = list.getItemById(id);

    listItem.deleteObject();

    clientContext.executeQueryAsync(
        function () {
            if (callback != null)
                callback();
        },
        function (sender, args) {
            console.log('Request failed. ' + args.get_message() + '\n' + args.get_stackTrace());
        }
    );
}

function createFavourite(card, link) {
    var title = $(card).find('.card-title').text();

    if ($(card).find('.card-title').attr('data-original-title') != null && $(card).find('.card-title').attr('data-original-title') != '')
        title = $(card).find('.card-title').attr('data-original-title');

    var url = $(card).find('.card-title').attr('href');

    if (title == null || title == '')
        title = $('#doc-title').text();

    if (url == null || url == '')
        url = decodeURIComponent(getUrlParam('doc'));

    var clientContext = new SP.ClientContext(_spPageContextInfo.siteAbsoluteUrl);
    var list = clientContext.get_web().get_lists().getByTitle('Favoritos');

    var itemCreateInfo = new SP.ListItemCreationInformation();
    var listItem = list.addItem(itemCreateInfo);

    listItem.set_item('Title', title);

    var urlValue = new SP.FieldUrlValue();
    urlValue.set_url(url);
    urlValue.set_description(title);

    listItem.set_item("DocUrl", urlValue);

    listItem.update();

    clientContext.load(listItem);

    clientContext.executeQueryAsync(
        function () {
            $(link).attr('data-id', listItem.get_id());
            var icon = $(link).find('.icon-heart-purple');
            icon.removeClass('icon-heart-purple');
            icon.addClass('icon-heart-red');

            var tooltip = $(link).tooltip({
                title: translate('FavouriteAdded'),
                placement: 'right'
            });

            tooltip.tooltip('show');

            setTimeout(function () {
                tooltip.tooltip('dispose');
            }, 2000);

            favouriteCommand();
        },
        function (sender, args) {
            console.log('Request failed. ' + args.get_message() + '\n' + args.get_stackTrace());
        }
    );
}

function checkFavourites() {
    var clientContext = new SP.ClientContext(_spPageContextInfo.siteAbsoluteUrl);
    var list = clientContext.get_web().get_lists().getByTitle('Favoritos');
    var camlQuery = new SP.CamlQuery();
    camlQuery.set_viewXml("<View><Query><Where><Eq><FieldRef Name='Author' LookupId='True'/><Value Type='Lookup'>" + _spPageContextInfo.userId + "</Value></Eq></Where><OrderBy><FieldRef Name='ID' Ascending='FALSE'/></OrderBy></Query></View>");

    var items = list.getItems(camlQuery);
    clientContext.load(items);

    clientContext.executeQueryAsync(
        function () {
            var favourites = new Array();
            var listItemEnumerator = items.getEnumerator();
            while (listItemEnumerator.moveNext()) {
                var result = listItemEnumerator.get_current();
                var title = result.get_item('DocUrl').get_url();
                favourites.push({
                    Title: title,
                    ID: result.get_id()
                });
            }

            $('.card').each(function () {
                var cardTitle = $(this).find('.card-title').attr('href');
                var index = favourites.map(function (e) { return e.Title; }).indexOf(cardTitle);
                if (index > -1) {
                    var link = $(this).find('[data-command="favourite"]');
                    $(link).attr('data-command-args', 'delete');

                    var icon = $(link).find('.icon-heart-purple');
                    icon.removeClass('icon-heart-purple');
                    icon.addClass('icon-heart-red');

                    $(this).find('a[data-command="favourite"]').attr('data-id', favourites[index].ID);
                }
            });

            $('.doc-results').each(function () {
                var cardTitle = $(this).find('.card-title').attr('href');
                var index = favourites.map(function (e) { return e.Title; }).indexOf(cardTitle);
                if (index > -1) {
                    var link = $(this).find('[data-command="favourite"]');
                    $(link).attr('data-command-args', 'delete');

                    var icon = $(this).find('.icon-heart-purple');
                    icon.removeClass('icon-heart-purple');
                    icon.addClass('icon-heart-red');

                    $(this).find('a[data-command="favourite"]').attr('data-id', favourites[index].ID);
                }
            });

            if ($('#document-info-page').length) {
                var docTitle = $('#doc-title').text();
                if (docTitle != null) {
                    var index = favourites.map(function (e) { return e.Title; }).indexOf(docTitle);
                    if (index > -1) {
                        var link = $('[data-command="favourite"]');
                        $(link).attr('data-command-args', 'delete');

                        var icon = $(link).find('.icon-heart-purple');
                        icon.removeClass('icon-heart-purple');
                        icon.addClass('icon-heart-red');

                        $(link).attr('data-id', favourites[index].ID);
                    }
                }
            }


            favouriteCommand();
        },
        function (sender, args) {
            console.log('Request failed. ' + args.get_message() + '\n' + args.get_stackTrace());
        }
    );
}