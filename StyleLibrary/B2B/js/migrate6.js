﻿var documentTypes = new Array();
var targetDocuments = new Array();
var permissions = new Array();

$(document).ready(function () {
    $('#btnDale').on('click', function () {
        loadDocuments(function () {
            publish(0);
        });
    });
});

function publish(index) {
    if (index == targetDocuments.length) {
        alert('Fin');
        return;
    }

    var targetDocument = targetDocuments[index];
    var currentIndex = index + 1;

    if (targetDocument.url != '' && targetDocument.url != null) {
        $('#documentList').prepend('<p>' + targetDocument.url + '</p>');
        var clientContext = SP.ClientContext.get_current();
        var file = clientContext.get_web().getFileByServerRelativeUrl(targetDocument.url);

        file.publish();

        clientContext.load(file);

        clientContext.executeQueryAsync(
            function () {
                publish(currentIndex);
            },
            function (sender, args) {
                $('#documentList').prepend('<p style="color:red">' + args.get_message() + '</p>');
                publish(currentIndex);
            }
        );
    }
    else {
        publish(currentIndex);
    }
}

function setPermissions(index) {
    if (index == targetDocuments.length) {
        alert('Fin');
        return;
    }

    var targetDocument = targetDocuments[index];
    var clientContext = SP.ClientContext.get_current();
    var list = clientContext.get_web().get_lists().getByTitle('Documentos B2B');
    var listItem = list.getItemById(targetDocument.id);

    clientContext.load(listItem);

    clientContext.executeQueryAsync(
        function () {
            var title = listItem.get_item('Title');
            if (title == null || title == '')
                title = listItem.get_item('FileLeafRef');

            var contentType;

            for (var i = 0; i < documentTypes.length; i++) {
                if (documentTypes[i].id == targetDocument.contentType.toString()) {
                    contentType = documentTypes[i].name;
                    break;
                }
            }

            if (contentType.toLowerCase() != "carpeta") {
                var currentPermissions = permissions.filter(obj => {
                    return obj.documentType === contentType;
                });

                listItem.breakRoleInheritance(false, false);

                for (var i = 0; i < currentPermissions.length; i++) {
                    var groups = clientContext.get_web().get_siteGroups();
                    var group = groups.getByName(currentPermissions[i].securityGroup);

                    var collRoleDefinitionBinding = SP.RoleDefinitionBindingCollection.newObject(clientContext);
                    collRoleDefinitionBinding.add(clientContext.get_web().get_roleDefinitions().getByType(currentPermissions[i].permission));

                    listItem.get_roleAssignments().add(group, collRoleDefinitionBinding);

                    clientContext.load(group);
                    clientContext.load(listItem);
                }

                clientContext.executeQueryAsync(
                    function () {
                        var next = index + 1;
                        setPermissions(next);

                        $('#documentList').prepend('<p>Documento ' + targetDocument.id + ': modificado.</p>');
                    },
                    function (sender, args) {
                        console.log('Request failed. ' + args.get_message() + '\n' + args.get_stackTrace());
                        ULSOnError(args.get_stackTrace(), window.location.href, 'Error occurred while updating File');
                    }
                );
            }
            else {
                var next = index + 1;
                setPermissions(next);
            }
        },
        function (sender, args) {
            console.log('Request failed. ' + args.get_message() + '\n' + args.get_stackTrace());
            ULSOnError(args.get_stackTrace(), window.location.href, 'Error occurred while updating File');
        }
    );
}

function loadDocuments(callback) {
    var clientContext = new SP.ClientContext(_spPageContextInfo.webAbsoluteUrl);
    var list = clientContext.get_web().get_lists().getByTitle('Documentos B2B');
    var query = SP.CamlQuery.createAllItemsQuery();
    var items = list.getItems(query);

    clientContext.load(items);

    clientContext.executeQueryAsync(
        function () {
            var listItemEnumerator = items.getEnumerator();

            while (listItemEnumerator.moveNext()) {
                var item = listItemEnumerator.get_current();
                var ctype = item.get_item('ContentTypeId').toString();

                if (ctype.indexOf('0x0120') == -1) {
                    targetDocuments.push({
                        id: item.get_id(),
                        url: item.get_item('FileRef')
                    });
                }
            }


            $('#documentList').prepend('<p>Documentos cargados.</p>');
            callback();
        },
        function (sender, args) {
            console.log('Request failed. ' + args.get_message() + '\n' + args.get_stackTrace());
        }
    );
}

function loadDocumentTypes(callback) {
    var clientContext = new SP.ClientContext(_spPageContextInfo.webAbsoluteUrl);
    var list = clientContext.get_web().get_lists().getByTitle('Documentos B2B');
    var contentTypes = list.get_contentTypes();

    clientContext.load(contentTypes);

    clientContext.executeQueryAsync(
        function () {
            var results = [];
            var contentTypesEnumerator = contentTypes.getEnumerator();

            while (contentTypesEnumerator.moveNext()) {
                var cType = contentTypesEnumerator.get_current();
                documentTypes.push({
                    id: cType.get_id(),
                    name: cType.get_name()
                });
            }

            $('#documentList').prepend('<p>Document types cargados.</p>');
            callback();
        },
        function (sender, args) {
            console.log('Request failed. ' + args.get_message() + '\n' + args.get_stackTrace());
        }
    );
}

function loadPermissions(callback) {
    var clientContext = new SP.ClientContext(_spPageContextInfo.siteAbsoluteUrl);
    var list = clientContext.get_web().get_lists().getByTitle('Permisos por tipo de documento');
    var query = SP.CamlQuery.createAllItemsQuery();
    var items = list.getItems(query);

    clientContext.load(items);

    clientContext.executeQueryAsync(
        function () {
            var listItemEnumerator = items.getEnumerator();

            while (listItemEnumerator.moveNext()) {
                var item = listItemEnumerator.get_current();
                var docType = item.get_item('B2BContentType');
                var securityGroup = item.get_item('SecurityGroup');
                var permission = item.get_item('PermissionLevel');
                var permissionLevel;

                switch (permission) {
                    case 'Contributor':
                        permissionLevel = SP.RoleType.contributor;
                        break;
                    case 'Administrator':
                        permissionLevel = SP.RoleType.administrator;
                        break;
                    default:
                        permissionLevel = SP.RoleType.reader;
                        break;
                }

                permissions.push({
                    documentType: docType,
                    securityGroup: securityGroup,
                    permission: permissionLevel
                });
            }

            $('#documentList').prepend('<p>Permisos cargados.</p>');

            callback();
        },
        function (sender, args) {
            console.log('Request failed. ' + args.get_message() + '\n' + args.get_stackTrace());
        }
    );
}