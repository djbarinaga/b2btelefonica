﻿var checkout = false;
var checkOutUserId = -1;

$(document).ready(function () {
    SP.SOD.executeFunc('sp.js', 'SP.ClientContext', initDocumentInfo);
});

function initDocumentInfo() {
    var doc = getUrlParam('doc');
    var docId = getUrlParam('docid');
    if (docId != null && docId != '' && doc != null && doc != '') {
        doc = doc.split('?')[0];
        var clientContext = new SP.ClientContext(_spPageContextInfo.webAbsoluteUrl);
        var list = clientContext.get_web().get_lists().getByTitle('Documentos B2B');
        var camlQuery = new SP.CamlQuery();
        camlQuery.set_viewXml("<View><Query><Where><Eq><FieldRef Name='ID'/><Value Type='Counter'>" + docId + "</Value></Eq></Where></Query><RowLimit>1</RowLimit></View>");
        var items = list.getItems(camlQuery);
        
        var file = clientContext.get_web().getFileByServerRelativeUrl(doc);

        clientContext.load(file, 'ListItemAllFields');
        clientContext.load(items);

        clientContext.executeQueryAsync(
            function () {
                var listItemEnumerator = items.getEnumerator();

                var result = null;
                var fileItem = file.get_listItemAllFields();

                while (listItemEnumerator.moveNext()) {
                    result = listItemEnumerator.get_current();
                }

                if (result == null)
                    result = fileItem;

                try {
                    var title = result.get_item('Title');
                    if (title == '' || title == null)
                        title = result.get_item('FileLeafRef');

                    checkout = result.get_item('CheckoutUser') != null;
                    if (result.get_item('CheckoutUser') != null)
                        checkOutUserId = result.get_item('CheckoutUser').get_lookupId();

                    var use = result.get_item('DocumentUse');
                    var language = result.get_item('DocLanguage');
                    if (language != null) {
                        language = language.toUpperCase();
                    }
                    var author = null;
                    var authorEmail = null;
                    var authorName = null;

                    if (result.get_item('Responsible') != null) {
                        author = result.get_item('Responsible');
                        authorName = author.get_lookupValue();
                        authorEmail = author.get_email();
                    }
                    else if (result.get_item('ResponsibleText') != null) {
                        authorName = result.get_item('ResponsibleText');
                        authorEmail = result.get_item('ResponsibleEmail');
                    }
                    else {
                        author = result.get_item('Author');
                        authorName = author.get_lookupValue();
                        authorEmail = author.get_email();
                    }

                    var authorImage = '/_layouts/15/userphoto.aspx?accountname=' + authorEmail;

                    var product = result.get_item('Product');
                    var productFamily = result.get_item('ProductFamily');
                    var size = result.get_item('File_x0020_Size');
                    var id = result.get_item("ID");
                    var contentType = result.get_item('ContentTypeId').toString();
                    var enablers = result.get_item('Enabler');
                    var modified = null;

                    if (result.get_item('FechaOrigen') != null) {
                        modified = result.get_item('FechaOrigen');
                    }
                    else {
                        modified = result.get_item('Created').toLocaleDateString();
                    }

                    var docImage = result.get_item('DocumentTypeImage');
                    if (docImage == null)
                        docImage = '/sites/B2BFinder.OCEWU/PublishingImages/pexels-photo-272980.jpeg';

                    size = bytesToSize(size);

                    var url = result.get_item('FileRef');
                    var viewurl = getFileUrl(url);

                    var description = result.get_item('DocumentDescription');

                    getRelatedDocuments(contentType, product, productFamily, id);
                    loadEnablers(enablers);

                    $('#doc-info-header').css('background', "url('" + docImage + "')");
                    $('#doc-info-header').css('background-size', "cover");

                    $('#doc-title').text(title);
                    $('#doc-date').text(modified);
                    $('#doc-use').text(use);
                    $('#doc-language').text(language);
                    $('#author-image').css('background', "url('" + authorImage + "')");
                    $('[data-id="author-name"]').text(authorName);
                    $('#author-mail').text(authorEmail);
                    $('#doc-size').text(size);
                    $('#doc-description').text(description);

                    $('#btnViewDocument').attr('href', viewurl);
                    $('#btnDownload').attr('href', url);
                    $('#btnDownload').attr('download', title);

                    $('#btnHistory').on('click', function () {
                        var isDisabled = $(this).closest('.disabled').length > 0;
                        if (!isDisabled)
                            openDialog('https://telefonicacorp.sharepoint.com/sites/B2BFinder.OCEWU/es-es/_layouts/15/Versions.aspx?List=39190973-ddd1-426a-9d07-3e5814c9a81e&id=' + result.get_id() + '&FileName=' + url, 'Historial de versiones');
                    });

                    $('#author-profile').attr('href', '/_layouts/15/me.aspx/?p=#' + authorEmail + '&v=work');
                    var editUrl = _spPageContextInfo.webAbsoluteUrl + '/paginas/editar-documento.aspx?';

                    if (checkOutUserId == _spPageContextInfo.userId || checkOutUserId == -1) {
                        if (result.get_item('HTML_x0020_File_x0020_Type') == "SharePoint.Link") {
                            editUrl = _spPageContextInfo.webAbsoluteUrl + '/paginas/enlace-a-documento.aspx?';
                        }

                        $('#btnEditDocument').attr('href', editUrl + 'docid=' + docId + '&doc=' + doc);
                    }
                    else
                        $('#btnEditDocument').closest('div.col').hide();

                    setPermissions();

                    var version = fileItem.get_item('_UIVersionString');
                    if (Number(version) % 1 > 0 && checkOutUserId == _spPageContextInfo.userId) {
                        $('#publish-div').removeClass('hide');
                        $('#btnPublish').on('click', function () {
                            var clientContext = new SP.ClientContext(_spPageContextInfo.webAbsoluteUrl);
                            var oWeb = clientContext.get_web();
                            var oFile = oWeb.getFileByServerRelativeUrl(doc);
                            if (checkout)
                                oFile.checkIn();

                            oFile.publish();
                            clientContext.load(oFile);
                            clientContext.executeQueryAsync(
                                function (sender, args) {
                                    bootbox.alert({
                                        title: translate('Información'),
                                        message: translate('DocPublishOk'),
                                        closeButton: false,
                                        callback: function () {
                                            window.location.reload();
                                        }
                                    });
                                },
                                function (sender, args) {
                                    console.log('Request failed. ' + args.get_message() + '\n' + args.get_stackTrace());
                                    noResults();
                                }
                            );
                        });
                    }

                    setToolbar();
                }
                catch {
                    noResults();
                }
            },
            function (sender, args) {
                console.log('Request failed. ' + args.get_message() + '\n' + args.get_stackTrace());
                noResults();
            }
        );
    }
    else {
        noResults();
    }
}

function setToolbar() {
    checkFavourites();
    checkBookmarks();
    $('[data-command="share"]').each(function () {
        var share = new Share({
            element: $(this),
            title: translate('Share')
        });
    });

    $('[data-command="alert"]').each(function () {
        $(this).on('click', function () {
            var itemId = getUrlParam('docid');
            var url = '/sites/B2BFinder.OCEWU/es-es/_layouts/15/SubNew.aspx?List=39190973-ddd1-426a-9d07-3e5814c9a81e&ID=' + itemId + '&isdlg=1';

            openDialog(url, translate('Alerts'));
        });
    });
}

function getFileVersions(fileUrl) {
    var url = _spPageContextInfo.webAbsoluteUrl + "/_api/Web/GetFileByServerRelativeUrl('" + fileUrl + "')/Versions?$orderby=ID desc&$top=1";

    var $ajax = $.ajax({
        url: url,
        type: "GET",
        dataType: "json",
        headers: {
            Accept: "application/json;odata=verbose"
        }
    });

    $ajax.done(function (data, textStatus, jqXHR) {
        var results = data.d.results;
        var version = results[0].VersionLabel;
        if (Number(version) % 1 > 0) {
            $('#publish-div').removeClass('hide');
            $('#btnPublish').on('click', function () {
                var clientContext = new SP.ClientContext(_spPageContextInfo.webAbsoluteUrl);
                var oWeb = clientContext.get_web();
                var oFile = oWeb.getFileByServerRelativeUrl(fileUrl);
                if (checkOut)
                    oFile.checkIn();

                oFile.publish();
                clientContext.load(oFile);
                clientContext.executeQueryAsync(
                    function (sender, args) {
                        bootbox.alert(translate('DocPublishOk'));
                    },
                    function (sender, args) {
                        console.log('Request failed. ' + args.get_message() + '\n' + args.get_stackTrace());
                        noResults();
                    }
                );
            });
        }
    });
}

function noResults() {
    $('#document-info-page').hide();

    bootbox.alert({
        title: translate("Information"),
        message: translate('DocNotFound'),
        callback: function () {
            window.location.href = document.referrer;
        }
    });
}

function getRelatedDocuments(contentType, product, productFamily, itemId) {
    var clientContext = SP.ClientContext.get_current();
    var list = clientContext.get_web().get_lists().getByTitle('Documentos B2B');
    var camlQuery = new SP.CamlQuery();
    camlQuery.set_viewXml("<View Scope='RecursiveAll'><Query><Where><And><BeginsWith><FieldRef Name='ContentTypeId' /><Value Type='Text'>" + contentType + "</Value></BeginsWith><Neq><FieldRef Name='ID' /><Value Type='Counter'>" + itemId + "</Value></Neq></And></Where></Query><RowLimit Paged='False'>12</RowLimit></View>");

    var items = list.getItems(camlQuery);
    clientContext.load(items);

    clientContext.executeQueryAsync(
        function () {
            var listItemEnumerator = items.getEnumerator();
            var html = '';
            var hasDocuments = false;
            while (listItemEnumerator.moveNext()) {
                hasDocuments = true;
                var result = listItemEnumerator.get_current();
                var fileRefParts = result.get_item('FileRef').split('.');
                var extension = fileRefParts[fileRefParts.length - 1];
                var fileName = result.get_item('FileLeafRef').replace('.' + extension, '');
                var title = result.get_item('Title');
                if (title == '' || title == null)
                    title = fileName;

                if (title.length > 45) {
                    title = title.substring(0, 42) + '...';
                }

                html += '<div class="col-4">';
                html += getDocumentIcon(extension);
                html += '<a href="' + result.get_item('FileRef') + '" target="_blank" class="anchor-icon">' + title + '</a>';
                html += '</div>';
            }

            if (hasDocuments)
                $('#documentByContentType').append(html);
            else
                $('#documentByContentType').closest('.row').hide();
        },
        function (sender, args) {
            console.log('Request failed. ' + args.get_message() + '\n' + args.get_stackTrace());
        }
    );
}

function getAuthorDocuments(author, itemId) {
    var clientContext = SP.ClientContext.get_current();
    var list = clientContext.get_web().get_lists().getByTitle('Documentos B2B');
    var camlQuery = new SP.CamlQuery();
    camlQuery.set_viewXml("<View><Query><Where><And><Eq><FieldRef Name='Author' LookupId='True'/><Value Type='Lookup'>" + author + "</Value></Eq><Neq><FieldRef Name='ID' /><Value Type='Counter'>" + itemId + "</Value></Neq></And></Where></Query><RowLimit Paged='False'>5</RowLimit></View>");

    var items = list.getItems(camlQuery);
    clientContext.load(items);

    clientContext.executeQueryAsync(
        function () {
            var listItemEnumerator = items.getEnumerator();
            var html = '';
            while (listItemEnumerator.moveNext()) {
                var result = listItemEnumerator.get_current();
                var extension = result.get_item('FileLeafRef').split('.')[1];
                var fileName = result.get_item('FileLeafRef').replace('.' + extension, '');
                html += '<li class="list-group-item">';
                html += getDocumentIcon(extension);
                html += '<a href="' + result.get_item('FileRef') + '" target="_blank" class="anchor-icon">' + fileName + '</a>';
                html += '</li>';
            }

            $('#documentsByUser').append(html);
        },
        function (sender, args) {
            console.log('Request failed. ' + args.get_message() + '\n' + args.get_stackTrace());
        }
    );
}

function loadEnablers(enablers) {
    if (enablers == null || enablers.length == 0) {
        $('#serviceEnablers').hide();
        return;
    }
    for (var i = 0; i < enablers.length; i++) {
        $('#serviceEnablers').append('<p>' + enablers[i] + '</p>');
    }
}

function setButtonMargins() {
    $('#buttons .col-6').each(function (index) {
        if (index > 1)
            $(this).addClass('mt-2');
        else
            $(this).removeClass('mt-2');
    });
}

function setPermissions() {
    $('[data-perm-mask]').each(function () {
        var $this = $(this);
        $(this).securityTrimmed({
            itemId: decodeURIComponent(getUrlParam('docid')),
            callback: function (result) {
                if (result)
                    $($this).show();
                else {
                    $($this).addClass('disabled');
                    $($this).find('a').removeAttr('href');
                }

                setButtonMargins();
            }
        });
    });
}