﻿var b2bContentTypeId = '0x010100E14B989B61244E439EC7531CCFC3AC1D';

$(document).ready(function () {
    SP.SOD.executeFunc('sp.js', 'SP.ClientContext', function () {
        loadDocumentTypes();
        getDocumentTypeName();

        $('#btnRename').on('click', function () {
            renameContentType();
        });

        $('#btnCreateNewCT').on('click', function () {
            createContentType();
        });
    });
});

function loadDocumentTypes() {
    if ($('#renameDocTypeList').length) {
        $('#renameDocTypeList').empty();

        var dialog = bootbox.dialog({
            title: "Información",
            message: '<div class="spinner-border text-info" role="status"><span class="sr-only">Loading...</span></div>' + translate('GettingInfo') + '...',
            closeButton: false
        });

        var clientContext = new SP.ClientContext(_spPageContextInfo.siteAbsoluteUrl);
        var contentTypes = clientContext.get_web().get_availableContentTypes();

        clientContext.load(contentTypes);

        clientContext.executeQueryAsync(
            function () {
                var results = [];
                var contentTypesEnumerator = contentTypes.getEnumerator();

                while (contentTypesEnumerator.moveNext()) {
                    var cType = contentTypesEnumerator.get_current();
                    var group = cType.get_group();
                    if (group.toLowerCase() == "b2b") {
                        results.push({
                            id: cType.get_id(),
                            name: cType.get_name()
                        });
                    }

                }

                results.sort(function (a, b) {
                    var textA = a.name.toUpperCase();
                    var textB = b.name.toUpperCase();
                    return (textA < textB) ? -1 : (textA > textB) ? 1 : 0;
                });

                for (var i = 0; i < results.length; i++) {
                    var result = results[i];
                    if (result.id.toString().indexOf('0x0120') == -1) {
                        $('#renameDocTypeList').append('<li><a href="rename-content-type.aspx?ctid=' + result.id + '&ctname=' + result.name + '">' + result.name + '</a></li>');
                    }
                }

                dialog.modal('hide');
            },
            function (sender, args) {
                dialog.modal('hide');
                console.log('Request failed. ' + args.get_message() + '\n' + args.get_stackTrace());
            }
        );
    }
}

function getDocumentTypeName() {
    if ($('#txtTitle').length) {
        var cTypeName = getUrlParam('ctname');

        if (cTypeName != '') {
            cTypeName = decodeURIComponent(cTypeName);

            $('#txtTitle').val(cTypeName);
        }
    }
}

function renameContentType() {
    var dialog = bootbox.dialog({
        title: "Información",
        message: '<div class="spinner-border text-info" role="status"><span class="sr-only">Loading...</span></div> ' + translate('ChangingName') + '...',
        closeButton: false
    });

    var cTypeId = decodeURIComponent(getUrlParam('ctid'));
    var clientContext = new SP.ClientContext(_spPageContextInfo.siteAbsoluteUrl);
    var contentTypes = clientContext.get_web().get_availableContentTypes();
    var ctype = contentTypes.getById(cTypeId);

    clientContext.load(ctype);

    clientContext.executeQueryAsync(
        function () {
            ctype.set_name($('#txtTitle').val());
            ctype.update(true);

            clientContext.load(ctype);
            clientContext.executeQueryAsync(
                function () {
                    renameChoiceField(function (result) {
                        if (result) {
                            dialog.modal('hide');

                            bootbox.alert({
                                title: translate('Information'),
                                message: '<p>' + translate('ChangeNameOk') + '</p>',
                                closeButton: false,
                                callback: function () {
                                    window.history.back();
                                }
                            });
                        }
                    });
                },
                function (sender, args) {
                    dialog.modal('hide');
                    bootbox.alert({
                        title: translate('Error'),
                        message: '<p>' + translate('Error') + '</p><p>' + args.get_message() + '</p>',
                        closeButton: false
                    });
                    console.log('Request failed. ' + args.get_message() + '\n' + args.get_stackTrace());
                }
            );
        },
        function (sender, args) {
            console.log('Request failed. ' + args.get_message() + '\n' + args.get_stackTrace());
        }
    );
}

function renameChoiceField(callback) {
    var clientContext = new SP.ClientContext(_spPageContextInfo.siteAbsoluteUrl);
    var field = clientContext.castTo(clientContext.get_web().get_availableFields().getByInternalNameOrTitle('B2B Content Type'), SP.FieldChoice);

    clientContext.load(field);

    clientContext.executeQueryAsync(
        function () {
            var oldName = decodeURIComponent(getUrlParam('ctname'));
            var newName = $('#txtTitle').val();

            var choices = field.get_choices();
            var hasChoice = false;

            for (var i = 0; i < choices.length; i++) {
                if (choices[i] == oldName) {
                    hasChoice = true;
                    choices[i] = newName;
                    break;
                }
            }

            if (!hasChoice)
                choices.push(newName);

            field.set_choices(choices);

            field.updateAndPushChanges();
            clientContext.executeQueryAsync(
                function () {
                    callback(true);
                },
                function (sender, args) {
                    callback(false);
                    console.log('Request failed. ' + args.get_message() + '\n' + args.get_stackTrace());
                }
            );
        },
        function (sender, args) {
            callback(false);
            console.log('Request failed. ' + args.get_message() + '\n' + args.get_stackTrace());
        }
    );
}

function createContentType() {
    if ($('#txtTitle').val() == '') {
        return;
    }

    var dialog = bootbox.dialog({
        title: translate('Information'),
        message: '<div class="spinner-border text-info" role="status"><span class="sr-only">Loading...</span></div>' + translate('Information') + '...',
        closeButton: false
    });

    var clientContext = new SP.ClientContext(_spPageContextInfo.siteAbsoluteUrl);
    var contentTypes = clientContext.get_web().get_contentTypes();
    var parentContentType = contentTypes.getById(b2bContentTypeId);

    var newContentType = new SP.ContentTypeCreationInformation();
    newContentType.set_name($('#txtTitle').val());
    newContentType.set_group('B2B');
    newContentType.set_parentContentType(parentContentType);

    contentTypes.add(newContentType);

    clientContext.load(contentTypes);

    clientContext.executeQueryAsync(
        function () {
            renameChoiceField(function (result) {
                if (result) {
                    dialog.modal('hide');

                    bootbox.alert({
                        title: translate('Information'),
                        message: '<p>' + translate('CreateDocumentTypeOk') + '</p>',
                        closeButton: false,
                        callback: function () {
                            window.history.back();
                        }
                    });
                }
            });
        },
        function (sender, args) {
            console.log('Request failed. ' + args.get_message() + '\n' + args.get_stackTrace());
        }
    );
}