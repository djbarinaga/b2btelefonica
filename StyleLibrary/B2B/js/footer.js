﻿(function ($) {
    $.fn.footer = function () {
        var $this = this;

        SP.SOD.executeFunc('sp.js', 'SP.ClientContext', function () {
            setSocialLinks();
            setCopyright();
        });

        function setSocialLinks() {
            var clientContext = new SP.ClientContext(_spPageContextInfo.siteAbsoluteUrl);
            var list = clientContext.get_web().get_lists().getByTitle('SocialLinks');
            var query = SP.CamlQuery.createAllItemsQuery();
            var items = list.getItems(query);
            clientContext.load(items);


            clientContext.executeQueryAsync(
                function () {
                    var listItemEnumerator = items.getEnumerator();

                    while (listItemEnumerator.moveNext()) {
                        var item = listItemEnumerator.get_current().get_fieldValues();
                        var url = item['URL'].get_url();
                        var icon = item['Icon'].get_url();

                        $($this).find('.social').append('<a href="' + url + '" target="_blank"><img src="' + icon + '"/></a>');
                    }
                },
                function (sender, args) {
                    console.log('Request failed. ' + args.get_message() + '\n' + args.get_stackTrace());
                }
            );
        }

        function setCopyright() {
            var clientContext = new SP.ClientContext(_spPageContextInfo.siteAbsoluteUrl);
            var list = clientContext.get_web().get_lists().getByTitle('FooterSettings');
            var query = SP.CamlQuery.createAllItemsQuery();
            var items = list.getItems(query);
            clientContext.load(items);


            clientContext.executeQueryAsync(
                function () {
                    var listItemEnumerator = items.getEnumerator();

                    while (listItemEnumerator.moveNext()) {
                        var item = listItemEnumerator.get_current().get_fieldValues();

                        if (item['Title'].toLowerCase() == 'copyright')
                            $($this).find('#copyright').text(item["Value"]);
                    }
                },
                function (sender, args) {
                    console.log('Request failed. ' + args.get_message() + '\n' + args.get_stackTrace());
                }
            );
        }
    };
}(jQuery));

$(document).ready(function () {
    $('#footer').footer();
});