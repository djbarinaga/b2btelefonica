﻿var keys = [
    {
        'culture': 'es',
        'keys': [
            {
                'key': 'ImageSelectorText',
                'value': 'Puede seleccionar uno o varios documentos para compartir, pinchando sobre la imagen'
            },
            {
                'key': 'ImageSelectorTitle',
                'value': 'Seleccionar documentos'
            },
            {
                'key': 'btnAddLinkTooltip',
                'value': 'Permite subir el link a un documento que está en otro repositorio (ej. OnDA, Learn4Sales, repositorio de Operaciones, etc.). De esta forma se puede evitar tener el mismo documento en varios repositorios'
            },
            {
                'key': 'Bookmarks',
                'value': 'Marcadores'
            },
            {
                'key': 'DeleteBookmarkConfirm',
                'value': '¿Desea eliminar este documento de sus marcadores?'
            },
            {
                'key': 'DeleteFavouriteConfirm',
                'value': '¿Desea eliminar este documento de sus favoritos?'
            },
            {
                'key': 'documents',
                'value': 'documentos'
            },
            {
                'key': 'Documents',
                'value': 'Documentos'
            },
            {
                'key': 'download',
                'value': 'descargar'
            },
            {
                'key': 'Download',
                'value': 'Descargar'
            },
            {
                'key': 'es',
                'value': 'Español'
            },
            {
                'key': 'en',
                'value': 'Inglés'
            },
            {
                'key': 'DocumentsB2B',
                'value': 'Documentos B2B'
            },
            {
                'key': 'Favourites',
                'value': 'Favoritos'
            },
            {
                'key': 'followUs',
                'value': 'Síguenos en'
            },
            {
                'key': 'highlights-m_',
                'value': 'destacados_'
            },
            {
                'key': 'Information',
                'value': 'Información'
            },
            {
                'key': 'NoFavouritesMessage',
                'value': 'Agregue documentos a sus favoritos para verlos aquí'
            },
            {
                'key': 'NoBookmarksMessage',
                'value': 'Agregue documentos a sus marcadores para verlos aquí'
            },
            {
                'key': 'RelatedDocuments',
                'value': 'Documentos relacionados'
            },
            {
                'key': 'Share',
                'value': 'Compartir'
            },
            {
                'key': 'Use',
                'value': 'Uso'
            },
            {
                'key': 'View',
                'value': 'Ver'
            },
            {
                'key': 'view',
                'value': 'ver'
            },
            {
                'key': 'VIEW',
                'value': 'VER'
            },
            {
                'key': 'viewall',
                'value': 'ver todos'
            },
            {
                'key': 'AddDocumentInfo',
                'value': 'Agregar información para el documento'
            },
            {
                'key': 'NewDocumentOk',
                'value': 'Los datos se han guardado correctamente.'
            },
            {
                'key': 'views',
                'value': 'vistas'
            },
            {
                'key': 'DropFiles',
                'value': 'Arrastra los ficheros aquí'
            },
            {
                'key': 'SelectFile',
                'value': 'Seleccionar ficheros'
            },
            {
                'key': 'Or',
                'value': 'o'
            },
            {
                'key': 'info',
                'value': 'info'
            },
            {
                'key': 'Country',
                'value': 'País'
            },
            {
                'key': 'Language',
                'value': 'Idioma'
            },
            {
                'key': 'ViewProfile',
                'value': 'Ver perfil'
            },
            {
                'key': 'Product',
                'value': 'Producto *'
            },
            {
                'key': 'Edit',
                'value': 'Editar'
            },
            {
                'key': 'Title',
                'value': 'Título *'
            },
            {
                'key': 'DocumentType',
                'value': 'Tipo de documento *'
            },
            {
                'key': 'ProductFamily',
                'value': 'Familia de producto *'
            },
            {
                'key': 'ExpiredDate',
                'value': 'Fecha de caducidad'
            },
            {
                'key': 'Description',
                'value': 'Descripción'
            },
            {
                'key': 'InternalUse',
                'value': 'Uso interno'
            },
            {
                'key': 'Save',
                'value': 'Guardar'
            },
            {
                'key': 'Delete',
                'value': 'Eliminar'
            },
            {
                'key': 'AddDocumentInfo',
                'value': 'Añadir información'
            },
            {
                'key': 'Cancel',
                'value': 'Cancelar'
            },
            {
                'key': 'DeleteDocumentConfirm',
                'value': '¿Desea eliminar este documento?'
            },
            {
                'key': 'DeleteDocumentConfirmTitle',
                'value': 'Eliminar documento'
            },
            {
                'key': 'DeleteDocumentOk',
                'value': '<p>El documento se ha eliminado correctamente.</p><p>Seguirá apareciendo en los resultados de búsqueda hasta que se realice la siguiente indexación.</p>'
            },
            {
                'key': 'DeleteDocumentOkTitle',
                'value': 'Eliminar documento'
            },
            {
                'key': 'FavouritesLight',
                'value': 'Desde esta página puedes ver y gestionar de <br/>forma sencilla'
            },
            {
                'key': 'FavouritesRegular',
                'value': 'todos tus documentos favoritos'
            },
            {
                'key': 'BookmarksXLight',
                'value': 'Desde esta página puedes ver y gestionar de <br/>forma sencilla'
            },
            {
                'key': 'BookmarksRegular',
                'value': 'todos tus marcadores'
            },
            {
                'key': 'SearchResults',
                'value': 'Resultados de'
            },
            {
                'key': 'search_',
                'value': 'búsqueda_'
            },
            {
                'key': 'Use:',
                'value': 'Uso:'
            },
            {
                'key': 'Language:',
                'value': 'Idioma:'
            },
            {
                'key': 'Format:',
                'value': 'Formato:'
            },
            {
                'key': 'Size:',
                'value': 'Tamaño:'
            },
            {
                'key': 'New',
                'value': 'Nuevo'
            },
            {
                'key': 'document_',
                'value': 'documento_'
            },
            {
                'key': 'Administration_',
                'value': 'Administración_'
            },
            {
                'key': 'My',
                'value': 'Mis'
            },
            {
                'key': 'favourites_',
                'value': 'favoritos_'
            },
            {
                'key': 'bookmarks_',
                'value': 'marcadores_'
            },
            {
                'key': 'History',
                'value': 'Historial'
            },
            {
                'key': 'document_',
                'value': 'documento_'
            },
            {
                'key': 'Type',
                'value': 'Tipo'
            },
            {
                'key': 'Document',
                'value': 'Documento'
            },
            {
                'key': 'HighlightDocuments',
                'value': '<span class="f-light">Documentos</span><br /><span class="f-regular">destacados</span>'
            },
            {
                'key': 'Mis favoritos',
                'value': 'Favoritos'
            },
            {
                'key': 'Mis marcadores',
                'value': 'Marcadores'
            },
            {
                'key': 'FavouriteAdded',
                'value': 'Documento añadido a tus favoritos.'
            },
            {
                'key': 'DocumentInternalUse',
                'value': 'Documento de uso interno'
            },
            {
                'key': 'OtherDocumentsOf',
                'value': 'Otros documentos de'
            },
            {
                'key': 'EditProperties',
                'value': 'Editar propiedades / Eliminar'
            },
            {
                'key': 'SaveDocumentTitle1',
                'value': 'El documento se guardará con el siguiente título'
            },
            {
                'key': 'SaveDocumentTitle2',
                'value': '¿Desea continuar?'
            },
            {
                'key': 'SaveDocumentTitleTitle',
                'value': 'Confirmación de título'
            },
            {
                'key': 'GettingInfo',
                'value': 'Obteniendo información'
            },
            {
                'key': 'ChangingName',
                'value': 'Cambiando nombre'
            },
            {
                'key': 'ChangeNameOk',
                'value': 'El nombre del tipo de documento se ha cambiado correctamente.'
            },
            {
                'key': 'ChangeNameError',
                'value': 'Ha ocurrido un error al intentar cambiar el nombre del tipo de contenido.'
            },
            {
                'key': 'CreatingDocumentType',
                'value': 'Creando tipo de documento'
            },
            {
                'key': 'CreateDocumentTypeOk',
                'value': 'El tipo de documento se ha creado correctamente.'
            },
            {
                'key': 'DocPublishOk',
                'value': 'El documento se ha publicado correctamente.'
            },
            {
                'key': 'DocNotFound',
                'value': '<p>No se ha encontrado el documento.</p><p>Es posible que otro usuario haya modificado las propiedades del documento y éste haya cambiado de ubicación. Cuando se modifican las propiedades de un documento se reubica en la carpeta correspondiente. Para que se actualice la nueva dirección es necesario esperar unos minutos hasta que el servicio de búsqueda vuelva a indexar el documento en su nueva ubicación.</p><p>El proceso de indexación se realiza cada 15 minutos. Si pasado ese tiempo el problema persiste, póngase en contacto con el administrador.</p>'
            },
            {
                'key': 'TitleRequired',
                'value': 'Introduzca un valor para el campo Título.'
            },
            {
                'key': 'UrlRequired',
                'value': 'Introduzca un valor para el campo Dirección URL del documento.'
            },
            {
                'key': 'ContentTypeRequired',
                'value': 'Introduzca un valor para el campo Tipo de documento.'
            },
            {
                'key': 'ProductFamilyRequired',
                'value': 'Introduzca un valor para el campo Familia de producto.'
            },
            {
                'key': 'ProductRequired',
                'value': 'Introduzca un valor para el campo Producto.'
            },
            {
                'key': 'SelectDocumentType',
                'value': 'Seleccione un tipo de documento.'
            },
            {
                'key': 'SelectProduct',
                'value': 'Seleccione un producto.'
            },
            {
                'key': 'Saving',
                'value': 'Guardando'
            },
            {
                'key': 'SavingDocInfo',
                'value': 'Guardando información del documento'
            },
            {
                'key': 'SetPermissionsFor',
                'value': 'Estableciendo permisos para'
            },
            {
                'key': 'CheckOutDoc',
                'value': 'Desprotegiendo documento'
            },
            {
                'key': 'PublishDoc',
                'value': 'Publicando documento'
            },
            {
                'key': 'CreatingCompressedFile',
                'value': 'Generando archivo comprimido'
            },
            {
                'key': 'UploadingFiles',
                'value': 'Subiendo archivos'
            },
            {
                'key': 'EndProcess',
                'value': 'Finalizando proceso'
            },
            {
                'key': 'SelectProductFamily',
                'value': 'Seleccione una familia'
            },
            {
                'key': 'Spanish',
                'value': 'Español'
            },
            {
                'key': 'English',
                'value': 'Inglés'
            },
            {
                'key': 'Documentos',
                'value': 'Documentos'
            },
            {
                'key': 'Date:',
                'value': 'Fecha:'
            },
            {
                'key': 'RefinableString01',
                'value': 'Producto'
            },
            {
                'key': 'RefinableString06',
                'value': 'Idioma'
            },
            {
                'key': 'SPContentType',
                'value': 'Tipo de documento'
            },
            {
                'key': 'RefinableString05',
                'value': 'Service Enabler'
            },
            {
                'key': 'RefinableString00',
                'value': 'Familia de productos'
            },
            {
                'key': 'RefinableString02',
                'value': 'Idioma'
            },
            {
                'key': 'FileType',
                'value': 'Formato'
            },
            {
                'key': 'RefinableString04',
                'value': 'Uso'
            },
            {
                'key': 'ShowMore',
                'value': 'Mostrar más'
            },
            {
                'key': 'ShowLess',
                'value': 'Mostrar menos'
            },
            {
                'key': 'SelectOption',
                'value': 'Selecciona una opción'
            },
            {
                'key': 'AddLinkToDocument',
                'value': 'Agregar enlace a documento'
            },
            {
                'key': 'txtSearchPlaceholder',
                'value': '¡Escribe lo que quieras buscar!'
            },
            {
                'key': 'LinkTo',
                'value': 'Enlace a'
            },
            {
                'key': 'CreatingLinkToDocument',
                'value': 'Creando enlace a documento'
            },
            {
                'key': 'DocumentUrl',
                'value': 'Dirección URL del documento *'
            },
            {
                'key': 'ResponsibleEmail',
                'value': 'Email del responsable'
            },
            {
                'key': 'UserNotFound',
                'value': 'No se ha encontrado el usuario.'
            },
            {
                'key': 'ShowingResults',
                'value': 'Mostrando resultados'
            },
            {
                'key': 'to',
                'value': 'a'
            },
            {
                'key': 'of',
                'value': 'de'
            },
            {
                'key': 'results',
                'value': 'resultados'
            },
            {
                'key': 'NewDocumentWarning',
                'value': 'recuerda que si quieres subir una nueva versión de un documento que ya está subido en el repositorio debes mantener el mismo nombre, en caso contrario se considerará que es un documento nuevo y aparecerán visibles ambos documentos'
            },
            {
                'key': 'IMPORTANT',
                'value': 'IMPORTANTE:'
            },
            {
                'key': 'UncheckAll',
                'value': 'Desmarcar todos'
            },
            {
                'key': 'UncheckAll-All',
                'value': 'Desmarcar todos los filtros'
            },
            {
                'key': 'CopyLinks',
                'value': 'Copiar enlaces'
            },
            {
                'key': 'SelectDocument',
                'value': 'Seleccionar documento'
            },
            {
                'key': 'sortBy',
                'value': 'Ordenar por'
            }
        ]
    },
    {
        'culture': 'en',
        'keys': [
            {
                'key': 'ImageSelectorText',
                'value': 'You can select one or more documents to share by clicking on the image'
            },
            {
                'key': 'ImageSelectorTitle',
                'value': 'Select documents'
            },
            {
                'key': 'btnAddLinkTooltip',
                'value': 'It allows you to upload the link to a document that is in another repository (OnDA, Learn4Sales, repositorio de Operaciones...). This way you can avoid having the same document in several repositories.'
            },
            {
                'key': 'RefinableString06',
                'value': 'Language'
            },
            {
                'key': 'sortBy',
                'value': 'Sort By'
            },
            {
                'key': 'RefinableString02',
                'value': 'Language'
            },
            {
                'key': 'SelectDocument',
                'value': 'Select document'
            },
            {
                'key': 'CopyLinks',
                'value': 'Copy links'
            },
            {
                'key': 'UncheckAll',
                'value': 'Uncheck all'
            },
            {
                'key': 'UncheckAll-All',
                'value': 'Uncheck all filters'
            },
            {
                'key': 'NewDocumentWarning',
                'value': 'remember that if you want to upload a new version of a document that is already uploaded in the repository you must keep the same name, otherwise it will be considered a new document and both documents will be visible'
            },
            {
                'key': 'IMPORTANT',
                'value': 'IMPORTANT:'
            },
            {
                'key': 'ShowingResults',
                'value': 'Showing results'
            },
            {
                'key': 'UserNotFound',
                'value': 'User not found.'
            },
            {
                'key': 'ResponsibleEmail',
                'value': 'Responsible email'
            },
            {
                'key': 'DocumentUrl',
                'value': 'Document URL'
            },
            {
                'key': 'CreatingLinkToDocument',
                'value': 'Creating link document'
            },
            {
                'key': 'LinkTo',
                'value': 'Link to'
            },
            {
                'key': 'txtSearchPlaceholder',
                'value': 'Type here what you are looking for!'
            },
            {
                'key': 'AddLinkToDocument',
                'value': 'Add link to Document'
            },
            {
                'key': 'SelectOption',
                'value': 'Select an Option'
            },
            {
                'key': 'ShowMore',
                'value': 'Show More'
            },
            {
                'key': 'ShowLess',
                'value': 'Show Less'
            },
            {
                'key': 'RefinableString01',
                'value': 'Product'
            },
            {
                'key': 'SPContentType',
                'value': 'Document Type'
            },
            {
                'key': 'RefinableString05',
                'value': 'Service Enabler'
            },
            {
                'key': 'RefinableString00',
                'value': 'Product Family'
            },
            {
                'key': 'FileType',
                'value': 'Format'
            },
            {
                'key': 'RefinableString04',
                'value': 'Use'
            },
            {
                'key': 'Producto',
                'value': 'Product'
            },
            {
                'key': 'Familia de producto',
                'value': 'Product Family'
            },
            {
                'key': 'Uso',
                'value': 'Use'
            },
            {
                'key': 'Tipo de documento',
                'value': 'Document Type'
            },
            {
                'key': 'Tipo de archivo',
                'value': 'File Type'
            },
            {
                'key': 'Spanish',
                'value': 'Spanish'
            },
            {
                'key': 'English',
                'value': 'English'
            },
            {
                'key': 'SelectProductFamily',
                'value': 'Select family'
            },
            {
                'key': 'Bookmarks',
                'value': 'Boomarks'
            },
            {
                'key': 'DeleteBookmarkConfirm',
                'value': 'Do you want to remove this document from your bookmarks?'
            },
            {
                'key': 'DeleteFavouriteConfirm',
                'value': 'Do you want to remove this document from your favorites?'
            },
            {
                'key': 'documents',
                'value': 'documents'
            },
            {
                'key': 'Documents',
                'value': 'Documents'
            },
            {
                'key': 'download',
                'value': 'download'
            },
            {
                'key': 'Download',
                'value': 'Download'
            },
            {
                'key': 'es',
                'value': 'Spanish'
            },
            {
                'key': 'en',
                'value': 'English'
            },
            {
                'key': 'DocumentsB2B',
                'value': 'B2B Documents'
            },
            {
                'key': 'Favourites',
                'value': 'Favorites'
            },
            {
                'key': 'followUs',
                'value': 'Follow us'
            },
            {
                'key': 'highlights-m_',
                'value': 'Featured_'
            },
            {
                'key': 'Information',
                'value': 'Information'
            },
            {
                'key': 'NoFavouritesMessage',
                'value': 'Add documents to your favorites to see them here'
            },
            {
                'key': 'NoBookmarksMessage',
                'value': 'Add documents to your bookmarks to see them here'
            },
            {
                'key': 'RelatedDocuments',
                'value': 'Related documents'
            },
            {
                'key': 'Share',
                'value': 'Share'
            },
            {
                'key': 'Use',
                'value': 'Use'
            },
            {
                'key': 'View',
                'value': 'View'
            },
            {
                'key': 'view',
                'value': 'view'
            },
            {
                'key': 'VIEW',
                'value': 'VIEW'
            },
            {
                'key': 'viewall',
                'value': 'view all'
            },
            {
                'key': 'AddDocumentInfo',
                'value': 'Add info for the document'
            },
            {
                'key': 'NewDocumentOk',
                'value': 'The data has been saved correctly.'
            },
            {
                'key': 'views',
                'value': 'views'
            },
            {
                'key': 'DropFiles',
                'value': 'Drop files here'
            },
            {
                'key': 'SelectFile',
                'value': 'Select files'
            },
            {
                'key': 'Or',
                'value': 'or'
            },
            {
                'key': 'info',
                'value': 'info'
            },
            {
                'key': 'Country',
                'value': 'Country'
            },
            {
                'key': 'Language',
                'value': 'Language'
            },
            {
                'key': 'ViewProfile',
                'value': 'View profile'
            },
            {
                'key': 'Product',
                'value': 'Product *'
            },
            {
                'key': 'Edit',
                'value': 'Edit'
            },
            {
                'key': 'Title',
                'value': 'Title *'
            },
            {
                'key': 'DocumentType',
                'value': 'Document Type *'
            },
            {
                'key': 'ProductFamily',
                'value': 'Product family *'
            },
            {
                'key': 'ExpiredDate',
                'value': 'Date of expiry'
            },
            {
                'key': 'Description',
                'value': 'Description'
            },
            {
                'key': 'InternalUse',
                'value': 'Internal Use'
            },
            {
                'key': 'Save',
                'value': 'Save'
            },
            {
                'key': 'Delete',
                'value': 'Delete'
            },
            {
                'key': 'AddDocumentInfo',
                'value': 'Add information'
            },
            {
                'key': 'Cancel',
                'value': 'Cancel'
            },
            {
                'key': 'DeleteDocumentConfirm',
                'value': 'Do you want to remove this document?'
            },
            {
                'key': 'DeleteDocumentConfirmTitle',
                'value': 'Remove document'
            },
            {
                'key': 'DeleteDocumentOk',
                'value': '<p> The document has been successfully deleted. </p> <p> It will continue to appear in the search results until the next index is done. </p>'
            },
            {
                'key': 'DeleteDocumentOkTitle',
                'value': 'Remove document'
            },
            {
                'key': 'FavouritesLight',
                'value': 'From this page you can easily view and</br>manage'
            },
            {
                'key': 'FavouritesRegular',
                'value': 'all your favorite documents'
            },
            {
                'key': 'BookmarksXLight',
                'value': 'From this page you can easily view and</br>manage'
            },
            {
                'key': 'BookmarksRegular',
                'value': 'all your bookmarks'
            },
            {
                'key': 'SearchResults',
                'value': 'Search'
            },
            {
                'key': 'search_',
                'value': 'results_'
            },
            {
                'key': 'Use:',
                'value': 'Use:'
            },
            {
                'key': 'Language:',
                'value': 'Language:'
            },
            {
                'key': 'Format:',
                'value': 'Format:'
            },
            {
                'key': 'Size:',
                'value': 'Size:'
            },
            {
                'key': 'New',
                'value': 'New'
            },
            {
                'key': 'document_',
                'value': 'document_'
            },
            {
                'key': 'Administration_',
                'value': 'Administration_'
            },
            {
                'key': 'My',
                'value': 'My'
            },
            {
                'key': 'favourites_',
                'value': 'favorites_'
            },
            {
                'key': 'bookmarks_',
                'value': 'bookmarks_'
            },
            {
                'key': 'History',
                'value': 'History'
            },
            {
                'key': 'document_',
                'value': 'document_'
            },
            {
                'key': 'Type',
                'value': 'Type'
            },
            {
                'key': 'Document',
                'value': 'Document'
            },
            {
                'key': 'HighlightDocuments',
                'value': '<span class="f-light">Featured</span><br /><span class="f-regular">documents</span>'
            },
            {
                'key': 'Mis favoritos',
                'value': 'Favorites'
            },
            {
                'key': 'Mis marcadores',
                'value': 'Bookmarks'
            },
            {
                'key': 'FavouriteAdded',
                'value': 'Document added to your favorites..'
            },
            {
                'key': 'DocumentInternalUse',
                'value': 'Internal use document'
            },
            {
                'key': 'OtherDocumentsOf',
                'value': 'Other documents of'
            },
            {
                'key': 'EditProperties',
                'value': 'Edit Properties / Delete'
            },
            {
                'key': 'SaveDocumentTitle1',
                'value': 'The document will be saved with the following title'
            },
            {
                'key': 'SaveDocumentTitle2',
                'value': 'Do you want to continue?'
            },
            {
                'key': 'SaveDocumentTitleTitle',
                'value': 'Title confirmation'
            },
            {
                'key': 'GettingInfo',
                'value': 'Gathering information'
            },
            {
                'key': 'ChangingName',
                'value': 'Changing name'
            },
            {
                'key': 'ChangeNameOk',
                'value': 'The name of the document type has been changed successfully.'
            },
            {
                'key': 'ChangeNameError',
                'value': 'An error occurred while trying to rename the content type.'
            },
            {
                'key': 'CreatingDocumentType',
                'value': 'Creating document type'
            },
            {
                'key': 'CreateDocumentTypeOk',
                'value': 'The document type was created successfully.'
            },
            {
                'key': 'DocPublishOk',
                'value': 'The document has been published successfully.'
            },
            {
                'key': 'DocNotFound',
                'value': 'The document was not found.'
            },
            {
                'key': 'TitleRequired',
                'value': 'Enter a value for the Title field.'
            },
            {
                'key': 'UrlRequired',
                'value': 'Enter a value for the URL field.'
            },
            {
                'key': 'ContentTypeRequired',
                'value': 'Enter a value for the Document Type field.'
            },
            {
                'key': 'ProductFamilyRequired',
                'value': 'Enter a value for the Product Family field.'
            },
            {
                'key': 'ProductRequired',
                'value': 'Enter a value for the Product field.'
            },
            {
                'key': 'SelectDocumentType',
                'value': 'Select Document Type.'
            },
            {
                'key': 'SelectProduct',
                'value': 'Select Product.'
            },
            {
                'key': 'Saving',
                'value': 'Saving'
            },
            {
                'key': 'SavingDocInfo',
                'value': 'Saving document information'
            },
            {
                'key': 'SetPermissionsFor',
                'value': 'Setting permissions for'
            },
            {
                'key': 'CheckOutDoc',
                'value': 'Unchecking document'
            },
            {
                'key': 'PublishDoc',
                'value': 'Publishing document'
            },
            {
                'key': 'CreatingCompressedFile',
                'value': 'Generating compressed file'
            },
            {
                'key': 'UploadingFiles',
                'value': 'Uploading files'
            },
            {
                'key': 'EndProcess',
                'value': 'Finishing process'
            },
            {
                'key': 'Inicio',
                'value': 'Home'
            },
            {
                'key': 'Nuevo documento',
                'value': 'New document'
            },
            {
                'key': 'Administración',
                'value': 'Administration'
            },
            {
                'key': 'Documentos',
                'value': 'Documents'
            },
            {
                'key': 'Documento de concepto',
                'value': 'Concept document'
            },
            {
                'key': 'Formularios',
                'value': 'Forms'
            },
            {
                'key': 'Marketing Manual',
                'value': 'Manual Marketing'
            },
            {
                'key': 'Anexos contractuales servicio',
                'value': 'Contractual annexes service'
            },
            {
                'key': 'Cotizador',
                'value': 'Quote'
            },
            {
                'key': 'Mapa de cobertura',
                'value': 'Coverage map'
            },
            {
                'key': 'Guías de uso',
                'value': 'Usage guides'
            },
            {
                'key': 'Plantilla de oferta',
                'value': 'Offer Template'
            },
            {
                'key': 'Presentación comercial a clientes',
                'value': 'Commercial presentation to customers'
            },
            {
                'key': 'Presentación preventas - ventas',
                'value': 'Presales presentation - sales'
            },
            {
                'key': 'Formaciones',
                'value': 'Training'
            },
            {
                'key': 'Casos de éxito',
                'value': 'Success stories'
            },
            {
                'key': 'Infografías',
                'value': 'Infographics'
            },
            {
                'key': 'Vídeos',
                'value': 'Videos'
            },
            {
                'key': 'Informes de analistas',
                'value': 'Analyst Reports'
            },
            {
                'key': 'Propuesta de valor B2B',
                'value': 'B2B Value Proposition'
            },
            {
                'key': 'Planes de activación',
                'value': 'Activation plans'
            },
            {
                'key': 'Manual técnico',
                'value': 'Technical manual'
            },
            {
                'key': 'Manual operativo',
                'value': 'Manual operativo'
            },
            {
                'key': 'Otros documentos DSS - Operaciones',
                'value': 'Other DSS documents - Operations'
            },
            {
                'key': 'Otros materiales',
                'value': 'Other materials'
            },
            {
                'key': 'Documento B2B',
                'value': 'B2B Document'
            }
        ]
    }
];

function translate(key) {
    
    var value = key;
    var currentKey = getCultureKey();
    for (var i = 0; i < currentKey.length; i++) {
        if (currentKey[i].key == key) {
            value = currentKey[i].value;
            break;
        }
    }

    return value;
}

function translateAll(element) {
    if (element != null) {
        $(element).find('[data-key]').each(function () {
            var text = translate($(this).data('key'));
            $(this).html(text);
        });
    }
}

function getCultureKey() {
    var currentKey = null;
    var culture = getCookie('culture').toLowerCase();
    for (var i = 0; i < keys.length; i++) {
        if (keys[i].culture == culture) {
            currentKey = keys[i].keys;
            break;
        }
    }

    return currentKey;
}

$(document).ready(function () {
    $('[data-key]').each(function () {
        var text = translate($(this).data('key'));
        $(this).html(text);
    });
});