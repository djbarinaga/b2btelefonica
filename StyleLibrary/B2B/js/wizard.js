﻿var canceled = false;
var tour;
var urlSteps;

var steps = [
    {
        'culture': 'es',
        'keys': [
            {
                url: '/sites/B2BFinder.OCEWU/es-es/Paginas/home.aspx',
                element: "#txtSearch",
                placement: "left",
                title: "Caja de Búsqueda",
                content: "Éste es el buscador de B2B Finder. Escribe el texto que quieras encontrar y pulsar la tecla Enter. Puedes utilizar también comodines de búsquedas como el asterisco (*) para encontrar la documentación que estás buscando.",
                callback: function () {
                    startWizard(urlSteps.length - 1);
                }
            },
            {
                url: '/sites/B2BFinder.OCEWU/es-es/Paginas/home.aspx',
                element: "#btnSearch",
                placement: "left",
                title: "Botón de Búsqueda",
                content: "También puedes pulsar este botón para lanzar la búsqueda una vez hayas introducido el texto a buscar."
            },
            {
                url: '/sites/B2BFinder.OCEWU/es-es/Paginas/home.aspx',
                element: "#menuLinks li a:contains('Inicio')",
                placement: "right",
                title: "Página de inicio",
                content: "Al pulsar sobre este menú se accede a la página de inicio de B2B Finder."
            },
            {
                url: '/sites/B2BFinder.OCEWU/es-es/Paginas/home.aspx',
                element: "#menuLinks li a:contains('Documentos')",
                placement: "right",
                title: "Documentos",
                content: "Pulsa aquí para ver todos los documentos de B2B Finder."
            },
            {
                url: '/sites/B2BFinder.OCEWU/es-es/Paginas/home.aspx',
                element: "#menuLinks li a:contains('Nuevo documento')",
                placement: "right",
                title: "Nuevo documento",
                content: "Desde aquí podrás acceder a la página para cargar nuevos documentos en el repositorio."
            },
            {
                url: '/sites/B2BFinder.OCEWU/es-es/Paginas/home.aspx',
                element: "#menuLinks li a:contains('Administración')",
                placement: "right",
                title: "Administración",
                content: "Pulsa en este enlace para acceder al área de administración, donde podrás gestionar desde los valores de determinados campos hasta las listas de configuración o la estructura del propio repositorio."
            },
            {
                url: '/sites/B2BFinder.OCEWU/es-es/Paginas/home.aspx',
                element: "#menuLinks li a:contains('Favoritos')",
                placement: "right",
                title: "Favoritos",
                content: 'Pulsa en este enlace para acceder tus documentos favoritos. Los documentos favoritos son aquellos documentos en los que has pulsado el botón <span class="icon-heart-red" style="line-height:60px"><span class="path1"></span><span class="path2"></span></span>.'
            },
            {
                url: '/sites/B2BFinder.OCEWU/es-es/Paginas/home.aspx',
                element: "#menuLinks li a:contains('Marcadores')",
                placement: "right",
                title: "Favoritos",
                content: 'Pulsa en este enlace para acceder tus marcadores. Los marcadores son aquellos documentos en los que has pulsado el botón <span class="icon-bookmark-orange" style="line-height:60px"><span class="path1"></span><span class="path2"></span></span>.'
            },
            {
                url: '/sites/B2BFinder.OCEWU/es-es/Paginas/home.aspx',
                element: "#faq",
                placement: "left",
                title: "Preguntas frecuentes",
                content: '¿Echas algo en falta? Pulsa este enlace para acceder a las preguntas frequentes.'
            },
            {
                url: '/sites/B2BFinder.OCEWU/es-es/Paginas/home.aspx',
                element: "#languageBar",
                placement: "left",
                title: "Menú de idiomas",
                content: "Pulsando sobre este enlace se puede cambiar el idioma de la aplicación entre Español e Inglés."
            },
            {
                url: '/sites/B2BFinder.OCEWU/es-es/Paginas/home.aspx',
                element: "#carousel",
                placement: "bottom",
                title: "Carrusel de noticias",
                content: "Aquí aparecerán tres noticias de interés particular acerca de B2B Finder."
            },
            {
                url: '/sites/B2BFinder.OCEWU/es-es/Paginas/home.aspx',
                element: ".module",
                placement: "top",
                title: "Documentos destacados",
                content: "En esta área podrás ver los 9 documentos más destacados en función del número de likes y visitas que tengan los documentos."
            },
            {
                url: '/sites/B2BFinder.OCEWU/es-es/Paginas/home.aspx',
                element: "#viewAllDocuments",
                placement: "left",
                title: "Ver todos",
                content: "Pulsando este enlace podrás acceder a los resultados de búsqueda del repositorio para ver todos los documentos."
            },
            {
                url: '/sites/B2BFinder.OCEWU/es-es/Paginas/home.aspx',
                element: "#btnList",
                placement: "left",
                title: "Vista de lista",
                content: "Pulsa este botón si quieres ver los documentos en formato lista, tal y como se muestran en la pantalla de resultados de búsqueda."
            },
            {
                url: '/sites/B2BFinder.OCEWU/es-es/Paginas/home.aspx',
                element: "#btnCard",
                placement: "left",
                title: "Vista de tarjeta",
                content: "Pulsa este botón si quieres ver los documentos en formato tarjeta, tal y como se muestran ahora."
            },
            {
                url: '/sites/B2BFinder.OCEWU/es-es/Paginas/home.aspx',
                element: ".card-container:first",
                placement: "right",
                title: "Tarjeta",
                content: "Aqui puedes ver la información resumida del documento."
            },
            {
                url: '/sites/B2BFinder.OCEWU/es-es/Paginas/home.aspx',
                element: ".doc-type:first span",
                placement: "bottom",
                title: "Tipo de documento",
                content: "Este icono te indicará de forma rápida el formato del documento."
            },
            {
                url: '/sites/B2BFinder.OCEWU/es-es/Paginas/home.aspx',
                element: ".card-title:first",
                placement: "right",
                title: "Título del documento",
                content: "Este es el título del documento. Pulsando sobre él podrás acceder a la versión on line del documento siempre que sea un documento de Office."
            },
            {
                url: '/sites/B2BFinder.OCEWU/es-es/Paginas/home.aspx',
                element: ".card-info:first",
                placement: "right",
                title: "Información del documento",
                content: "Aquí verás la fecha de última modificación del documento, el idioma en el que está disponible y su peso."
            },
            {
                url: '/sites/B2BFinder.OCEWU/es-es/Paginas/home.aspx',
                element: ".card-image:first",
                placement: "right",
                title: "Imagen del documento",
                content: "Esta imagen te permitirá identificar de forma rápida el tipo de documento."
            },
            {
                url: '/sites/B2BFinder.OCEWU/es-es/Paginas/home.aspx',
                element: ".doc-enablers:first",
                placement: "right",
                title: "Service Enablers",
                content: "Si el documento tiene asociado uno o varios Service Enablers, en esta sección aparecerán todos los que tenga asignados."
            },
            {
                url: '/sites/B2BFinder.OCEWU/es-es/Paginas/home.aspx',
                element: '[data-command="favourite"]:first',
                placement: "right",
                title: "Favoritos",
                content: '<p>Este icono te permite agregar el documento a tus favoritos.</p><ul class="list-group list-group-flush"><li class="list-group-item"><span class="icon-heart-red" style="line-height:60px"><span class="path1"></span><span class="path2"></span></span>Indica que el documento ya está en tus favoritos. Si pulsas sobre el icono, podrás eliminarlo de tus favoritos.</li><li class="list-group-item"><span class="icon-heart-purple" style="line-height:60px"><span class="path1"></span><span class="path2"></span></span>Indica que el documento no está en tus favoritos. Pulsando sobre el icono podrás agregarlo a tus documentos favoritos.</li></ul>'
            },
            {
                url: '/sites/B2BFinder.OCEWU/es-es/Paginas/home.aspx',
                element: '[data-command="bookmark"]:first',
                placement: "right",
                title: "Marcadores",
                content: '<p>Este icono te permite agregar el documento a tus marcadores.</p><ul class="list-group list-group-flush"><li class="list-group-item"><span class="icon-bookmark-orange" style="line-height:60px"><span class="path1"></span><span class="path2"></span></span>Indica que el documento ya está en tus marcadores. Si pulsas sobre el icono, podrás eliminarlo de tus marcadores.</li><li class="list-group-item"><span class="icon-bookmark-purple" style="line-height:60px"><span class="path1"></span><span class="path2"></span></span>Indica que el documento no está en tus marcadores. Pulsando sobre el icono podrás agregarlo a tus documentos favoritos.</li></ul>'
            },
            {
                url: '/sites/B2BFinder.OCEWU/es-es/Paginas/home.aspx',
                element: '[data-command="share"]:first',
                placement: "right",
                title: "Compartir",
                content: 'Pulsa este botón para obtener la url del documento y poder copiarla para compartirla con otros usuarios.',
                callback: function () {
                    startWizard(urlSteps.length - 1);
                }
            },
            {
                url: '/sites/B2BFinder.OCEWU/es-es/Paginas/home.aspx',
                element: '[data-command="alert"]:first',
                placement: "right",
                title: "Alertas",
                content: 'Pulsa este botón para suscribirte a alertas de correo electrónico sobre el documento. Podrás especificar sobre qué acciones quieres recibir alertas y con qué frecuencia.',
                callback: function () {
                    startWizard(urlSteps.length - 1);
                }
            },
            {
                url: '/sites/B2BFinder.OCEWU/es-es/Paginas/home.aspx',
                element: '[data-key="info"]:first',
                placement: "right",
                title: "Información del documento",
                content: 'Pulsa este botón para acceder a la página de información del documento, donde podrás además realizar otras acciones y ver otros documentos relacionados.',
                callback: function () {
                    startWizard(urlSteps.length - 1);
                }
            },
            {
                url: '/sites/B2BFinder.OCEWU/es-es/Paginas/home.aspx',
                element: '[data-key="view"]:first',
                placement: "right",
                title: "Ver documento",
                content: 'Si el documento es un documento de Office (Word, Excel o PowerPoint), pulsando en este botón accederás al documento en la herramienta online correspondiente. Si no es un documento de Office, entonces se iniciará la descarga. Si tu navegador lo permite, también podrás abrir en el mismo un documento PDF.',
                callback: function () {
                    startWizard(urlSteps.length - 1);
                }
            },
            {
                url: '/sites/B2BFinder.OCEWU/es-es/Paginas/home.aspx',
                element: '[data-key="download"]:first',
                placement: "right",
                title: "Descargar documento",
                content: 'Pulsa este botón si quieres descargarte una versión del documento a tu equipo. Los cambios que realices en el documento no serán visibles por otras personas hasta que vuelvas a cargarlo al repositorio.',
                callback: function () {
                    startWizard(urlSteps.length - 1);
                }
            },
            {
                url: '/sites/B2BFinder.OCEWU/es-es/Paginas/home.aspx',
                element: "#anchorWizard",
                placement: "left",
                title: "Asistente",
                content: '<p>Recuerda que desde este enlace puedes lanzar el asistente cuando lo necesites.</p><p>Vamos a recargar la página para dejarlo todo como estaba.</p>',
                callback: function () {
                    window.location.reload();
                }
            },
            {
                url: '/sites/B2BFinder.OCEWU/es-es/Paginas/nuevo-documento.aspx',
                element: "#drag-div-container",
                placement: "top",
                title: "Cargar documento",
                content: 'Utiliza esta área para cargar tus documentos. Puedes arrastrar documentos desde tu ordenador o pulsar el botón <strong>Seleccionar fichero</strong> para seleccionar los ficheros a cargar.'
            },
            {
                url: '/sites/B2BFinder.OCEWU/es-es/Paginas/nuevo-documento.aspx',
                element: "#drag-div-container",
                placement: "top",
                title: "Cargar documento",
                content: 'Cuando arrastres o selecciones los ficheros, se iniciará automáticamente la carga de los mismos a B2BFinder.'
            },
            {
                url: '/sites/B2BFinder.OCEWU/es-es/Paginas/nuevo-documento.aspx',
                element: "#drag-div-container",
                placement: "top",
                title: "Cargar documento",
                content: 'Una vez finalice la carga, aparecerá un formulario para que completes la información de cada documento. Si tienes algún problema con el formulario, pulsa el botón del asistente del menú superior.'
            },
            {
                url: '/sites/B2BFinder.OCEWU/es-es/Paginas/nuevo-documento.aspx',
                element: "#document-metadata",
                placement: "top",
                title: "Formulario",
                content: "Este es el fomulario para completar los datos del documento."
            },
            {
                url: '/sites/B2BFinder.OCEWU/es-es/Paginas/nuevo-documento.aspx',
                parent: '#document-metadata',
                element: '[data-id="txtTitle"]:first',
                placement: "left",
                title: "Título",
                content: "Introduce aquí el título del documento."
            },
            {
                url: '/sites/B2BFinder.OCEWU/es-es/Paginas/nuevo-documento.aspx',
                parent: '#document-metadata',
                element: '[data-id="selectDocumentType"]:first',
                placement: "left",
                title: "Tipo de documento",
                content: "Selecciona un tipo de documento. Si no seleccionas ninguno, se asignará automáticamente el tipo de documento <strong>Documento B2B</strong>"
            },
            {
                url: '/sites/B2BFinder.OCEWU/es-es/Paginas/nuevo-documento.aspx',
                parent: '#document-metadata',
                element: '[data-id="selectProduct"]:first',
                placement: "left",
                title: "Producto",
                content: "Selecciona un producto."
            },
            {
                url: '/sites/B2BFinder.OCEWU/es-es/Paginas/nuevo-documento.aspx',
                parent: '#document-metadata',
                element: '[data-id="txtDescription"]:first',
                placement: "top",
                title: "Descripción",
                content: "Si quieres, puedes agregar una descripción al documento, que será útil para que el resto de usuarios puedan hacerse una idea rápida del contenido del documento."
            },
            {
                url: '/sites/B2BFinder.OCEWU/es-es/Paginas/nuevo-documento.aspx',
                parent: '#document-metadata',
                element: '[data-id="chkInternalUse"]:first',
                placement: "left",
                title: "Uso del documento",
                content: "Selecciona esta casilla si el documento es sólo de uso interno."
            },
            {
                url: '/sites/B2BFinder.OCEWU/es-es/Paginas/nuevo-documento.aspx',
                parent: '#document-metadata',
                element: '#btnSave',
                placement: "left",
                title: "Guardar",
                content: "Una vez completes todos los datos, pulsa este botón para guardar la información."
            },
            {
                url: '/sites/B2BFinder.OCEWU/es-es/Paginas/administracion.aspx',
                element: '#admin-links',
                placement: "top",
                title: "Administración",
                content: "<p>En esta página puedes configurar diferentes áreas de B2B Finder, como los grupos de usuarios, la asociación entre tipos de documentos y sus imágenes, los permisos por tipo de documento, o gestionar las preguntas frecuentes o los banners de la Home.</p><p>Pasa el ratón por encima de cada tarjeta para ver una descripción de lo que puedes hacer.</p>"
            },
            {
                url: '/sites/B2BFinder.OCEWU/es-es/Paginas/administracion.aspx',
                element: 'div[data-group="Permisos"]',
                placement: "top",
                title: "Permisos",
                content: "En esta sección encontrarás los elementos para gestionar los permisos de la aplicación."
            },
            {
                url: '/sites/B2BFinder.OCEWU/es-es/Paginas/administracion.aspx',
                element: 'div[data-group="Listas de configuración"]',
                placement: "top",
                title: "Listas de configuración",
                content: "En esta sección encontrarás los elementos para gestionar las listas que permiten configurar diferentes aspectos de B2B Finder."
            },
            {
                url: '/sites/B2BFinder.OCEWU/es-es/Paginas/administracion.aspx',
                element: 'div[data-group="Alta de documento"]',
                placement: "top",
                title: "Alta de documento",
                content: "Desde esta sección puedes gestionar aquellos elementos que influyen en el alta de un documento. Agrega o modifica la asociación entre tipos de documentos y Enablers, las imágenes para cada tipo de documento o la forma en la que se codifica el título de un documento."
            },
            {
                url: '/sites/B2BFinder.OCEWU/es-es/Paginas/administracion.aspx',
                element: 'div[data-group="Otros"]',
                placement: "top",
                title: "Otros",
                content: "Aquí tienes diferentes enlaces para administrar el resto de contenidos de B2B Finder."
            },
            {
                url: '/sites/B2BFinder.OCEWU/es-es/Paginas/mis-favoritos.aspx',
                element: '#favouritesTable',
                placement: "top",
                title: "Favoritos",
                content: 'Ésta es la lista de tus documentos favoritos, todos los documentos en los que has pulsado el botón <span class="icon-heart-purple" style="line-height:60px"><span class="path1"></span><span class="path2"></span></span>'
            },
            {
                url: '/sites/B2BFinder.OCEWU/es-es/Paginas/mis-favoritos.aspx',
                element: '#favouritesTable th:eq(0)',
                placement: "top",
                title: "Tipo",
                content: 'En esta columna se muestra el icono que te indica el formato del documento.'
            },
            {
                url: '/sites/B2BFinder.OCEWU/es-es/Paginas/mis-favoritos.aspx',
                element: '#favouritesTable th:eq(1)',
                placement: "top",
                title: "Título del documento",
                content: 'En esta columna se muestra el nombre del documento. Si el documento es un documento de Office, pulsando sobre el enlace se abrirá en la herramienta de Office 365 correspondiente.'
            },
            {
                url: '/sites/B2BFinder.OCEWU/es-es/Paginas/mis-favoritos.aspx',
                element: 'a[data-command="delete"]:first',
                placement: "left",
                title: "Eliminar favorito",
                content: 'Pulsando este botón podrás eliminar el documento de tus favoritos.'
            },
            {
                url: '/sites/B2BFinder.OCEWU/es-es/Paginas/mis-favoritos.aspx',
                element: 'td.last a:eq(1)',
                placement: "left",
                title: "Descargar documento",
                content: 'Pulsando este botón podrás descargar el documento a tu equipo.'
            },
            {
                url: '/sites/B2BFinder.OCEWU/es-es/Paginas/mis-marcadores.aspx',
                element: '#bookmarksTable',
                placement: "top",
                title: "Marcadores",
                content: 'Ésta es la lista de tus documentos favoritos, todos los documentos en los que has pulsado el botón <span class="icon-bookmark-purple" style="line-height:60px"><span class="path1"></span><span class="path2"></span></span>'
            },
            {
                url: '/sites/B2BFinder.OCEWU/es-es/Paginas/mis-marcadores.aspx',
                element: '#bookmarksTable th:eq(0)',
                placement: "top",
                title: "Tipo",
                content: 'En esta columna se muestra el icono que te indica el formato del documento.'
            },
            {
                url: '/sites/B2BFinder.OCEWU/es-es/Paginas/mis-marcadores.aspx',
                element: '#bookmarksTable th:eq(1)',
                placement: "top",
                title: "Título del documento",
                content: 'En esta columna se muestra el nombre del documento. Si el documento es un documento de Office, pulsando sobre el enlace se abrirá en la herramienta de Office 365 correspondiente.'
            },
            {
                url: '/sites/B2BFinder.OCEWU/es-es/Paginas/mis-marcadores.aspx',
                element: 'a[data-command="delete"]:first',
                placement: "left",
                title: "Eliminar favorito",
                content: 'Pulsando este botón podrás eliminar el documento de tus favoritos.'
            },
            {
                url: '/sites/B2BFinder.OCEWU/es-es/Paginas/mis-marcadores.aspx',
                element: 'td.last a:eq(1)',
                placement: "left",
                title: "Descargar documento",
                content: 'Pulsando este botón podrás descargar el documento a tu equipo.'
            },
            {
                url: '/sites/B2BFinder.OCEWU/es-es/Paginas/gestion-de-usuarios.aspx',
                element: '#mainContent',
                placement: "left",
                type: 'help',
                title: "Gestión de usuarios",
                content: '<p>​Para gestionar los usuarios de B2B Finder debes hacerlo desde la página de administración de grupos y usuarios de SharePoint. Aquí te explicamos de forma sencilla cómo hacerlo.<br/></p><ul><li>Pulsa sobre cualquiera de los nombres de los grupos.</li><li>En la&#160;​página de administración del grupo sobre el que hayas pulsado, verás el listado de usuarios del grupo y una barra de herramientas para poder realizar diferentes acciones.<br/></li><li>Para&#160;<strong>agregar un usuario</strong>, pulsa el botón&#160;<strong>New</strong> e introduce el nombre del usuario o su email. Puedes introducir varios usuarios.<br/></li><li>Para&#160;<strong>eliminar un usuario</strong> del grupo, selecciona el usuario que quieras eliminar pulsando sobre la casilla que aparece a la izquierda de su nombre y, a continuación, pulsa el botón&#160;<strong>Actions</strong> de la barra de herramientas. En las opciones que se despliegan, selecciona&#160;<strong>Remove Users from Group</strong>​. Puedes seleccionar varios usuarios si deseas eliminar varios usuarios a la vez.<br/></li></ul><div><br/></div><div><br/></div><br/>'
            },
            {
                url: '/sites/B2BFinder.OCEWU/es-es/Paginas/busqueda.aspx',
                element: '#search-results-zone',
                placement: "top",
                title: "Resultados de búsqueda",
                content: 'Éste es el panel de resultados de búsqueda donde aparecen los documentos que coinciden con el criterio de búsqueda introducido.'
            },
            {
                url: '/sites/B2BFinder.OCEWU/es-es/Paginas/busqueda.aspx',
                element: '#refiners-panel',
                placement: "top",
                title: "Panel de refinamiento",
                content: '<p>En esta columna tienes los diferentes filtros que puedes aplicar a los resultados de búsqueda encontrados.</p><p>Los valores que aparecen aquí varían según los resultados encontrados.</p><p>Puedes seleccionar uno o varios filtros para refinar los resultados de búsqueda.</p>'
            },
            {
                url: '/sites/B2BFinder.OCEWU/es-es/Paginas/busqueda.aspx',
                element: '.doc-type-image:first',
                placement: "right",
                title: "Selección de documento",
                content: '<p>Puedes seleccionar los documentos pulsando sobre la imagen para después copiar el enlace de todos los documentos seleccionados y poder compartirlos por email.</p>'
            }
        ]
    },
    {
        'culture': 'en',
        'keys': [
            {
                url: '/sites/B2BFinder.OCEWU/es-es/Paginas/busqueda.aspx',
                element: '.doc-type-image:first',
                placement: "right",
                title: "Document Selection",
                content: '<p>You can select the documents by clicking on the image and then copy the link of all the selected documents and share them by email.</p>'
            },
            {
                url: '/sites/B2BFinder.OCEWU/es-es/Paginas/home.aspx',
                element: "#txtSearch",
                placement: "left",
                title: "Search box",
                content: "This is the B2B Finder search engine. Type the text you want to find and press the Enter key. You can also use search wildcards such as the asterisk (*) to find the documentation you are looking for.",
                callback: function () {
                    startWizard(urlSteps.length - 1);
                }
            },
            {
                url: '/sites/B2BFinder.OCEWU/es-es/Paginas/home.aspx',
                element: "#btnSearch",
                placement: "left",
                title: "Search button",
                content: "You can also press this button to launch the search once you have entered the text to search."
            },
            {
                url: '/sites/B2BFinder.OCEWU/es-es/Paginas/home.aspx',
                element: "#menuLinks li a:contains('Inicio')",
                placement: "right",
                title: "Home",
                content: "By clicking on this menu you can access the B2B Finder home page."
            },
            {
                url: '/sites/B2BFinder.OCEWU/es-es/Paginas/home.aspx',
                element: "#menuLinks li a:contains('Nuevo documento')",
                placement: "right",
                title: "New document",
                content: "From here you can access the page to load new documents in the repository."
            },
            {
                url: '/sites/B2BFinder.OCEWU/es-es/Paginas/home.aspx',
                element: "#menuLinks li a:contains('Administración')",
                placement: "right",
                title: "Administration",
                content: "Click on this link to access the administration area, where you can manage from the values ​​of certain fields to the configuration lists or the structure of the repository itself."
            },
            {
                url: '/sites/B2BFinder.OCEWU/es-es/Paginas/home.aspx',
                element: "#menuLinks li a:contains('Favoritos')",
                placement: "right",
                title: "Favorites",
                content: 'Click on this link to access your favorite documents. The favorite documents are those documents in which you pressed the <span class = "icon-heart-red" style = "line-height: 60px"> <span class = "path1"> </span> <span class = "path2"> </span> </span>.'
            },
            {
                url: '/sites/B2BFinder.OCEWU/es-es/Paginas/home.aspx',
                element: "#menuLinks li a:contains('Marcadores')",
                placement: "right",
                title: "Boomarks",
                content: 'Click on this link to access your bookmarks. Markers are those documents in which you pressed the <span class = "icon-bookmark-orange" style = "line-height: 60px"> <span class = "path1"> </span> <span class = " path2 "> </span> </span>.'
            },
            {
                url: '/sites/B2BFinder.OCEWU/es-es/Paginas/home.aspx',
                element: "#faq",
                placement: "left",
                title: "FAQ",
                content: 'Do you miss something? Click this link to access the frequently asked questions.'
            },
            {
                url: '/sites/B2BFinder.OCEWU/es-es/Paginas/home.aspx',
                element: "#languageBar",
                placement: "left",
                title: "Languaje bar",
                content: "By clicking on this link you can change the application language between Spanish and English."
            },
            {
                url: '/sites/B2BFinder.OCEWU/es-es/Paginas/home.aspx',
                element: "#carousel",
                placement: "bottom",
                title: "News carousel",
                content: "Three news of particular interest about B2B Finder will appear here."
            },
            {
                url: '/sites/B2BFinder.OCEWU/es-es/Paginas/home.aspx',
                element: ".module",
                placement: "top",
                title: "Featured documents",
                content: "In this area you can see the 9 most prominent documents based on the number of likes and visits the documents have."
            },
            {
                url: '/sites/B2BFinder.OCEWU/es-es/Paginas/home.aspx',
                element: "#viewAllDocuments",
                placement: "left",
                title: "View all",
                content: "By clicking on this link you will be able to access the repository search results to see all the documents."
            },
            {
                url: '/sites/B2BFinder.OCEWU/es-es/Paginas/home.aspx',
                element: "#btnList",
                placement: "left",
                title: "List view",
                content: "Press this button if you want to see the documents in list format, as they are displayed on the search results screen."
            },
            {
                url: '/sites/B2BFinder.OCEWU/es-es/Paginas/home.aspx',
                element: "#btnCard",
                placement: "left",
                title: "Card view",
                content: "Press this button if you want to see the documents in card format, as they are shown now."
            },
            {
                url: '/sites/B2BFinder.OCEWU/es-es/Paginas/home.aspx',
                element: ".card-container:first",
                placement: "right",
                title: "Card",
                content: "Here you can see the summary information of the document."
            },
            {
                url: '/sites/B2BFinder.OCEWU/es-es/Paginas/home.aspx',
                element: ".doc-type:first span",
                placement: "bottom",
                title: "Document type",
                content: "This icon will quickly indicate the format of the document."
            },
            {
                url: '/sites/B2BFinder.OCEWU/es-es/Paginas/home.aspx',
                element: ".card-title:first",
                placement: "right",
                title: "Document title",
                content: "This is the title of the document. By clicking on it you can access the online version of the document whenever it is an Office document."
            },
            {
                url: '/sites/B2BFinder.OCEWU/es-es/Paginas/home.aspx',
                element: ".card-info:first",
                placement: "right",
                title: "Document information",
                content: "Here you will see the date of last modification of the document, the language in which it is available and its size."
            },
            {
                url: '/sites/B2BFinder.OCEWU/es-es/Paginas/home.aspx',
                element: ".card-image:first",
                placement: "right",
                title: "Document image",
                content: "This image will allow you to quickly identify the type of document."
            },
            {
                url: '/sites/B2BFinder.OCEWU/es-es/Paginas/home.aspx',
                element: ".doc-enablers:first",
                placement: "right",
                title: "Service Enablers",
                content: "If the document has one or more Service Enablers associated, in this section all those assigned will appear."
            },
            {
                url: '/sites/B2BFinder.OCEWU/es-es/Paginas/home.aspx',
                element: '[data-command="favourite"]:first',
                placement: "right",
                title: "Favorites",
                content: '<p> This icon allows you to add the document to your favorites. </p> <ul class = "list-group list-group-flush"> <li class = "list-group-item"> <span class = " icon-heart-red "style =" line-height: 60px "> <span class =" path1 "> </span> <span class =" path2 "> </span> </span> Indicates that the document is already in your favorites If you click on the icon, you can remove it from your favorites. </li> <li class = "list-group-item"> <span class = "icon-heart-purple" style = "line-height: 60px"> < span class = "path1"> </span> <span class = "path2"> </span> </span> Indicates that the document is not in your favorites. By clicking on the icon you can add it to your favorite documents. </li> </ul>'
            },
            {
                url: '/sites/B2BFinder.OCEWU/es-es/Paginas/home.aspx',
                element: '[data-command="bookmark"]:first',
                placement: "right",
                title: "Bookmarks",
                content: '<p> This icon allows you to add the document to your bookmarks. </p> <ul class = "list-group list-group-flush"> <li class = "list-group-item"> <span class = " icon-bookmark-orange "style =" line-height: 60px "> <span class =" path1 "> </span> <span class =" path2 "> </span> </span> Indicates that the document is already in your bookmarks. If you click on the icon, you can remove it from your bookmarks. </li> <li class = "list-group-item"> <span class = "icon-bookmark-purple" style = "line-height: 60px"> < span class = "path1"> </span> <span class = "path2"> </span> </span> Indicates that the document is not in your bookmarks. By clicking on the icon you can add it to your favorite documents. </li> </ul>'
            },
            {
                url: '/sites/B2BFinder.OCEWU/es-es/Paginas/home.aspx',
                element: '[data-command="share"]:first',
                placement: "right",
                title: "Share",
                content: 'Press this button to get the document url and copy it to share it with other users.',
                callback: function () {
                    startWizard(urlSteps.length - 1);
                }
            },
            {
                url: '/sites/B2BFinder.OCEWU/es-es/Paginas/home.aspx',
                element: '[data-command="alert"]:first',
                placement: "right",
                title: "Alerts",
                content: 'Press this button to subscribe to email alerts about the document. You can specify which actions you want to receive alerts and how often.',
                callback: function () {
                    startWizard(urlSteps.length - 1);
                }
            },
            {
                url: '/sites/B2BFinder.OCEWU/es-es/Paginas/home.aspx',
                element: '[data-key="info"]:first',
                placement: "right",
                title: "Document information",
                content: 'Press this button to access the document information page, where you can also perform other actions and see other related documents.',
                callback: function () {
                    startWizard(urlSteps.length - 1);
                }
            },
            {
                url: '/sites/B2BFinder.OCEWU/es-es/Paginas/home.aspx',
                element: '[data-key="view"]:first',
                placement: "right",
                title: "View document",
                content: 'If the document is an Office document (Word, Excel or PowerPoint), clicking on this button you will access the document in the corresponding online tool. If it is not an Office document, then the download will start. If your browser allows it, you can also open a PDF document in it.',
                callback: function () {
                    startWizard(urlSteps.length - 1);
                }
            },
            {
                url: '/sites/B2BFinder.OCEWU/es-es/Paginas/home.aspx',
                element: '[data-key="download"]:first',
                placement: "right",
                title: "Download document",
                content: 'Press this button if you want to download a version of the document to your device. The changes you make in the document will not be visible by other people until you reload it to the repository.',
                callback: function () {
                    startWizard(urlSteps.length - 1);
                }
            },
            {
                url: '/sites/B2BFinder.OCEWU/es-es/Paginas/home.aspx',
                element: "#anchorWizard",
                placement: "left",
                title: "Wizard",
                content: '<p>Remember that from this link you can launch the wizard when you need it. </p> <p> We will reload the page to leave everything as it was. </p>',
                callback: function () {
                    window.location.reload();
                }
            },
            {
                url: '/sites/B2BFinder.OCEWU/es-es/Paginas/nuevo-documento.aspx',
                element: "#drag-div-container",
                placement: "top",
                title: "Load document",
                content: 'Use this area to upload your documents. You can drag documents from your computer or press the <strong> Select file </strong> button to select the files to upload.'
            },
            {
                url: '/sites/B2BFinder.OCEWU/es-es/Paginas/nuevo-documento.aspx',
                element: "#drag-div-container",
                placement: "top",
                title: "Load document",
                content: 'When you drag or select the files, they will automatically start loading them to B2BFinder.'
            },
            {
                url: '/sites/B2BFinder.OCEWU/es-es/Paginas/nuevo-documento.aspx',
                element: "#drag-div-container",
                placement: "top",
                title: "Load document",
                content: 'Once the upload is complete, a form will appear for you to complete the information in each document. If you have any problem with the form, press the assistant button in the top menu.'
            },
            {
                url: '/sites/B2BFinder.OCEWU/es-es/Paginas/nuevo-documento.aspx',
                element: "#document-metadata",
                placement: "top",
                title: "Form",
                content: "This is the form to complete the document data."
            },
            {
                url: '/sites/B2BFinder.OCEWU/es-es/Paginas/nuevo-documento.aspx',
                parent: '#document-metadata',
                element: '[data-id="txtTitle"]:first',
                placement: "left",
                title: "Title",
                content: "Enter the title of the document here."
            },
            {
                url: '/sites/B2BFinder.OCEWU/es-es/Paginas/nuevo-documento.aspx',
                parent: '#document-metadata',
                element: '[data-id="selectDocumentType"]:first',
                placement: "left",
                title: "Document type",
                content: "Select a type of document. If you do not select any, the document type <strong> B2B Document </strong> will be automatically assigned"
            },
            {
                url: '/sites/B2BFinder.OCEWU/es-es/Paginas/nuevo-documento.aspx',
                parent: '#document-metadata',
                element: '[data-id="selectProduct"]:first',
                placement: "left",
                title: "Product",
                content: "Select a product."
            },
            {
                url: '/sites/B2BFinder.OCEWU/es-es/Paginas/nuevo-documento.aspx',
                parent: '#document-metadata',
                element: '[data-id="txtDescription"]:first',
                placement: "top",
                title: "Description",
                content: "If you want, you can add a description to the document, which will be useful so that other users can get a quick idea of ​​the content of the document."
            },
            {
                url: '/sites/B2BFinder.OCEWU/es-es/Paginas/nuevo-documento.aspx',
                parent: '#document-metadata',
                element: '[data-id="chkInternalUse"]:first',
                placement: "left",
                title: "Document use",
                content: "Check this box if the document is for internal use only."
            },
            {
                url: '/sites/B2BFinder.OCEWU/es-es/Paginas/nuevo-documento.aspx',
                parent: '#document-metadata',
                element: '#btnSave',
                placement: "left",
                title: "Save",
                content: "Once you complete all the data, press this button to save the information."
            },
            {
                url: '/sites/B2BFinder.OCEWU/es-es/Paginas/administracion.aspx',
                element: 'div[data-group="Permisos"]',
                placement: "top",
                title: "Permissions",
                content: "In this section you will find the elements to manage the permissions of the application."
            },
            {
                url: '/sites/B2BFinder.OCEWU/es-es/Paginas/mis-favoritos.aspx',
                element: '#favouritesTable',
                placement: "top",
                title: "Favorites",
                content: 'This is the list of your favorite documents, all the documents in which you pressed the <span class = "icon-heart-purple" style = "line-height: 60px"> <span class = "path1"> </ span> <span class = "path2"> </span> </span>'
            },
            {
                url: '/sites/B2BFinder.OCEWU/es-es/Paginas/mis-favoritos.aspx',
                element: '#favouritesTable th:eq(0)',
                placement: "top",
                title: "Type",
                content: 'This column shows the icon that indicates the format of the document.'
            },
            {
                url: '/sites/B2BFinder.OCEWU/es-es/Paginas/mis-favoritos.aspx',
                element: '#favouritesTable th:eq(1)',
                placement: "top",
                title: "Document title",
                content: 'This column shows the name of the document. If the document is an Office document, clicking on the link will open in the corresponding Office 365 tool.'
            },
            {
                url: '/sites/B2BFinder.OCEWU/es-es/Paginas/mis-favoritos.aspx',
                element: 'a[data-command="delete"]:first',
                placement: "left",
                title: "Delete favorite",
                content: 'By pressing this button you can delete the document from your favorites.'
            },
            {
                url: '/sites/B2BFinder.OCEWU/es-es/Paginas/mis-favoritos.aspx',
                element: 'td.last a:eq(1)',
                placement: "left",
                title: "Download document",
                content: 'By pressing this button you can download the document to your computer.'
            },
            {
                url: '/sites/B2BFinder.OCEWU/es-es/Paginas/mis-marcadores.aspx',
                element: '#bookmarksTable',
                placement: "top",
                title: "Bookmarks",
                content: 'This is the list of your favorite documents, all the documents in which you pressed the <span class = "icon-bookmark-purple" style = "line-height: 60px"> <span class = "path1"> </ span> <span class = "path2"> </span> </span>'
            },
            {
                url: '/sites/B2BFinder.OCEWU/es-es/Paginas/mis-marcadores.aspx',
                element: '#bookmarksTable th:eq(0)',
                placement: "top",
                title: "Type",
                content: 'This column shows the icon that indicates the format of the document.'
            },
            {
                url: '/sites/B2BFinder.OCEWU/es-es/Paginas/mis-marcadores.aspx',
                element: '#bookmarksTable th:eq(1)',
                placement: "top",
                title: "Document title",
                content: 'This column shows the name of the document. If the document is an Office document, clicking on the link will open in the corresponding Office 365 tool.'
            },
            {
                url: '/sites/B2BFinder.OCEWU/es-es/Paginas/mis-marcadores.aspx',
                element: 'a[data-command="delete"]:first',
                placement: "left",
                title: "Delete Bookmark",
                content: 'By pressing this button you can delete the document from your bookmarks.'
            },
            {
                url: '/sites/B2BFinder.OCEWU/es-es/Paginas/mis-marcadores.aspx',
                element: 'td.last a:eq(1)',
                placement: "left",
                title: "Download document",
                content: 'By pressing this button you can download the document to your computer.'
            }
        ]
    }
];

function getCultureSteps() {
    var culture = getCookie('culture');

    for (var i = 0; i < steps.length; i++) {
        if (steps[i].culture == culture) {
            return steps[i].keys;
        }
    }
}

function prepareWizard() {
    urlSteps = new Array();
    var url = window.location.href.replace("https://telefonicacorp.sharepoint.com", "");
    var urlSplit = url.split('#');
    url = urlSplit[0];
    urlSplit = url.split('?');
    url = urlSplit[0];

    var cultureSteps = getCultureSteps();

    var stepsArray = cultureSteps.filter(function(obj) {
        return url.toLowerCase() === obj.url.toLowerCase();
    });

    //Comprobamos si existen todos los elementos del array de pasos
    for (var i = 0; i < stepsArray.length; i++) {
        if (stepsArray[i].parent != null && ($(stepsArray[i].parent).length == 0 || $(stepsArray[i].parent).css('display') === 'none')) {
            continue;
        }

        if ($(stepsArray[i].element).length > 0 && $(stepsArray[i].element).css('display') !== 'none') {
            urlSteps.push(stepsArray[i]);
        }
    }

    if (urlSteps.length > 0) {
        $('#anchorWizard').parent().show();
    }
}

$(document).ajaxStop(function () {
    var wizardCookie = getCookie('wizard');

    if (wizardCookie == '') {
        $('#welcomeModal').modal('show');
    }

    $('#anchorWizard').on('click', function () {
        canceled = false;
        if ($('#welcomeModal').length > 0)
            $('#welcomeModal').modal('show');
        else {
            $('#anchorWizard').prop('disabled', 'disabled');
            prepareWizard();
            if (urlSteps.length > 1)
                startWizard(0);
            else
                startHelp();
        }
    });

    $('#btnLaunchWizard').on('click', function () {
        launchWizard();
    });

    $('#btnCancelWizard').on('click', function () {
        canceled = true;
        launchWizard();
        setCookie('wizard', 'completed', 365);
    });
});

function launchWizard() {
    $('#welcomeModal').on('hidden.bs.modal', function () {
        if (!canceled) {
            prepareWizard();
            startWizard(0);
        }
    });

    $('#welcomeModal').modal('hide');
}

function startWizard(index, next) {
    if (index == urlSteps.length) {
        setCookie('wizard', 'completed', 365);
        return false;
    }

    var $index = index;

    //Deshabilitamos los popover que tenga
    $(urlSteps[index].element).popover('dispose');

    var nextIndex = index + 1;
    var prevIndex = index - 1;

    var scrollTop;
    var offsetTop = $(urlSteps[index].element).offset().top;
    var $window = $(window);
    height = $(urlSteps[index].element).outerHeight();
    windowHeight = $window.height();

    switch (urlSteps[index].placement) {
        case 'top':
            scrollTop = Math.max(0, offsetTop - (windowHeight / 2));
            break;
        case 'left':
        case 'right':
            scrollTop = Math.max(0, (offsetTop + height / 2) - (windowHeight / 2));
            break;
        case 'bottom':
            scrollTop = Math.max(0, (offsetTop + height) - (windowHeight / 2));
            break;
    }

    scrollTop = (next || next == null ? $('#s4-workspace').scrollTop() + scrollTop : $('#s4-workspace').scrollTop() - scrollTop);

    if (prevIndex >= 0) {
        $(urlSteps[prevIndex].element).effect('transfer', { to: urlSteps[index].element, className: "wizard-step" }, 500, function () {
            $(urlSteps[index].element).addClass('wizard-step');
        });
    }
    else {
        $(urlSteps[index].element).addClass('wizard-step');
    }

    $('#s4-workspace').animate(
        {
            scrollTop: scrollTop
        }, 1000, function () {
            var popover = $(urlSteps[index].element).popover({
                container: 'body',
                title: urlSteps[index].title,
                content: urlSteps[index].content,
                template: '<div class="popover wizard" role="tooltip"><div class="arrow"></div><h3 class="popover-header"></h3><div class="popover-body"></div><div class="toolbar"><a href="#" class="btn btn-gray" role="prev">Anterior</a><a href="#" class="btn btn-gray" role="next">Siguiente</a><a href="#" class="btn btn-blue09" role="end">Finalizar</a></div></div>',
                html: true,
                placement: urlSteps[index].placement
            });

            popover.popover('show');

            if (nextIndex == urlSteps.length)
                $('.popover.wizard [role="next"]').hide();

            if (index == 0)
                $('.popover.wizard [role="prev"]').hide();


            $('.popover.wizard [role="next"]').on('click', function () {
                $(urlSteps[$index].element).removeClass('wizard-step');
                popover.popover('dispose');

                startWizard(nextIndex);
                return;
            });

            $('.popover.wizard [role="prev"]').on('click', function () {
                $(urlSteps[$index].element).removeClass('wizard-step');
                popover.popover('dispose');

                startWizard(prevIndex, false);
                return;
            });

            $('.popover.wizard [role="end"]').on('click', function () {
                setCookie('wizard', 'completed', 365);
                $('#anchorWizard').removeProp('disabled');
                popover.popover('dispose');
                $(urlSteps[$index].element).removeClass('wizard-step');
                if (urlSteps[$index].callback != null) {
                    urlSteps[$index].callback();
                }
            });
        });
}

function startHelp() {
    var step = urlSteps[0];

    bootbox.dialog({
        title: step.title,
        message: step.content,
        size: 'large'
    });
}

function showTooltip() {
    setCookie('wizard', 'completed', 365);

    var etop = $('#anchorWizard').offset().top;

    //Restamos el alto del popover
    etop = etop - 200;

    $('#s4-workspace').animate(
        {
            scrollTop: etop
        }, 1000, function () {
            var popover = $('#anchorWizard').popover({
                container: 'body',
                title: 'Ayuda',
                content: '<p>Recuerda que desde este enlace puedes lanzar el asistente cuando lo necesites.</p><p>Vamos a recargar la página para dejarlo todo como estaba.</p>',
                html: true,
                trigger: 'manual'
            }).popover('show');

            $('#anchorWizard').addClass('wizard-step');

            var timer = setTimeout(function () {
                window.location.reload();
            }, 5000);
        });
}


function getNextIndex(index) {
    var nextIndex = urlSteps.length - 1;
    for (var i = index + 1; i < urlSteps.length; i++) {
        if ($(urlSteps[i].element).length > 0 && $(urlSteps[i].element).css('display') !== 'none') {
            nextIndex = i;
            break;
        }
    }

    return nextIndex;
}

function getPrevIndex(index) {
    var prevIndex = 0;
    for (var i = index - 1; i >= 0; i--) {
        if ($(urlSteps[i].element).length > 0 && $(urlSteps[i].element).css('display') !== 'none') {
            prevIndex = i;
            break;
        }
    }

    return prevIndex;
}