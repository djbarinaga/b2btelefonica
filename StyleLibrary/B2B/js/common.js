﻿function bytesToSize(bytes) {
    var sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB'];
    if (bytes == 0) return '0 Byte';
    var i = parseInt(Math.floor(Math.log(bytes) / Math.log(1024)));
    return Math.round(bytes / Math.pow(1024, i), 2) + ' ' + sizes[i];
};

function getDocumentIcon(type) {
    var documentIcon = '';
    switch (type.toLowerCase()) {
        case 'ppt':
        case 'pptx':
            documentIcon = '<span class="icon-ppt"></span>';
            break;
        case 'doc':
        case 'docx':
            documentIcon = '<span class="icon-doc"></span>';
            break;
        case 'xls':
        case 'xlsx':
            documentIcon = '<span class="icon-xls"></span>';
            break;
        case 'pdf':
            documentIcon = '<span class="icon-pdf"></span>';
            break;
        case 'png':
        case 'jpg':
        case 'bmp':
        case 'gif':
            documentIcon = '<span class="icon-file-picture"></span>';
            break;
        case 'zip':
            documentIcon = '<span class="icon-file-zip"></span>';
            break;
        default:
            documentIcon = '<span class="icon-file-empty"></span>';
            break;
    }

    return documentIcon;
}

function getImageFromCulture(culture) {
    var imageUrl = '';
    switch (culture.toLowerCase()) {
        case 'es-es':
            imageUrl = 'spain.png';
            break;
        default:
            imageUrl = '';
            break;
    }

    if (imageUrl != '')
        imageUrl = _spPageContextInfo.siteServerRelativeUrl + '/style library/b2b/images/' + imageUrl;

    return imageUrl;
}

function getFileUrl(file) {
    var webView = '?Web=1';
    var fileUrl = '';

    if (typeof file == "string") {
        fileUrl = file + webView;
    }
    else {
        var extension = file.FileType;

        var filePath = file.ParentLink.toLowerCase().split('/forms')[0];
        var fileName = file.Filename;
        fileUrl = filePath + '/' + fileName + webView;
    }

    return fileUrl;
}

function getDownloadLink(file) {
    var fileUrl = '';

    if (typeof file == "string") {
        return file;
    }

    var extension = file.FileType;

    switch (extension.toLowerCase()) {
        case 'png':
            var filePath = file.Path;
            var formsIndex = filePath.indexOf('Forms');
            var doclibUrl = filePath.substring(0, formsIndex);
            var fileName = file.Filename;
            fileUrl = doclibUrl + fileName;
            break;
        default:
            fileUrl = file.Path;
            break;
    }

    return fileUrl;
}

function getFileNameFromUrl(url) {
    var splitUrl = url.split('/');

    return splitUrl[splitUrl.length - 1];
}

function getCultureFromUrl(url, variationLabels) {
    var splitUrl = url.split('/');
    var culture = '';
    for (var i = 0; i < splitUrl.length; i++) {
        var index = variationLabels.map(function (e) {
            return e.Title.toLowerCase();
        }).indexOf(splitUrl[i].toLowerCase());

        if (index > -1) {
            culture = splitUrl[i];
            break;
        }
    }

    return culture;
}

function getDateAsString(date) {
    var esMonths = ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'];
    var enMonths = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];

    var months;

    var culture = getCookie('culture').toLowerCase();
    if (culture == 'es')
        months = esMonths;
    else
        months = enMonths;

    var monthIndex = date.getMonth();
    var month = months[monthIndex];
    var day = date.getDate();
    var year = date.getFullYear();

    return month + ' ' + day + ', ' + year;
}

function confirmPopover(element, title, content, placement, trigger, callback) {
    var popover = $(element).popover({
        container: 'body',
        content: content,
        template: '<div class="popover confirm" role="tooltip"><div class="arrow"></div><h3 class="popover-header"></h3><div class="popover-body"></div><div class="toolbar"><a href="#" role="confirm">Sí</a><a href="#" role="cancel">No</a></div></div>',
        html: true,
        trigger: trigger,
        placement: placement,
        title: title
    });

    popover.on('shown.bs.popover', function () {
        $('.popover.confirm').find('a[role="confirm"]').on('click', function () {
            if (callback != null)
                callback(true);
        });

        $('.popover.confirm').find('a[role="cancel"]').on('click', function () {
            if (callback != null)
                callback(false);
        });
    });

    return popover;
}

function customPopover(options) {
    var template = '<div class="popover custom" role="tooltip"><div class="arrow"></div><h3 class="popover-header"></h3><div class="popover-body"></div><div class="toolbar">';
    
    for (var i = 0; i < options.buttons.length; i++) {
        var button = options.buttons[i];
        var buttonHref = '#';

        if (button.Url != null)
            buttonHref = button.Url;

        template += '<a href="' + buttonHref + '" id="' + button.Id + '">' + button.Text + '</a>';
    }

    template += '</div></div>';

    if (options.placement == null)
        options.placement = 'left';

    if (options.trigger == null)
        options.trigger = 'click';

    var popover = $(options.element).popover({
        container: 'body',
        content: options.content,
        template: template,
        html: true,
        trigger: options.trigger,
        placement: options.placement,
        title: options.title
    });

    popover.on('shown.bs.popover', function () {
        for (var i = 0; i < options.buttons.length; i++) {
            var button = options.buttons[i];
            var buttonClick = button.Callback;

            if (buttonClick != null) {
                $(document).on('click', '.popover.custom #' + button.Id, function () {
                    buttonClick(true);
                });
            }
        }
    });

    return popover;
}

function getUrlParam(param) {
    var query = window.location.search.substring(1);
    var vars = query.split("&");
    for (var i = 0; i < vars.length; i++) {
        var pair = vars[i].split("=");
        if (pair[0] == param) { return pair[1]; }
    }
    return (false);
}

function getCookie(cname) {
    var name = cname + "=";
    var decodedCookie = decodeURIComponent(document.cookie);
    var ca = decodedCookie.split(';');
    for (var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
}

function setCookie(cname, cvalue, exdays) {
    var d = new Date();
    d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
    var expires = "expires=" + d.toUTCString();
    document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
}