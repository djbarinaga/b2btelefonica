﻿$(document).ready(function () {
    SP.SOD.executeFunc('sp.js', 'SP.ClientContext', initBookmarks);
});

function initBookmarks() {
    //setBookmarks();
    var inDesignMode = document.forms[MSOWebPartPageFormName].MSOLayout_InDesignMode.value;
    if (inDesignMode != "1") {
        loadMyBookmarks();
    }
}


//Obtiene los elementos bookmark del usuario
function setBookmarks(showPopover) {
    var clientContext = new SP.ClientContext(_spPageContextInfo.siteAbsoluteUrl);
    var list = clientContext.get_web().get_lists().getByTitle('Bookmarks');
    var camlQuery = new SP.CamlQuery();
    camlQuery.set_viewXml("<View><Query><Where><Eq><FieldRef Name='Author' LookupId='True'/><Value Type='Lookup'>" + _spPageContextInfo.userId + "</Value></Eq></Where><OrderBy><FieldRef Name='ID' Ascending='FALSE'/></OrderBy></Query><RowLimit>5</RowLimit></View>");

    var items = list.getItems(camlQuery);
    clientContext.load(items);

    clientContext.executeQueryAsync(
        function () {
            $('#btnBookmarks').popover('dispose');
            var hasResults = false;
            var listItemEnumerator = items.getEnumerator();
            var html = '<ul class="list-group list-group-flush" id="bookmarks">';
            while (listItemEnumerator.moveNext()) {
                hasResults = true;
                var result = listItemEnumerator.get_current();

                var title = result.get_item('DocUrl').get_description();
                var url = result.get_item('DocUrl').get_url();
                var urlParts = url.split('?');
                var urlParts2 = urlParts[0].split('.');
                var extension = urlParts2[urlParts2.length - 1];

                html += '<li class="list-group-item">';
                html += getDocumentIcon(extension);
                html += '<a href="' + url + '" target="_blank" class="anchor-icon">' + title + '</a>';
                html += '</li>';
            }

            if (!hasResults)
                html = '<p>' + translate('NoBookmarksMessage') + '</p>';

            var popover = customPopover({
                element: $('#btnBookmarks'),
                title: translate('Bookmarks') + '<span class="icon-bookmark-blue"><span class="path1"></span><span class="path2"></span></span>',
                placement: 'left',
                content: html,
                buttons: [
                    {
                        Id: 'btnViewAll',
                        Url: _spPageContextInfo.listUrl + '/mis-marcadores.aspx',
                        Text: translate('viewall')
                    }
                ]
            });

            if (showPopover) {
                popover.popover('show');
                setTimeout(function () { popover.popover('hide'); }, 2000);
            }
        },
        function (sender, args) {
            console.log('Request failed. ' + args.get_message() + '\n' + args.get_stackTrace());
        }
    );
}

function loadMyBookmarks() {
    if ($('#bookmarksTable').length > 0) {
        $('#bookmarksTable tbody').empty();
        var clientContext = new SP.ClientContext(_spPageContextInfo.siteAbsoluteUrl);
        var list = clientContext.get_web().get_lists().getByTitle('Bookmarks');
        var camlQuery = new SP.CamlQuery();
        camlQuery.set_viewXml("<View><Query><Where><Eq><FieldRef Name='Author' LookupId='True'/><Value Type='Lookup'>" + _spPageContextInfo.userId + "</Value></Eq></Where><OrderBy><FieldRef Name='ID' Ascending='FALSE'/></OrderBy></Query></View>");

        var items = list.getItems(camlQuery);
        clientContext.load(items);

        clientContext.executeQueryAsync(
            function () {
                var hasResults = false;
                var listItemEnumerator = items.getEnumerator();

                while (listItemEnumerator.moveNext()) {
                    hasResults = true;
                    var result = listItemEnumerator.get_current();

                    var title = result.get_item('DocUrl').get_description();
                    var url = result.get_item('DocUrl').get_url();
                    var urlParts = url.split('?');
                    var urlParts2 = urlParts[0].split('.');
                    var extension = urlParts2[urlParts2.length - 1];
                    var docIcon = getDocumentIcon(extension);
                    var fileName = getFileNameFromUrl(urlParts[0]);
                    var id = result.get_id();

                    $('#bookmarksTable tbody').append('<tr><td>' + docIcon + '</td><td><a href="' + url + '" target="_blank">' + title + '</a></td><td class="last"><a href="#" data-id="' + id + '" title="Eliminar" data-command="delete"><span class="icon-cross"></span></a><a href="' + urlParts[0] + '" title="Descargar" download="' + fileName + '"><span class="icon-download"></span></a></td></tr>');
                }

                setBookmarksTableEvents();
            },
            function (sender, args) {
                console.log('Request failed. ' + args.get_message() + '\n' + args.get_stackTrace());
            }
        );
    }
}

function setBookmarksTableEvents() {
    $('#bookmarksTable').find('a[data-command="delete"]').each(function () {
        var id = $(this).data('id');

        var popover = confirmPopover($(this), translate('Information'), translate('DeleteBookmarkConfirm'), 'right', 'click', function (result) {
            if (result) {
                deleteBookmark(id, function () {
                    popover.popover('dispose');
                    $('a[data-id="' + id + '"]').closest('tr').fadeOut('fast', function () {
                        $(this).remove();
                    });
                });
            }
        });
    });
}

function bookmarkCommand() {
    $('[data-command="bookmark"]').each(function () {
        var link = $(this);
        $(link).unbind('click');

        $(link).on('click', function () {
            var commandArgs = $(this).attr('data-command-args');

            if (commandArgs == 'add') {
                var parentCard = $(this).closest('.card');
                if (parentCard.length == 0)
                    parentCard = $(this).closest('.doc-results');

                createBookmark(parentCard, link);

                $(this).attr('data-command-args', 'delete');
            }
            else {
                var id = $(link).attr('data-id');

                var popover = confirmPopover($(this), translate('Information'), translate('DeleteBookmarkConfirm'), 'right', 'manual', function (result) {
                    if (result)
                        deleteBookmark(id, function () {
                            $(link).find('.icon-bookmark-orange').removeClass('icon-bookmark-orange').addClass('icon-bookmark-purple');
                            $(popover).popover('dispose');

                            $(link).attr('data-command-args', 'add');
                            bookmarkCommand();
                        });
                });

                popover.popover('show');
            }
        });
    });
}

function setBookmarkPopoverToolbar(popover) {
    var id = $(popover).attr('data-id');
    $('.popover-bookmark[data-id="' + id + '"]').find('[data-command="delete"]').on('click', function () {
        deleteBookmark(id);
    });

    $('.popover-bookmark[data-id="' + id + '"]').find('[data-command="cancel"]').on('click', function () {
        $(popover).popover('hide');
    });
}

function deleteBookmark(id, callback) {
    var clientContext = new SP.ClientContext(_spPageContextInfo.siteAbsoluteUrl);
    var list = clientContext.get_web().get_lists().getByTitle('Bookmarks');

    var listItem = list.getItemById(id);

    listItem.deleteObject();

    clientContext.executeQueryAsync(
        function () {
            if (callback != null)
                callback();
        },
        function (sender, args) {
            console.log('Request failed. ' + args.get_message() + '\n' + args.get_stackTrace());
        }
    );
}

function createBookmark(card, link) {
    var title = $(card).find('.card-title').text();

    if ($(card).find('.card-title').attr('data-original-title') != null && $(card).find('.card-title').attr('data-original-title') != '')
        title = $(card).find('.card-title').attr('data-original-title');

    var url = $(card).find('.card-title').attr('href');

    if (title == null || title == '')
        title = $('#doc-title').text();

    if (url == null || url == '')
        url = decodeURIComponent(getUrlParam('doc'));

    var clientContext = new SP.ClientContext(_spPageContextInfo.siteAbsoluteUrl);
    var list = clientContext.get_web().get_lists().getByTitle('Bookmarks');

    var itemCreateInfo = new SP.ListItemCreationInformation();
    var listItem = list.addItem(itemCreateInfo);

    listItem.set_item('Title', title);

    var urlValue = new SP.FieldUrlValue();
    urlValue.set_url(url);
    urlValue.set_description(title);

    listItem.set_item("DocUrl", urlValue);

    listItem.update();

    clientContext.load(listItem);

    clientContext.executeQueryAsync(
        function () {
            $(link).attr('data-id', listItem.get_id());
            var icon = $(link).find('.icon-bookmark-purple');
            icon.removeClass('icon-bookmark-purple');
            icon.addClass('icon-bookmark-orange');

            bookmarkCommand();
        },
        function (sender, args) {
            console.log('Request failed. ' + args.get_message() + '\n' + args.get_stackTrace());
        }
    );
}

function checkBookmarks() {
    var clientContext = new SP.ClientContext(_spPageContextInfo.siteAbsoluteUrl);
    var list = clientContext.get_web().get_lists().getByTitle('Bookmarks');
    var camlQuery = new SP.CamlQuery();
    camlQuery.set_viewXml("<View><Query><Where><Eq><FieldRef Name='Author' LookupId='True'/><Value Type='Lookup'>" + _spPageContextInfo.userId + "</Value></Eq></Where><OrderBy><FieldRef Name='ID' Ascending='FALSE'/></OrderBy></Query></View>");

    var items = list.getItems(camlQuery);
    clientContext.load(items);

    clientContext.executeQueryAsync(
        function () {
            var bookmarks = new Array();
            var listItemEnumerator = items.getEnumerator();
            while (listItemEnumerator.moveNext()) {
                var result = listItemEnumerator.get_current();
                var title = result.get_item('DocUrl').get_url();
                bookmarks.push({
                    Title: title,
                    ID: result.get_id()
                });
            }

            $('.card').each(function () {
                var cardTitle = $(this).find('.card-title').attr('href');
                var index = bookmarks.map(function (e) { return e.Title; }).indexOf(cardTitle);
                if (index > -1) {
                    var icon = $(this).find('.icon-bookmark-purple');
                    icon.removeClass('icon-bookmark-purple');
                    icon.addClass('icon-bookmark-orange');

                    $(this).find('a[data-command="bookmark"]').attr('data-id', bookmarks[index].ID);
                }
            });

            $('.doc-results').each(function () {
                var cardTitle = $(this).find('.card-title').attr('href');
                var index = bookmarks.map(function (e) { return e.Title; }).indexOf(cardTitle);
                if (index > -1) {
                    var icon = $(this).find('.icon-bookmark-purple');
                    icon.removeClass('icon-bookmark-purple');
                    icon.addClass('icon-bookmark-orange');

                    $(this).find('a[data-command="bookmark"]').attr('data-id', bookmarks[index].ID);
                }
            });

            if ($('#document-info-page').length) {
                var docTitle = $('#doc-title').text();
                if (docTitle != null) {
                    var index = bookmarks.map(function (e) { return e.Title; }).indexOf(docTitle);
                    if (index > -1) {
                        var link = $('[data-command="bookmark"]');
                        $(link).attr('data-command-args', 'delete');

                        var icon = $(link).find('.icon-bookmark-purple');
                        icon.removeClass('icon-bookmark-purple');
                        icon.addClass('icon-bookmark-orange');

                        $(link).attr('data-id', bookmarks[index].ID);
                    }
                }
            }

            bookmarkCommand();
        },
        function (sender, args) {
            console.log('Request failed. ' + args.get_message() + '\n' + args.get_stackTrace());
        }
    );
}