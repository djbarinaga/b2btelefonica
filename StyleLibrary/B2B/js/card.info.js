﻿(function ($) {
    $.fn.documentInfo = function () {
        var $this = this;
        var maxLength = 45;
        var isDocResult = $(this).hasClass('doc-results');

        if (isDocResult)
            maxLength = 80;

        SP.SOD.executeFunc('sp.js', 'SP.ClientContext', function () {
            var relativeUrl = $($this).data('url');

            var clientContext = SP.ClientContext.get_current();
            var file = clientContext.get_web().getFileByServerRelativeUrl(relativeUrl);
            clientContext.load(file, 'ListItemAllFields');


            clientContext.executeQueryAsync(
                function () {
                    var docEnablers = $($this).find('.doc-enablers');

                    var result = file.get_listItemAllFields();
                    var item = result.get_fieldValues();
                    var title = item["Title"];
                    
                    if (title != null) {
                        if (title.length > maxLength) {
                            title = title.substring(0, maxLength - 3) + '...';
                            $($this).find('.card-title').text(title).attr('title', item["Title"]).tooltip();
                        }
                        else {
                            $($this).find('.card-title').text(title);
                        }
                    }

                    if (item["DocumentUse"] != null) {
                        $($this).find('.doc-use').text(item["DocumentUse"].toLowerCase());
                    }

                    if (item['DocLanguage']) {
                        $($this).find('[data-field="DocLanguage"]').text(item['DocLanguage'].toUpperCase());
                    }
                    else {
                        $($this).find('[data-field="DocLanguage"]').text('ES');
                    }

                    $($this).find('.card-title').removeClass('hide');
                },
                function (sender, args) {
                    console.log('Request failed. ' + args.get_message() + '\n' + args.get_stackTrace());
                }
            );
        });
    };
}(jQuery));