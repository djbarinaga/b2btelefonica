﻿(function ($) {
    $.fn.faq = function () {
        var $this = this;

        SP.SOD.executeFunc('sp.js', 'SP.ClientContext', function () {
            getFaqs();
        });

        function getFaqs() {
            var language = getCookie("culture");
            var clientContext = new SP.ClientContext(_spPageContextInfo.siteAbsoluteUrl);
            var list = clientContext.get_web().get_lists().getByTitle('Faqs');
            var camlQuery = new SP.CamlQuery();
            camlQuery.set_viewXml("<View><Query><Where><Eq><FieldRef Name='Idioma'/><Value Type='Text'>" + language + "</Value></Eq></Where></Query><OrderBy><FieldRef Name='Orden'/></OrderBy></View>");

            var items = list.getItems(camlQuery);
            clientContext.load(items);


            clientContext.executeQueryAsync(
                function () {
                    var listItemEnumerator = items.getEnumerator();

                    while (listItemEnumerator.moveNext()) {
                        var item = listItemEnumerator.get_current().get_fieldValues();
                        var title = item["Title"];
                        var content = item["Contenido"];
                        var order = item["Orden"];

                        var html = '<div class="doc-form border mb-3">';
                        html += '<div class="bg-light p-3 m-0" id="faq-' + order + '">';
                        html += '<h5 class="mb-0">';
                        html += '<a href="#" data-toggle="collapse" data-target="#collapse-' + order + '">';
                        html += order + '. ' + title;
                        html += '</a>';
                        html += '</h5>';
                        html += '</div>';
                        html += '<div id="collapse-' + order + '" class="collapse p-5">';
                        html += '<div>';
                        html += content;
                        html += '</div>';
                        html += '</div>';
                        html += '</div>';

                        $($this).append(html);
                    }
                },
                function (sender, args) {
                    console.log('Request failed. ' + args.get_message() + '\n' + args.get_stackTrace());
                }
            );
        }
    };
}(jQuery));

$(document).ready(function () {
    $('#faq-list').faq();
});