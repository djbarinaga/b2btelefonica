﻿$(document).ready(function () {
    $('#admin-links').hide();

    SP.SOD.executeFunc('sp.js', 'SP.ClientContext', function () {
        loadGroups();
    });

    $(document).on('click', '.flip-card', function () {
        var url = $(this).data('url');
        window.location.href = url;
    });
});

function loadGroups() {
    var clientContext = new SP.ClientContext(_spPageContextInfo.webAbsoluteUrl);
    var list = clientContext.get_web().get_lists().getByTitle('Grupos de administración');
    var query = SP.CamlQuery.createAllItemsQuery();
    var items = list.getItems(query);

    clientContext.load(items);

    clientContext.executeQueryAsync(
        function () {
            var listItemEnumerator = items.getEnumerator();
            var counter = 1;

            while (listItemEnumerator.moveNext()) {
                var item = listItemEnumerator.get_current();
                var title = item.get_item('Title');
                var description = item.get_item('Descripcion');
                var group = item.get_item('Grupo');
                var image = item.get_item('Imagen').get_url();
                var url = item.get_item('URL').get_url();

                $('#admin-links').append('<div class="flip-card" data-target="' + group + '" data-url="' + url + '"><div class="flip-card-inner"><div class="flip-card-front"><img src="' + image + '" class="img-fluid"><h3>' + title + '</h3></div><div class="flip-card-back"><h3>' + title + '</h3> <p>' + description + '</p> </div></div></div>');
            }

            sort();
        },
        function (sender, args) {
            console.log('Request failed. ' + args.get_message() + '\n' + args.get_stackTrace());
        }
    );
}

function loadAdminLinks() {
    var clientContext = new SP.ClientContext(_spPageContextInfo.webAbsoluteUrl);
    var list = clientContext.get_web().get_lists().getByTitle('Administracion');
    var query = SP.CamlQuery.createAllItemsQuery();
    var items = list.getItems(query);

    clientContext.load(items);

    clientContext.executeQueryAsync(
        function () {
            var listItemEnumerator = items.getEnumerator();

            while (listItemEnumerator.moveNext()) {
                var item = listItemEnumerator.get_current();
                var url = item.get_item('URL').get_url();

                $('#admin-links').append('<div data-target="' + item.get_item('Grupo').get_lookupValue() + '"><a style="color:#333;" href="' + url + '" target="_blank">' + item.get_item('Title') + '</a></div>');
            }

            sort();
        },
        function (sender, args) {
            console.log('Request failed. ' + args.get_message() + '\n' + args.get_stackTrace());
        }
    );
}

function sort() {
    var groups = new Array();
    $('#admin-links').find('[data-target]').each(function () {
        var group = $(this).data('target');
        if (groups.indexOf(group) == -1)
            groups.push(group);
    });

    for (var i = 0; i < groups.length; i++) {
        $('#admin-links').append('<div data-group="' + groups[i] + '" class="row"><h3 style="width:100%">' + groups[i] + '</h3></div>');
    }

    $('[data-target]').each(function () {
        var group = $(this).data('target');
        $('[data-group="' + group + '"]').append($(this));
    });

    $('#admin-links').show();
}