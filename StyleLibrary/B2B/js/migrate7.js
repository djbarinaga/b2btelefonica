﻿var targetDocuments = new Array();
var documentTypeImages = new Array();
var documentTypeDescriptions = new Array();
var documentTypes = new Array();
var documentUploaded = new Array();
var products = new Array();
var productFamily = new Array();
var enablers = new Array();
var permissions = new Array();
var maxLength = 16;
var use = ["Interno", "Externo"];
var currentIndex = 0;
var usersNotFound = new Array();


$(document).ready(function () {
    SP.SOD.executeFunc('sp.js', 'SP.ClientContext', function () {
        loadEnablers();
        loadDocumentTypeImages();
        loadDocumentTypes();
        //loadProductFamily();
        //loadProduct();
    });

    $('#btnDale').on('click', function () {
        loadDocuments();
        //setFoldersContentType();
    });
});

function saveMetadata(index) {
    if (index == targetDocuments.length) {
        alert('Fin');
        return;
    }

    var document = targetDocuments[index];
    var clientContext = SP.ClientContext.get_current();
    var list = clientContext.get_web().get_lists().getByTitle('Documentos B2B');
    var listItem = list.getItemById(document["id"]);

    clientContext.load(listItem);

    clientContext.executeQueryAsync(
        function () {
            var ct = listItem.get_contentType();

            clientContext.load(ct);

            clientContext.executeQueryAsync(
                function () {
                    var ctypeName = ct.get_name();
                    if (ctypeName.toLowerCase() != "carpeta") {
                        var im = getImage(ctypeName);
                        if (im == null)
                            im = documentTypeImages[0];

                        listItem.set_item('DocumentTypeImage', im.image);

                        listItem.update();

                        clientContext.executeQueryAsync(
                            function () {
                                $('#documentList').prepend('<p>Elemento:' + index + ' actualizado.</p>');
                                var currentIndex = index;
                                currentIndex++;
                                saveMetadata(currentIndex);
                            },
                            function (sender, args) {
                                var currentIndex = index;
                                currentIndex++;
                                saveMetadata(currentIndex);

                                console.log('Request failed. ' + args.get_message() + '\n' + args.get_stackTrace());
                            }
                        );
                    }
                    else {
                        var currentIndex = index;
                        currentIndex++;
                        saveMetadata(currentIndex);
                    }
                },
                function (sender, args) {
                    var currentIndex = index;
                    currentIndex++;
                    saveMetadata(currentIndex);

                    console.log('Request failed. ' + args.get_message() + '\n' + args.get_stackTrace());
                }
            );
        },
        function (sender, args) {
            var currentIndex = index;
            currentIndex++;
            saveMetadata(currentIndex);

            console.log('Request failed. ' + args.get_message() + '\n' + args.get_stackTrace());
        }
    );
}

function findDocument(fileName) {
    var docFound = null;

    for (var i = 0; i < documentsB2B.length; i++) {
        var doc = documentsB2B[i];
        if (doc.File.toLowerCase() == fileName.toLowerCase()) {
            docFound = doc;
            break;
        }
    }

    return docFound;
}

function loadDocuments() {
    var clientContext = new SP.ClientContext(_spPageContextInfo.webAbsoluteUrl);
    var list = clientContext.get_web().get_lists().getByTitle('Documentos B2B');
    var query = SP.CamlQuery.createAllItemsQuery();
    var items = list.getItems(query);

    clientContext.load(items);

    clientContext.executeQueryAsync(
        function () {
            var listItemEnumerator = items.getEnumerator();

            while (listItemEnumerator.moveNext()) {
                var item = listItemEnumerator.get_current();

                targetDocuments.push({
                    id: item.get_id(),
                    fileName: item.get_item('FileLeafRef')
                });
            }


            $('#documentList').prepend('<p>Documentos cargados.</p>');
            setUsers(0);

            //saveMetadata(0);
        },
        function (sender, args) {
            console.log('Request failed. ' + args.get_message() + '\n' + args.get_stackTrace());
        }
    );
}

function loadEnablers() {
    var clientContext = new SP.ClientContext(_spPageContextInfo.siteAbsoluteUrl);
    var list = clientContext.get_web().get_lists().getByTitle('Enablers');
    var query = SP.CamlQuery.createAllItemsQuery();
    var items = list.getItems(query);

    clientContext.load(items);

    clientContext.executeQueryAsync(
        function () {
            var listItemEnumerator = items.getEnumerator();

            while (listItemEnumerator.moveNext()) {
                var item = listItemEnumerator.get_current();
                var docType = item.get_item('B2BContentType');
                var enabler = item.get_item('Enabler');

                enablers.push({
                    documentType: docType,
                    enablers: enabler
                });
            }

            $('#documentList').prepend('<p>Enablers cargados.</p>');
        },
        function (sender, args) {
            console.log('Request failed. ' + args.get_message() + '\n' + args.get_stackTrace());
        }
    );
}

function loadPermissions() {
    var clientContext = new SP.ClientContext(_spPageContextInfo.siteAbsoluteUrl);
    var list = clientContext.get_web().get_lists().getByTitle('Permisos por tipo de documento');
    var query = SP.CamlQuery.createAllItemsQuery();
    var items = list.getItems(query);

    clientContext.load(items);

    clientContext.executeQueryAsync(
        function () {
            var listItemEnumerator = items.getEnumerator();

            while (listItemEnumerator.moveNext()) {
                var item = listItemEnumerator.get_current();
                var docType = item.get_item('B2BContentType');
                var securityGroup = item.get_item('SecurityGroup');
                var permission = item.get_item('PermissionLevel');
                var permissionLevel;

                switch (permission) {
                    case 'Contributor':
                        permissionLevel = SP.RoleType.contributor;
                        break;
                    case 'Administrator':
                        permissionLevel = SP.RoleType.administrator;
                        break;
                    default:
                        permissionLevel = SP.RoleType.reader;
                        break;
                }

                permissions.push({
                    documentType: docType,
                    securityGroup: securityGroup,
                    permission: permissionLevel
                });
            }
        },
        function (sender, args) {
            console.log('Request failed. ' + args.get_message() + '\n' + args.get_stackTrace());
        }
    );
}

function loadDocumentTypeImages() {
    var clientContext = new SP.ClientContext(_spPageContextInfo.siteAbsoluteUrl);
    var list = clientContext.get_web().get_lists().getByTitle('Imagenes tipos de documento');
    var query = SP.CamlQuery.createAllItemsQuery();
    var items = list.getItems(query);

    clientContext.load(items);

    clientContext.executeQueryAsync(
        function () {
            var listItemEnumerator = items.getEnumerator();

            while (listItemEnumerator.moveNext()) {
                var item = listItemEnumerator.get_current();
                documentTypeImages.push(
                    {
                        documentType: item.get_item('B2BContentType'),
                        image: item.get_item('Image').get_url()
                    }
                );
            }

            $('#documentList').prepend('<p>Imágenes cargadas.</p>');
        },
        function (sender, args) {
            console.log('Request failed. ' + args.get_message() + '\n' + args.get_stackTrace());
        }
    );
}

function loadProductFamily() {
    var clientContext = new SP.ClientContext(_spPageContextInfo.siteAbsoluteUrl);
    var list = clientContext.get_web().get_lists().getByTitle('Familia de productos');
    var query = SP.CamlQuery.createAllItemsQuery();
    var items = list.getItems(query);

    clientContext.load(items);

    clientContext.executeQueryAsync(
        function () {
            var listItemEnumerator = items.getEnumerator();

            while (listItemEnumerator.moveNext()) {
                var item = listItemEnumerator.get_current();
                productFamily.push({
                    id: item.get_id(),
                    name: item.get_item('Title'),
                    code: item.get_item('Code')
                });
            }

            $('#documentList').prepend('<p>Familias de productos cargadas.</p>');
        },
        function (sender, args) {
            console.log('Request failed. ' + args.get_message() + '\n' + args.get_stackTrace());
        }
    );
}

function loadProduct() {
    var clientContext = new SP.ClientContext(_spPageContextInfo.siteAbsoluteUrl);
    var list = clientContext.get_web().get_lists().getByTitle('Productos');
    var query = SP.CamlQuery.createAllItemsQuery();
    var items = list.getItems(query);

    clientContext.load(items);

    clientContext.executeQueryAsync(
        function () {
            var listItemEnumerator = items.getEnumerator();

            while (listItemEnumerator.moveNext()) {
                var item = listItemEnumerator.get_current();
                products.push({
                    id: item.get_id(),
                    familyId: item.get_item('ProductFamily').get_lookupId(),
                    name: item.get_item('Title'),
                    code: item.get_item('Code')
                });
            }

            $('#documentList').prepend('<p>Productos cargadas.</p>');
        },
        function (sender, args) {
            console.log('Request failed. ' + args.get_message() + '\n' + args.get_stackTrace());
        }
    );
}

function loadDocumentTypes() {
    var clientContext = new SP.ClientContext(_spPageContextInfo.webAbsoluteUrl);
    var list = clientContext.get_web().get_lists().getByTitle('Documentos B2B');
    var contentTypes = list.get_contentTypes();

    clientContext.load(contentTypes);

    clientContext.executeQueryAsync(
        function () {
            var results = [];
            var contentTypesEnumerator = contentTypes.getEnumerator();

            while (contentTypesEnumerator.moveNext()) {
                var cType = contentTypesEnumerator.get_current();
                documentTypes.push({
                    id: cType.get_id(),
                    name: cType.get_name()
                });
            }

            $('#documentList').prepend('<p>Document types cargados.</p>');
        },
        function (sender, args) {
            console.log('Request failed. ' + args.get_message() + '\n' + args.get_stackTrace());
        }
    );
}

function getProduct(familyId) {
    var product = null;

    for (var i = 0; i < products.length; i++) {
        if (products[i].familyId == familyId) {
            product = products[i];
            break;
        }
    }

    return product;
}

function getImage(cType) {
    var image = null;

    for (var i = 0; i < documentTypeImages.length; i++) {
        if (documentTypeImages[i].documentType == cType) {
            image = documentTypeImages[i];
            break;
        }
    }

    return image;
}

function getDocumentType(cType) {
    var ct = null;

    for (var i = 0; i < documentTypes.length; i++) {
        if (documentTypes[i].name.toLowerCase() == cType.toLowerCase()) {
            ct = documentTypes[i];
            break;
        }
    }

    return ct;
}

function checkDocumentType(cType) {
    var exists = false;

    for (var i = 0; i < documentTypes.length; i++) {
        if (documentTypes[i].name == cType) {
            exists = true;
            break;
        }
    }

    return exists;
}

function getEnabler(cType) {
    var enabler = null;

    for (var i = 0; i < enablers.length; i++) {
        if (enablers[i].documentType.toLowerCase() == cType.toLowerCase()) {
            enabler = enablers[i];
            break;
        }
    }

    return enabler;
}

function dale() {
    if (currentIndex == documentsB2B.length) {
        alert('Fin');
    }
    var sourceDocument = documentsB2B[currentIndex];
    if (sourceDocument["Familia de producto"] != '') {
        //Cogemos el nombre del documento
        var fileName = sourceDocument.File;

        var clientContext = new SP.ClientContext(_spPageContextInfo.webAbsoluteUrl);
        var list = clientContext.get_web().get_lists().getByTitle('Documentos B2B');
        var camlQuery = new SP.CamlQuery();
        camlQuery.set_viewXml("<Where><Contains><FieldRef Name='FileLeafRef'/><Value Type='Text'>" + fileName + "</Value></Contains></Where><RowLimit>1</RowLimit>");

        var items = list.getItems(camlQuery);
        clientContext.load(items);

        clientContext.executeQueryAsync(
            function () {
                var listItemEnumerator = items.getEnumerator();
                while (listItemEnumerator.moveNext()) {
                    var listItem = listItemEnumerator.get_current();

                    listItem.set_item('ProductFamily', sourceDocument["Familia de producto"]);

                    if (sourceDocument["Producto"] != '')
                        listItem.set_item('Product', sourceDocument["Producto"]);

                    if (sourceDocument["Tipo de documento"] != "") {
                        var dc = getDocumentType(sourceDocument["Tipo de documento"]);
                        listItem.set_item('ContentTypeId', dc.id);

                        var im = getImage(sourceDocument["Tipo de documento"]);
                        if (im == null)
                            im = documentTypeImages[0];

                        listItem.set_item('DocumentTypeImage', im.image);
                    }

                    if (sourceDocument["Uso"] != '') {
                        listItem.set_item('DocumentUse', sourceDocument["Uso"]);
                    }

                    $('#documentList').prepend('<p>Elemento:' + listItem.get_id() + '.</p>');

                    listItem.update();

                    clientContext.executeQueryAsync(
                        function () {
                            $('#documentList').prepend('<p>Elemento:' + listItem.get_id() + ' actualizado.</p>');
                            currentIndex++;
                            dale();
                        },
                        function (sender, args) {
                            currentIndex++;
                            dale();

                            console.log('Request failed. ' + args.get_message() + '\n' + args.get_stackTrace());
                        }
                    );
                }
            },
            function (sender, args) {
                console.log('Request failed. ' + args.get_message() + '\n' + args.get_stackTrace());
            }
        );
    }
}

function setFoldersContentType() {
    var ctx = SP.ClientContext.get_current();
    var list = ctx.get_web().get_lists().getByTitle("Documentos B2B");

    var setFolderInternal = function (parentFolder) {
        var ctx = parentFolder.get_context();
        var curFolders = parentFolder.get_folders();
        ctx.load(curFolders);
        ctx.executeQueryAsync(
            function () {
                var folderEnumerator = curFolders.getEnumerator();
                while (folderEnumerator.moveNext()) {
                    var curFolder = folderEnumerator.get_current();
                    var folderName = curFolder.get_name();

                    $('#documentList').prepend('<p>Elemento:' + folderName + '</p>');

                    var listItem = curFolder.get_listItemAllFields();
                    listItem.set_item('ContentTypeId', '0x0120');

                    listItem.update();

                    ctx.executeQueryAsync(
                        function () {
                            $('#documentList').prepend('<p>' + folderName + ' actualizado.</p>');
                            setFolderInternal(curFolder);
                        },
                        function (sender, args) {
                            console.log('Request failed. ' + args.get_message() + '\n' + args.get_stackTrace());
                        }
                    );
                }
            },
            function (sender, args) {
                console.log('Request failed. ' + args.get_message() + '\n' + args.get_stackTrace());
            });
    };

    setFolderInternal(list.get_rootFolder());
}

function checkFiles(index) {
    if (index == docJSON.length) {
        alert('Fin');
        return;
    }
    var currentDocument = docJSON[index];
    if (currentDocument.TipoDocumento != '') {
        var existsContentType = checkDocumentType(currentDocument.TipoDocumento);
        if (!existsContentType) {
            $('#documentList').prepend('<p>' + currentDocument.File + ': ' + currentDocument.TipoDocumento + '</p>');
            var contentType = getDocumentType(currentDocument.TipoDocumento);

            $('#documentList').prepend('<p>' + currentDocument.File + ': ' + contentType.name + '</p>');

            var image = getImage(contentType.name);

            var properties = {};
            properties["ContentTypeId"] = contentType.id;
            properties["DocumentTypeImage"] = image.image;

            updateListItem(currentDocument.File, properties, function () {
                $('#documentList').prepend('<p>Documento ' + currentDocument.File + ' actualizado</p>');
                var currentIndex = index;
                currentIndex++;
                checkFiles(currentIndex);
            });
        }   
        else {
            var currentIndex = index;
            currentIndex++;
            checkFiles(currentIndex);
        }
    }
    else {
        var currentIndex = index;
        currentIndex++;
        checkFiles(currentIndex);
    }
}

function updateListItem(fileName, properties, callback) {
    var targetDocument = null;
    for (var i = 0; i < targetDocuments.length; i++) {
        if (targetDocuments[i].fileName == fileName) {
            targetDocument = targetDocuments[i];
            break;
        }
    }

    if (targetDocument != null) {
        var context = SP.ClientContext.get_current();
        var list = context.get_web().get_lists().getByTitle('Documentos B2B');

        var listItem = list.getItemById(targetDocument.id);

        for (var propName in properties) {
            listItem.set_item(propName, properties[propName]);
        }

        listItem.update();

        context.executeQueryAsync(
            function () {
                callback(true);
            },
            function () {
                callback(false);
            });
    }
    else {
        callback(false);
    }
}

function setUsers(index) {
    if (index == docJSON.length) {
        $('#documentList').empty();

        for (var i = 0; i < usersNotFound.length; i++) {
            $('#documentList').prepend('<p>' + usersNotFound[i] + '</p>');
        }

        alert('Fin');
        return;
    }

    var currentIndex = index;
    var currentDocument = docJSON[index];

    if (currentDocument.CreatedBy != '') {
        var userLogin = "i:0#.f|membership|" + currentDocument.CreatedBy;
        ensureUser(userLogin, function (result) {
            if (result != null) {
                var user = new SP.FieldUserValue();
                user.set_lookupId(result);

                var properties = {};
                properties["Editor"] = user;
                properties["ResponsibleText"] = currentDocument.CreatedBy;
                properties["Responsible"] = user;

                updateListItem(currentDocument.File, properties, function (data) {
                    if (data) {
                        $('#documentList').prepend('<p style="color:green;">Documento ' + currentDocument.File + ' actualizado a ' + currentDocument.CreatedBy + '</p>');
                    }

                    currentIndex++;
                    setUsers(currentIndex);
                });
            }
            else {
                if (usersNotFound.indexOf(currentDocument.CreatedBy) == -1)
                    usersNotFound.push(currentDocument.CreatedBy);

                var properties = {};
                properties["ResponsibleText"] = currentDocument.CreatedBy;

                updateListItem(currentDocument.File, properties, function (data) {
                    if (data) {
                        $('#documentList').prepend('<p style="color:red;">Usuario ' + currentDocument.CreatedBy + ' no encontrado</p>');
                        $('#documentList').prepend('<p style="color:red;">Documento ' + currentDocument.File + ' actualizado</p>');
                    }

                    currentIndex++;
                    setUsers(currentIndex);
                });
            }
        });
    }
    else {
        currentIndex++;
        setUsers(currentIndex);
    }
}

function ensureUser(userLogin, callback) {
    var context = new SP.ClientContext("https://telefonicacorp.sharepoint.com/sites/B2BFinder.OCEWU");
    var newUser = context.get_web().ensureUser(userLogin);
    context.load(newUser);

    context.executeQueryAsync(
        function () {
            callback(newUser.get_id());
        },
        function (sender, args) {
            callback(null);
        }
    );
}