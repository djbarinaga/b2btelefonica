﻿var template = '<div class="carousel-item {active}" style="background:url({fondo});background-size:cover;background-repeat:no-repeat;">' +
    '<div class="carousel-caption d-none d-md-block">' +
    '<h3>{title}</h3>' +
    '<p>{description}</p>' +
    '</div>' +
    '</div>';

$(document).ready(function () {
    SP.SOD.executeFunc('sp.js', 'SP.ClientContext', initCarousel);
});

function initCarousel() {
    var language = getCookie("culture");
    var clientContext = new SP.ClientContext(_spPageContextInfo.webAbsoluteUrl);
    var list = clientContext.get_web().get_lists().getByTitle('Banners');
    var camlQuery = new SP.CamlQuery();
    camlQuery.set_viewXml("<View><Query><Where><Eq><FieldRef Name='Idioma'/><Value Type='Text'>" + language + "</Value></Eq></Where></Query></View>");
    var items = list.getItems(camlQuery);

    clientContext.load(items);

    clientContext.executeQueryAsync(
        function () {
            var listItemEnumerator = items.getEnumerator();
            var counter = 0;

            while (listItemEnumerator.moveNext()) {
                var item = listItemEnumerator.get_current().get_fieldValues();
                var title = item["Title"];
                var description = item["Descripcion"];
                var fondo = item["Fondo"].get_url();

                var currentItem = template.replace('{fondo}', fondo).replace('{title}', title).replace('{description}', description);

                if (counter == 0)
                    currentItem = currentItem.replace('{active}', 'active');
                else
                    currentItem = currentItem.replace('{active}', '');

                $('.carousel-inner').append(currentItem);

                counter++;
            }


            //if (counter > 1) {
            //    var html = '<ol class="carousel-indicators">';

            //    for (var i = 0; i < counter; i++) {
            //        if (i == 0)
            //            html += '<li data-target="#carousel" data-slide-to="' + i + '" class="active"></li>';
            //        else
            //            html += '<li data-target="#carousel" data-slide-to="' + i + '"></li>';
            //    }

            //    html += '</ol>';

            //    $('#carousel').prepend(html);
            //}

        },
        function (sender, args) {
            console.log('Request failed. ' + args.get_message() + '\n' + args.get_stackTrace());
        }
    );
}