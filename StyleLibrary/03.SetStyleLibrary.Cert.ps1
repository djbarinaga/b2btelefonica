﻿$url = '<URL>'

Connect-PnPOnline -Url $url -Credentials (Get-Credential)

Function Create-StyleLibraryFolders {

	Write-Host 'Creando carpeta B2B en Style Library'
	Add-PnPFolder -Name B2B -Folder 'Style Library'

	Write-Host 'Creando carpeta css en Style Library/B2B'
	Add-PnPFolder -Name css -Folder 'Style Library/B2B'

	Write-Host 'Creando carpeta js en Style Library/B2B'
	Add-PnPFolder -Name js -Folder 'Style Library/B2B'

	Write-Host 'Creando carpeta images en Style Library/B2B'
	Add-PnPFolder -Name images -Folder 'Style Library/B2B'

	Write-Host 'Creando carpeta fonts en Style Library/B2B'
	Add-PnPFolder -Name fonts -Folder 'Style Library/B2B'
}

Function Upload-StyleLibreryFiles {

    Param(
            [parameter(Mandatory=$false)][Switch]$Css,
			[parameter(Mandatory=$false)][Switch]$Js,
			[parameter(Mandatory=$false)][Switch]$Images,
			[parameter(Mandatory=$false)][Switch]$Fonts,
			[parameter(Mandatory=$false)][Switch]$All
         )

	$web = Get-PnPWeb
	$relativeUrl = $web.ServerRelativeUrl

	#Hojas de estilo
	if($Css.IsPresent -or $All.IsPresent){
		Get-ChildItem "B2B\css" | 
		Foreach-Object {
    
			$filePath = $_.FullName
			$fileUrl = $relativeUrl + "/Style Library/B2B/css/" + $_.Name

			Write-Host 'Agregando archivo '$_.Name' a Style Library/B2B/css'
			Add-PnPFile -Path $filePath -Folder "Style Library/B2B/css" -CheckOut

			Write-Host 'Protegiendo archivo ' $fileUrl
			Set-PnPFileCheckedIn -Url $fileUrl -CheckinType Major
		}
	}

	#JavaScript
	if($Js.IsPresent -or $All.IsPresent){
		Get-ChildItem "B2B\js" | 
		Foreach-Object {
    
			$filePath = $_.FullName
			$fileUrl = $relativeUrl + "/Style Library/B2B/js/" + $_.Name

			Write-Host 'Agregando archivo '$_.Name' a Style Library/B2B/js'
			Add-PnPFile -Path $filePath -Folder "Style Library/B2B/js" -CheckOut

			Write-Host 'Protegiendo archivo ' $fileUrl
			Set-PnPFileCheckedIn -Url $fileUrl -CheckinType Major
		}
	}

	#Imágenes
	if($Images.IsPresent -or $All.IsPresent){
		Get-ChildItem "B2B\images" | 
		Foreach-Object {
    
			$filePath = $_.FullName
			$fileUrl = $relativeUrl + "/Style Library/B2B/images/" + $_.Name

			Write-Host 'Agregando archivo '$_.Name' a Style Library/B2B/images'
			Add-PnPFile -Path $filePath -Folder "Style Library/B2B/images" -CheckOut

			Write-Host 'Protegiendo archivo ' $fileUrl
			Set-PnPFileCheckedIn -Url $fileUrl -CheckinType Major
		}
	}

	#Fuentes
	if($Fonts.IsPresent -or $All.IsPresent){
		Get-ChildItem "B2B\fonts" | 
		Foreach-Object {
    
			$filePath = $_.FullName
			$fileUrl = $relativeUrl + "/Style Library/B2B/fonts/" + $_.Name

			Write-Host 'Agregando archivo '$_.Name' a Style Library/B2B/fonts'
			Add-PnPFile -Path $filePath -Folder "Style Library/B2B/fonts" -CheckOut

			Write-Host 'Protegiendo archivo ' $fileUrl
			Set-PnPFileCheckedIn -Url $fileUrl -CheckinType Major
		}
	}
}

Create-StyleLibraryFolders
Upload-StyleLibreryFiles -All