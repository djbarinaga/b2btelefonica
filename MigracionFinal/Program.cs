﻿using Microsoft.SharePoint.Client;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security;
using System.Text;
using System.Threading.Tasks;

namespace MigracionFinal
{
    class Program
    {
        static string[] files;
        static void Main(string[] args)
        {
            files = Directory.GetFiles(@"C:\Proyectos\Avantgarde\Telefónica\MIGRACION BUENO\No Migrados");
            SaveBinaryDirect(0);
        }

        public static void SaveBinaryDirect(int index)
        {
            string filePath = files[index];
            ClientContext ctx = new ClientContext("https://telefonicacorp.sharepoint.com/sites/B2BFinder.OCEWU/es-es");
            ctx.Credentials = new SharePointOnlineCredentials("djimenezbarinaga@gmail.com", GetSecureString("Siripiripr0m3t30"));

            using (FileStream fs = new FileStream(filePath, FileMode.Open))
            {
                Console.WriteLine($"Cargando archivo {filePath}");
                Microsoft.SharePoint.Client.File.SaveBinaryDirect(ctx, string.Format("/Documentos B2B/{0}", System.IO.Path.GetFileName(filePath)), fs, false);
            }

        }

        private static SecureString GetSecureString(string pwd)
        {
            char[] chars = pwd.ToCharArray();
            SecureString securePassword = new SecureString();

            for (int i = 0; i < chars.Length; i++)
            {
                securePassword.AppendChar(chars[i]);
            }

            return securePassword;
        }
    }
}
