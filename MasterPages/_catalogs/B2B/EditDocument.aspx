﻿<%@ Page Language="C#" Inherits="Microsoft.SharePoint.Publishing.PublishingLayoutPage,Microsoft.SharePoint.Publishing,Version=16.0.0.0,Culture=neutral,PublicKeyToken=71e9bce111e9429c" meta:progid="SharePoint.WebPartPage.Document" %>

<%@ Register TagPrefix="SharePointWebControls" Namespace="Microsoft.SharePoint.WebControls" Assembly="Microsoft.SharePoint, Version=16.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="WebPartPages" Namespace="Microsoft.SharePoint.WebPartPages" Assembly="Microsoft.SharePoint, Version=16.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="PublishingWebControls" Namespace="Microsoft.SharePoint.Publishing.WebControls" Assembly="Microsoft.SharePoint.Publishing, Version=16.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="PublishingNavigation" Namespace="Microsoft.SharePoint.Publishing.Navigation" Assembly="Microsoft.SharePoint.Publishing, Version=16.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<asp:Content Contentplaceholderid="PlaceHolderAdditionalPageHead" runat="server">
    <link rel="stylesheet" href='<asp:Literal runat="server" Text="<%$SPUrl:~sitecollection/Style Library/B2B/css/bootstrap-datepicker.css%>" />' />
    <script src='<asp:Literal runat="server" Text="<%$SPUrl:~sitecollection/Style Library/B2B/js/bootstrap-datepicker.min.js%>" />'></script>
    <script src='<asp:Literal runat="server" Text="<%$SPUrl:~sitecollection/Style Library/B2B/js/bootstrap-datepicker.es.min.js%>" />'></script>
    <script src='<asp:Literal runat="server" Text="<%$SPUrl:~sitecollection/Style Library/B2B/js/editdocument.js%>" />'></script>
</asp:Content>
<asp:Content Contentplaceholderid="PlaceHolderPageTitle" runat="server">
	<SharePointWebControls:FieldValue FieldName="Title" runat="server"/>
</asp:Content>
<asp:Content Contentplaceholderid="PlaceHolderPageTitleInTitleArea" runat="server">
	<SharePointWebControls:FieldValue FieldName="Title" runat="server"/>
</asp:Content>
<asp:Content Contentplaceholderid="PlaceHolderMain" runat="server">
    <PublishingWebControls:EditModePanel runat="server" CssClass="edit-mode-panel title-edit">
        <div class="container">
            <SharePointWebControls:TextField runat="server" FieldName="Title"/>
            <SharePointWebControls:NoteField runat="server" FieldName="Comments"/>
            <PublishingWebControls:RichImageField FieldName="PublishingRollupImage" runat="server" />
        </div>
	</PublishingWebControls:EditModePanel>
    <div id="search-div" class="row no-gutters">
        <div class="col">
            <div id="search-controls-div">
                <input type="text" class="form-control f-light" id="txtSearch" value="¡Escribe lo que quieras buscar!"/>
                <ul class="list-group" id="searchDropDown"></ul>
                <a href="#" id="btnSearch" data-intro="SearchButton">
                    <span class="icon-search"></span>
                </a>
            </div>
        </div>
    </div>
    <div id="page-header">
        <div class="container">
            <div class="row">
                <div id="page-image" class="col">
                    <img src="/sites/B2BFinder.OCEWU/style library/b2b/images/newdocument.png" class="img-fluid"/>
                </div>
                <div id="page-title" class="col-9">
                    <h2><span data-key="Edit" class="f-xlight">Nuevo</span>&nbsp;<span data-key="document_" class="f-regular">document_</span></h2>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row" id="document-metadata">
            <div class="col-12">
                <div class="form-row">
                    <div class="form-group col-4">
                        <img class="img-fluid" id="documentTypeImage"/>
                    </div>
                    <div class="form-group col-8">
                        <div class="form-row">
                            <div class="form-group col-12">
                                <label data-key="Title"></label>
                                <input type="text" id="txtTitle" class="form-control" />
                            </div>
                            
                        </div>
                        <div class="form-row">
                            <div class="form-group col-6">
                                <label data-key="DocumentType"></label>
                                <select id="selectDocumentType" class="form-control"></select>
                            </div>
                            <div class="form-group col-6">
                                <label data-key="Language"></label>
                                <select id="selectLanguage" class="form-control">
                                    <option value="es" data-key="Spanish"></option>
                                    <option value="en" data-key="English"></option>
                                </select>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-6">
                                <label data-key="ProductFamily"></label>
                                <select id="selectProductFamily" class="form-control"></select>
                            </div>
                            <div class="form-group col-6">
                                <label data-key="Product"></label>
                                <select id="selectProduct" class="form-control"></select>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col-12">
                        <label data-key="Description"></label>
                        <textarea class="form-control" rows="6" id="txtDescription"></textarea>
                    </div>
                </div>
                <div class="form-check">
                    <input class="form-check-input" type="checkbox" value="" id="chkInternalUse">
                    <label class="form-check-label" for="chkInternalUse" data-key="InternalUse"></label>
                </div>
                <button type="button" class="btn btn-blue09 float-right ml-2" data-key="Save" id="btnSave"></button>
                <button type="button" class="btn btn-gray float-right ml-2" data-key="Delete" id="btnDelete"></button>
                <button type="button" class="btn btn-gray float-right ml-2" data-key="Cancel" id="btnCancel"></button>
            </div>
        </div>
    </div>
</asp:Content>
