﻿<%@ Page Language="C#" Inherits="Microsoft.SharePoint.Publishing.PublishingLayoutPage,Microsoft.SharePoint.Publishing,Version=16.0.0.0,Culture=neutral,PublicKeyToken=71e9bce111e9429c" meta:progid="SharePoint.WebPartPage.Document" %>

<%@ Register TagPrefix="SharePointWebControls" Namespace="Microsoft.SharePoint.WebControls" Assembly="Microsoft.SharePoint, Version=16.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="WebPartPages" Namespace="Microsoft.SharePoint.WebPartPages" Assembly="Microsoft.SharePoint, Version=16.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="PublishingWebControls" Namespace="Microsoft.SharePoint.Publishing.WebControls" Assembly="Microsoft.SharePoint.Publishing, Version=16.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="PublishingNavigation" Namespace="Microsoft.SharePoint.Publishing.Navigation" Assembly="Microsoft.SharePoint.Publishing, Version=16.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<asp:Content Contentplaceholderid="PlaceHolderAdditionalPageHead" runat="server">
    <link rel="stylesheet" href='<asp:Literal runat="server" Text="<%$SPUrl:~sitecollection/Style Library/B2B/css/bootstrap-datepicker.css%>" />' />
    <script src='<asp:Literal runat="server" Text="<%$SPUrl:~sitecollection/Style Library/B2B/js/bootstrap-datepicker.min.js%>" />'></script>
    <script src='<asp:Literal runat="server" Text="<%$SPUrl:~sitecollection/Style Library/B2B/js/bootstrap-datepicker.es.min.js%>" />'></script>
    <script src='<asp:Literal runat="server" Text="<%$SPUrl:~sitecollection/Style Library/B2B/js/newdocument.js%>" />'></script>
</asp:Content>
<asp:Content Contentplaceholderid="PlaceHolderPageTitle" runat="server">
	<SharePointWebControls:FieldValue FieldName="Title" runat="server"/>
</asp:Content>
<asp:Content Contentplaceholderid="PlaceHolderPageTitleInTitleArea" runat="server">
	<SharePointWebControls:FieldValue FieldName="Title" runat="server"/>
</asp:Content>
<asp:Content Contentplaceholderid="PlaceHolderMain" runat="server">
    <div id="search-div" class="row no-gutters">
        <div class="col">
            <div id="search-controls-div">
                <input type="text" class="form-control f-light" id="txtSearch" value="¡Escribe lo que quieras buscar!"/>
                <ul class="list-group" id="searchDropDown"></ul>
                <a href="#" id="btnSearch" data-intro="SearchButton">
                    <span class="icon-search"></span>
                </a>
            </div>
        </div>
    </div>
    <div id="page-header">
        <div class="container">
            <div class="row">
                <div id="page-image" class="col">
                    <img src="/sites/B2BFinder.OCEWU/style library/b2b/images/newdocument.png" class="img-fluid"/>
                </div>
                <div id="page-title" class="col-9">
                    <h2><span data-key="New" class="f-xlight">Nuevo</span>&nbsp;<span data-key="document_" class="f-regular">document_</span></h2>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row" id="drag-div-container">
            <p><strong data-key="IMPORTANT"></strong> <span data-key="NewDocumentWarning"></span></p>
            <div class="col-12" id="drag-div">
                <div id="drop-file">
                    <p data-key="DropFiles"></p>
                    <p data-key="Or"></p>
                    <p data-key="SelectOption"></p>
                    <button type="button" id="btnSelectFile" class="btn btn-gray" data-key="SelectFile">Seleccionar fichero</button>
                    <a role="button" id="btnAddLink" href="enlace-a-documento.aspx" class="btn btn-gray" data-key="AddLinkToDocument"></a>
                    <input type="file" id="txtFile" style="display:none;" multiple/>
                </div>
                <div id="file-info" class="row"></div>
            </div>
        </div>
        <div class="row" id="document-metadata" style="display:none;">
            <div class="col-12">
                <div class="accordion mb-5" id="accordion">
                    <div class="doc-form border mb-3">
                        <h5 data-id="doc-name" class="bg-light p-3 m-0">
                            <a href="#" data-toggle="collapse" data-id="doc-url" data-target=""><span data-key="AddDocumentInfo"></a>
                        </h5>
                        <div data-id="doc-collapse" class="collapse p-5">
                            <div class="form-row">
                                <div class="form-group col-4">
                                    <img class="img-fluid" data-id="documentTypeImage"/>
                                </div>
                                <div class="form-group col-8">
                                    <div class="form-row">
                                        <div class="form-group col-12">
                                            <label data-key="Title"></label>
                                            <input type="text" data-id="txtTitle" class="form-control" />
                                        </div>
                                    </div>
                                    <div class="form-row">
                                        <div class="form-group col-8">
                                            <label data-key="DocumentType"></label>
                                            <select data-id="selectDocumentType" class="form-control"></select>
                                        </div>
                                        <div class="form-group col-4">
                                            <label data-key="Language"></label>
                                            <select data-id="selectLanguage" class="form-control">
                                                <option value="es" data-key="Spanish"></option>
                                                <option value="en" data-key="English"></option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-row">
                                        <div class="form-group col-6">
                                            <label data-key="ProductFamily"></label>
                                            <select data-id="selectProductFamily" class="form-control"></select>
                                        </div>
                                        <div class="form-group col-6">
                                            <label data-key="Product"></label>
                                            <select data-id="selectProduct" class="form-control">
                                                <option value="-1" data-key="SelectProductFamily"></option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="form-group col-12">
                                    <label data-key="Description"></label>
                                    <textarea class="form-control" rows="6" data-id="txtDescription"></textarea>
                                </div>
                            </div>
                            <div class="form-check">
                                <input class="form-check-input" type="checkbox" value="" data-id="chkInternalUse">
                                <label class="form-check-label" for="chkInternalUse" data-key="InternalUse"></label>
                            </div>
                        </div>
                    </div>
                </div>

                <button type="button" class="btn btn-blue09 float-right ml-2" data-key="Save" id="btnSave"></button>
                <button type="button" class="btn btn-gray float-right ml-2" data-key="Cancel" id="btnCancel"></button>
            </div>
        </div>
    </div>
</asp:Content>
