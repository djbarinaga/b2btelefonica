﻿<%@ Page Language="C#" Inherits="Microsoft.SharePoint.Publishing.PublishingLayoutPage,Microsoft.SharePoint.Publishing,Version=16.0.0.0,Culture=neutral,PublicKeyToken=71e9bce111e9429c" meta:progid="SharePoint.WebPartPage.Document" %>

<%@ Register TagPrefix="SharePointWebControls" Namespace="Microsoft.SharePoint.WebControls" Assembly="Microsoft.SharePoint, Version=16.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="WebPartPages" Namespace="Microsoft.SharePoint.WebPartPages" Assembly="Microsoft.SharePoint, Version=16.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="PublishingWebControls" Namespace="Microsoft.SharePoint.Publishing.WebControls" Assembly="Microsoft.SharePoint.Publishing, Version=16.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="PublishingNavigation" Namespace="Microsoft.SharePoint.Publishing.Navigation" Assembly="Microsoft.SharePoint.Publishing, Version=16.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<asp:Content Contentplaceholderid="PlaceHolderAdditionalPageHead" runat="server">
	<script src='<asp:Literal runat="server" Text="<%$SPUrl:~sitecollection/Style Library/B2B/js/pageconfig.js%>" />'></script>
</asp:Content>
<asp:Content Contentplaceholderid="PlaceHolderPageTitle" runat="server">
	<SharePointWebControls:FieldValue FieldName="Title" runat="server"/>
</asp:Content>
<asp:Content Contentplaceholderid="PlaceHolderPageTitleInTitleArea" runat="server">
	<SharePointWebControls:FieldValue FieldName="Title" runat="server"/>
</asp:Content>
<asp:Content Contentplaceholderid="PlaceHolderMain" runat="server">
    <PublishingWebControls:EditModePanel runat="server" CssClass="edit-mode-panel title-edit">
        <div class="container">
            <SharePointWebControls:TextField runat="server" FieldName="Title"/>
        </div>
	</PublishingWebControls:EditModePanel>
    <div id="search-div" class="row no-gutters">
        <div class="col">
            <div id="search-controls-div">
                <input type="text" class="form-control f-light" id="txtSearch" value="¡Escribe lo que quieras buscar!"/>
                <ul class="list-group" id="searchDropDown"></ul>
                <a href="#" id="btnSearch" data-intro="SearchButton">
                    <span class="icon-search"></span>
                </a>
            </div>
        </div>
    </div>
    <div id="page-header">
        <div class="container">
            <div class="row">
                <div id="page-title">
                    <h2 class="f-regular">
                        <SharePointWebControls:FieldValue FieldName="Title" runat="server"/>
                    </h2>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-md-4">
                <div class="admin-card">
                    <PublishingWebControls:RichImageField FieldName="PublishingRollupImage" AllowHyperLinks="false" runat="server" />
                </div>
            </div>
            <div class="col-md-8">
                <PublishingWebControls:RichHtmlField FieldName="PublishingPageContent" HasInitialFocus="True" MinimumEditHeight="400px" runat="server"/>
            </div>
        </div>
        <div class="row">
            <WebPartPages:WebPartZone runat="server" Title="Zona principal" ID="WebPartZone7"><ZoneTemplate></ZoneTemplate></WebPartPages:WebPartZone>
        </div>
    </div>
</asp:Content>
