﻿<%@ Page Language="C#" Inherits="Microsoft.SharePoint.Publishing.PublishingLayoutPage,Microsoft.SharePoint.Publishing,Version=16.0.0.0,Culture=neutral,PublicKeyToken=71e9bce111e9429c" meta:progid="SharePoint.WebPartPage.Document" %>

<%@ Register TagPrefix="SharePointWebControls" Namespace="Microsoft.SharePoint.WebControls" Assembly="Microsoft.SharePoint, Version=16.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="WebPartPages" Namespace="Microsoft.SharePoint.WebPartPages" Assembly="Microsoft.SharePoint, Version=16.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="PublishingWebControls" Namespace="Microsoft.SharePoint.Publishing.WebControls" Assembly="Microsoft.SharePoint.Publishing, Version=16.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="PublishingNavigation" Namespace="Microsoft.SharePoint.Publishing.Navigation" Assembly="Microsoft.SharePoint.Publishing, Version=16.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<asp:Content Contentplaceholderid="PlaceHolderAdditionalPageHead" runat="server">
    <script type="text/javascript" src="/sites/B2BFinder.OCEWU/Style Library/b2b/js/admin-links.js"></script>
</asp:Content>
<asp:Content Contentplaceholderid="PlaceHolderPageTitle" runat="server">
	<SharePointWebControls:FieldValue FieldName="Title" runat="server"/>
</asp:Content>
<asp:Content Contentplaceholderid="PlaceHolderPageTitleInTitleArea" runat="server">
	<SharePointWebControls:FieldValue FieldName="Title" runat="server"/>
</asp:Content>
<asp:Content Contentplaceholderid="PlaceHolderMain" runat="server">
    <div id="search-div" class="row no-gutters">
        <div class="col">
            <div id="search-controls-div">
                <input type="text" class="form-control f-light" id="txtSearch" value="¡Escribe lo que quieras buscar!"/>
                <ul class="list-group" id="searchDropDown"></ul>
                <a href="#" id="btnSearch" data-intro="SearchButton">
                    <span class="icon-search"></span>
                </a>
            </div>
        </div>
    </div>
    <div id="page-header">
        <div class="container">
            <div class="row">
                <div id="page-image" class="col">
                    <img src="/sites/B2BFinder.OCEWU/es-es/PublishingImages/iconos/admin.png" class="img-fluid"/>
                </div>
                <div id="page-title" class="col-9">
                    <h2><span data-key="Administration_" class="f-regular"></span></h2>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div id="admin-links"></div>
    </div>
</asp:Content>
