﻿<%@ Page Language="C#" Inherits="Microsoft.SharePoint.Publishing.PublishingLayoutPage,Microsoft.SharePoint.Publishing,Version=16.0.0.0,Culture=neutral,PublicKeyToken=71e9bce111e9429c" meta:progid="SharePoint.WebPartPage.Document" %>

<%@ Register TagPrefix="SharePointWebControls" Namespace="Microsoft.SharePoint.WebControls" Assembly="Microsoft.SharePoint, Version=16.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="WebPartPages" Namespace="Microsoft.SharePoint.WebPartPages" Assembly="Microsoft.SharePoint, Version=16.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="PublishingWebControls" Namespace="Microsoft.SharePoint.Publishing.WebControls" Assembly="Microsoft.SharePoint.Publishing, Version=16.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="PublishingNavigation" Namespace="Microsoft.SharePoint.Publishing.Navigation" Assembly="Microsoft.SharePoint.Publishing, Version=16.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<asp:Content Contentplaceholderid="PlaceHolderAdditionalPageHead" runat="server">
	<script src='<asp:Literal runat="server" Text="<%$SPUrl:~sitecollection/Style Library/B2B/js/pageconfig.js%>" />'></script>
</asp:Content>
<asp:Content Contentplaceholderid="PlaceHolderPageTitle" runat="server">
	<SharePointWebControls:FieldValue FieldName="Title" runat="server"/>
</asp:Content>
<asp:Content Contentplaceholderid="PlaceHolderPageTitleInTitleArea" runat="server">
	<SharePointWebControls:FieldValue FieldName="Title" runat="server"/>
</asp:Content>
<asp:Content Contentplaceholderid="PlaceHolderMain" runat="server">
    <PublishingWebControls:EditModePanel runat="server" CssClass="edit-mode-panel title-edit">
        <div class="container">
            <SharePointWebControls:TextField runat="server" FieldName="Title"/>
            <SharePointWebControls:NoteField runat="server" FieldName="Comments"/>
            <PublishingWebControls:RichImageField FieldName="PublishingRollupImage" runat="server" />
        </div>
	</PublishingWebControls:EditModePanel>
    <div id="page-header">
        <div id="page-title">
            <h2><SharePointWebControls:FieldValue FieldName="Title" runat="server"/></h2>
        </div>
        <div id="page-caption"></div>
        <div id="page-image"></div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-3">
                <WebPartPages:WebPartZone runat="server" Title="Zona izquierda" ID="WebPartZone1"><ZoneTemplate></ZoneTemplate></WebPartPages:WebPartZone>
            </div>
            <div class="col-9">
                <WebPartPages:WebPartZone runat="server" Title="Zona principal" ID="WebPartZone7"><ZoneTemplate></ZoneTemplate></WebPartPages:WebPartZone>
            </div>
        </div>
    </div>
</asp:Content>
