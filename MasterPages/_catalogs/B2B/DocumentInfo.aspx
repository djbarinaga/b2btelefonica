﻿<%@ Page Language="C#" Inherits="Microsoft.SharePoint.Publishing.PublishingLayoutPage,Microsoft.SharePoint.Publishing,Version=16.0.0.0,Culture=neutral,PublicKeyToken=71e9bce111e9429c" meta:progid="SharePoint.WebPartPage.Document" %>

<%@ Register TagPrefix="SharePointWebControls" Namespace="Microsoft.SharePoint.WebControls" Assembly="Microsoft.SharePoint, Version=16.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="WebPartPages" Namespace="Microsoft.SharePoint.WebPartPages" Assembly="Microsoft.SharePoint, Version=16.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="PublishingWebControls" Namespace="Microsoft.SharePoint.Publishing.WebControls" Assembly="Microsoft.SharePoint.Publishing, Version=16.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="PublishingNavigation" Namespace="Microsoft.SharePoint.Publishing.Navigation" Assembly="Microsoft.SharePoint.Publishing, Version=16.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<asp:Content Contentplaceholderid="PlaceHolderAdditionalPageHead" runat="server">
    <link rel="stylesheet" href='<asp:Literal runat="server" Text="<%$SPUrl:~sitecollection/Style Library/B2B/css/bootstrap-datepicker.css%>" />' />
    <script src='<asp:Literal runat="server" Text="<%$SPUrl:~sitecollection/Style Library/B2B/js/bootstrap-datepicker.min.js%>" />'></script>
    <script src='<asp:Literal runat="server" Text="<%$SPUrl:~sitecollection/Style Library/B2B/js/bootstrap-datepicker.es.min.js%>" />'></script>
    <script src='<asp:Literal runat="server" Text="<%$SPUrl:~sitecollection/Style Library/B2B/js/pageconfig.js%>" />'></script>
    <script src='<asp:Literal runat="server" Text="<%$SPUrl:~sitecollection/Style Library/B2B/js/document.info.js%>" />'></script>
</asp:Content>
<asp:Content Contentplaceholderid="PlaceHolderPageTitle" runat="server">
	<SharePointWebControls:FieldValue FieldName="Title" runat="server"/>
</asp:Content>
<asp:Content Contentplaceholderid="PlaceHolderPageTitleInTitleArea" runat="server">
	<SharePointWebControls:FieldValue FieldName="Title" runat="server"/>
</asp:Content>
<asp:Content Contentplaceholderid="PlaceHolderMain" runat="server">
    <div id="search-div" class="row no-gutters">
        <div class="col">
            <div id="search-controls-div">
                <input type="text" class="form-control f-light" id="txtSearch" value="¡Escribe lo que quieras buscar!"/>
                <ul class="list-group" id="searchDropDown"></ul>
                <a href="#" id="btnSearch" data-intro="SearchButton">
                    <span class="icon-search"></span>
                </a>
            </div>
        </div>
    </div>
    <div id="document-info-page">
        <div id="doc-info-header">
            <div class="container">
                <div class="row">
                    <div class="col-md-6">
                        <div id="doc-title"></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="row mt-5">
                <div class="col">
                    <div class="text-right" id="docToolbar">
                        <a href="#" class="card-link" data-command="favourite" data-command-args="add" data-intro="Favourite">
                            <span class="icon-heart-purple"><span class="path1"></span><span class="path2"></span></span>
                        </a>
                        <a href="#" class="card-link" data-command="bookmark" data-command-args="add" data-intro="Bookmark">
                            <span class="icon-bookmark-purple"><span class="path1"></span><span class="path2"></span></span>
                        </a>
                        <a href="#" class="card-link" data-command="share" data-intro="Share">
                            <span class="icon-share2"></span>
                        </a>
                        <a href="#" class="card-link" data-command="alert" data-intro="Alert">
                            <span class="icon-bell"></span>
                        </a>
                    </div>
                </div>
            </div>
            <div class="row no-gutters mt-2">
                <div class="col-sm-12 col-md-7 pr-2">
                    <div class="row no-gutters" id="author-info">
                        <div class="col-3">
                            <div id="author-image"></div>
                        </div>
                        <div class="col-9 pl-3 pt-2">
                            <p class="s-16">RESPONSABLE:</p>
                            <div data-id="author-name" class="author-name"></div>
                            <div id="author-job-title"></div>
                            <div id="author-mail"></div>
                            <div id="author-workphone"></div>
                            <div>
                                <a href="#" id="author-profile" data-key="ViewProfile">Ver perfil</a>
                            </div>
                        </div>
                    </div>

                    <div class="row no-gutters mt-2 p-3" id="doc-info">
                        <div class="col-6 pr-2">
                            <p class="mb-3 s-16">CARACTERÍSTICAS:</p>
                            <p><span data-key="Date:" class="text-uppercase f-bold">Fecha</span>&nbsp;<span id="doc-date" class="text-uppercase"></span></p>
                            <p><span data-key="Use:" class="text-uppercase f-bold">Uso</span>&nbsp;<span id="doc-use" class="text-uppercase"></span></p>
                            <p><span data-key="Language:" class="f-bold text-uppercase">Language</span>&nbsp;<span id="doc-language" class="text-uppercase"></span></p>
                            <p><span data-key="Size:" class="f-bold text-uppercase">Peso</span>&nbsp;<span id="doc-size" class="text-uppercase"></span></p>
                        </div>
                        <div class="col-6 pr-2" id="serviceEnablers">
                            <p class="mb-3 s-16">SERVICE ENABLER:</p>
                        </div>
                    </div>
                </div>
                <div class="col-sm-12 col-md-5">
                    <div class="row no-gutters" id="buttons">
                        <div class="col-6 pr-2" data-perm-mask="viewListItems">
                            <div class="bg-blue03 doc-info-buttons">
                                <a role="button" id="btnViewDocument" href="#" target="_blank">
                                    <p data-key="View"></p>
                                </a>
                            </div>
                        </div>
                        <div class="col-6 pr-2" data-perm-mask="editListItems">
                            <div class="bg-blue03 doc-info-buttons">
                                <a role="button" id="btnEditDocument">
                                    <p data-key="EditProperties"></p>
                                </a>
                            </div>
                        </div>
                        <div class="col-6 pr-2" data-perm-mask="viewVersions">
                            <div class="bg-blue03 doc-info-buttons">
                                <a role="button" href="#" id="btnHistory">
                                    <p data-key="History"></p>
                                </a>
                            </div>
                        </div>
                        <div class="col-6 pr-2" data-perm-mask="viewListItems">
                            <div class="bg-blue03 doc-info-buttons">
                                <a role="button" id="btnDownload">
                                    <p data-key="Download"></p>
                                </a>
                            </div>
                        </div>
                        <div class="col m-1 hide" id="publish-div">
                            <a role="button" href="#" id="btnPublish" data-key="Publish"></a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row mt-5" id="documentByContentType">
                <h2 data-key="RelatedDocuments"></h2>
            </div>
        </div>
    </div>
</asp:Content>
