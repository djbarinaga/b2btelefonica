﻿using CsvHelper;
using Microsoft.SharePoint.Client;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MigracionPM
{
    class Program
    {
        public class FileInfo
        {
            public string File { get; set; }
            public string Url { get; set; }
            public string Folder { get; set; }
            public string Library { get; set; }
            public string CreatedBy { get; set; }
            public DateTime CreatedDate { get; set; }
            public string ModifiedBy { get; set; }
            public DateTime ModifiedDate { get; set; }
            public long Size { get; set; }
        }

        static DateTime processTime = DateTime.MinValue;

        static List<FileInfo> files = new List<FileInfo>();
        static string path = @"C:\Proyectos\Avantgarde\Telefónica\Desarrollo\Migracion\Logos";

        static void Main(string[] args)
        {
            if (args.Contains("-date"))
            {
                int index = Array.IndexOf(args, "-date");
                string insertedDate = args[index + 1];

                processTime = Convert.ToDateTime(insertedDate);

                Console.WriteLine($"Fecha establecida: {processTime}");
            }

            ClientContext context = new ClientContext("https://logos.telefonicabusinesssolutions.com/pm");
            context.Credentials = new System.Net.NetworkCredential("wuedji01", "Telefonica@19", "tefgad");

            string[] docLibraries = new string[] { "Workshops / Webinars", "Customer Material", "Internal Material" };

            foreach(string docLibrary in docLibraries)
            {
                Console.WriteLine("***********************");
                Console.WriteLine($"Procesando {docLibrary}");

                List list = context.Web.Lists.GetByTitle(docLibrary);
                Folder rootFolder = list.RootFolder;

                context.Load(list);
                context.Load(rootFolder);
                context.ExecuteQuery();

                GetFiles(rootFolder, context, docLibrary);
            }

            Console.WriteLine("");
            Console.WriteLine("Generando fichero CSV...");
            using (StreamWriter writer = new StreamWriter(string.Format(@"C:\Proyectos\Avantgarde\Telefónica\Desarrollo\Reports\PMDocuments_{0}.csv", processTime.ToString("yyyyMMdd"))))
            {
                writer.WriteLine("Library|Folder|File|Size|CreatedBy|CreatedDate|ModifiedBy|ModifiedDate");
                writer.Flush();

                foreach (FileInfo file in files)
                {
                    writer.WriteLine($"{file.Library}|{file.Folder}|{file.File}|{file.Size}|{file.CreatedBy}|{file.CreatedDate}|{file.ModifiedBy}|{file.ModifiedDate}");
                    writer.Flush();
                }
            }

            Console.WriteLine("Fichero generado!");

            Console.ReadLine();
        }


        private static void GetFiles(Folder mainFolder, ClientContext clientContext, string library)
        {
            clientContext.Load(mainFolder, k => k.Files, k => k.Folders);
            clientContext.ExecuteQuery();

            foreach (var folder in mainFolder.Folders)
            {
                GetFiles(folder, clientContext, library);
            }

            int counter = 0;
            int itemCount = mainFolder.Files.Count;


            if (itemCount > 0)
            {
                Console.WriteLine($"\tFicheros en {mainFolder.Name}: {itemCount}");
            }
            else
            {
                files.Add(new FileInfo()
                {
                    Library = library,
                    Folder = mainFolder.ServerRelativeUrl
                });
            }

            int percentage = 0;

            foreach (var file in mainFolder.Files)
            {
                if (DateTime.Compare(file.TimeLastModified, processTime) >= 0)
                {
                    Console.Write("\t\tObteniendo ficheros de {0}...\r", mainFolder.Name);

                    var fileRef = file.ServerRelativeUrl;
                    var fileName = file.Name;
                    
                    string author = string.Empty;
                    string editor = string.Empty;

                    try
                    {
                        clientContext.Load(file.Author);
                        clientContext.ExecuteQuery();
                        author = file.Author.Email;
                    }
                    catch(Exception ex)
                    {

                    }

                    try
                    {
                        clientContext.Load(file.ModifiedBy);
                        clientContext.ExecuteQuery();
                        editor = file.ModifiedBy.Email;
                    }
                    catch (Exception ex)
                    {

                    }

                    files.Add(new FileInfo()
                    {
                        Library = library,
                        Folder = mainFolder.ServerRelativeUrl,
                        File = fileName,
                        Url = fileRef,
                        CreatedBy = author,
                        CreatedDate = file.TimeCreated,
                        ModifiedBy = editor,
                        ModifiedDate = file.TimeLastModified,
                        Size = file.Length
                    });

                    if (counter < itemCount)
                        Console.Write("\t\tObteniendo ficheros de {0}... {1}%\r", mainFolder.Name, percentage);
                }

                counter++;

                percentage = (counter * 100) / itemCount;

                if (percentage == 100)
                    Console.WriteLine("\t\tObteniendo ficheros de {0}... {1}%", mainFolder.Name, percentage);
            }

        }
    }
}
